﻿CREATE TABLE [dbo].[UOM] (
    [UOMCode]  VARCHAR (20) NOT NULL,
    [UOMDesc]  VARCHAR (50) NOT NULL,
    [UOMGroup] VARCHAR (20) NOT NULL,
    [Factor]   REAL         NULL
);


GO
GRANT SELECT
    ON OBJECT::[dbo].[UOM] TO PUBLIC
    AS [dbo];

