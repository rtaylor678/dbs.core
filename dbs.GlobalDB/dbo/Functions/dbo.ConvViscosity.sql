﻿
create function dbo.ConvViscosity(@valToConv float, @fromUnits varchar(255), @toUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @temp float, @Result float
	IF @toUnits = @fromUnits 
		SET @Result = @valToConv
	ELSE IF @fromUnits = 'CS50C' 
		SET @Result = @valToConv
	ELSE IF @fromUnits = 'CS122F' 
		SET @Result = @valToConv
	ELSE IF @fromUnits = 'CS212F' 
		SET @Result = @valToConv
	ELSE BEGIN
		IF @fromUnits = 'CS40C' 
		BEGIN -- Only convert to SSU100F
			-- First, calculate SSU104
			SELECT @temp = 4.6324*@valToConv + (1+0.03264*@valToConv)/
						((3930.2 + 262.7*@valToConv + 23.97*SQUARE(@valToConv) +
						 1.646*Power(@valToConv,3))/100000)
			SELECT @Result = @temp/(1+0.000061*(104-100))
		END
		ELSE IF @fromUnits = 'CS100C' -- Only convert to SSU210F
			SELECT @Result = (1+0.000061*(210-100))*(4.6324*@valToConv + (1+0.03264*@valToConv)/
						((3930.2 + 262.7*@valToConv + 23.97*SQUARE(@valToConv) +
						 1.646*Power(@valToConv,3))/100000))
		ELSE IF @fromUnits = 'SSU100F' 
		BEGIN  -- Only convert to CS40C
			-- Calculate SSU104
			SELECT @temp = @valToConv * (1+0.000061*(104-100))
			SELECT @Result = 0.21587 * @temp - ((11069 + @temp)/(Power(@temp,3) + 37003))
		END
		ELSE IF @fromUnits = 'SSU210F' -- Only convert to CS100C
			SELECT @Result = 0.21443*@valToConv - ((11219*@valToConv)/(Power(@valToConv, 3) + 37755))
		ELSE 
			SET @Result = NULL
	END
	RETURN @Result
END
GO
GRANT VIEW DEFINITION
    ON OBJECT::[dbo].[ConvViscosity] TO PUBLIC
    AS [dbo];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[ConvViscosity] TO PUBLIC
    AS [dbo];

