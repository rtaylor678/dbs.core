﻿CREATE AGGREGATE [dbo].[WtAvg](@valueA FLOAT (53), @WtFactor FLOAT (53))
    RETURNS FLOAT (53)
    EXTERNAL NAME [CustomAggregates].[CustomAggregates.WtAvg];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[WtAvg] TO PUBLIC
    AS [dbo];

