﻿CREATE AGGREGATE [dbo].[WtAvgNZ](@valueA FLOAT (53), @WtFactor FLOAT (53))
    RETURNS FLOAT (53)
    EXTERNAL NAME [CustomAggregates].[CustomAggregates.WtAvgNZ];


GO
GRANT EXECUTE
    ON OBJECT::[dbo].[WtAvgNZ] TO PUBLIC
    AS [dbo];

