﻿
CREATE FUNCTION [dbo].[LimitMax](@Value float, @Max float)
RETURNS float
AS
BEGIN
	DECLARE @ResultVar float
	IF @Value > @Max
		SET @ResultVar = @Max
	ELSE
		SET @ResultVar = @Value

	RETURN @ResultVar

END
GO
GRANT EXECUTE
    ON OBJECT::[dbo].[LimitMax] TO PUBLIC
    AS [dbo];

