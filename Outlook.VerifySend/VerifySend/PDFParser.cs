﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text.pdf;
using ICSharpCode;

namespace VerifySend
{
    class PDFParser
    {
        private static int _numberOfCharsToKeep = 15;

        public class pdfSearchResults
        {
            public bool Success { get; set; }
            public string Result { get; set; }
        }

        #region Search PDF
        public pdfSearchResults searchPDF(string attachment, string password)
        {
            pdfSearchResults pdfsr = new pdfSearchResults();
            try
            {
                string str = "test123";
                byte[] bytes = new byte[str.Length * sizeof(char)];
                System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);

                System.Text.Encoding enc = System.Text.Encoding.ASCII;
                byte[] passwordBytes = enc.GetBytes(password);

                //PdfReader reader = new PdfReader(attachment, bytes);
                PdfReader reader = new PdfReader(attachment, passwordBytes);

                int totalLen = 68;
                float charUnit = ((float)totalLen) / (float)reader.NumberOfPages;
                string pdfText = string.Empty;

                for (int page = 1; page <= reader.NumberOfPages; page++)
                {
                    pdfText += ExtractTextFromPDFBytes(reader.GetPageContent(page)) + " ";
                }
                
                //return pdfText;
                //pdfSearchResults pdfsr = new pdfSearchResults();
                pdfsr.Success = true;
                pdfsr.Result = pdfText;
                //return pdfsr;
            }
            catch (Exception ex)
            {
                //if (ex.Message.Contains("Input string was not in a correct format."))
                //{
                //    pdfsr.Success = false;
                //    pdfsr.Result = "Error processing PDF. The file may not be password protected, do not enter a password if prompted.";
                //}
                //else if (ex.Message.Contains("Bad user password"))
                //{
                //    pdfsr.Success = false;
                //    pdfsr.Result = "The password you supplied is not correct for attachment " + attachment + ". Verify that the CAPS LOCK key is off and be sure to use the correct capitalization.";
                //}
                //else
                //{
                //    pdfsr.Success = false;
                //    pdfsr.Result = "Error processing pdf.";
                //}
                pdfsr.Success = false;
            }
            return pdfsr;
        }
        #endregion

        #region ExtractTextFromPDFBytes
        /// <summary>
        /// This method processes an uncompressed Adobe (text) object and extracts text.
        /// </summary>
        /// <param name="input">uncompressed</param>
        /// <returns></returns>
        private string ExtractTextFromPDFBytes(byte[] input)
        {
            if (input == null || input.Length == 0) return "";

            try
            {
                string resultString = "";

                bool inTextObject = false;
                bool nextLiteral = false;
                int bracketDepth = 0;
                char[] previousCharacters = new char[_numberOfCharsToKeep];

                for (int j = 0; j < _numberOfCharsToKeep; j++) previousCharacters[j] = ' ';

                for (int i = 0; i < input.Length; i++)
                {
                    char c = (char)input[i];

                    if (inTextObject)
                    {
                        if (bracketDepth == 0)
                        {
                            if (CheckToken(new string[] { "TD", "Td" }, previousCharacters))
                            {
                                resultString += " ";
                            }
                            else
                            {
                                if (CheckToken(new string[] { "'", "T*", "\"" }, previousCharacters))
                                {
                                    resultString += " ";
                                }
                                else
                                {
                                    if (CheckToken(new string[] { "Tj" }, previousCharacters))
                                    {
                                        resultString += " ";
                                    }
                                }
                            }
                        }

                        if (bracketDepth == 0 && CheckToken(new string[] { "ET" }, previousCharacters))
                        {
                            inTextObject = false;
                            resultString += " ";
                        }
                        else
                        {
                            if ((c == '(') && (bracketDepth == 0) && (!nextLiteral))
                            {
                                bracketDepth = 1;
                            }
                            else
                            {
                                if ((c == ')') && (bracketDepth == 1) && (!nextLiteral))
                                {
                                    bracketDepth = 0;
                                }
                                else
                                {
                                    if (bracketDepth == 1)
                                    {
                                        if (c == '\\' && !nextLiteral)
                                        {
                                            nextLiteral = true;
                                        }
                                        else
                                        {
                                            if (((c >= ' ') && (c <= '~')) || ((c >= 128) && (c < 255)))
                                            {
                                                resultString += c.ToString();
                                            }
                                            nextLiteral = false;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for (int j = 0; j < _numberOfCharsToKeep - 1; j++)
                    {
                        previousCharacters[j] = previousCharacters[j + 1];
                    }
                    previousCharacters[_numberOfCharsToKeep - 1] = c;

                    if (!inTextObject && CheckToken(new string[] { "BT" }, previousCharacters))
                    {
                        inTextObject = true;
                    }
                }
                return resultString;
            }
            catch (Exception)
            {
                return "";
            }
        }
        #endregion

        #region CheckToken
        /// <summary>
        /// Check if a certain 2 character token just came along (e.g. BT)
        /// </summary>
        /// <param name="search">the searched token</param>
        /// <param name="recent">the recent character array</param>
        /// <returns></returns>
        private bool CheckToken(string[] tokens, char[] recent)
        {
            try
            {
                foreach (string token in tokens)
                {
                    if ((recent[_numberOfCharsToKeep - 3] == token[0]) &&
                        (recent[_numberOfCharsToKeep - 2] == token[1]) &&
                        ((recent[_numberOfCharsToKeep - 1] == ' ') ||
                        (recent[_numberOfCharsToKeep - 1] == 0x0d) ||
                        (recent[_numberOfCharsToKeep - 1] == 0x0a)) &&
                        ((recent[_numberOfCharsToKeep - 4] == ' ') ||
                        (recent[_numberOfCharsToKeep - 4] == 0x0d) ||
                        (recent[_numberOfCharsToKeep - 4] == 0x0a))
                        )
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }
        #endregion
    }
}
