﻿CREATE TABLE [dbo].[CurrencyConv] (
    [CurrencyCode] CHAR (4)       NOT NULL,
    [Year]         SMALLINT       NOT NULL,
    [Month]        [dbo].[tMonth] NOT NULL,
    [ConvRate]     REAL           NOT NULL,
    [SaveDate]     SMALLDATETIME  CONSTRAINT [DF_CurrencyConv_SaveDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CurrencyConv] PRIMARY KEY CLUSTERED ([CurrencyCode] ASC, [Year] ASC, [Month] ASC) WITH (FILLFACTOR = 90)
);

