﻿CREATE TABLE [dbo].[APIScale] (
    [StudyYear] [dbo].[StudyYear] NOT NULL,
    [APIScale]  TINYINT           NOT NULL,
    [APILevel]  TINYINT           NOT NULL,
    [MinAPI]    REAL              NOT NULL,
    [MaxAPI]    REAL              NOT NULL,
    [Adj]       [dbo].[Price]     NOT NULL,
    [SaveDate]  SMALLDATETIME     CONSTRAINT [DF_APIScale_SaveDate] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_Scale_1__17] PRIMARY KEY CLUSTERED ([StudyYear] ASC, [APIScale] ASC, [APILevel] ASC) WITH (FILLFACTOR = 90)
);

