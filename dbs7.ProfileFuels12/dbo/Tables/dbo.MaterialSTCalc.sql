﻿CREATE TABLE [dbo].[MaterialSTCalc] (
    [SubmissionID]  INT                   NOT NULL,
    [Scenario]      [dbo].[Scenario]      NOT NULL,
    [Category]      [dbo].[YieldCategory] NOT NULL,
    [GrossBbl]      FLOAT (53)            NULL,
    [GrossMT]       FLOAT (53)            NULL,
    [GrossValueMUS] REAL                  NULL,
    [NetBbl]        FLOAT (53)            NULL,
    [NetMT]         FLOAT (53)            NULL,
    [NetValueMUS]   REAL                  NULL,
    CONSTRAINT [PK_MatCategoryTotals_1__14] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [Category] ASC) WITH (FILLFACTOR = 90)
);

