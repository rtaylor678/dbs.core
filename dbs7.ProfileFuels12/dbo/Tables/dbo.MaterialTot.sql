﻿CREATE TABLE [dbo].[MaterialTot] (
    [SubmissionID]   INT        NOT NULL,
    [PVP]            FLOAT (53) NULL,
    [PVPPcnt]        REAL       NULL,
    [TotInputBbl]    FLOAT (53) NULL,
    [TotInputMT]     FLOAT (53) NULL,
    [NetInputBbl]    FLOAT (53) NULL,
    [NetInputMT]     FLOAT (53) NULL,
    [GainBbl]        REAL       NULL,
    [LWSBbl]         REAL       NULL,
    [LWBbl]          REAL       NULL,
    [LubeBbl]        REAL       NULL,
    [WaxBbl]         REAL       NULL,
    [SpecBbl]        REAL       NULL,
    [ByProdBbl]      REAL       NULL,
    [LWSPcntInput]   REAL       NULL,
    [LWSBblPerDay]   REAL       NULL,
    [MarginInputBbl] REAL       NULL,
    CONSTRAINT [PK_MaterialTot] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);

