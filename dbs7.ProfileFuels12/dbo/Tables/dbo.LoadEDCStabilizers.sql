﻿CREATE TABLE [dbo].[LoadEdcStabilizers] (
    [SubmissionID]    INT           NOT NULL,
    [EffDate]         SMALLDATETIME NOT NULL,
    [AnnInputBbl]     FLOAT (53)    NULL,
    [AnnCokeBbl]      FLOAT (53)    NULL,
    [AnnElecConsMWH]  FLOAT (53)    NULL,
    [AnnRSCRUDE_RAIL] FLOAT (53)    NULL,
    [AnnRSCRUDE_TT]   FLOAT (53)    NULL,
    [AnnRSCRUDE_TB]   FLOAT (53)    NULL,
    [AnnRSCRUDE_OMB]  FLOAT (53)    NULL,
    [AnnRSCRUDE_BB]   FLOAT (53)    NULL,
    [AnnRSPROD_RAIL]  FLOAT (53)    NULL,
    [AnnRSPROD_TT]    FLOAT (53)    NULL,
    [AnnRSPROD_TB]    FLOAT (53)    NULL,
    [AnnRSPROD_OMB]   FLOAT (53)    NULL,
    [AnnRSPROD_BB]    FLOAT (53)    NULL,
    CONSTRAINT [PK_LoadEdcStabilizers_1] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [EffDate] ASC)
);

