﻿CREATE TABLE [dbo].[CustomUnitData] (
    [SubmissionID]   INT                  NOT NULL,
    [UnitID]         [dbo].[UnitID]       NOT NULL,
    [Property]       VARCHAR (30)         NOT NULL,
    [FactorSet]      [dbo].[FactorSet]    CONSTRAINT [DF_CustomUnitData_FactorSet] DEFAULT ('N/A') NOT NULL,
    [Currency]       [dbo].[CurrencyCode] CONSTRAINT [DF_CustomUnitData_Currency] DEFAULT ('N/A') NOT NULL,
    [USValue]        REAL                 NULL,
    [MetValue]       REAL                 NULL,
    [USTarget]       REAL                 NULL,
    [MetTarget]      REAL                 NULL,
    [SortKey]        SMALLINT             NULL,
    [USDescription]  VARCHAR (50)         NULL,
    [MetDescription] VARCHAR (50)         NULL,
    CONSTRAINT [PK_CustomUnitData] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [UnitID] ASC, [Property] ASC, [FactorSet] ASC, [Currency] ASC) WITH (FILLFACTOR = 90)
);

