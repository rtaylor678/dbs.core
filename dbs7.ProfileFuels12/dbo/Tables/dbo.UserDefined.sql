﻿CREATE TABLE [dbo].[UserDefined] (
    [SubmissionID]    INT           NOT NULL,
    [HeaderText]      VARCHAR (50)  CONSTRAINT [DF_UserDefined_HeaderText] DEFAULT ('Client supplied KPIs') NOT NULL,
    [VariableDesc]    VARCHAR (100) NOT NULL,
    [RptValue]        FLOAT (53)    NULL,
    [DecPlaces]       TINYINT       NULL,
    [RptValue_Target] FLOAT (53)    NULL,
    [RptValue_Avg]    FLOAT (53)    NULL,
    [RptValue_Ytd]    FLOAT (53)    NULL,
    CONSTRAINT [PK_UserDefined] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [HeaderText] ASC, [VariableDesc] ASC) WITH (FILLFACTOR = 90)
);

