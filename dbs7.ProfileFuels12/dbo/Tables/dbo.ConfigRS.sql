﻿CREATE TABLE [dbo].[ConfigRS] (
    [SubmissionID]  INT                 NOT NULL,
    [UnitID]        [dbo].[UnitID]      NOT NULL,
    [ProcessID]     [dbo].[ProcessID]   NOT NULL,
    [ProcessType]   [dbo].[ProcessType] NOT NULL,
    [AvgSize]       REAL                NULL,
    [Throughput]    REAL                NULL,
    [Vessels]       REAL                NULL,
    [BPD]           REAL                NULL,
    [PcntOwnership] NUMERIC (5, 2)      NULL,
    CONSTRAINT [PK_ConfigRSNew] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [UnitID] ASC) WITH (FILLFACTOR = 90)
);

