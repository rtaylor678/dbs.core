﻿CREATE TABLE [dbo].[CrudeTot] (
    [SubmissionID] INT        NOT NULL,
    [TotBbl]       FLOAT (53) NULL,
    [AvgGravity]   REAL       NULL,
    [AvgSulfur]    REAL       NULL,
    [AvgCost]      REAL       NULL,
    [TotMT]        FLOAT (53) NULL,
    [AvgDensity]   REAL       NULL,
    CONSTRAINT [PK_CrudeTot] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);

