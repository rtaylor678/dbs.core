﻿CREATE TABLE [dbo].[MaintRoutHist] (
    [RefineryID]  CHAR (6)             NOT NULL,
    [DataSet]     VARCHAR (15)         NOT NULL,
    [PeriodStart] SMALLDATETIME        NOT NULL,
    [PeriodEnd]   SMALLDATETIME        NOT NULL,
    [Currency]    [dbo].[CurrencyCode] NOT NULL,
    [RoutCost]    REAL                 NULL,
    [RoutMatl]    REAL                 NULL,
    [Reported]    BIT                  DEFAULT (0) NOT NULL,
    CONSTRAINT [PK_MaintRoutHistNew] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [DataSet] ASC, [PeriodStart] ASC, [Currency] ASC) WITH (FILLFACTOR = 90)
);

