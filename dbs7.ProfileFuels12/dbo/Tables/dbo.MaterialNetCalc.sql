﻿CREATE TABLE [dbo].[MaterialNetCalc] (
    [SubmissionID] INT                   NOT NULL,
    [Scenario]     [dbo].[Scenario]      NOT NULL,
    [Category]     [dbo].[YieldCategory] NOT NULL,
    [MaterialID]   [dbo].[MaterialID]    NOT NULL,
    [Bbl]          FLOAT (53)            NULL,
    [MT]           FLOAT (53)            NULL,
    [PricePerBbl]  REAL                  NULL,
    [PricePerMT]   REAL                  NULL,
    CONSTRAINT [PK_MatNetCalc] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Scenario] ASC, [Category] ASC, [MaterialID] ASC) WITH (FILLFACTOR = 90)
);

