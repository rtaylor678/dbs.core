﻿CREATE TABLE [dbo].[CoCustom] (
    [CompanyID]   VARCHAR (10) NOT NULL,
    [CustomType]  VARCHAR (3)  NOT NULL,
    [CustomGroup] TINYINT      NOT NULL,
    CONSTRAINT [PK_CustomReports] PRIMARY KEY CLUSTERED ([CompanyID] ASC, [CustomType] ASC, [CustomGroup] ASC) WITH (FILLFACTOR = 90),
    CONSTRAINT [FK_CoCustom_CustomTypes] FOREIGN KEY ([CustomType]) REFERENCES [dbo].[CustomTypes] ([CustomType]) ON UPDATE CASCADE
);

