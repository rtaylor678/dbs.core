﻿CREATE TABLE [dbo].[UnitTargetsNew] (
    [SubmissionID] INT                  NOT NULL,
    [UnitID]       INT                  NOT NULL,
    [Property]     VARCHAR (50)         NOT NULL,
    [Target]       REAL                 NULL,
    [CurrencyCode] [dbo].[CurrencyCode] NULL,
    CONSTRAINT [PK_UnitTargets_TEST] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [UnitID] ASC, [Property] ASC) WITH (FILLFACTOR = 90)
);

