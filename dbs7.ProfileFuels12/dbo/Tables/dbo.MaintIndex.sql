﻿CREATE TABLE [dbo].[MaintIndex] (
    [SubmissionID]       INT                  NOT NULL,
    [Currency]           [dbo].[CurrencyCode] NOT NULL,
    [FactorSet]          [dbo].[FactorSet]    NOT NULL,
    [TAIndex]            REAL                 NULL,
    [RoutIndex]          REAL                 NULL,
    [MaintIndex]         REAL                 NULL,
    [TAMatlIndex]        REAL                 NULL,
    [RoutMatlIndex]      REAL                 NULL,
    [MaintMatlIndex]     REAL                 NULL,
    [TAIndex_Avg]        REAL                 NULL,
    [RoutIndex_Avg]      REAL                 NULL,
    [MaintIndex_Avg]     REAL                 NULL,
    [TAMatlIndex_Avg]    REAL                 NULL,
    [RoutMatlIndex_Avg]  REAL                 NULL,
    [MaintMatlIndex_Avg] REAL                 NULL,
    [TAEffIndex]         REAL                 NULL,
    [RoutEffIndex]       REAL                 NULL,
    [MaintEffIndex]      REAL                 NULL,
    [TAEffIndex_Avg]     REAL                 NULL,
    [RoutEffIndex_Avg]   REAL                 NULL,
    [MaintEffIndex_Avg]  REAL                 NULL,
    [AdjMaintIndex]      REAL                 NULL,
    [AdjMaintEffIndex]   REAL                 NULL,
    CONSTRAINT [PK_MaintIndex] PRIMARY KEY CLUSTERED ([SubmissionID] ASC, [Currency] ASC, [FactorSet] ASC) WITH (FILLFACTOR = 90)
);

