﻿CREATE TABLE [dbo].[MessageLog] (
    [SubmissionID] INT           NOT NULL,
    [Source]       VARCHAR (30)  CONSTRAINT [DF_MessageLog_Source] DEFAULT ('') NOT NULL,
    [Severity]     CHAR (1)      NOT NULL,
    [MessageText]  VARCHAR (255) CONSTRAINT [DF_MessageLog_MessageText] DEFAULT ('') NOT NULL,
    [MessageTime]  DATETIME      CONSTRAINT [DF_MessageLog_MessageTime1__22] DEFAULT (getdate()) NOT NULL
);


GO
CREATE CLUSTERED INDEX [IX_MessageLog]
    ON [dbo].[MessageLog]([MessageTime] ASC) WITH (FILLFACTOR = 90);


GO
CREATE NONCLUSTERED INDEX [IX_MessageLog_1]
    ON [dbo].[MessageLog]([SubmissionID] ASC) WITH (FILLFACTOR = 90);

