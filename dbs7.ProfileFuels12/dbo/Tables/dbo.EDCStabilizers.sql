﻿CREATE TABLE [dbo].[EdcStabilizers] (
    [SubmissionID]    INT        NOT NULL,
    [AnnInputBbl]     FLOAT (53) NULL,
    [AnnCokeBbl]      FLOAT (53) NULL,
    [AnnElecConsMWH]  FLOAT (53) NULL,
    [AnnRSCRUDE_RAIL] FLOAT (53) NULL,
    [AnnRSCRUDE_TT]   FLOAT (53) NULL,
    [AnnRSCRUDE_TB]   FLOAT (53) NULL,
    [AnnRSCRUDE_OMB]  FLOAT (53) NULL,
    [AnnRSCRUDE_BB]   FLOAT (53) NULL,
    [AnnRSPROD_RAIL]  FLOAT (53) NULL,
    [AnnRSPROD_TT]    FLOAT (53) NULL,
    [AnnRSPROD_TB]    FLOAT (53) NULL,
    [AnnRSPROD_OMB]   FLOAT (53) NULL,
    [AnnRSPROD_BB]    FLOAT (53) NULL,
    CONSTRAINT [PK_EdcStabilizers] PRIMARY KEY CLUSTERED ([SubmissionID] ASC) WITH (FILLFACTOR = 90)
);

