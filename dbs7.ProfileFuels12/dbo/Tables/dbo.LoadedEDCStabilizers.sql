﻿CREATE TABLE [dbo].[LoadedEdcStabilizers] (
    [RefineryID]      CHAR (6)      NOT NULL,
    [DataSet]         VARCHAR (15)  CONSTRAINT [DF_LoadedEdcStabilizers_DataSet] DEFAULT ('ACTUAL') NOT NULL,
    [EffDate]         SMALLDATETIME NOT NULL,
    [EffUntil]        SMALLDATETIME NULL,
    [AnnInputBbl]     FLOAT (53)    NULL,
    [AnnCokeBbl]      FLOAT (53)    NULL,
    [AnnElecConsMWH]  FLOAT (53)    NULL,
    [AnnRSCRUDE_RAIL] FLOAT (53)    NULL,
    [AnnRSCRUDE_TT]   FLOAT (53)    NULL,
    [AnnRSCRUDE_TB]   FLOAT (53)    NULL,
    [AnnRSCRUDE_OMB]  FLOAT (53)    NULL,
    [AnnRSCRUDE_BB]   FLOAT (53)    NULL,
    [AnnRSPROD_RAIL]  FLOAT (53)    NULL,
    [AnnRSPROD_TT]    FLOAT (53)    NULL,
    [AnnRSPROD_TB]    FLOAT (53)    NULL,
    [AnnRSPROD_OMB]   FLOAT (53)    NULL,
    [AnnRSPROD_BB]    FLOAT (53)    NULL,
    CONSTRAINT [PK_LoadedEdcStabilizers] PRIMARY KEY CLUSTERED ([RefineryID] ASC, [DataSet] ASC, [EffDate] ASC) WITH (FILLFACTOR = 90)
);

