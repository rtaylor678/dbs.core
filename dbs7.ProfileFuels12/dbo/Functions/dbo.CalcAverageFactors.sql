﻿CREATE FUNCTION [dbo].[CalcAverageFactors](@RefineryID varchar(6), @DataSet varchar(15), @FactorSet dbo.FactorSet, @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT a.FactorSet
		, AvgEdc = [$(GlobalDB)].dbo.WtAvg(a.Edc, s.NumDays)
		, AvgUEdc = [$(GlobalDB)].dbo.WtAvg(a.UEdc, s.NumDays)
		, SumUEdc = SUM(1.0*a.UEdc*s.NumDays)
		, UtilPcnt = [$(GlobalDB)].dbo.WtAvg(a.UtilPcnt, Edc*s.NumDays)
		, AvgProcessEdc = [$(GlobalDB)].dbo.WtAvg(a.TotProcessEdc, s.NumDays)
		, AvgProcessUEdc = [$(GlobalDB)].dbo.WtAvg(a.TotProcessUEdc, s.NumDays)
		, ProcessUtilPcnt = [$(GlobalDB)].dbo.WtAvg(a.TotProcessUtilPcnt, TotProcessEdc*s.NumDays)
		, UtilOSTA = [$(GlobalDB)].dbo.WtAvg(a.UtilOSTA, TotProcessEdc*s.NumDays)
		, EnergyUseDay = [$(GlobalDB)].dbo.WtAvg(a.EnergyUseDay, s.NumDays)
		, AvgStdEnergy = [$(GlobalDB)].dbo.WtAvg(a.TotStdEnergy, s.NumDays)
		, EII = [$(GlobalDB)].dbo.WtAvg(a.EII, a.TotStdEnergy*s.NumDays)
		, LossGainBpD = SUM(a.ReportLossGain)/SUM(s.NumDays*1.0)
		, AvgStdGainBpD = SUM(a.EstGain)/SUM(s.NumDays*1.0)
		, VEI = [$(GlobalDB)].dbo.WtAvgNN(a.VEI, EstGain)
		, Complexity = [$(GlobalDB)].dbo.WtAvg(a.Complexity, a.Edc*s.NumDays)
		, NonMaintPersEffDiv = SUM(a.NonMaintPersEffDiv*s.FractionOfYear)
		, MaintPersEffDiv = SUM(a.MaintPersEffDiv*s.FractionOfYear)
		, PersEffDiv = SUM(a.PersEffDiv*s.FractionOfYear)
		, MaintEffDiv = SUM(a.MaintEffDiv*s.FractionOfYear)
		, AvgAnnNonMaintPersEffDiv = [$(GlobalDB)].dbo.WtAvg(a.NonMaintPersEffDiv,s.NumDays)
		, AvgAnnMaintPersEffDiv = [$(GlobalDB)].dbo.WtAvg(a.MaintPersEffDiv,s.NumDays)
		, AvgAnnPersEffDiv = [$(GlobalDB)].dbo.WtAvg(a.PersEffDiv,s.NumDays)
		, AvgAnnMaintEffDiv = [$(GlobalDB)].dbo.WtAvg(a.MaintEffDiv,s.NumDays)
	FROM FactorTotCalc a INNER JOIN dbo.Submissions s ON s.SubmissionID = a.SubmissionID   
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	AND (a.FactorSet = @FactorSet OR @FactorSet IS NULL)
	GROUP BY a.FactorSet
	)


