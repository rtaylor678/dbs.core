﻿
CREATE FUNCTION [dbo].[SLAverageYield](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE 
AS
RETURN (
SELECT NetInputkBPD = NetInputBbl/1000/(SELECT DATEDIFF(DAY, MIN(PeriodStart), MAX(PeriodEnd)) FROM dbo.SubmissionsAll WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList))
	, GainPcnt = CASE WHEN NetInputBbl > 0 THEN 100*GainBbl/NetInputBbl ELSE NULL END
	, PVPPcnt = CASE WHEN NetInputBbl > 0 THEN 100*PVP/NetInputBbl ELSE NULL END
FROM dbo.SLSumMaterialTot(@SubmissionList) m
)

