﻿CREATE FUNCTION [dbo].[GetMechUnavailTA](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @UnitID int)
RETURNS real
AS
BEGIN
	DECLARE @MechUnavailTA real

	SELECT @MechUnavailTA = MechUnavailTA
	FROM MaintTA 
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND TAID > 0 AND UnitID = @UnitID 
	AND (NextTADate > @EndDate OR NextTADate IS NULL) AND TADate < @EndDate
	
	IF @MechUnavailTA IS NULL
		SET @MechUnavailTA = 0

	RETURN @MechUnavailTA
END
