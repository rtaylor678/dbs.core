﻿CREATE  FUNCTION [dbo].[CalcAverageROI](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, 
	@FactorSet varchar(8), @Scenario Scenario, @Currency CurrencyCode)
RETURNS @ROI TABLE (FactorSet varchar(8), Scenario dbo.Scenario, Currency dbo.CurrencyCode, ROI real NULL, VAI real NULL, RV real NULL, TotCptl real NULL)
AS
BEGIN

INSERT @ROI (FactorSet, Scenario, Currency, ROI, VAI, RV, TotCptl)
SELECT r.FactorSet, r.Scenario, r.Currency,
ROI = CASE WHEN SUM(r.TotCptl*s.FractionOfYear) > 0 THEN SUM(r.ROI*r.TotCptl*s.FractionOfYear)/SUM(r.TotCptl*s.FractionOfYear) ELSE NULL END,
VAI = CASE WHEN SUM(f.UEdc*s.NumDays) > 0 THEN SUM(r.VAI*f.UEdc*s.NumDays)/SUM(f.UEdc*s.NumDays) ELSE NULL END,
RV = SUM(r.RV*s.FractionOfYear)/SUM(s.FractionOfYear),
TotCptl = SUM(r.TotCptl*s.FractionOfYear)/SUM(s.FractionOfYear)
FROM ROICalc r INNER JOIN Submissions s ON s.SubmissionID = r.SubmissionID
INNER JOIN FactorTotCalc f ON f.SubmissionID = r.SubmissionID AND f.FactorSet = r.FactorSet
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
AND r.Scenario = ISNULL(@Scenario, r.Scenario) AND r.FactorSet = ISNULL(@FactorSet, r.FactorSet) AND r.Currency = ISNULL(@Currency, r.Currency)
GROUP BY r.FactorSet, r.Scenario, r.Currency

RETURN

END


