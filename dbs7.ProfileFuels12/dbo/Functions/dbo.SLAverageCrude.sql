﻿
CREATE FUNCTION [dbo].[SLAverageCrude](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT TotBbl = SUM(TotBbl), TotMT = SUM(TotMT)
		, Density = [$(GlobalDB)].dbo.WtAvg(AvgDensity, TotBbl)
		, Gravity = dbo.KGM3toAPI([$(GlobalDB)].dbo.WtAvg(AvgDensity, TotBbl))
		, Sulfur = [$(GlobalDB)].dbo.WtAvgNZ(AvgSulfur,TotMT)
	FROM CrudeTot c INNER JOIN @SubmissionList s ON s.SubmissionID = c.SubmissionID
)

