﻿CREATE FUNCTION dbo.SLAverageMarginROI(@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet, @Currency dbo.CurrencyCode, @Scenario varchar(8))
RETURNS @margins TABLE (FactorSet varchar(8) NOT NULL, Scenario varchar(8) NOT NULL, Currency CurrencyCode NOT NULL
	, GPV real NULL, RMC real NULL, GrossMargin real NULL, OpEx real NULL, OthRevenue real NULL, CashMargin real NULL
	, ROI real NULL, RORV real NULL
	, RawMatCostK real NULL, ProdValueK real NULL, OthRevenueK real NULL, TotCashOpExK real NULL, NetInputBbl float NULL
	, RV real NULL, WorkingCptl real NULL, TotCptl real NULL, NumDays real NULL)
AS BEGIN
	INSERT @margins (FactorSet, Scenario, Currency, RawMatCostK, ProdValueK, OthRevenueK, TotCashOpExK, NetInputBbl, RV, WorkingCptl, NumDays)
	SELECT o.FactorSet, mtc.Scenario, mtc.Currency, mtc.RawMatCost*1000, mtc.ProdValue*1000, o.OthRevenue, o.TotCashOpEx, mt.NetInputBbl, rv.AvgRV, wc.WorkingCptlM, wc.NumDays
	FROM dbo.SLSumMaterialTotCost(@SubmissionList, @Currency, @Scenario) mtc 
	INNER JOIN dbo.SLSumOpEx(@SubmissionList, @FactorSet, @Currency) o ON o.Currency = mtc.Currency
	INNER JOIN dbo.SLSumMaterialTot(@SubmissionList) mt ON 1 = 1
	LEFT JOIN dbo.SLAverageWorkingCptl(@SubmissionList, @Scenario, @Currency) wc ON wc.Scenario = mtc.Scenario AND wc.Currency = mtc.Currency
	CROSS APPLY dbo.SLAverageRV(@SubmissionList, o.FactorSet, mtc.Currency) rv

	UPDATE @margins
	SET   GPV = ProdValueK*1000/NetInputBbl
		, RMC = RawMatCostK*1000/NetInputBbl
		, GrossMargin = (ProdValueK - RawMatCostK)*1000/NetInputBbl
		, OpEx = TotCashOpExK*1000/NetInputBbl
		, OthRevenue = OthRevenueK*1000/NetInputBbl
		, TotCptl = ISNULL(RV,0) + ISNULL(WorkingCptl,0)
	WHERE NetInputBbl > 0

	UPDATE @margins SET CashMargin = GPV - RMC + OthRevenue - OpEx
	UPDATE @margins 
	SET ROI = 100*(CashMargin*NetInputBbl/1e6)/((TotCptl)*NumDays/365), 
		RORV = 100*(CashMargin*NetInputBbl/1e6)/(RV*NumDays/365)

	RETURN
END
