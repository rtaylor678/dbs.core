﻿

CREATE FUNCTION [dbo].[SLProcessAvailability](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT s.RefineryID, s.DataSet, g.FactorSet, g.ProcessGrouping AS ProcessID, 
	MechAvail_Ann = [$(GlobalDB)].dbo.WtAvg(m.MechAvail_Ann,m.PeriodEdc), MechAvailSlow_Ann = [$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Ann,m.PeriodEdc), MechAvailOSTA = [$(GlobalDB)].dbo.WtAvg(m.MechAvailOSTA, m.PeriodEdc*CASE WHEN m.PeriodHrs > 0 THEN m.PeriodHrsOSTA/m.PeriodHrs END),
	OpAvail_Ann = [$(GlobalDB)].dbo.WtAvg(m.OpAvail_Ann,m.PeriodEdc), OpAvailSlow_Ann = [$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Ann,m.PeriodEdc),
	OnStream_Ann = [$(GlobalDB)].dbo.WtAvg(m.OnStream_Ann,m.PeriodEdc), OnStreamSlow_Ann = [$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Ann,m.PeriodEdc), 
	MechAvail_Act = [$(GlobalDB)].dbo.WtAvg(m.MechAvail_Act,m.PeriodEdc), MechAvailSlow_Act = [$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Act,m.PeriodEdc),
	OpAvail_Act = [$(GlobalDB)].dbo.WtAvg(m.OpAvail_Act,m.PeriodEdc), OpAvailSlow_Act = [$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Act,m.PeriodEdc),
	OnStream_Act = [$(GlobalDB)].dbo.WtAvg(m.OnStream_Act,m.PeriodEdc), OnStreamSlow_Act = [$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Act,m.PeriodEdc),
	MechUnavailTA_Ann = [$(GlobalDB)].dbo.WtAvg(m.MechUnavailTA_Ann,m.PeriodEdc), MechUnavailTA_Act = [$(GlobalDB)].dbo.WtAvg(m.MechUnavailTA_Act,m.PeriodEdc), 
	MechUnavailPlan = [$(GlobalDB)].dbo.WtAvg(m.MechUnavailPlan,m.PeriodEdc), MechUnavailUnp = [$(GlobalDB)].dbo.WtAvg(m.MechUnavailUnp,m.PeriodEdc), 
	MechUnavail_Ann = [$(GlobalDB)].dbo.WtAvg(m.MechUnavail_Ann,m.PeriodEdc), MechUnavail_Act = [$(GlobalDB)].dbo.WtAvg(m.MechUnavail_Act,m.PeriodEdc), 
	RegUnavail = [$(GlobalDB)].dbo.WtAvg(m.RegUnavail,m.PeriodEdc), RegUnavailPlan = [$(GlobalDB)].dbo.WtAvg(m.RegUnavailPlan,m.PeriodEdc), RegUnavailUnp = [$(GlobalDB)].dbo.WtAvg(m.RegUnavailUnp,m.PeriodEdc), 
	OpUnavail_Ann = [$(GlobalDB)].dbo.WtAvg(m.OpUnavail_Ann,m.PeriodEdc), OpUnavail_Act = [$(GlobalDB)].dbo.WtAvg(m.OpUnavail_Act,m.PeriodEdc), 
	OthUnavailEconomic = [$(GlobalDB)].dbo.WtAvg(m.OthUnavailEconomic,m.PeriodEdc), OthUnavailExternal = [$(GlobalDB)].dbo.WtAvg(m.OthUnavailExternal,m.PeriodEdc), OthUnavailUnitUpsets = [$(GlobalDB)].dbo.WtAvg(m.OthUnavailUnitUpsets,m.PeriodEdc), OthUnavailOffsiteUpsets = [$(GlobalDB)].dbo.WtAvg(m.OthUnavailOffsiteUpsets,m.PeriodEdc), OthUnavailOther = [$(GlobalDB)].dbo.WtAvg(m.OthUnavailOther,m.PeriodEdc), 
	OthUnavail = [$(GlobalDB)].dbo.WtAvg(m.OthUnavail,m.PeriodEdc), OthUnavailPlan = [$(GlobalDB)].dbo.WtAvg(m.OthUnavailPlan,m.PeriodEdc), OthUnavailUnp = [$(GlobalDB)].dbo.WtAvg(m.OthUnavailUnp,m.PeriodEdc), 
	TotUnavail_Ann = [$(GlobalDB)].dbo.WtAvg(m.TotUnavail_Ann,m.PeriodEdc), TotUnavail_Act = [$(GlobalDB)].dbo.WtAvg(m.TotUnavail_Act,m.PeriodEdc), TotUnavailUnp = [$(GlobalDB)].dbo.WtAvg(m.TotUnavailUnp,m.PeriodEdc)
	FROM [dbo].[SLUnitAvailability](@SubmissionList) m 
	INNER JOIN (dbo.SLLastPeriods(@SubmissionList) s INNER JOIN RefProcessGroupings g ON g.SubmissionID = s.SubmissionID)  --This filters the results to only those units reported in last month of period*/
		ON g.UnitID = m.UnitID AND g.FactorSet = m.FactorSet
	GROUP BY s.RefineryID, s.DataSet, g.FactorSet, g.ProcessGrouping
)




