﻿
CREATE  FUNCTION [dbo].[SLAverageOpEx](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode)
RETURNS @OpEx TABLE (FactorSet varchar(8), Scenario dbo.Scenario, Currency dbo.CurrencyCode, TotCashOpExUEdc real NULL, VolOpExUEdc real NULL, NonVolOpExUEdc real NULL, 
	NEOpExUEdc real NULL, NEOpExEdc real NULL, NEI real NULL, TotCashOpExBbl real NULL)
AS
BEGIN

DECLARE @OpExScenario dbo.Scenario; SET @OpExScenario = 'CLIENT' -- Using Client Energy prices for all pricing scenarios

; WITH OpExByDataType AS (
	SELECT FactorSet, Currency, Scenario, DataType, TotCashOpEx, STNonVol, STVol, NEOpEx, TAAdj 
	FROM dbo.SLAverageOpExByDataType(@SubmissionList, @FactorSet, @OpExScenario, @Currency, NULL))
INSERT INTO @OpEx (FactorSet, Scenario, Currency, NEOpExEdc, TotCashOpExUEdc, VolOpExUEdc, NonVolOpExUEdc, NEOpExUEdc, NEI, TotCashOpExBbl)
SELECT e.FactorSet, e.Scenario, e.Currency, e.NEOpEx, u.TotCashOpEx, u.STVol, u.STNonVol, u.NEOpEx, nei.NEOpEx, Bbl.TotCashOpEx
FROM OpExByDataType e
LEFT JOIN OpExByDataType u ON u.FactorSet = e.FactorSet AND u.Scenario = e.Scenario AND u.Currency = e.Currency AND u.DataType = 'UEdc'
LEFT JOIN OpExByDataType nei ON nei.FactorSet = e.FactorSet AND nei.Scenario = e.Scenario AND nei.Currency = e.Currency AND nei.DataType = 'NEI'
LEFT JOIN OpExByDataType Bbl ON Bbl.FactorSet = e.FactorSet AND Bbl.Scenario = e.Scenario AND Bbl.Currency = e.Currency AND Bbl.DataType = 'Bbl'
WHERE e.DataType = 'Edc'

RETURN
END

