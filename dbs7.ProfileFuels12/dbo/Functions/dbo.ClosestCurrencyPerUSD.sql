﻿CREATE FUNCTION [dbo].[ClosestCurrencyPerUSD](@CurrencyCode CurrencyCode, @StartDate smalldatetime)
RETURNS real
AS
BEGIN
	DECLARE @FirstYear smallint, @LastYear smallint, @NewCurrencyCode CurrencyCode
	DECLARE @ConvRate real

     	SELECT  @ConvRate = dbo.CurrencyPerUSD(@CurrencyCode, @StartDate)
	IF @ConvRate IS NULL
	BEGIN
		DECLARE @FirstMonth smalldatetime, @LastMonth smalldatetime

		SELECT 	@FirstMonth = MIN(CAST(CONVERT(varchar(2), [Month]) + '/1/' + CONVERT(varchar(4), [Year]) AS smalldatetime)), 
			@LastMonth = MAX(CAST(CONVERT(varchar(2), [Month]) + '/1/' + CONVERT(varchar(4), [Year]) AS smalldatetime))	
		FROM CurrencyConv WHERE CurrencyCode = 'USD'

		IF @StartDate < @FirstMonth 
        		SELECT @ConvRate = dbo.CurrencyPerUSD(@CurrencyCode, @FirstMonth)
		ELSE
		BEGIN
			SELECT @NewCurrencyCode = NewCurrency, @FirstMonth = CAST('1/1/' + CONVERT(varchar(4), LastYear+1) AS smalldatetime)
			FROM Currency_LU WHERE CurrencyCode = @CurrencyCode
			IF @NewCurrencyCode IS NULL
				SELECT @ConvRate = dbo.CurrencyPerUSD(@CurrencyCode, @LastMonth)
			ELSE
				SELECT @ConvRate = dbo.CurrencyPerUSD(@CurrencyCode, @FirstMonth) *
                	    		(dbo.ClosestCurrencyPerUSD(@NewCurrencyCode, @StartDate) / dbo.CurrencyPerUSD(@NewCurrencyCode, @FirstMonth))
		END
	END
	RETURN @ConvRate
END
