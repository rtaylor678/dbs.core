﻿CREATE FUNCTION [dbo].[RVSulfurValue](@Sulfur AS Real)
RETURNS real
AS
BEGIN
	DECLARE @Value real
	SELECT @Value = CASE WHEN @Sulfur < 0.25 THEN 0.8
			WHEN @Sulfur < 1.5 THEN 0.16*@Sulfur + 0.76
			WHEN @Sulfur < 3.5 then 0.05*@Sulfur + 0.925
			WHEN @Sulfur >= 3.5 THEN 1.1 END
	RETURN @Value
END
