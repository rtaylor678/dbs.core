﻿
CREATE FUNCTION [dbo].[SLAveragePersKPIs](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet FactorSet)
RETURNS @KPIs TABLE (FactorSet varchar(8) NOT NULL, TotWHrEdc real NULL, OCCWHrEdc real NULL, MpsWhrEdc real NULL, NonMaintWHrEdc real NULL
		, TotMaintForceWHrEdc real NULL, NonTAMaintWHrEdc real NULL, TAMaintWHrEdc real NULL
		, PEI real NULL
		, NonMaintPEI real NULL
		, MaintPEI real NULL, NonTAMaintPEI real NULL, TAMaintPEI real NULL)
AS
BEGIN

	DECLARE @Tot TABLE (FactorSet varchar(8) NOT NULL, TotWHrEdc real NULL, OCCWHrEdc real NULL, MpsWhrEdc real NULL, PEI real NULL)
	INSERT @Tot(FactorSet, TotWHrEdc, OCCWHrEdc, MpsWhrEdc, PEI)
	SELECT FactorSet, TotWHrEdc = AVG(CASE WHEN SectionID = 'TP' THEN WHrEdc END)
		, OCCWHrEdc = AVG(CASE WHEN SectionID = 'TO' THEN WHrEdc END)
		, MpsWhrEdc = AVG(CASE WHEN SectionID = 'TM' THEN WHrEdc END)
		, PEI = AVG(CASE WHEN SectionID = 'TP' THEN EffIndex END)
	FROM [dbo].[SLAveragePersSTCalc](@SubmissionList, @FactorSet) st 
	WHERE SectionID IN ('TO','TM','TP')
	GROUP BY FactorSet

	DECLARE @NonTA TABLE (FactorSet varchar(8) NOT NULL, NonTAMaintPEI real NULL, NonTAMaintWHrEdc real NULL, NonMaintPEI real NULL, NonMaintWHrEdc real NULL
				, MaintPEIDivisor float NULL, WHrEdcDivisor float NULL, NonMaintPEIDivisor float NULL)
	INSERT @NonTA (FactorSet, NonTAMaintPEI, NonTAMaintWHrEdc, NonMaintPEI, NonMaintWHrEdc, MaintPEIDivisor, WHrEdcDivisor, NonMaintPEIDivisor)
	SELECT ptc.FactorSet
		, NonTAMaintPEI = [$(GlobalDB)].dbo.WtAvg(ptc.NonTAMaintPEI, ptc.MaintPEIDivisor)
		, NonTAMaintWHrEdc = [$(GlobalDB)].dbo.WtAvg(ptc.NonTAMaintForceWHrEdc, ptc.WHrEdcDivisor)
		, NonMaintPEI = [$(GlobalDB)].dbo.WtAvg(ptc.NonMaintPEI, ptc.NonMaintPEIDivisor)
		, NonMaintWHrEdc = [$(GlobalDB)].dbo.WtAvg(ptc.NonMaintWHrEdc, ptc.WHrEdcDivisor)
		, MaintPEIDivisor = SUM(ptc.MaintPEIDivisor), WHrEdcDivisor = SUM(ptc.WHrEdcDivisor), NonMaintPEIDivisor = SUM(ptc.NonMaintPEIDivisor)
	FROM dbo.SubmissionsAll s INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	INNER JOIN PersTotCalc ptc ON ptc.SubmissionID = s.SubmissionID AND ptc.Currency = 'USD'
	WHERE ptc.FactorSet = ISNULL(@FactorSet, ptc.FactorSet)
	GROUP BY ptc.FactorSet

	/* Get Current T/A personnel adjustments */
	DECLARE @TAAdj TABLE (FactorSet varchar(8) NOT NULL
					, OCCTAWHrEffIndex real NULL, OCCTAmPEI real NULL, OCCTAWHrEdc real NULL
					, MPSTAWHrEffIndex real NULL, MPSTAmPEI real NULL, MPSTAWHrEdc real NULL)
	INSERT @TAAdj (FactorSet, OCCTAWHrEffIndex, OCCTAmPEI, OCCTAWHrEdc, MPSTAWHrEffIndex, MPSTAmPEI, MPSTAWHrEdc)
	SELECT FactorSet, OCCTAWHrEffIndex, OCCTAmPEI, OCCTAWHrEdc, MPSTAWHrEffIndex, MPSTAmPEI, MPSTAWHrEdc
	FROM dbo.SLPersTAAdj(@SubmissionList, @FactorSet)

	INSERT @KPIs (FactorSet, TotWHrEdc, OCCWHrEdc, MpsWhrEdc, NonMaintWHrEdc
		, TotMaintForceWHrEdc, NonTAMaintWHrEdc, TAMaintWHrEdc
		, PEI, NonMaintPEI, MaintPEI, NonTAMaintPEI, TAMaintPEI)
	SELECT t.FactorSet, t.TotWHrEdc, t.OCCWHrEdc, t.MpsWhrEdc, nta.NonMaintWHrEdc
		, TotMaintForceWHrEdc = nta.NonTAMaintWHrEdc + ta.OCCTAWHrEdc + ta.MPSTAWHrEdc, nta.NonTAMaintWHrEdc, TAMaintWHrEdc = ta.OCCTAWHrEdc + ta.MPSTAWHrEdc
		, t.PEI, nta.NonMaintPEI
		, MaintPEI = nta.NonTAMaintPEI + ta.OCCTAmPEI + ta.MPSTAmPEI, nta.NonTAMaintPEI, TAMaintPEI = ta.OCCTAmPEI + ta.MPSTAmPEI
	FROM @Tot t INNER JOIN @NonTA nta ON nta.FactorSet = t.FactorSet
	INNER JOIN @TAAdj ta ON ta.FactorSet = t.FactorSet

RETURN

END

