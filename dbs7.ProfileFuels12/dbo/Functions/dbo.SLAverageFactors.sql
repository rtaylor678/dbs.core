﻿CREATE FUNCTION [dbo].[SLAverageFactors](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet)
RETURNS TABLE
AS
RETURN (
	WITH MonthlyTotals AS (
	SELECT a.FactorSet, s.PeriodYear, s.PeriodMonth, RefCount = COUNT(*), NumDays = AVG(s.NumDays), FractionOfYear = AVG(s.FractionOfYear)
		, SumEdc = SUM(a.Edc)
		, SumUEdc = SUM(a.UEdc)
		, UtilPcnt = [$(GlobalDB)].dbo.WtAvg(a.UtilPcnt, a.Edc*s.NumDays)
		, SumProcessEdc = SUM(a.TotProcessEdc)
		, SumProcessUEdc = SUM(a.TotProcessUEdc)
		, ProcessUtilPcnt = [$(GlobalDB)].dbo.WtAvg(a.TotProcessUtilPcnt, a.TotProcessEdc*s.NumDays)
		, UtilOSTA = [$(GlobalDB)].dbo.WtAvg(a.UtilOSTA, TotProcessEdc*s.NumDays)
		, SumEnergyUseDay = SUM(a.EnergyUseDay)
		, SumStdEnergy = SUM(a.TotStdEnergy)
		, EII = [$(GlobalDB)].dbo.WtAvg(a.EII, a.TotStdEnergy*s.NumDays)
		, SumReportLossGain = SUM(a.ReportLossGain)
		, SumEstGain = SUM(a.EstGain)
		, VEI = [$(GlobalDB)].dbo.WtAvgNN(a.VEI, a.EstGain)
		, Complexity = [$(GlobalDB)].dbo.WtAvg(a.Complexity, a.Edc*s.NumDays)
		, NonMaintPersEffDiv = SUM(a.NonMaintPersEffDiv*s.FractionOfYear)
		, MaintPersEffDiv = SUM(a.MaintPersEffDiv*s.FractionOfYear)
		, PersEffDiv = SUM(a.PersEffDiv*s.FractionOfYear)
		, MaintEffDiv = SUM(a.MaintEffDiv*s.FractionOfYear)
		, SumAnnNonMaintPersEffDiv = SUM(a.NonMaintPersEffDiv)
		, SumAnnMaintPersEffDiv = SUM(a.MaintPersEffDiv)
		, SumAnnPersEffDiv = SUM(a.PersEffDiv)
		, SumAnnMaintEffDiv = SUM(a.MaintEffDiv)
		, SumRV = SUM(a.RV)
	FROM FactorTotCalc a INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = a.SubmissionID
	INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID
	WHERE (a.FactorSet = @FactorSet OR @FactorSet IS NULL)
	GROUP BY a.FactorSet, s.PeriodYear, s.PeriodMonth)
	SELECT FactorSet
		, AvgEdc = [$(GlobalDB)].dbo.WtAvg(SumEdc, NumDays)
		, AvgUEdc = [$(GlobalDB)].dbo.WtAvg(SumUEdc, NumDays)
		, SumUEdc = SUM(1.0*SumUEdc*NumDays)
		, UtilPcnt = [$(GlobalDB)].dbo.WtAvg(UtilPcnt, SumEdc*NumDays)
		, AvgProcessEdc = [$(GlobalDB)].dbo.WtAvg(SumProcessEdc, NumDays)
		, AvgProcessUEdc = [$(GlobalDB)].dbo.WtAvg(SumProcessUEdc, NumDays)
		, ProcessUtilPcnt = [$(GlobalDB)].dbo.WtAvg(ProcessUtilPcnt, SumProcessEdc*NumDays)
		, UtilOSTA = [$(GlobalDB)].dbo.WtAvg(UtilOSTA, SumProcessEdc*NumDays)
		, EnergyUseDay = [$(GlobalDB)].dbo.WtAvg(SumEnergyUseDay, NumDays)
		, AvgStdEnergy = [$(GlobalDB)].dbo.WtAvg(SumStdEnergy, NumDays)
		, EII = [$(GlobalDB)].dbo.WtAvg(EII, SumStdEnergy*NumDays)
		, LossGainBpD = SUM(SumReportLossGain)/SUM(NumDays*1.0)
		, AvgStdGainBpD = SUM(SumEstGain)/SUM(NumDays*1.0)
		, VEI = [$(GlobalDB)].dbo.WtAvgNN(VEI, SumEstGain)
		, Complexity = [$(GlobalDB)].dbo.WtAvg(Complexity, SumEdc*NumDays)
		, NonMaintPersEffDiv = SUM(NonMaintPersEffDiv)
		, MaintPersEffDiv = SUM(MaintPersEffDiv)
		, PersEffDiv = SUM(PersEffDiv)
		, MaintEffDiv = SUM(MaintEffDiv)
		, AvgAnnNonMaintPersEffDiv = [$(GlobalDB)].dbo.WtAvg(SumAnnNonMaintPersEffDiv,NumDays)
		, AvgAnnMaintPersEffDiv = [$(GlobalDB)].dbo.WtAvg(SumAnnMaintPersEffDiv,NumDays)
		, AvgAnnPersEffDiv = [$(GlobalDB)].dbo.WtAvg(SumAnnPersEffDiv,NumDays)
		, AvgAnnMaintEffDiv = [$(GlobalDB)].dbo.WtAvg(SumAnnMaintEffDiv,NumDays)
		, AvgRV = [$(GlobalDB)].dbo.WtAvg(SumRV, NumDays)
		, NumDays = SUM(NumDays)
	FROM MonthlyTotals WHERE RefCount = (SELECT MAX(RefCount) FROM MonthlyTotals)
	GROUP BY FactorSet
	)

