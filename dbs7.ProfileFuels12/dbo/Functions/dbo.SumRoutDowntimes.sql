﻿CREATE FUNCTION [dbo].[SumRoutDowntimes](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
		SELECT mr.UnitID, fc.FactorSet, MaintDown = SUM(mr.MaintDown), MaintSlow = SUM(mr.MaintSlow), 
		RegDown = SUM(mr.RegDown), RegSlow = SUM(mr.RegSlow), 
		OthDown = SUM(mr.OthDown), OthSlow = SUM(mr.OthSlow), PeriodHrs = SUM(mr.PeriodHrs),
		OthDownEconomic = SUM(mr.OthDownEconomic), OthDownExternal = SUM(mr.OthDownExternal), OthDownUnitUpsets = SUM(mr.OthDownUnitUpsets), OthDownOffsiteUpsets = SUM(mr.OthDownOffsiteUpsets), OthDownOther = SUM(mr.OthDownOther), 
		UnpRegDown = SUM(mr.UnpRegDown), UnpMaintDown = SUM(mr.UnpMaintDown), UnpOthDown  = SUM(mr.UnpOthDown),
		PeriodEdc = SUM(mr.PeriodHrs/365.*fc.EdcNoMult)
		FROM MaintRout mr INNER JOIN dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartDate, @EndDate) s ON s.SubmissionID = mr.SubmissionID
		INNER JOIN FactorCalc fc ON fc.SubmissionID = mr.SubmissionID AND fc.UnitID = mr.UnitID
		WHERE mr.PeriodHrs > 0
		GROUP BY mr.UnitID, fc.FactorSet
	)


