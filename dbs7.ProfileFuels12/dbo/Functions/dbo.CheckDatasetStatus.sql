﻿CREATE FUNCTION [dbo].[CheckDataSetStatus](@RefineryID char(6), @DataSet varchar(15), @PeriodYear smallint, @PeriodMonth smallint)
RETURNS tinyint
AS
BEGIN
	DECLARE @Status tinyint

	DECLARE @PeriodEnd smalldatetime
	SELECT @PeriodEnd = PeriodEnd
	FROM dbo.SubmissionsAll WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet AND UseSubmission = 1

	IF @PeriodEnd IS NULL
		SET @Status = 1
	ELSE IF EXISTS (SELECT * FROM dbo.SubmissionsAll WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UseSubmission = 1 AND CalcsNeeded IS NOT NULL)
		SET @Status = 2
	ELSE
		SET @Status = 0
		
	RETURN @Status
END

