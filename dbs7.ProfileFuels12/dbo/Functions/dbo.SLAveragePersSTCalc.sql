﻿CREATE FUNCTION [dbo].[SLAveragePersSTCalc](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet)
RETURNS @sections TABLE (
	FactorSet varchar(8) NOT NULL,
	SectionID char(2) NOT NULL,
	EffIndex real NULL,
	EffDivisor real NULL,
	WHrEdc real NULL,
	EdcDivisor real NULL)
AS
BEGIN

/* Get Non-TA average KPIs */
INSERT @sections (FactorSet, SectionID, EffIndex, EffDivisor, WHrEdc, EdcDivisor)
SELECT FactorSet, SectionID, TotWHrEffIndex = [$(GlobalDB)].dbo.WtAvg(TotWHrEffIndex, EffDivisor), EffDivisor = SUM(EffDivisor)
	, TotWHrEdc = [$(GlobalDB)].dbo.WtAvg(TotWHrEdc, WHrEdcDivisor), WHrEdcDivisor = SUM(WHrEdcDivisor)
FROM PersSTCalcNonTA
WHERE FactorSet = ISNULL(@FactorSet, FactorSet) AND SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)
GROUP BY FactorSet, SectionID

/* Get Current T/A personnel adjustments */
DECLARE @TAAdj TABLE (FactorSet varchar(8) NOT NULL, OCCTAWHrEffIndex real NULL, OCCTAWHrEdc real NULL, MPSTAWHrEffIndex real NULL, MPSTAWHrEdc real NULL)
INSERT @TAAdj (FactorSet, OCCTAWHrEffIndex, OCCTAWHrEdc, MPSTAWHrEffIndex, MPSTAWHrEdc)
SELECT FactorSet, OCCTAWHrEffIndex, OCCTAWHrEdc, MPSTAWHrEffIndex, MPSTAWHrEdc
FROM dbo.SLPersTAAdj(@SubmissionList, @FactorSet)

/* Multiply current T/A indicators by average divisor and add to non-T/A */
UPDATE s SET EffIndex = ISNULL(s.EffIndex,0) + ISNULL(ta.OCCTAWHrEffIndex,0)
FROM @sections s INNER JOIN @TAAdj ta ON ta.FactorSet = s.FactorSet
WHERE s.SectionID IN ('OM','TO','TP')
UPDATE s SET EffIndex = ISNULL(s.EffIndex,0) + ISNULL(ta.MPSTAWHrEffIndex,0)
FROM @sections s INNER JOIN @TAAdj ta ON ta.FactorSet = s.FactorSet
WHERE s.SectionID IN ('MM','TM','TP')
UPDATE @sections SET WHrEdc = EffIndex*EffDivisor/EdcDivisor WHERE EdcDivisor > 0 AND SectionID IN ('OM','MM','TO','TM','TP')

RETURN

END

