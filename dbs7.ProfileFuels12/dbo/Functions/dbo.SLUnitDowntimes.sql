﻿CREATE FUNCTION [dbo].[SLUnitDowntimes](@SubmissionList dbo.SubmissionIDList READONLY)
RETURNS TABLE
AS
RETURN (
	SELECT r.RefineryID, r.DataSet, r.UnitID, r.FactorSet, r.MaintDown, r.MaintSlow, r.RegDown, r.RegSlow, r.OthDown, r.OthSlow, r.PeriodHrs, r.PeriodEdc
		, r.OthDownEconomic, r.OthDownExternal, r.OthDownUnitUpsets, r.OthDownOffsiteUpsets, r.OthDownOther, r.UnpRegDown, r.UnpMaintDown, r.UnpOthDown
		, TADown = dbo.GetTADowntime(r.RefineryID, r.DataSet, p.StartDate, p.EndDate, r.UnitID)
		, MechUnavailTA = dbo.GetMechUnavailTA(r.RefineryID, r.DataSet, p.StartDate, p.EndDate, r.UnitID)
	FROM dbo.SLSumRoutDowntimes(@SubmissionList) r
	INNER JOIN (SELECT s.RefineryID, s.DataSet, MIN(s.PeriodStart) AS StartDate, MAX(PeriodEnd) AS EndDate FROM dbo.SubmissionsAll s INNER JOIN @SubmissionList sl ON sl.SubmissionID = s.SubmissionID GROUP BY s.RefineryID, s.DataSet) p
	ON p.RefineryID = r.RefineryID AND p.DataSet = r.DataSet
)


