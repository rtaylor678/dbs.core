﻿CREATE  function [dbo].[ConvGravity](@valToConv float, @fromUnits varchar(255), @toUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @Density float, @Result float
	IF @toUnits = @fromUnits
		SET @Result = @valToConv
	ELSE BEGIN
		-- First, convert the density to specific gravity
		IF @fromUnits = 'SG' 
			SELECT @Density = @valToConv*1000
		ELSE IF @fromUnits = 'API'
		BEGIN
			SELECT @Density = 1000*(141.5/(@valToConv+131.5))
		END
		ELSE IF @fromUnits = 'KGM3' 
			SELECT @Density = @valToConv
		ELSE 
			SET @Density = NULL

		-- Second, convert from Specific Gravity to the desired units
		IF @toUnits = 'SG' 
			SELECT @Result = @Density/1000
		ELSE IF @toUnits = 'API'
			SELECT @Result = (1000*141.5/@Density)-131.5
		ELSE IF @toUnits = 'KGM3' 
			SELECT @Result = @Density
		ELSE
			SET @Result = NULL
	END
	RETURN @Result
END
