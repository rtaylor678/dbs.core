﻿/*
@ProcessUtilPcnt, @TotProcessEdc, @TotProcessUEdc, @OffsitesEdc, @Complexity
@EII, @EnergyUseDay, @TotStdEnergy, @FuelUseDay 
@OpAvail, @MechUnavailTA, NonTAOpUnavail = @NonTAUnavail, @NumDays
MechAvail, @NonTAMechUnavail
@MaintEffIndex, @RoutCost, @MaintEffDiv, TAIndex
mPersEffIndex, MaintWHr, OCCMaintWHr, MPSMaintWHr, mPersEffDiv
@OpExUEdc, @TAAdj, @EnergyCost, @TotCashOpEx, @UEdc
@NEOpExEdc, @NEOpEx, @Edc
@ExchRate
*/

CREATE FUNCTION [dbo].[CalcBashneftKPIs] (@SubmissionList dbo.SubmissionIDList READONLY, 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
RETURNS @KPIData TABLE (
	ProcessUtilPcnt real NULL, ProcessEdc real NULL, ProcessUEdc real NULL, OffsitesEdc real NULL, Complexity real NULL,
	EII real NULL, EnergyUseDay real NULL, TotStdEnergy real NULL, FuelUseDay real NULL,
	OpAvail real NULL, MechUnavailTA real NULL, NonTAOpUnavail real NULL, TADD real NULL, NTAMDD real NULL, RPDD real NULL, NumDays real NULL,
	MechAvail real NULL, NonTAMechUnavail real NULL,
	MaintEffIndex real NULL, AnnTACost real NULL, RoutCost real NULL, MaintEffDiv real NULL, TAIndex real NULL,
	mPersEffIndex real NULL, MaintWHr real NULL, OCCMaintWHr real NULL, MPSMaintWHr real NULL, mPersEffDiv real NULL, 
	NEOpExEdc real NULL, NEOpEx real NULL, Edc real NULL,
	TotCashOpExUEdc real NULL, TAAdj real NULL, EnergyCost real NULL, TotCashOpEx real NULL, UEdc real NULL, ExchRate real NULL
)

AS
BEGIN

SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

IF EXISTS (SELECT * FROM Submissions WHERE CalcsNeeded IS NOT NULL AND SubmissionID IN (SELECT SubmissionID FROM @SubmissionList))
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN
END

DECLARE	
	@ProcessUtilPcnt real, @TotProcessEdc real, @TotProcessUEdc real, @OffsitesEdc real, @Complexity real,
	@EII real, @EnergyUseDay real, @FuelUseDay real, @TotStdEnergy real,
	@OpAvail real, @MechAvail real, @MechUnavailTA real, @NonTAMechUnavail real, @NonTAOpUnavail real, @RegUnavail real,
	@TADD real, @NTAMDD real, @RPDD real, @NumDays real,
	@MaintEffIndex real, @TAAdj real, @AnnTACost real, @RoutCost real, @MaintEffDiv real, @Edc real, @TAIndex real,
	@mPersEffIndex real, @MaintWHr real, @OCCMaintWHr real, @MPSMaintWHr real, @mPersEffDiv real,
	@OpExUEdc real, @EnergyCost real, @TotCashOpEx real, @UEdc real, @NEOpExEdc real, @NEOpEx real, @ExchRate real


IF @UOM = 'MET'
	SELECT @EnergyUseDay = @EnergyUseDay * 1.055, @FuelUseDay = @FuelUseDay * 1.055, @TotStdEnergy = @TotStdEnergy * 1.055

SELECT @OffsitesEdc = @Edc - @TotProcessEdc

SELECT @ExchRate = dbo.AvgExchangeRate('RUB', MIN(PeriodStart), MAX(PeriodEnd))
FROM Submissions WHERE SubmissionID IN (SELECT SubmissionID FROM @SubmissionList)

INSERT @KPIData (ProcessUtilPcnt, ProcessEdc, ProcessUEdc, OffsitesEdc, Complexity,
	EII, EnergyUseDay, TotStdEnergy, FuelUseDay, 
	OpAvail, MechUnavailTA, NonTAOpUnavail, TADD, NTAMDD, RPDD, NumDays,
	MechAvail, NonTAMechUnavail,
	MaintEffIndex, AnnTACost, RoutCost, MaintEffDiv, TAIndex,
	mPersEffIndex, MaintWHr, OCCMaintWHr, MPSMaintWHr, mPersEffDiv, 
	NEOpExEdc, NEOpEx, Edc,
	TotCashOpExUEdc, TAAdj, EnergyCost, TotCashOpEx, UEdc, ExchRate)
SELECT @ProcessUtilPcnt, @TotProcessEdc, @TotProcessUEdc, @OffsitesEdc, @Complexity, 	
	@EII, @EnergyUseDay, @TotStdEnergy, @FuelUseDay,
	@OpAvail, @MechUnavailTA, @NonTAOpUnavail, TADD = @MechUnavailTA/100*@NumDays, NTAMDD = @NonTAMechUnavail/100*@NumDays, RPDD = @RegUnavail/100*@NumDays, @NumDays,
	@MechAvail, @NonTAMechUnavail,
	@MaintEffIndex, @TAAdj/1000, @RoutCost/1000, @MaintEffDiv/1000000, @TAIndex,
	@mPersEffIndex, @MaintWHr/1000, @OCCMaintWHr/1000, @MPSMaintWHr/1000, @mPersEffDiv/1000,
	@NEOpExEdc, @NEOpEx/1000, @Edc,
	@OpExUEdc, @TAAdj/1000, @EnergyCost/1000, @TotCashOpEx/1000, @UEdc, @ExchRate

	RETURN
END
