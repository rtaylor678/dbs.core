﻿CREATE Function [dbo].[APItoKGM3](@API real)
RETURNS real
AS
BEGIN
	DECLARE @KGM3 real
	IF @API <= 0
		SELECT @KGM3 = NULL
	ELSE
		SELECT @KGM3 = 1000*(141.5/(@API+131.5))
	RETURN @KGM3
END
