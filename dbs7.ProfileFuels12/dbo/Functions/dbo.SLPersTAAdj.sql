﻿CREATE FUNCTION [dbo].[SLPersTAAdj](@SubmissionList dbo.SubmissionIDList READONLY, @FactorSet dbo.FactorSet)
RETURNS @TAAdj TABLE (FactorSet varchar(8) NOT NULL, OCCTAWHrEffIndex real NULL, OCCTAmPEI real NULL, OCCTAWHrEdc real NULL, MPSTAWHrEffIndex real NULL, MPSTAmPEI real NULL, MPSTAWHrEdc real NULL)
AS
BEGIN
	INSERT @TAAdj(FactorSet, OCCTAWHrEffIndex, OCCTAmPEI, OCCTAWHrEdc, MPSTAWHrEffIndex, MPSTAmPEI, MPSTAWHrEdc)
	SELECT ftc.FactorSet, OCCTAWHrEffIndex = [$(GlobalDB)].dbo.WtAvg(o.TotWHrEffIndex, o.EffDivisor), OCCTAmPEI = [$(GlobalDB)].dbo.WtAvg(o.MaintPersEffIndex, o.MaintPersEffDivisor), OCCTAWHrEdc = [$(GlobalDB)].dbo.WtAvg(o.TotWHrEdc, o.WHrEdcDivisor)
						, MPSTAWHrEffIndex = [$(GlobalDB)].dbo.WtAvg(m.TotWHrEffIndex, m.EffDivisor), MPSTAmPEI = [$(GlobalDB)].dbo.WtAvg(m.MaintPersEffIndex, m.MaintPersEffDivisor), MPSTAWHrEdc = [$(GlobalDB)].dbo.WtAvg(m.TotWHrEdc, m.WHrEdcDivisor)
	FROM dbo.SLLastPeriods(@SubmissionList) s INNER JOIN FactorTotCalc ftc ON ftc.SubmissionID = s.SubmissionID
	LEFT JOIN PersCalc o ON o.SubmissionID = s.SubmissionID AND o.FactorSet = ftc.FactorSet AND o.PersId = 'OCCTAADJ'
	LEFT JOIN PersCalc m ON m.SubmissionID = s.SubmissionID AND m.FactorSet = ftc.FactorSet AND m.PersId = 'MPSTAADJ'
	WHERE ftc.FactorSet = ISNULL(@FactorSet, ftc.FactorSet)
	GROUP BY ftc.FactorSet
	
	RETURN
END

