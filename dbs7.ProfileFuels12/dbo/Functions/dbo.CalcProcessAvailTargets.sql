﻿
CREATE FUNCTION [dbo].[CalcProcessAvailTargets](@SubmissionID int)
RETURNS TABLE
AS
RETURN (
	SELECT ProcessID = pg.ProcessGrouping, f.FactorSet, 
	MechAvail_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Ann_Target,f.EdcNoMult), 
	MechAvailSlow_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Ann_Target,f.EdcNoMult), 
	OpAvail_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Ann_Target,f.EdcNoMult), 
	OpAvailSlow_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Ann_Target,f.EdcNoMult), 
	OnStream_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Ann_Target,f.EdcNoMult), 
	OnStreamSlow_Ann_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Ann_Target,f.EdcNoMult), 
	MechAvail_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvail_Act_Target,f.EdcNoMult), 
	MechAvailSlow_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.MechAvailSlow_Act_Target,f.EdcNoMult), 
	OpAvail_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvail_Act_Target,f.EdcNoMult), 
	OpAvailSlow_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OpAvailSlow_Act_Target,f.EdcNoMult), 
	OnStream_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStream_Act_Target,f.EdcNoMult), 
	OnStreamSlow_Act_Target=[$(GlobalDB)].dbo.WtAvg(m.OnStreamSlow_Act_Target,f.EdcNoMult)
	FROM MaintCalc m 
	INNER JOIN FactorCalc f ON f.SubmissionID = m.SubmissionID AND f.UnitID = m.UnitID
	INNER JOIN RefProcessGroupings pg ON pg.SubmissionID = m.SubmissionID AND pg.UnitID = m.UnitID
	WHERE m.SubmissionID = @SubmissionID AND f.EdcNoMult > 0
	GROUP BY pg.ProcessGrouping, f.FactorSet
)


