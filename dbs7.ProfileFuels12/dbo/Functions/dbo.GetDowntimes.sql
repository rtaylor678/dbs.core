﻿CREATE FUNCTION [dbo].[GetDowntimes](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime)
RETURNS TABLE
AS
RETURN (
	SELECT r.UnitID, r.FactorSet, r.MaintDown, r.MaintSlow, r.RegDown, r.RegSlow, r.OthDown, r.OthSlow, r.PeriodHrs, r.PeriodEdc
		, r.OthDownEconomic, r.OthDownExternal, r.OthDownUnitUpsets, r.OthDownOffsiteUpsets, r.OthDownOther, r.UnpRegDown, r.UnpMaintDown, r.UnpOthDown
		, TADown = dbo.GetTADowntime(@RefineryID, @DataSet, @StartDate, @EndDate, r.UnitID)
		, MechUnavailTA = dbo.GetMechUnavailTA(@RefineryID, @DataSet, @StartDate, @EndDate, r.UnitID)
	FROM dbo.SumRoutDowntimes(@RefineryID, @DataSet, @StartDate, @EndDate) r
)


