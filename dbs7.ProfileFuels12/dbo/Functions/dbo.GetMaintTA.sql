﻿CREATE FUNCTION [dbo].[GetMaintTA](@RefineryID varchar(6), @DataSet varchar(15), @UnitID int, @TAID int)
RETURNS TABLE 
AS
RETURN (
	SELECT ta.UnitID, ta.TAID, ta.TADate, ta.TAHrsDown, ta.TACostLocal, ta.TAMatlLocal
		, ta.TAOCCSTH, ta.TAOCCOVT, ta.TAMPSSTH, ta.TampSovtPcnt, ta.TAContOCC, ta.TAContMPS
		, ta.PrevTADate, ta.TAExceptions, ta.TACurrency
	FROM MaintTA ta
	WHERE ta.RefineryID = @RefineryID AND ta.DataSet = @DataSet
	AND (ta.UnitID = @UnitID OR @UnitID IS NULL)
	AND (ta.TAID = @TAID OR @TAID IS NULL)
)
