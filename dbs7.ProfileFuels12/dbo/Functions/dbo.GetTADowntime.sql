﻿CREATE FUNCTION [dbo].[GetTADowntime](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @UnitID int)
RETURNS real
AS
BEGIN
	DECLARE @TADown real

	SELECT @TADown = SUM(CASE WHEN RestartDate > @StartDate AND TADate < @EndDate THEN 
							DATEDIFF(hh, CASE WHEN TADate < @StartDate THEN @StartDate ELSE TADate END, 
										CASE WHEN RestartDate > @EndDate THEN @EndDate ELSE RestartDate END) 
						ELSE 0 END)
	FROM MaintTA 
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND ISNULL(TAExceptions, 0) = 0 AND TAID > 0 AND UnitID = @UnitID
	
	IF @TADown IS NULL
		SET @TADown = 0

	RETURN @TADown
END
