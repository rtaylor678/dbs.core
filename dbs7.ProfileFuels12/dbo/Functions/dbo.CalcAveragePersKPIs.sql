﻿CREATE FUNCTION [dbo].[CalcAveragePersKPIs](@RefineryID varchar(6), @DataSet varchar(15), @StartDate smalldatetime, @EndDate smalldatetime, @FactorSet FactorSet)
RETURNS @KPIs TABLE (FactorSet varchar(8) NOT NULL, OCCWHrEdc real NULL, MpsWhrEdc real NULL, TotWHrEdc real NULL, TotMaintForceWHrEdc real NULL
		, PEI real NULL, MaintPEI real NULL, NonMaintPEI real NULL
		, OCCEqPEdc real NULL, MPSEqPEdc real NULL, TotEqPEdc real NULL)
AS
BEGIN
	INSERT @KPIs (FactorSet, TotWHrEdc, OCCWHrEdc, MpsWhrEdc, PEI, TotEqPEdc, OCCEqPEdc, MPSEqPEdc, TotMaintForceWHrEdc, MaintPEI, NonMaintPEI)
	SELECT ptc.FactorSet, TotWHrEdc = [$(GlobalDB)].dbo.WtAvg(tp.TotWHrEdc, tp.WHrEdcDivisor), OCCWHrEdc = [$(GlobalDB)].dbo.WtAvg(o.TotWHrEdc, tp.WHrEdcDivisor), MpsWhrEdc = [$(GlobalDB)].dbo.WtAvg(m.TotWHrEdc, tp.WHrEdcDivisor)
		, PEI = [$(GlobalDB)].dbo.WtAvg(tp.TotWHrEffIndex, tp.EffDivisor)
		, TotEqPEdc = [$(GlobalDB)].dbo.WtAvg(tp.TotEqPEdc, tp.EqPEdcDivisor), OCCEqPEdc = [$(GlobalDB)].dbo.WtAvg(o.TotEqPEdc, tp.EqPEdcDivisor), MPSEqPEdc = [$(GlobalDB)].dbo.WtAvg(m.TotEqPEdc, tp.EqPEdcDivisor)
		, TotMaintForceWHrEdc = [$(GlobalDB)].dbo.WtAvg(ptc.TotMaintForceWHrEdc, ptc.WHrEdcDivisor)
		, MaintPEI = [$(GlobalDB)].dbo.WtAvg(ptc.MaintPEI, ptc.MaintPEIDivisor)
		, NonMaintPEI = [$(GlobalDB)].dbo.WtAvg(ptc.NonMaintPEI, ptc.NonMaintPEIDivisor)
	FROM Submissions s INNER JOIN PersTotCalc ptc ON ptc.SubmissionID = s.SubmissionID AND ptc.Currency = 'USD'
	INNER JOIN PersSTCalc tp ON tp.SubmissionID = s.SubmissionID AND tp.SectionID = 'TP' AND tp.FactorSet = ptc.FactorSet
	LEFT JOIN PersSTCalc o ON o.SubmissionID = s.SubmissionID AND o.SectionID = 'TO' AND o.FactorSet = ptc.FactorSet
	LEFT JOIN PersSTCalc m ON m.SubmissionID = s.SubmissionID AND m.SectionID = 'TM' AND m.FactorSet = ptc.FactorSet
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartDate AND s.PeriodStart < @EndDate AND s.UseSubmission = 1
	AND ptc.FactorSet = ISNULL(@FactorSet, ptc.FactorSet)
	GROUP BY ptc.FactorSet


RETURN

END


