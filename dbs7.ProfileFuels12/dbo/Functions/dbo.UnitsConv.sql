﻿CREATE function [dbo].[UnitsConv](@valToConv float, @fromUnits varchar(255), @toUnits varchar(255))
RETURNS float
BEGIN
	DECLARE @DivLocTo int, @DivLocFrom int
	DECLARE @UOMGroupFrom varchar(20), @FactorFrom float
	DECLARE @UOMGroupTo varchar(20), @FactorTo float
	DECLARE @Result float

	IF @toUnits = @fromUnits 
		SET @Result = @valToConv
	ELSE IF CHARINDEX('/', @toUnits) > 0 
	BEGIN
		SELECT 	@DivLocTo = CHARINDEX('/', @toUnits),
			@DivLocFrom = CHARINDEX('/', @fromUnits)
		SELECT @Result = dbo.UnitsConv(@valToConv, SUBSTRING(@fromUnits, 1, @DivLocFrom-1), SUBSTRING(@toUnits, 1, @DivLocTo-1))/
                    		dbo.UnitsConv(1, SUBSTRING(@fromUnits, @DivLocFrom+1, LEN(@fromUnits)), SUBSTRING(@toUnits, @DivLocTo+1, LEN(@toUnits)))
	END
	ELSE IF dbo.CaseSensitiveSearch(@toUnits, 'p', 1) > 0
	BEGIN
		SELECT 	@DivLocTo = dbo.CaseSensitiveSearch(@toUnits, 'p', 1),
			@DivLocFrom = dbo.CaseSensitiveSearch(@fromUnits, 'p', 1)
		SELECT @Result = dbo.UnitsConv(@valToConv, SUBSTRING(@fromUnits, 1, @DivLocFrom-1), SUBSTRING(@toUnits, 1, @DivLocTo-1))
	END
	ELSE IF CHARINDEX('*', @toUnits) > 0
	BEGIN
		SELECT 	@DivLocTo = CHARINDEX('*', @toUnits),
			@DivLocFrom = CHARINDEX('*', @fromUnits)
		SELECT @Result = dbo.UnitsConv(@valToConv, SUBSTRING(@fromUnits, 1, @DivLocFrom-1), SUBSTRING(@toUnits, 1, @DivLocTo-1))*
				dbo.UnitsConv(1, SUBSTRING(@fromUnits,@DivLocFrom+1, LEN(@fromUnits)), SUBSTRING(@toUnits, @DivLocTo+1, LEN(@toUnits)))
	END
	ELSE BEGIN
		SELECT @UOMGroupFrom = UOMGroup, @FactorFrom = Factor
		FROM UOM WHERE UOMCode = @fromUnits
		SELECT @UOMGroupTo = UOMGroup, @FactorTo = Factor
		FROM UOM WHERE UOMCode = @toUnits

		SELECT @Result = CASE ISNULL(@UOMGroupFrom, '')
			WHEN '' THEN @valToConv
			WHEN 'Gravity' THEN dbo.ConvGravity(@valToConv, @fromUnits, @toUnits)
			WHEN 'Temperature' THEN dbo.ConvTemp(@valToConv, @fromUnits, @toUnits)
			WHEN 'Viscosity' THEN dbo.ConvViscosity(@valToConv, @fromUnits, @toUnits)
			ELSE @valToConv * (@FactorTo/@FactorFrom) END
	END
	RETURN @Result
END
