﻿CREATE DEFAULT [dbo].[defProcessType]
    AS '';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defProcessType]', @objname = N'[dbo].[Config].[ProcessType]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defProcessType]', @objname = N'[dbo].[Factors].[ProcessType]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defProcessType]', @objname = N'[dbo].[ConfigRS].[ProcessType]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defProcessType]', @objname = N'[dbo].[ProcessType_LU].[ProcessType]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defProcessType]', @objname = N'[dbo].[SensHeatCriteria].[ProcessType]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defProcessType]', @objname = N'[dbo].[NewFactors].[ProcessType]';


GO
EXECUTE sp_bindefault @defname = N'[dbo].[defProcessType]', @objname = N'[dbo].[ProcessType]';

