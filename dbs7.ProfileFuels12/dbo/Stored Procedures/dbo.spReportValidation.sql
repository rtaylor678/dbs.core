﻿CREATE  PROC [dbo].[spReportValidation] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT dc.DataCheckId, lu.Header, lu.DataCheckText, ItemDescHeader,
Value1Header, Value2Header, Value3Header, Value4Header, Value5Header, 
Value6Header, Value7Header, dc.ItemSortKey, dc.ItemDesc, 
Value1, Value2, Value3, Value4, Value5, Value6, Value7 
FROM DataChecks dc 
INNER JOIN Datacheck_LU lu ON dc.DataCheckId = lu.DataCheckId 
INNER JOIN Submissions s ON dc.SubmissionID = s.SubmissionID 
WHERE s.RefineryID=@RefineryID AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.UseSubmission = 1
ORDER BY lu.RptOrder, dc.ItemSortKey

