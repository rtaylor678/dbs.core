﻿CREATE PROCEDURE [dbo].[DUMP_PSD_2]

	@RefNum nvarchar(10),
	@DataSetID nvarchar(10),
	@CapColName nvarchar(10)
	
	AS
	DECLARE @SQL as nvarchar(2000)
	
	SET @SQL = 'SELECT s.Location,s.PeriodStart,s.PeriodEnd,s.NumDays as DaysInPeriod,s.RptCurrency as Currency,s.UOM, 
                                    c.UnitID,c.UnitName, c.ProcessID, c.ProcessType, ISNULL(c.' + @CapColName + ' ,0) AS ' + @CapColName + '  , ISNULL(c.UtilPcnt,0) AS UtilPcnt,  
                                    ISNULL(EdcNoMult,0) AS Edc, ISNULL(UEdcNoMult,0) AS UEdc, d.DisplayTextUS, d.DisplayTextMET  
                                    FROM config c,FactorCalc fc, ProcessID_LU p ,DisplayUnits_LU d,Submissions s  
                                     WHERE c.ProcessID = p.ProcessID AND p.DisplayUnits = d.DisplayUnits AND  
                                    c.UnitID = fc.UnitID AND c.SubmissionID = fc.SubmissionID AND  
                                    c.ProcessID IN (''STEAMGEN'', ''ELECGEN'', ''FCCPOWER'')  
                                     AND fc.FactorSet= + StudyYear.ToString +  AND s.SubmissionID=c.SubmissionID AND c.SubmissionID IN  
                                    (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=' + @DataSetID + ' AND  
                                     RefineryID=' + @RefNum + ') ORDER BY PeriodStart DESC'
                                     
                                     EXEC @SQL

