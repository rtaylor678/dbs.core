﻿CREATE PROC [dbo].[DS_Absence_LU]
	
AS

SELECT RTRIM(CategoryID) as CategoryID,RTRIM(Description) as Description,SortKey,IncludeForInput,RTRIM(ParentID) AS ParentID, RTRIM(Indent) AS Indent,RTRIM(DetailStudy) AS DetailStudy,RTRIM(DetailProfile) AS DetailProfile FROM Absence_LU ORDER BY SortKey
