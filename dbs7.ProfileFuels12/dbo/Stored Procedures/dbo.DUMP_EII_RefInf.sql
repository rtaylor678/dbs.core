﻿CREATE PROCEDURE [dbo].[DUMP_EII_RefInf]
	@CurrencyCode nvarchar(10),
	@UOM nvarchar(10),
	@StudyYear nvarchar(20),
	@DataSetID nvarchar(10),
	@RefNum nvarchar(10)
	
	AS
	
	SELECT s.Location,s.PeriodStart,s.PeriodEnd,
                     s.NumDays as DaysInPeriod, @CurrencyCode AS Currency,@UOM AS UOM , 
                    EII,EnergyUseDay AS EIIDailyUsage,TotStdEnergy AS EIIStdEnergy,
                    VEI,EstGain AS VEIStdGain,ReportLossGain AS VEIActualGain,
                    SensHeatUtilCap AS SensGrossInput, SensHeatStdEnergy ,
                    '44-(0.23*CrudeGravity)' As SensHeatFormula,(44-(SensHeatStdEnergy/SensHeatUtilCap))/.23 AS CrudeGravity 
                    FROM FactorTotCalc f,Submissions s  WHERE FactorSet=@StudyYear
                     AND  f.SubmissionID=s.SubmissionID AND f.SubmissionID IN 
                    (SELECT Distinct SubmissionID FROM Submissions WHERE  DataSet=@DataSetID
                     AND  RefineryID=@RefNum
                     ) ORDER BY PeriodStart DESC

