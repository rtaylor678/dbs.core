﻿CREATE PROC [dbo].[spReportMonroeKPI2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @StartYTD smalldatetime, @Start12Mo smalldatetime, @Start24Mo smalldatetime
SELECT @StartYTD = p.StartYTD, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)

SELECT 
	EII = m.EII, EII_Ytd = avgYTD.EII, EII_Avg = avg12mo.EII, 
	EnergyUseDay = m.EnergyUseDay, EnergyUseDay_Ytd = avgYTD.EnergyUseDay, EnergyUseDay_Avg = avg12mo.EnergyUseDay, 
	TotStdEnergy = m.TotStdEnergy, TotStdEnergy_Ytd = avgYTD.TotStdEnergy, TotStdEnergy_Avg = avg12mo.TotStdEnergy, 

	Edc = m.Edc/1000, Edc_Ytd = avgYTD.Edc/1000, Edc_Avg = avg12mo.Edc/1000, 

	UtilOSTA = m.UtilOSTA, UtilOSTA_Ytd = avgYTD.UtilOSTA, UtilOSTA_Avg = avg12mo.UtilOSTA, 
	ProcessEdc = m.TotProcessEdc/1000, ProcessEdc_Ytd = avgYTD.TotProcessEdc/1000, ProcessEdc_Avg = avg12mo.TotProcessEdc/1000, 
	ProcessUEdc = m.TotProcessUEdc/1000, ProcessUEdc_Ytd = avgYTD.TotProcessUEdc/1000, ProcessUedc_Avg = avg12mo.TotProcessUEdc/1000, 
	MechUnavailTA_Act = m.MechUnavailTA_Act, MechUnavailTA_Act_Ytd = avgYTD.MechUnavailTA_Act, MechUnavailTA_Act_Avg = avg12mo.MechUnavailTA_Act,
	
	VEI = m.VEI, VEI_Ytd = avgYTD.VEI, VEI_Avg = avg12mo.VEI, 
	NetInputBPD = m.NetInputBPD, NetInputBPD_Ytd = avgYTD.NetInputBPD, NetInputBPD_Avg = avg12mo.NetInputBPD,
	ReportLossGain = m.ReportLossGain, ReportLossGain_Ytd = avgYTD.ReportLossGain, ReportLossGain_Avg = avg12mo.ReportLossGain, 
	EstGain = m.EstGain, EstGain_Ytd = avgYTD.EstGain, EstGain_Avg = avg12mo.EstGain, 

	MechAvail = m.MechAvail, MechAvail_Ytd = avgYTD.MechAvail, MechAvail_Avg = avg24mo.MechAvail, 
	MechUnavailTA = m.MechUnavailTA, MechUnavailTA_Ytd = avgYTD.MechUnavailTA, MechUnavailTA_Avg = avg24mo.MechUnavailTA, 
	MechUnavailNonTA = m.MechUnavailRout, MechUnavailNonTA_Ytd = avgYTD.MechUnavailRout, MechUnavailNonTA_Avg = avg24mo.MechUnavailRout, 

	OpAvail = m.OpAvail, OpAvail_Ytd = avgYTD.OpAvail, OpAvail_Avg = avg24mo.OpAvail, 
	RegUnavail = m.RegUnavail, RegUnavail_Ytd = avgYTD.RegUnavail, RegUnavail_Avg = avg24mo.RegUnavail, 

	TotWHrEdc = m.PersIndex, TotWhrEdc_Ytd = avgYTD.PersIndex, TotWhrEdc_Avg = avg12mo.PersIndex, 
	AnnTAWhr = m.MaintTAWHr_k, AnnTAWhr_Ytd = avgYTD.MaintTAWHr_k, AnnTAWhr_Avg = avg12mo.MaintTAWHr_k,
	NonTAWHr = m.TotNonTAWHr_k, NonTAWHr_Ytd = avgYTD.TotNonTAWHr_k, NonTAWHr_Avg = avg12mo.TotNonTAWHr_k, 

	MPEI = m.mPersEffIndex, MPEI_Ytd = avgYTD.mPersEffIndex, MPEI_Avg = avg12mo.mPersEffIndex, 
	MPEIStd = m.mPersEffDiv/1000, MPEIStd_Ytd = avgYTD.mPersEffDiv/1000, MPEIStd_Avg = avg12mo.mPersEffDiv/1000, 
	NonTAMaintHrs = m.MaintNonTAWHr_k, NonTAMaintHrs_Ytd = avgYTD.MaintNonTAWHr_k, NonTAMaintHrs_Avg = avg12mo.MaintNonTAWHr_k,

	MaintIndex = m.MaintIndex, MaintIndex_Ytd = avgYTD.MaintIndex, MaintIndex_Avg = avg24mo.MaintIndex, 
	TAAdj = m.AnnTACost, TAAdj_Ytd = avgYTD.AnnTACost, TAAdj_Avg = mi24.TAEffIndex*avg24mo.MaintEffDiv/100/1000,
	RoutCost = m.RoutCost, RoutCost_Ytd = avgYTD.RoutCost, RoutCost_Avg = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1000, 

	MEI = m.MaintEffIndex, MEI_Ytd = avgYTD.MaintEffIndex, MEI_Avg = avg24mo.MaintEffIndex, 
	MEIStd = m.MaintEffDiv/1000, MEIStd_Ytd = avgYTD.MaintEffDiv/1000, MEIStd_Avg = avg24mo.MaintEffDiv/1000, 

	NEOpExEdc = m.NEOpExEdc, NEOpExEdc_Ytd = avgYTD.NEOpExEdc, NEOpExEdc_Avg = avg12mo.NEOpExEdc, 
	NEOpEx = m.NEOpEx, NEOpEx_Ytd = avgYTD.NEOpEx, NEOpEx_Avg = avg12mo.NEOpEx, 

	TotCashOpExUEdc = m.OpExUEdc, TotCashOpExUEdc_Ytd = avgYTD.OpExUEdc, TotCashOpExUedc_Avg = avg12mo.OpExUEdc, 
	EnergyCost = m.EnergyCost, EnergyCost_Ytd = avgYTD.EnergyCost, EnergyCost_Avg = avg12mo.EnergyCost, 
	TotCashOpEx = m.TotCashOpEx, TotCashOpEx_Ytd = avgYTD.TotCashOpEx, TotCashOpEx_Avg = avg12mo.TotCashOpEx, 
	UEdc = m.UEdc/1000, UEdc_Ytd = avgYTD.UEdc/1000, Uedc_Avg = avg12mo.UEdc/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM) avgYTD ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1
