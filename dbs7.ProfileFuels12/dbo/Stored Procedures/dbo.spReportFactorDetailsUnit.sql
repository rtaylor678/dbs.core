﻿


CREATE    PROC [dbo].[spReportFactorDetailsUnit] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency= @Currency, UOM = @UOM, 
lu.Description, c.UnitName, c.ProcessID, c.ProcessType, c.Cap, c.RptCap, c.UtilPcnt,
f.StdEnergy, f.StdGain, f.EdcFactor, ISNULL(EdcNoMult,0)*ISNULL(MultiFactor,1)/1000 as kEdc, ISNULL(UEdcNoMult,0)*ISNULL(MultiFactor,1)/1000 as kUEdc, 
MaintEffDiv/1000 as MaintEffDiv, NonMaintPersEffDiv/1000 as nmPersEffDiv, MaintPersEffDiv/1000 as mPersEffDiv, PersEffDiv/1000 as PersEffDiv, NEOpExEffDiv/1000 as NEOpExEffDiv, lu.SortKey, c.UnitID,
ProcessGrouping = CASE rpg.ProcessGrouping WHEN 'OperProc' THEN 'Process' WHEN 'AncUnits' THEN 'Ancillary' WHEN 'IdleProc' THEN 'Idle' ELSE 'Utility' END,
UnitNameRussian = CASE c.ProcessID WHEN 'ELECDIST' THEN N'Электрическая распределительная система' WHEN 'TNK+BLND' THEN CASE c.ProcessType WHEN 'TCAP' THEN N'Емкость резервуаров' WHEN 'TCNT' THEN N'Количество резервуаров' WHEN 'TBPD' THEN N'Барр. сырья/день' ELSE c.UnitName END WHEN 'OFFCOKE' THEN N'Перемещение и погрузка кокса' ELSE c.UnitName END
FROM Config c INNER JOIN FactorCalc f ON c.SubmissionID=f.SubmissionID AND f.UnitID=c.UnitID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
INNER JOIN Submissions s ON s.SubmissionID = c.SubmissionID
LEFT JOIN RefProcessGroupings rpg ON rpg.SubmissionID = c.SubmissionID AND rpg.FactorSet = f.FactorSet AND rpg.UnitID = c.UnitID AND rpg.ProcessGrouping IN ('AncUnits','OperProc','IdleProc')
WHERE f.FactorSet=@FactorSet AND c.ProcessID NOT IN ('BLENDING')  
AND s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
ORDER BY s.PeriodStart DESC, lu.SortKey ASC, c.UnitID ASC


