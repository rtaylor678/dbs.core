﻿CREATE PROCEDURE [dbo].[DS_GetMaintTAOther]
	@RefineryID nvarchar(10)
AS
BEGIN
	SELECT TAID, M.UnitID, P.SortKey, RTRIM(P.ProcessID) as ProcessID, 
                    RTRIM(P.Description) as UnitName, TADate, TAHrsDown, TACostLocal, 
                    TALaborCostLocal,  TAExpLocal, TACptlLocal, TAOvhdLocal, TAOCCSTH, TAOCCOVT, TAMPSSTH, TampSovtPcnt, TAContOCC, TAContMPS,
                    PrevTADate, TAExceptions 
                    FROM 	MaintTA M, ProcessID_LU P 
                    WHERE 	M.ProcessID = P.ProcessID  AND ProfileProcFacility = 'N' AND MaintDetails = 'Y' 
                    AND RefineryID = @RefineryID
END
