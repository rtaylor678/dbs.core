﻿
CREATE PROC spReportUnitAvailCalc(@RefineryID varchar(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(8) = 'ACTUAL', @FactorSet varchar(8) = '2012', @NumMonths tinyint)
AS
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime
SELECT @PeriodEnd = PeriodEnd FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
SELECT @PeriodStart = DATEADD(mm, -@NumMonths, @PeriodEnd)

;WITH downtime AS (
SELECT rpg.UnitID, [$(GlobalDB)].dbo.WtAvg(fc.EdcNoMult, mr.PeriodHrs)/1000 AS AvgEdc, SUM(ISNULL(RegDown,0)) AS RegDown, SUM(ISNULL(MaintDown,0)) AS MaintDown, SUM(ISNULL(OthDown,0)) AS OthDown, SUM(ISNULL(RegSlow,0)+ISNULL(MaintSlow,0)+ISNULL(OthSlow,0)) AS Slowdowns, SUM(mr.PeriodHrs) AS PeriodHrs
	, MAX(s.PeriodStart) AS LastPeriodStart
	, MechUnavailTA = dbo.GetMechUnavailTA(@RefineryID, @DataSet, MIN(s.PeriodStart), MAX(s.PeriodEnd), rpg.UnitID)
	, LastSubmission = (SELECT SubmissionID FROM dbo.GetSubmission(@RefineryID, DATEPART(yyyy, MAX(s.PeriodStart)), DATEPART(mm,MAX(s.PeriodStart)), @DataSet))
FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd) s
INNER JOIN dbo.RefProcessGroupings rpg ON rpg.SubmissionID = s.SubmissionID AND rpg.FactorSet = @FactorSet AND rpg.ProcessGrouping = 'OperProc'
INNER JOIN MaintRout mr ON mr.SubmissionID = rpg.SubmissionID AND mr.UnitID = rpg.UnitID
INNER JOIN FactorCalc fc ON fc.SubmissionID = rpg.SubmissionID AND fc.UnitID = rpg.UnitID AND fc.FactorSet = rpg.FactorSet
GROUP BY rpg.UnitID
)
SELECT c.UnitName, c.ProcessID, d.AvgEdc, d.RegDown, d.MaintDown, d.OthDown, d.Slowdowns, d.PeriodHrs, d.MechUnavailTA
	, MechAvail = 100 - MaintDown/PeriodHrs*100 - MechUnavailTA
	, OpAvail = 100 - (MaintDown+RegDown)/PeriodHrs*100 - MechUnavailTA
	, OnStream = 100 - (MaintDown+RegDown+OthDown)/PeriodHrs*100 - MechUnavailTA
	, OnStreamSlow = 100 - (MaintDown+RegDown+OthDown+Slowdowns)/PeriodHrs*100 - MechUnavailTA
FROM downtime d LEFT JOIN Config c ON c.SubmissionID = d.LastSubmission AND c.UnitID = d.UnitID
INNER JOIN ProcessID_LU pid ON pid.ProcessID = c.ProcessID
ORDER BY pid.SortKey, c.UnitID

