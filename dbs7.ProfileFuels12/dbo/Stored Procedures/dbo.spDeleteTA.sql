﻿CREATE PROC [dbo].[spDeleteTA](@RefineryID varchar(6), @DataSet varchar(15), @UnitID int, @TAID int)
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @RecalcFrom smalldatetime
	SELECT @RecalcFrom = TADate FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
	IF EXISTS (SELECT * FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TADate > @RecalcFrom)
		RETURN 1
	ELSE BEGIN
		DELETE FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
		DELETE FROM MaintTACost WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
		
		-- Update the Next T/A date in MaintTA and MaintTACost
		EXEC spUpdateNextTADate @RefineryID, @DataSet

		-- Tag the submissions to make updates for turnaround changes
		UPDATE Submissions
		SET CalcsNeeded = 'T'
		WHERE RefineryID = @RefineryID AND DataSet = @DataSet
		AND PeriodEnd >= @RecalcFrom AND CalcsNeeded IS NULL
	END
END

