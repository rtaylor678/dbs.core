﻿CREATE PROC [dbo].[SS_GetInputUserDefined]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            u.HeaderText,u.VariableDesc,CAST(u.RptValue AS real) AS RptValue,CAST(u.RptValue_Target AS real) AS RptValue_Target,CAST(u.RptValue_Avg AS real) AS RptValue_Avg,CAST(u.RptValue_Ytd AS real) AS RptValue_Ytd,CAST( u.DecPlaces AS int) AS DecPlaces 
            FROM  
            dbo.UserDefined u   
            ,dbo.Submissions s  
            WHERE   
            u.SubmissionID = s.SubmissionID AND 
            u.SubmissionID IN  
            (SELECT DISTINCT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1)

