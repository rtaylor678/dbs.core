﻿
CREATE PROC [dbo].[SS_GetInputEnergy]
	@RefineryID nvarchar(10),
	@DataSet nvarchar(20)='ACTUAL'
AS

SELECT s.SubmissionID, s.PeriodStart, s.PeriodEnd, 
            RTRIM(e.TransType) as TransType,RTRIM(e.TransferTo) As TransferTo, RTRIM(e.EnergyType) as EnergyType, 
            e.TransCode,e.RptSource,e.RptPriceLocal,elu.SortKey,e.Hydrogen,e.Methane,e.Ethane,e.Ethylene,
            e.Propane,e.Propylene,e.Butane,e.Isobutane,e.Butylenes,e.C5Plus,e.H2S,e.CO,e.NH3,e.SO2,e.CO2,e.N2 
            FROM  
            dbo.Energy e ,Energy_LU elu  
            ,dbo.Submissions s  
            WHERE  
            e.SubmissionID = s.SubmissionID AND 
            elu.TransType=e.TransType AND elu.TransferTo=e.TransferTo AND  elu.EnergyType=e.EnergyType AND elu.SortKey < 100 AND  
            (e.SubmissionID IN  
            (SELECT SubmissionID FROM dbo.Submissions
            WHERE RefineryID=@RefineryID and DataSet = @DataSet and UseSubmission=1))
          

