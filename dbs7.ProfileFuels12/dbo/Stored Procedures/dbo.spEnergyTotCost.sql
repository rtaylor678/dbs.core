﻿CREATE  PROC [dbo].[spEnergyTotCost] (@SubmissionID int)
AS

SET NOCOUNT ON 

UPDATE EnergyCost
SET CostK = EnergyCost.CostMBTU*EnergyCons.MBTU/1000
FROM EnergyCost INNER JOIN EnergyCons 
	ON EnergyCons.SubmissionID = EnergyCost.SubmissionID AND EnergyCons.TransType = EnergyCost.TransType
	AND EnergyCons.EnergyType = EnergyCost.EnergyType
WHERE EnergyCost.SubmissionID = @SubmissionID

DELETE FROM EnergyTotCost WHERE SubmissionID = @SubmissionID

INSERT INTO EnergyTotCost(SubmissionID, Currency, Scenario, PurFGCostK, PurLiquidCostK, PurSolidCostK, PurSteamCostK, PurThermCostK, PurPowerCostK, PurTotCostK, ProdFGCostK, ProdOthCostK, ProdTotCostK, TotCostK)
SELECT SubmissionID, Currency, Scenario,
PurFGCostK = SUM(CASE WHEN TransType = 'CPU' AND EnergyType IN ('C2', 'NG', 'FG', 'FG_', 'LBG', 'LCV') THEN CostK ELSE 0 END),
PurLiquidCostK = SUM(CASE WHEN TransType = 'CPU' AND EnergyType NOT IN ('C2', 'NG', 'FG', 'FG_', 'LBG', 'LCV', 'COK' ,'STM', 'LLH', 'PT') THEN CostK ELSE 0 END),
PurSolidCostK = SUM(CASE WHEN TransType = 'CPU' AND EnergyType = 'COK' THEN CostK ELSE 0 END),
PurSteamCostK = SUM(CASE WHEN TransType = 'CPU' AND EnergyType IN ('STM','LLH') THEN CostK ELSE 0 END),
PurThermCostK = SUM(CASE WHEN TransType = 'CPU' AND EnergyType <> 'PT' THEN CostK ELSE 0 END),
PurPowerCostK = SUM(CASE WHEN TransType = 'CPU' AND EnergyType = 'PT' THEN CostK ELSE 0 END),
PurTotCostK = SUM(CASE WHEN TransType = 'CPU' THEN CostK ELSE 0 END),
ProdFGCostK = SUM(CASE WHEN TransType = 'CPR' AND EnergyType = 'FG' THEN CostK ELSE 0 END), 
ProdOthCostK = SUM(CASE WHEN TransType = 'CPR' AND EnergyType <> 'FG' THEN CostK ELSE 0 END), 
ProdTotCostK = SUM(CASE WHEN TransType = 'CPR' THEN CostK ELSE 0 END), 
TotCostK = SUM(CostK)
FROM EnergyCost
WHERE SubmissionID = @SubmissionID
GROUP BY SubmissionID, Currency, Scenario
