﻿CREATE     PROC [dbo].[spSKCustomUnitData](@SubmissionID AS int)
AS

DECLARE @IsMetric bit, @RptCurrency CurrencyCode, @PeriodStart smalldatetime, @NumDays real, @FractionOfYear real
SELECT @IsMetric = CASE WHEN UOM = 'MET' THEN 1 ELSE 0 END, @RptCurrency = RptCurrency, @NumDays = NumDays, @FractionOfYear = FractionOfYear, @PeriodStart = PeriodStart
FROM SubmissionsAll
WHERE SubmissionID = @SubmissionID

DELETE FROM CustomUnitData WHERE SubmissionID = @SubmissionID

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, UnitID, 'Cap_k', 'N/A', 'N/A'
, ActualCap/CASE WHEN lu.DBUnits = lu.KCapacityUnits THEN 1 ELSE 1000 END
, ActualCap/CASE WHEN lu.DBUnits = lu.KCapacityUnits THEN 1 ELSE 1000 END/lu.MetricToUSMult
, NULL, NULL -- No Targets
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE c.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, UnitID, 'UtilPcnt', 'N/A', 'N/A'
, c.UtilPcnt
, c.UtilPcnt
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'UtilPcnt_Target')
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'UtilPcnt_Target')
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE c.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'Edc_k', c.FactorSet, 'N/A'
, c.EdcNoMult*p.MultiFactor/1000
, c.EdcNoMult*p.MultiFactor/1000
, NULL, NULL -- No Targets
FROM FactorCalc c INNER JOIN FactorProcessCalc p ON p.SubmissionID = c.SubmissionID AND p.ProcessID = c.ProcessID AND p.FactorSet = c.FactorSet
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE c.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'UEdc_k', c.FactorSet, 'N/A'
, c.UEdcNoMult*p.MultiFactor/1000
, c.UEdcNoMult*p.MultiFactor/1000
, NULL, NULL -- No Targets
FROM FactorCalc c INNER JOIN FactorProcessCalc p ON p.SubmissionID = c.SubmissionID AND p.ProcessID = c.ProcessID AND p.FactorSet = c.FactorSet
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE c.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'UtilOSTA', 'N/A', 'N/A'
, AVG(c.UtilOSTA)
, AVG(c.UtilOSTA)
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'UtilOSTA_Target')
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'UtilOSTA_Target')
FROM FactorCalc c INNER JOIN FactorProcessCalc p ON p.SubmissionID = c.SubmissionID AND p.ProcessID = c.ProcessID AND p.FactorSet = c.FactorSet
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE c.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')
GROUP BY c.UnitID

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, UnitID, 'TADaysDown', 'N/A', 'N/A'
, TAHrsDown/24.0
, TAHrsDown/24.0
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'TADaysDown_Target')
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'TADaysDown_Target')
FROM TAForSubmission ta INNER JOIN ProcessID_LU lu ON lu.ProcessID = ta.ProcessID
WHERE ta.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, UnitID, 'AnnTADownDays', 'N/A', 'N/A'
, TAHrsDown/24.0/TAIntYrs
, TAHrsDown/24.0/TAIntYrs
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'AnnTADownDays_Target')
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'AnnTADownDays_Target')
FROM TAForSubmission ta INNER JOIN ProcessID_LU lu ON lu.ProcessID = ta.ProcessID
WHERE ta.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, UnitID, 'TAIntDays', 'N/A', 'N/A'
, TAIntDays
, TAIntDays
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'TAIntDays_Target')
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'TAIntDays_Target')
FROM TAForSubmission ta INNER JOIN ProcessID_LU lu ON lu.ProcessID = ta.ProcessID
WHERE ta.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, UnitID, 'TAMatlPcnt', 'N/A', 'N/A'
, TAMatlPcnt
, TAMatlPcnt
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'TAMatlPcnt_Target')
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = ta.UnitID AND t.Property = 'TAMatlPcnt_Target')
FROM TAForSubmission ta INNER JOIN ProcessID_LU lu ON lu.ProcessID = ta.ProcessID
WHERE ta.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F')

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, UnitID, 'RoutMatlPcnt', 'N/A', 'N/A'
, mc.AnnRoutMatl/mc.AnnRoutCost*100
, mc.AnnRoutMatl/mc.AnnRoutCost*100
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'RoutMatlPcnt_Target')
, (SELECT Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'RoutMatlPcnt_Target')
FROM MaintCost mc INNER JOIN ProcessID_LU lu ON lu.ProcessID = mc.ProcessID
WHERE mc.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F') AND mc.AnnRoutCost > 0 AND mc.Currency = @RptCurrency

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, pd.Property, 'N/A', 'N/A'
, pd.SAValue
, CASE WHEN @IsMetric = 1 THEN RptValue WHEN lu.USUOM IS NULL THEN pd.RptValue ELSE dbo.UnitsConv(RptValue, lu.USUOM, lu.MetricUOM) END
, (SELECT CASE WHEN @IsMetric = 0 THEN t.Target WHEN lu.USUOM IS NULL THEN t.Target ELSE dbo.UnitsConv(t.Target, lu.MetricUOM, lu.USUOM) END FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = pd.Property + '_Target')
, (SELECT CASE WHEN @IsMetric = 1 THEN t.Target WHEN lu.USUOM IS NULL THEN t.Target ELSE dbo.UnitsConv(t.Target, lu.USUOM, lu.MetricUOM) END FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = pd.Property + '_Target')
FROM Config c INNER JOIN ProcessData pd ON pd.SubmissionID = c.SubmissionID AND pd.UnitID = c.UnitID
INNER JOIN Table2_LU lu ON lu.ProcessID = c.ProcessID AND lu.Property = pd.Property
WHERE c.SubmissionID = @SubmissionID

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT mc.SubmissionID, mc.UnitID, 'TACostCap', FactorSet = 'N/A', mc.Currency
, mc.AnnTACost*lu.CostCapMult/c.Cap
, mc.AnnTACost*lu.CostCapMult/(c.Cap/lu.MetricToUSMult)
, (SELECT t.Target*CASE WHEN @IsMetric = 0 THEN 1 ELSE lu.MetricToUSMult END*dbo.ExchangeRate(t.CurrencyCode,mc.Currency,@PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'TACost_Target')
, (SELECT t.Target/CASE WHEN @IsMetric = 1 THEN 1 ELSE lu.MetricToUSMult END*dbo.ExchangeRate(t.CurrencyCode,mc.Currency,@PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'TACost_Target')
FROM MaintCost mc INNER JOIN Config c ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE mc.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F') AND c.Cap > 0

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT mc.SubmissionID, mc.UnitID, 'RoutCostCap', FactorSet = 'N/A', mc.Currency
, mc.AnnRoutCost*lu.CostCapMult/c.Cap
, mc.AnnRoutCost*lu.CostCapMult/(c.Cap/lu.MetricToUSMult)
, (SELECT t.Target*CASE WHEN @IsMetric = 0 THEN 1 ELSE lu.MetricToUSMult END*dbo.ExchangeRate(t.CurrencyCode,mc.Currency,@PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'RoutCost_Target')
, (SELECT t.Target/CASE WHEN @IsMetric = 1 THEN 1 ELSE lu.MetricToUSMult END*dbo.ExchangeRate(t.CurrencyCode,mc.Currency,@PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'RoutCost_Target')
FROM MaintCost mc INNER JOIN Config c ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE mc.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F') AND c.Cap > 0

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT mc.SubmissionID, mc.UnitID, 'MaintCostCap', FactorSet = 'N/A', mc.Currency
, mc.AnnMaintCost*lu.CostCapMult/c.Cap
, mc.AnnMaintCost*lu.CostCapMult/(c.Cap/lu.MetricToUSMult)
, (SELECT t.Target*CASE WHEN @IsMetric = 0 THEN 1 ELSE lu.MetricToUSMult END*dbo.ExchangeRate(t.CurrencyCode,mc.Currency,@PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'MaintCostCap_Target')
, (SELECT t.Target/CASE WHEN @IsMetric = 1 THEN 1 ELSE lu.MetricToUSMult END*dbo.ExchangeRate(t.CurrencyCode,mc.Currency,@PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = mc.UnitID AND t.Property = 'MaintCostCap_Target')
FROM MaintCost mc INNER JOIN Config c ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID
INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID
WHERE mc.SubmissionID = @SubmissionID AND lu.ProcessGroup IN ('P','F') AND c.Cap > 0

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'OpEx', 'N/A', ctc.Currency
, pd.RptValue*1000/(c.Cap*@NumDays)*dbo.ExchangeRate(@RptCurrency, ctc.Currency, @PeriodStart)
, pd.RptValue*1000/(c.Cap*@NumDays)*dbo.ExchangeRate(@RptCurrency, ctc.Currency, @PeriodStart)
, (SELECT t.Target*dbo.ExchangeRate(t.CurrencyCode, ctc.Currency, @PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'OpEx_Target')
, (SELECT t.Target*dbo.ExchangeRate(t.CurrencyCode, ctc.Currency, @PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'OpEx_Target')
FROM Config c INNER JOIN ProcessData pd ON pd.SubmissionID = c.SubmissionID AND pd.UnitID = c.UnitID
INNER JOIN Table2_LU lu ON lu.ProcessID = c.ProcessID AND lu.Property = pd.Property
INNER JOIN SubmissionsAll s ON s.SubmissionID = c.SubmissionID
INNER JOIN CurrenciesToCalc ctc ON ctc.RefineryID = s.RefineryID
WHERE c.SubmissionID = @SubmissionID AND lu.Property = 'CashOpEx' AND c.Cap > 0

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'GM', 'N/A', ctc.Currency
, pd.RptValue*1000/(c.Cap*@NumDays)*dbo.ExchangeRate(@RptCurrency, ctc.Currency, @PeriodStart)
, pd.RptValue*1000/(c.Cap*@NumDays)*dbo.ExchangeRate(@RptCurrency, ctc.Currency, @PeriodStart)
, (SELECT t.Target*dbo.ExchangeRate(t.CurrencyCode, ctc.Currency, @PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'GM_Target')
, (SELECT t.Target*dbo.ExchangeRate(t.CurrencyCode, ctc.Currency, @PeriodStart) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'GM_Target')
FROM Config c INNER JOIN ProcessData pd ON pd.SubmissionID = c.SubmissionID AND pd.UnitID = c.UnitID
INNER JOIN Table2_LU lu ON lu.ProcessID = c.ProcessID AND lu.Property = pd.Property
INNER JOIN SubmissionsAll s ON s.SubmissionID = c.SubmissionID
INNER JOIN CurrenciesToCalc ctc ON ctc.RefineryID = s.RefineryID
WHERE c.SubmissionID = @SubmissionID AND lu.Property = 'GrossMargin' AND c.Cap > 0

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT o.SubmissionID, o.UnitID, 'NCM', o.FactorSet, o.Currency, gm.USValue - o.USValue, gm.MetValue - o.MetValue, gm.USTarget - o.USTarget, gm.MetTarget - o.MetTarget
FROM CustomUnitData o INNER JOIN CustomUnitData gm ON gm.SubmissionID = o.SubmissionID AND gm.UnitID = o.UnitID AND gm.FactorSet = o.FactorSet AND gm.Currency = o.Currency
WHERE o.SubmissionID = @SubmissionID AND o.Property = 'OpEx' AND gm.Property = 'GM'

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'PersIndex', c.FactorSet, 'N/A'
, pd.RptValue/(c.EdcNoMult*fpc.MultiFactor/100*@FractionOfYear)
, pd.RptValue/(c.EdcNoMult*fpc.MultiFactor/100*@FractionOfYear)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'PersIndex_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'PersIndex_Target')
FROM FactorCalc c INNER JOIN FactorProcessCalc fpc ON fpc.SubmissionID = c.SubmissionID AND fpc.ProcessID = c.ProcessID AND fpc.FactorSet = c.FactorSet
INNER JOIN ProcessData pd ON pd.SubmissionID = c.SubmissionID AND pd.UnitID = c.UnitID
WHERE c.SubmissionID = @SubmissionID AND pd.Property = 'TotWHr' AND c.EdcNoMult > 0

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'EII', c.FactorSet, 'N/A'
, dbo.UnitsConv(pd.RptValue, CASE WHEN @IsMetric = 0 THEN 'MBTU' ELSE 'GJ' END, 'MBTU')/(c.StdEnergy*@NumDays)*100
, dbo.UnitsConv(pd.RptValue, CASE WHEN @IsMetric = 0 THEN 'MBTU' ELSE 'GJ' END, 'MBTU')/(c.StdEnergy*@NumDays)*100
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'EII_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'EII_Target')
FROM FactorCalc c INNER JOIN ProcessData pd ON pd.SubmissionID = c.SubmissionID AND pd.UnitID = c.UnitID
WHERE c.SubmissionID = @SubmissionID AND pd.Property = 'EnergyCons' AND c.StdEnergy > 0

-- Reported on table 2 so calculation not necessary
/*
INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, pd.UnitID, 'PostPerShift', 'N/A', 'N/A'
, pd.RptValue/(2080*@FractionOfYear)
, pd.RptValue/(2080*@FractionOfYear)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'PostPerShift_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'PostPerShift_Target')
FROM ProcessData pd 
WHERE pd.SubmissionID = @SubmissionID AND pd.Property = 'TotWHr'
*/

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, pd.UnitID, 'EnergyConsPerWt', 'N/A', 'N/A'
, dbo.UnitsConv(pd.RptValue, CASE WHEN @IsMetric = 1 THEN 'GJ' ELSE 'MBTU' END, 'BTU')/(dbo.UnitsConv(dbo.UnitsConv(c.UtilCap, 'B', 'M3')*dbo.UnitsConv(fd.SAValue, 'API', 'KGM3'), 'KG', 'LB')*@NumDays)
, dbo.UnitsConv(pd.RptValue, CASE WHEN @IsMetric = 1 THEN 'GJ' ELSE 'MBTU' END, 'KCAL')/(dbo.UnitsConv(c.UtilCap, 'B', 'M3')*dbo.UnitsConv(fd.SAValue, 'API', 'KGM3')*@NumDays)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'EnergyConsPerWt_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = pd.UnitID AND t.Property = 'EnergyConsPerWt_Target')
FROM ProcessData pd INNER JOIN Config c ON c.SubmissionID = pd.SubmissionID AND c.UnitID = pd.UnitID
INNER JOIN ProcessData fd ON fd.SubmissionID = pd.SubmissionID AND fd.UnitID = pd.UnitID AND fd.Property = 'FeedGravity'
WHERE pd.SubmissionID = @SubmissionID AND pd.Property = 'EnergyCons'

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'ProdN2A', 'N/A', 'N/A'
, ISNULL(fa.RptValue, 0)+ISNULL(fn.RptValue,0)
, ISNULL(fa.RptValue, 0)+ISNULL(fn.RptValue,0)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'ProdN2A_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'ProdN2A_Target')
FROM Config c 
LEFT JOIN ProcessData fa ON c.SubmissionID = fa.SubmissionID AND c.UnitID = fa.UnitID  AND fa.Property = 'FeedArom'
LEFT JOIN ProcessData fn ON fn.SubmissionID = c.SubmissionID AND fn.UnitID = c.UnitID AND fn.Property = 'FeedNap'
WHERE c.SubmissionID = @SubmissionID AND c.ProcessID = 'REF'

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'ProdLt', 'N/A', 'N/A'
, ISNULL(fa.RptValue, 0)+ISNULL(fn.RptValue,0)
, ISNULL(fa.RptValue, 0)+ISNULL(fn.RptValue,0)
, (SELECT SUM(t.Target) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property IN ('ProdC3_Target', 'ProdLtGasNap_Target'))
, (SELECT SUM(t.Target) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property IN ('ProdC3_Target', 'ProdLtGasNap_Target'))
FROM Config c 
LEFT JOIN ProcessData fa ON c.SubmissionID = fa.SubmissionID AND c.UnitID = fa.UnitID  AND fa.Property = 'ProdC3'
LEFT JOIN ProcessData fn ON fn.SubmissionID = c.SubmissionID AND fn.UnitID = c.UnitID AND fn.Property = 'ProdLtGasNap'
WHERE c.SubmissionID = @SubmissionID AND c.ProcessID = 'FCC'

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'ProdKeroDiesel', 'N/A', 'N/A'
, ISNULL(fa.RptValue, 0)+ISNULL(fn.RptValue,0)
, ISNULL(fa.RptValue, 0)+ISNULL(fn.RptValue,0)
, (SELECT SUM(t.Target) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property IN ('ProdKJet_Target', 'ProdDiesel_Target'))
, (SELECT SUM(t.Target) FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property IN ('ProdKJet_Target', 'ProdDiesel_Target'))
FROM Config c 
LEFT JOIN ProcessData fa ON c.SubmissionID = fa.SubmissionID AND c.UnitID = fa.UnitID  AND fa.Property = 'ProdKJet'
LEFT JOIN ProcessData fn ON fn.SubmissionID = c.SubmissionID AND fn.UnitID = c.UnitID AND fn.Property = 'ProdDiesel'
WHERE c.SubmissionID = @SubmissionID AND c.ProcessID = 'HYC'

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'MechAvail', 'N/A', 'N/A'
, mc.MechAvail_Act, mc.MechAvail_Act
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'MechAvail_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'MechAvail_Target')
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID AND lu.ProcessGroup IN ('P','F')
INNER JOIN MaintCalc mc ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID 
WHERE c.SubmissionID = @SubmissionID 

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'MechAvailPlan', 'N/A', 'N/A'
, 100 - ISNULL(mc.MechUnavailPlan, 0)
, 100 - ISNULL(mc.MechUnavailPlan, 0)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'MechAvailPlan_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'MechAvailPlan_Target')
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID AND lu.ProcessGroup IN ('P','F')
INNER JOIN MaintCalc mc ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID 
WHERE c.SubmissionID = @SubmissionID 

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'MechAvailUnp', 'N/A', 'N/A'
, 100 - ISNULL(mc.MechUnavailUnp, 0)
, 100 - ISNULL(mc.MechUnavailUnp, 0)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'MechAvailUnp_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'MechAvailUnp_Target')
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID AND lu.ProcessGroup IN ('P','F')
INNER JOIN MaintCalc mc ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID 
WHERE c.SubmissionID = @SubmissionID 

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'OpAvailPlan', 'N/A', 'N/A'
, 100 - ISNULL(mc.MechUnavailPlan, 0) - ISNULL(mc.RegUnavailPlan, 0)
, 100 - ISNULL(mc.MechUnavailPlan, 0) - ISNULL(mc.RegUnavailPlan, 0)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'OpAvailPlan_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'OpAvailPlan_Target')
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID AND lu.ProcessGroup IN ('P','F')
INNER JOIN MaintCalc mc ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID 
WHERE c.SubmissionID = @SubmissionID 

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'OpAvailUnp', 'N/A', 'N/A'
, 100 - ISNULL(mc.MechUnavailUnp, 0) - ISNULL(mc.RegUnavailUnp, 0)
, 100 - ISNULL(mc.MechUnavailUnp, 0) - ISNULL(mc.RegUnavailUnp, 0)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'OpAvailUnp_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'OpAvailUnp_Target')
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID AND lu.ProcessGroup IN ('P','F')
INNER JOIN MaintCalc mc ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID 
WHERE c.SubmissionID = @SubmissionID 

INSERT CustomUnitData(SubmissionID, UnitID, Property, FactorSet, Currency, USValue, MetValue, USTarget, MetTarget)
SELECT @SubmissionID, c.UnitID, 'RegUnavail', 'N/A', 'N/A'
, ISNULL(mc.RegUnavail, 0), ISNULL(mc.RegUnavail, 0)
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'RegUnavail_Target')
, (SELECT t.Target FROM UnitTargetsNew t WHERE t.SubmissionID = @SubmissionID AND t.UnitID = c.UnitID AND t.Property = 'RegUnavail_Target')
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID AND lu.ProcessGroup IN ('P','F')
INNER JOIN MaintCalc mc ON c.SubmissionID = mc.SubmissionID AND c.UnitID = mc.UnitID 
WHERE c.SubmissionID = @SubmissionID
