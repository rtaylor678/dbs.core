﻿CREATE       PROC [dbo].[spTACalcs](@RefineryID varchar(6), @DataSet varchar(15), @UnitID UnitID, @TAID int)
AS
SET NOCOUNT ON

IF @TAID = 0
	RETURN

DECLARE @TADate smalldatetime, @PrevTADate smalldatetime, @TAHrsDown real, 
	@TACostLocal real, @TAMatlLocal real, @TALaborCostLocal real, @TAExpLocal real, @TACptlLocal real, @TAOvhdLocal real,
	@TAOCCSTH real, @TAOCCOVT real, @TAMPSSTH real, @TampSovtPcnt real, @TAContOCC real, @TAContMPS real,
	@TAExceptions tinyint, @TACurrency CurrencyCode
SELECT 	@TADate = TADate, @PrevTADate = PrevTADate, @TAHrsDown = TAHrsDown, 
	@TACostLocal = TACostLocal, @TAMatlLocal = TAMatlLocal, @TACurrency = TACurrency,
	@TALaborCostLocal = TALaborCostLocal, @TAExpLocal = TAExpLocal, @TACptlLocal = TACptlLocal, @TAOvhdLocal = TAOvhdLocal,
	@TAOCCSTH = TAOCCSTH, @TAOCCOVT = TAOCCOVT, @TAMPSSTH = TAMPSSTH, @TampSovtPcnt = TampSovtPcnt, 
	@TAContOCC = TAContOCC, @TAContMPS = TAContMPS, @TAExceptions = TAExceptions
FROM MaintTA
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID
DECLARE @TACostUS real, @TAMatlUS real, @TALaborCostUS real, @TAExpUS real, @TACptlUS real, @TAOvhdUS real, @TAConvRate real
IF @TACurrency IS NULL
	SELECT @TACurrency = RptCurrency FROM Submissions
	WHERE RefineryID = @RefineryID AND DataSet = @DataSet
SELECT @TAConvRate = dbo.ExchangeRate(@TACurrency, 'USD', @TADate)
SELECT	@TACostUS = @TACostLocal * @TAConvRate
	, @TAMatlUS = @TAMatlLocal * @TAConvRate
	, @TALaborCostUS = @TALaborCostLocal * @TAConvRate
	, @TAExpUS = @TAExpLocal * @TAConvRate
	, @TACptlUS = @TACptlLocal * @TAConvRate
	, @TAOvhdUS = @TAOvhdLocal * @TAConvRate
DECLARE @TAIntDays real, @TAIntYrs real, @RestartDate smalldatetime,
	@AnnTACostLocal real, @AnnTAMatlLocal real, @AnnTACost real, @AnnTAMatl real,
	@AnnTALaborCostLocal real, @AnnTALaborCost real,
	@AnnTAExpLocal real, @AnnTAExp real,
	@AnnTACptlLocal real, @AnnTACptl real,
	@AnnTAOvhdLocal real, @AnnTAOvhd real,
	@TAMPSOVT real, @AnnTAOCCSTH real, @AnnTAOCCOVT	real, @AnnTAMPSSTH real, @AnnTAMPSOVT real,
	@AnnTAContOCCWHr real, @AnnTAContMPSWHr real, @MechUnavailTA real, @TAMatlPcnt real
SELECT 	@TAIntDays = DATEDIFF(d, @PrevTADate, @TADate),
	@TAIntYrs = DATEDIFF(d, @PrevTADate, @TADate)/365.25,
	@RestartDate = DATEADD(hh, @TAHrsDown, @TADate),
	@TAMPSOVT = @TAMPSSTH * ISNULL(@TampSovtPcnt, 0)/100,
	@TAMatlPcnt = CASE WHEN @TACostLocal > 0 THEN @TAMatlLocal / @TACostLocal * 100 END
IF @TAIntYrs > 0
	SELECT 	@AnnTACostLocal = @TACostLocal/@TAIntYrs, 
		@AnnTAMatlLocal = @TAMatlLocal/@TAIntYrs, 
		@AnnTALaborCostLocal = @TALaborCostLocal/@TAIntYrs, 
		@AnnTAExpLocal = @TAExpLocal/@TAIntYrs, 
		@AnnTACptlLocal = @TACptlLocal/@TAIntYrs, 
		@AnnTAOvhdLocal = @TAOvhdLocal/@TAIntYrs, 
		@AnnTACost = @TACostUS/@TAIntYrs, @AnnTAMatl = @TAMatlUS/@TAIntYrs, 
		@AnnTALaborCost = @TALaborCostLocal/@TAIntYrs, 
		@AnnTAExp = @TAExpUS/@TAIntYrs, 
		@AnnTACptl = @TACptlUS/@TAIntYrs, 
		@AnnTAOvhd = @TAOvhdUS/@TAIntYrs, 
		@AnnTAOCCSTH = ISNULL(@TAOCCSTH, 0) / @TAIntYrs,
		@AnnTAOCCOVT = ISNULL(@TAOCCOVT, 0) / @TAIntYrs,
		@AnnTAMPSSTH = ISNULL(@TAMPSSTH, 0) / @TAIntYrs,
		@AnnTAMPSOVT = ISNULL(@TAMPSOVT, 0) / @TAIntYrs,
		@AnnTAContOCCWHr = ISNULL(@TAContOCC, 0) / @TAIntYrs,
		@AnnTAContMPSWHr = ISNULL(@TAContMPS, 0) / @TAIntYrs,
		@MechUnavailTA = @TAHrsDown/(@TAIntDays*24)*100
UPDATE MaintTA
SET 	TACostUS = @TACostUS, TAMatlUS = @TAMatlUS, TALaborCostUS = @TALaborCostUS, TAExpUS = @TAExpUS, TACptlUS = @TACptlUS, TAOvhdUS = @TAOvhdUS,
	TAIntDays = @TAIntDays, TAIntYrs = @TAIntYrs, RestartDate = @RestartDate,
	AnnTACostLocal = @AnnTACostLocal, AnnTAMatlLocal = @AnnTAMatlLocal, AnnTACost = @AnnTACost, AnnTAMatl = @AnnTAMatl, 
	AnnTALaborCostLocal = @AnnTALaborCostLocal, AnnTAExpLocal = @AnnTAExpLocal, AnnTACptlLocal = @AnnTACptlLocal, AnnTAOvhdLocal = @AnnTAOvhdLocal,
	AnnTALaborCost = @AnnTALaborCost, AnnTAExp = @AnnTAExp, AnnTACptl = @AnnTACptl, AnnTAOvhd = @AnnTAOvhd,
	TAMPSOVT = @TAMPSOVT, AnnTAOCCSTH = @AnnTAOCCSTH, AnnTAOCCOVT = @AnnTAOCCOVT, 
	AnnTAMPSSTH = @AnnTAMPSSTH, AnnTAMPSOVT = @AnnTAMPSOVT, 
	AnnTAContOCCWHr = @AnnTAContOCCWHr, AnnTAContMPSWHr = @AnnTAContMPSWHr,
	MechUnavailTA = @MechUnavailTA, TAMatlPcnt = @TAMatlPcnt, TACurrency = @TACurrency
WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND UnitID = @UnitID AND TAID = @TAID

EXEC spTACosts @RefineryID, @DataSet, @UnitID, @TAID

