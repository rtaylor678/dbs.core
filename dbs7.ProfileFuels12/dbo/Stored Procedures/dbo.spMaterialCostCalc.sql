﻿CREATE  PROC [dbo].[spMaterialCostCalc](@SubmissionID int)
AS

DELETE FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID
DELETE FROM DataChecks WHERE SubmissionID = @SubmissionID AND DataCheckId = 'NoStudy$'

-- Load client-supplied prices
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS)
SELECT SubmissionID, 'CLIENT', SortKey, MaterialID, Category, PriceLocal, PriceUS
FROM Yield WHERE SubmissionID = @SubmissionID

-- Load study prices
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS)
SELECT y.SubmissionID, p.StudyYear, y.SortKey, y.MaterialID, y.Category, ISNULL(p.PricePerBbl*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM SubmissionsAll WHERE SubmissionID = @SubmissionID), 0), p.PricePerBbl
FROM SubmissionsAll s INNER JOIN Yield y ON y.SubmissionID = s.SubmissionID
INNER JOIN MaterialPrices p ON p.RefineryID = s.RefineryID AND p.MaterialID = y.MaterialID
WHERE s.SubmissionID = @SubmissionID AND y.MaterialID NOT IN ('CRD','RFC','RFS','RFCG','M106')

-- Load Crude
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS)
SELECT t.SubmissionID, p.Scenario, 101, 'CRD', 'RMI', ISNULL(p.AvgCost*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM SubmissionsAll WHERE SubmissionID = @SubmissionID), 0), ISNULL(p.AvgCost, 0)
FROM CrudeTot t INNER JOIN CrudeTotPrice p ON p.SubmissionID = t.SubmissionID
WHERE t.SubmissionID = @SubmissionID AND p.Scenario <> 'CLIENT'

-- Load Refinery-produced fuels
DECLARE @Scenarios TABLE
	(Scenario varchar(8))
INSERT @Scenarios
SELECT DISTINCT Scenario FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID AND Scenario <> 'CLIENT'

IF EXISTS (SELECT * FROM @Scenarios)
BEGIN
	INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS)
	SELECT SubmissionID, s.Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS
	FROM MaterialCostCalc m, @Scenarios s
	WHERE m.SubmissionID = @SubmissionID AND m.Scenario = 'CLIENT' AND m.MaterialID IN ('RFC','RFS','RFCG','M106')
END

-- Load materials that do not have study pricing
IF EXISTS (SELECT * FROM @Scenarios)
BEGIN
	SELECT c.SubmissionID, s.Scenario, c.SortKey, c.MaterialID, c.Category, c.PriceLocal, c.PriceUS
	INTO #Missing
	FROM MaterialCostCalc c, @Scenarios s
	WHERE c.SubmissionID = @SubmissionID AND c.Scenario = 'CLIENT' 
	AND NOT EXISTS (SELECT * FROM MaterialTotCost x WHERE x.SubmissionID = c.SubmissionID AND x.Scenario <> s.Scenario)

	INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS)
	SELECT m.SubmissionID, s.Scenario, m.SortKey, m.MaterialID, m.Category, m.PriceLocal, m.PriceUS
	FROM #Missing m, @Scenarios s

	INSERT DataChecks (SubmissionID, DataCheckId, ItemSortKey, ItemDesc, Value1, Value2)
	SELECT y.SubmissionID, 'NoStudy$', y.SortKey, y.MaterialName, y.MaterialID, m.Scenario
	FROM #Missing m INNER JOIN Yield y ON y.SubmissionID = m.SubmissionID AND y.SortKey = m.SortKey
	WHERE y.MaterialID <> 'GAIN' AND y.Bbl <> 0

	IF @@ROWCOUNT > 0
	BEGIN
		SELECT * FROM DataChecks WHERE SubmissionID = @SubmissionID AND DataCheckId = 'NoStudy$'
		DELETE FROM DataChecks WHERE SubmissionID = @SubmissionID AND DataCheckId = 'NoStudy$'
	END

	DROP TABLE #Missing
END
