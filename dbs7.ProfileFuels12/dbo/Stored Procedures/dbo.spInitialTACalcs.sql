﻿CREATE PROC [dbo].[spInitialTACalcs](@RefineryID varchar(6), @DataSet varchar(15))
AS

DELETE FROM MaintTACost
WHERE NOT EXISTS (SELECT * FROM MaintTA ta
	WHERE ta.RefineryID = MaintTACost.RefineryID AND ta.DataSet = MaintTACost.DataSet AND ta.UnitID = MaintTACost.UnitID AND ta.UnitID = MaintTACost.UnitID)
AND RefineryID = @RefineryID AND DataSet = @DataSet

DECLARE @UnitID int, @TAID int
DECLARE cTA CURSOR
FOR
	SELECT UnitID, TAID
	FROM MaintTA
	WHERE RefineryID = @RefineryID
	AND DataSet = @DataSet
OPEN cTA
FETCH NEXT FROM cTA INTO @UnitID, @TAID
WHILE @@FETCH_STATUS = 0
BEGIN
	EXEC spTACalcs @RefineryID, @DataSet, @UnitID, @TAID
	FETCH NEXT FROM cTA INTO @UnitID, @TAID
END
CLOSE cTA
DEALLOCATE cTA

EXEC spUpdateNextTADate @RefineryID, @DataSet
