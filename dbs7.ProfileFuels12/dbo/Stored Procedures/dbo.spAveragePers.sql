﻿CREATE PROC [dbo].[spAveragePers](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@OCCAbsPcnt real OUTPUT, @MPSAbsPcnt real OUTPUT, @OccoVtPcnt real OUTPUT, @MPSOVTPcnt real OUTPUT,
	@ProcoCcmpsRatio real OUTPUT, @MaintOCCMPSRatio real OUTPUT)
AS

SELECT @MPSAbsPcnt = CASE WHEN SUM(p.STH) > 0 THEN SUM(a.MPSAbs)/SUM(p.STH)*100 END,
@MPSOVTPcnt = CASE WHEN SUM(p.STH) > 0 THEN SUM(p.OVTHours)/SUM(p.STH)*100 END
FROM Submissions s INNER JOIN AbsenceTot a ON s.SubmissionID = a.SubmissionID   
INNER JOIN PersST p ON p.SubmissionID = s.SubmissionID AND p.SectionID = 'TM'
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd

SELECT @OCCAbsPcnt = CASE WHEN SUM(p.STH) > 0 THEN SUM(a.OCCAbs)/SUM(p.STH)*100 END,
@OccoVtPcnt = CASE WHEN SUM(p.STH) > 0 THEN SUM(p.OVTHours)/SUM(p.STH)*100 END
FROM Submissions s INNER JOIN AbsenceTot a ON s.SubmissionID = a.SubmissionID   
INNER JOIN PersST p ON p.SubmissionID = s.SubmissionID AND p.SectionID = 'TO'
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd

SELECT @MaintOCCMPSRatio = CASE WHEN SUM(CASE WHEN p.SectionID = 'MM' THEN NumPers END) > 0 THEN SUM(CASE WHEN p.SectionID = 'OM' THEN NumPers END)/SUM(CASE WHEN p.SectionID = 'MM' THEN NumPers END) END,
@ProcoCcmpsRatio = CASE WHEN SUM(CASE WHEN p.SectionID = 'MO' THEN NumPers END) > 0 THEN SUM(CASE WHEN p.SectionID = 'OO' THEN NumPers END)/SUM(CASE WHEN p.SectionID = 'MO' THEN NumPers END) END
FROM Submissions s INNER JOIN PersST p ON p.SubmissionID = s.SubmissionID AND p.SectionID IN ('OO', 'OM', 'MO', 'MM')
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet 
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd

