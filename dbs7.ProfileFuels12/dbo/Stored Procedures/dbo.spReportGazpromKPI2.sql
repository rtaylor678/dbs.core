﻿




CREATE   PROC [dbo].[spReportGazpromKPI2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
SET @FactorSet = '2012'

DECLARE	@RefUtilPcnt real, @RefUtilPcnt_QTR real, @RefUtilPcnt_Avg real, @RefUtilPcnt_Ytd real, 
	@ProcessUtilPcnt real, @ProcessUtilPcnt_QTR real, @ProcessUtilPcnt_Avg real, @ProcessUtilPcnt_Ytd real, 
	@TotProcessEdc real, @TotProcessEdc_QTR real, @TotProcessEdc_Avg real, @TotProcessEdc_Ytd real, 
	@TotProcessUEdc real, @TotProcessUEdc_QTR real, @TotProcessUedc_Avg real, @TotProcessUEdc_Ytd real, 
	
	@OpAvail real, @OpAvail_QTR real, @OpAvail_Avg real, @OpAvail_Ytd real, 
	@MechUnavailTA real, @MechUnavailTA_QTR real, @MechUnavailTA_Avg real, @MechUnavailTA_Ytd real, 
	@NonTAUnavail real, @NonTAUnavail_QTR real, @NonTAUnavail_Avg real, @NonTAUnavail_Ytd real, 

	@EII real, @EII_QTR real, @EII_Avg real, @EII_Ytd real, 
	@EnergyUseDay real, @EnergyUseDay_QTR real, @EnergyUseDay_Avg real, @EnergyUseDay_Ytd real, 
	@TotStdEnergy real, @TotStdEnergy_QTR real, @TotStdEnergy_Avg real, @TotStdEnergy_Ytd real, 

	@VEI real, @VEI_QTR real, @VEI_Avg real, @VEI_Ytd real, 
	@ReportLossGain real, @ReportLossGain_QTR real, @ReportLossGain_Avg real, @ReportLossGain_Ytd real, 
	@EstGain real, @EstGain_QTR real, @EstGain_Avg real, @EstGain_Ytd real, 

	@Gain real, @Gain_QTR real, @Gain_Avg real, @Gain_Ytd real, 
	@RawMatl real, @RawMatl_QTR real, @RawMatl_Avg real, @RawMatl_Ytd real, 
	@ProdYield real, @ProdYield_QTR real, @ProdYield_Avg real, @ProdYield_Ytd real, 

	@PersIndex real, @PersIndex_QTR real, @PersIndex_Avg real, @PersIndex_Ytd real, 
	@AnnTAWhr real, @AnnTAWhr_QTR real, @AnnTAWhr_Avg real, @AnnTAWhr_Ytd real,
	@NonTAWHr real, @NonTAWHr_QTR real, @NonTAWHr_Avg real, @NonTAWHr_Ytd real,
	@Edc real, @Edc_QTR real, @Edc_Avg real, @Edc_Ytd real, 
	@UEdc real, @UEdc_QTR real, @Uedc_Avg real, @UEdc_Ytd real, 

	@TotMaintForceWHrEdc real, @TotMaintForceWHrEdc_QTR real, @TotMaintForceWHrEdc_Avg real, @TotMaintForceWHrEdc_Ytd real, 
	@MaintTAWHr real, @MaintTAWHr_QTR real, @MaintTAWHr_Avg real, @MaintTAWHr_Ytd real, 
	@MaintNonTAWHr real, @MaintNonTAWHr_QTR real, @MaintNonTAWHr_Avg real, @MaintNonTAWHr_Ytd real, 

	@MaintIndex real, @MaintIndex_QTR real, @MaintIndex_Avg real, @MaintIndex_Ytd real, 
	@RoutIndex real, @RoutIndex_QTR real, @RoutIndex_Avg real, @RoutIndex_Ytd real,
	@AnnTACost real, @AnnTACost_QTR real, @AnnTACost_Avg real, @AnnTACost_Ytd real, 
	@RoutCost real, @RoutCost_QTR real, @RoutCost_Avg real, @RoutCost_Ytd real, 

	@NEOpExEdc real, @NEOpExEdc_QTR real, @NEOpExEdc_Avg real, @NEOpExEdc_Ytd real, 
	@NEOpEx real, @NEOpEx_QTR real, @NEOpEx_Avg real, @NEOpEx_Ytd real, 

	@OpExUEdc real, @OpExUEdc_QTR real, @OpExUedc_Avg real, @OpExUEdc_Ytd real, 
	@EnergyCost real, @EnergyCost_QTR real, @EnergyCost_Avg real, @EnergyCost_Ytd real, 
	@TAAdj real, @TAAdj_QTR real, @TAAdj_Avg real, @TAAdj_Ytd real,
	@TotCashOpEx real, @TotCashOpEx_QTR real, @TotCashOpEx_Avg real , @TotCashOpEx_Ytd real
DECLARE @spResult smallint
EXEC @spResult = [dbo].[spReportGazpromKPICalc2] @RefineryID, @PeriodYear, @PeriodMonth, @DataSet, 
	@FactorSet, @Scenario, @Currency, @UOM,
	@EII = @EII OUTPUT, @EII_QTR = @EII_QTR OUTPUT, @EII_Avg = @EII_Avg OUTPUT, @EII_Ytd = @EII_Ytd OUTPUT, 
	@EnergyUseDay = @EnergyUseDay OUTPUT, @EnergyUseDay_QTR = @EnergyUseDay_QTR OUTPUT, @EnergyUseDay_Avg = @EnergyUseDay_Avg OUTPUT, @EnergyUseDay_Ytd = @EnergyUseDay_Ytd OUTPUT, 
	@TotStdEnergy = @TotStdEnergy OUTPUT, @TotStdEnergy_QTR = @TotStdEnergy_QTR OUTPUT, @TotStdEnergy_Avg = @TotStdEnergy_Avg OUTPUT, @TotStdEnergy_Ytd = @TotStdEnergy_Ytd OUTPUT, 
	@RefUtilPcnt = @RefUtilPcnt OUTPUT, @RefUtilPcnt_QTR = @RefUtilPcnt_QTR OUTPUT, @RefUtilPcnt_Avg = @RefUtilPcnt_Avg OUTPUT, @RefUtilPcnt_Ytd = @RefUtilPcnt_Ytd OUTPUT, 
	@Edc = @Edc OUTPUT, @Edc_QTR = @Edc_QTR OUTPUT, @Edc_Avg = @Edc_Avg OUTPUT, @Edc_Ytd = @Edc_Ytd OUTPUT, 
	@UEdc = @UEdc OUTPUT, @UEdc_QTR = @UEdc_QTR OUTPUT, @Uedc_Avg = @Uedc_Avg OUTPUT, @UEdc_Ytd = @UEdc_Ytd OUTPUT, 
	@VEI = @VEI OUTPUT, @VEI_QTR = @VEI_QTR OUTPUT, @VEI_Avg = @VEI_Avg OUTPUT, @VEI_Ytd = @VEI_Ytd OUTPUT, 
	@ReportLossGain = @ReportLossGain OUTPUT, @ReportLossGain_QTR = @ReportLossGain_QTR OUTPUT, @ReportLossGain_Avg = @ReportLossGain_Avg OUTPUT, @ReportLossGain_Ytd = @ReportLossGain_Ytd OUTPUT, 
	@EstGain = @EstGain OUTPUT, @EstGain_QTR = @EstGain_QTR OUTPUT, @EstGain_Avg = @EstGain_Avg OUTPUT, @EstGain_Ytd = @EstGain_Ytd OUTPUT, 
	@OpAvail = @OpAvail OUTPUT, @OpAvail_QTR = @OpAvail_QTR OUTPUT, @OpAvail_Avg = @OpAvail_Avg OUTPUT, @OpAvail_Ytd = @OpAvail_Ytd OUTPUT, 
	@MechUnavailTA = @MechUnavailTA OUTPUT, @MechUnavailTA_QTR = @MechUnavailTA_QTR OUTPUT, @MechUnavailTA_Avg = @MechUnavailTA_Avg OUTPUT, @MechUnavailTA_Ytd = @MechUnavailTA_Ytd OUTPUT, 
	@NonTAUnavail = @NonTAUnavail OUTPUT, @NonTAUnavail_QTR = @NonTAUnavail_QTR OUTPUT, @NonTAUnavail_Avg = @NonTAUnavail_Avg OUTPUT, @NonTAUnavail_Ytd = @NonTAUnavail_Ytd OUTPUT, 
	@RoutIndex = @RoutIndex OUTPUT, @RoutIndex_QTR = @RoutIndex_QTR OUTPUT, @RoutIndex_Avg = @RoutIndex_Avg OUTPUT, @RoutIndex_Ytd = @RoutIndex_Ytd OUTPUT,
	@RoutCost = @RoutCost OUTPUT, @RoutCost_QTR = @RoutCost_QTR OUTPUT, @RoutCost_Avg = @RoutCost_Avg OUTPUT, @RoutCost_Ytd = @RoutCost_Ytd OUTPUT, 
	@PersIndex = @PersIndex OUTPUT, @PersIndex_QTR = @PersIndex_QTR OUTPUT, @PersIndex_Avg = @PersIndex_Avg OUTPUT, @PersIndex_Ytd = @PersIndex_Ytd OUTPUT, 
	@AnnTAWhr = @AnnTAWhr OUTPUT, @AnnTAWhr_QTR = @AnnTAWhr_QTR OUTPUT, @AnnTAWhr_Avg = @AnnTAWhr_Avg OUTPUT, @AnnTAWhr_Ytd = @AnnTAWhr_Ytd OUTPUT,
	@NonTAWHr = @NonTAWHr OUTPUT, @NonTAWHr_QTR = @NonTAWHr_QTR OUTPUT, @NonTAWHr_Avg = @NonTAWHr_Avg OUTPUT, @NonTAWHr_Ytd = @NonTAWHr_Ytd OUTPUT,
	@NEOpExEdc = @NEOpExEdc OUTPUT, @NEOpExEdc_QTR = @NEOpExEdc_QTR OUTPUT, @NEOpExEdc_Avg = @NEOpExEdc_Avg OUTPUT, @NEOpExEdc_Ytd = @NEOpExEdc_Ytd OUTPUT, 
	@NEOpEx = @NEOpEx OUTPUT, @NEOpEx_QTR = @NEOpEx_QTR OUTPUT, @NEOpEx_Avg = @NEOpEx_Avg OUTPUT, @NEOpEx_Ytd = @NEOpEx_Ytd OUTPUT, 
	@OpExUEdc = @OpExUEdc OUTPUT, @OpExUEdc_QTR = @OpExUEdc_QTR OUTPUT, @OpExUedc_Avg = @OpExUedc_Avg OUTPUT, @OpExUEdc_Ytd = @OpExUEdc_Ytd OUTPUT, 
	@TAAdj = @TAAdj OUTPUT, @TAAdj_QTR = @TAAdj_QTR OUTPUT, @TAAdj_Avg = @TAAdj_Avg OUTPUT, @TAAdj_Ytd = @TAAdj_Ytd OUTPUT,
	@EnergyCost = @EnergyCost OUTPUT, @EnergyCost_QTR = @EnergyCost_QTR OUTPUT, @EnergyCost_Avg = @EnergyCost_Avg OUTPUT, @EnergyCost_Ytd = @EnergyCost_Ytd OUTPUT, 
	@TotCashOpEx = @TotCashOpEx OUTPUT, @TotCashOpEx_QTR = @TotCashOpEx_QTR OUTPUT, @TotCashOpEx_Avg = @TotCashOpEx_Avg OUTPUT, @TotCashOpEx_Ytd = @TotCashOpEx_Ytd OUTPUT

IF @spResult > 0	
	RETURN @spResult
ELSE 

SELECT 
	EII = @EII, EII_QTR = @EII_QTR, EII_Avg = @EII_Avg, EII_Ytd = @EII_Ytd, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_QTR = @EnergyUseDay_QTR, EnergyUseDay_Avg = @EnergyUseDay_Avg, EnergyUseDay_Ytd = @EnergyUseDay_Ytd, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_QTR = @TotStdEnergy_QTR, TotStdEnergy_Avg = @TotStdEnergy_Avg, TotStdEnergy_Ytd = @TotStdEnergy_Ytd, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_QTR = @RefUtilPcnt_QTR, UtilPcnt_Avg = @RefUtilPcnt_Avg, UtilPcnt_Ytd = @RefUtilPcnt_Ytd, 
	Edc = @Edc, Edc_QTR = @Edc_QTR, Edc_Avg = @Edc_Avg, Edc_Ytd = @Edc_Ytd, 
	UtilUEdc = @Edc*@RefUtilPcnt/100, UtilUEdc_QTR = @Edc_QTR*@RefUtilPcnt_QTR/100, UtilUedc_Avg = @Edc_Avg*@RefUtilPcnt_Avg/100, UtilUEdc_Ytd = @Edc_Ytd*@RefUtilPcnt_Ytd/100, 
	
--	ProcessUtilPcnt = @ProcessUtilPcnt, ProcessUtilPcnt_QTR = @ProcessUtilPcnt_QTR, ProcessUtilPcnt_Avg = @ProcessUtilPcnt_Avg,
--	TotProcessEdc = @TotProcessEdc, TotProcessEdc_QTR = @TotProcessEdc_QTR, TotProcessEdc_Avg = @TotProcessEdc_Avg, 
--	TotProcessUEdc = @TotProcessUEdc, TotProcessUEdc_QTR = @TotProcessUEdc_QTR, TotProcessUedc_Avg = @TotProcessUedc_Avg, 
	
	VEI = @VEI, VEI_QTR = @VEI_QTR, VEI_Avg = @VEI_Avg, VEI_Ytd = @VEI_Ytd, 
	ReportLossGain = @ReportLossGain, ReportLossGain_QTR = @ReportLossGain_QTR, ReportLossGain_Avg = @ReportLossGain_Avg, ReportLossGain_Ytd = @ReportLossGain_Ytd, 
	EstGain = @EstGain, EstGain_QTR = @EstGain_QTR, EstGain_Avg = @EstGain_Avg, EstGain_Ytd = @EstGain_Ytd, 

	OpAvail = @OpAvail, OpAvail_QTR = @OpAvail_QTR, OpAvail_Avg = @OpAvail_Avg, OpAvail_Ytd = @OpAvail_Ytd, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_QTR = @MechUnavailTA_QTR, MechUnavailTA_Avg = @MechUnavailTA_Avg, MechUnavailTA_Ytd = @MechUnavailTA_Ytd, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_QTR = @NonTAUnavail_QTR, NonTAUnavail_Avg = @NonTAUnavail_Avg, NonTAUnavail_Ytd = @NonTAUnavail_Ytd, 

	MaintIndex = @MaintIndex, MaintIndex_QTR = @MaintIndex_QTR, MaintIndex_Avg = @MaintIndex_Avg, MaintIndex_Ytd = @MaintIndex_Ytd, 
	AnnTACost = @AnnTACost, AnnTACost_QTR = @AnnTACost_QTR, AnnTACost_Avg = @AnnTACost_Avg, AnnTACost_Ytd = @AnnTACost_Ytd, 
	RoutCost = @RoutCost, RoutCost_QTR = @RoutCost_QTR, RoutCost_Avg = @RoutCost_Avg, RoutCost_Ytd = @RoutCost_Ytd, 
	RoutIndex = @RoutIndex, RoutIndex_QTR = @RoutIndex_QTR, RoutIndex_Avg = @RoutIndex_Avg, RoutIndex_Ytd = @RoutIndex_Ytd, 

	TotWHrEdc = @PersIndex, TotWHrEdc_QTR = @PersIndex_QTR, TotWhrEdc_Avg = @PersIndex_Avg, TotWhrEdc_Ytd = @PersIndex_Ytd, 
	AnnTAWhr = @AnnTAWhr, AnnTAWhr_QTR = @AnnTAWhr_QTR, AnnTAWhr_Avg = @AnnTAWhr_Avg, AnnTAWhr_Ytd = @AnnTAWhr_Ytd,
	NonTAWHr = @NonTAWHr, NonTAWHr_QTR = @NonTAWHr_QTR, NonTAWHr_Avg = @NonTAWHr_Avg, NonTAWHr_Ytd = @NonTAWHr_Ytd, 

	NEOpExEdc = @NEOpExEdc, NEOpExEdc_QTR = @NEOpExEdc_QTR, NEOpExEdc_Avg = @NEOpExEdc_Avg, NEOpExEdc_Ytd = @NEOpExEdc_Ytd, 
	NEOpEx = @NEOpEx, NEOpEx_QTR = @NEOpEx_QTR, NEOpEx_Avg = @NEOpEx_Avg, NEOpEx_Ytd = @NEOpEx_Ytd, 

	TotCashOpExUEdc = @OpExUEdc, TotCashOpExUEdc_QTR = @OpExUEdc_QTR, TotCashOpExUedc_Avg = @OpExUedc_Avg, TotCashOpExUEdc_Ytd = @OpExUEdc_Ytd, 
	TAAdj = @TAAdj, TAAdj_QTR = @TAAdj_QTR, TAAdj_Avg = @TAAdj_Avg, TAAdj_Ytd = @TAAdj_Ytd,
	EnergyCost = @EnergyCost, EnergyCost_QTR = @EnergyCost_QTR, EnergyCost_Avg = @EnergyCost_Avg, EnergyCost_Ytd = @EnergyCost_Ytd, 
	TotCashOpEx = @TotCashOpEx, TotCashOpEx_QTR = @TotCashOpEx_QTR, TotCashOpEx_Avg = @TotCashOpEx_Avg, TotCashOpEx_Ytd = @TotCashOpEx_Ytd, 
	UEdc = @UEdc, UEdc_QTR = @UEdc_QTR, Uedc_Avg = @Uedc_Avg, UEdc_Ytd = @UEdc_Ytd



