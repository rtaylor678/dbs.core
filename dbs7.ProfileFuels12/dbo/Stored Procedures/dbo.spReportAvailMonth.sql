﻿
CREATE PROC [dbo].[spReportAvailMonth](@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
	SELECT MechUnavailTA = (SELECT MechUnavailTA_Ann FROM MaintAvailCalc a WHERE a.SubmissionID = GenSum.SubmissionID AND a.FactorSet = GenSum.FactorSet)
		, MechAvail, OpAvail, OnStream, OnStreamSlow, PeriodStart
	FROM GenSum
	WHERE UOM = @UOM AND Currency = @Currency AND FactorSet = @FactorSet AND Scenario = @Scenario
	AND SubmissionID = (SELECT SubmissionID FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet))
	
