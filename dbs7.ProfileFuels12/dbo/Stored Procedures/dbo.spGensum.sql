﻿CREATE    PROC [dbo].[spGensum] (@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @NumDays real, @NetInputBbl real, @FractionOfYear real
SELECT @NumDays = NumDays, @FractionOfYear = FractionOfYear FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
SELECT @NetInputBbl = NetInputBbl FROM MaterialTot WHERE SubmissionID = @SubmissionID
DECLARE @General TABLE (
	[SubmissionID] [int] NOT NULL ,
	[RefineryID] [char] (6) NOT NULL ,
	[DataSet] [varchar] (15) NOT NULL ,
	[PeriodStart] [smalldatetime] NOT NULL ,
	[PeriodEnd] [smalldatetime] NOT NULL ,
	[DaysInPeriod] [smallint] NOT NULL ,
	[CoLoc] [varchar] (50) NULL ,
	[NetInputBPD] [real] NULL ,
	[CrudeInputBPD] [real] NULL ,
	[OthInputBPD] [real] NULL ,
	[CrudeAPI] [real] NULL ,
	[CrudeSulfur] [real] NULL ,
	[GainPcnt] [real] NULL ,
	[EnergyConsPerBbl] [real] NULL ,
	[EnergyConsPerBbl_Pur] [real] NULL ,
	[EnergyConsPerBbl_Prod] [real] NULL ,
	[OccoVtPcnt] [real] NULL ,
	[MPSOvtPcnt] [real] NULL ,
	[ProcoCcmpsRatio] [real] NULL ,
	[MaintOCCMPSRatio] [real] NULL ,
	[OCCAbsPcnt] [real] NULL ,
	[OCCAbsPcnt_Sick] [real] NULL ,
	[OCCAbsPcnt_Other] [real] NULL ,
	[MPSAbsPcnt] [real] NULL ,
	[MPSAbsPcnt_Sick] [real] NULL ,
	[MPSAbsPcnt_Other] [real] NULL
)
DECLARE @FSOnly TABLE (
	[FactorSet] varchar(8) NOT NULL ,
	[Edc] [float] NULL ,
	[UEdc] [float] NULL ,
	[UEdcDays] [float] NULL ,
	[UtilPcnt] [real] NULL ,
	[UtilOSTA] [real] NULL ,
	[ProcessUtilPcnt] [real] NULL ,
	[MechAvail] [real] NULL ,
	[OpAvail] [real] NULL ,
	[OnStream] [real] NULL ,
	[OnStreamSlow] [real] NULL ,
	[VEI] [real] NULL ,
	[EII] [real] NULL ,
	[TotWHrEdc] [real] NULL ,
	[OCCWHrEdc] [real] NULL ,
	[OCCWHrEdc_Oper] [real] NULL ,
	[OCCWHrEdc_Maint] [real] NULL ,
	[OCCWHrEdc_Admin] [real] NULL ,
	[MpsWhrEdc] [real] NULL ,
	[MpsWhrEdc_Oper] [real] NULL ,
	[MpsWhrEdc_Maint] [real] NULL ,
	[MpsWhrEdc_Tech] [real] NULL ,
	[MpsWhrEdc_Admin] [real] NULL,
	[TotEqPEdc] [real] NULL ,
	[OCCEqPEdc] [real] NULL ,
	[OCCEqPEdc_Oper] [real] NULL ,
	[OCCEqPEdc_Maint] [real] NULL ,
	[OCCEqPEdc_Admin] [real] NULL ,
	[MPSEqPEdc] [real] NULL ,
	[MPSEqPEdc_Oper] [real] NULL ,
	[MPSEqPEdc_Maint] [real] NULL ,
	[MPSEqPEdc_Tech] [real] NULL ,
	[MPSEqPEdc_Admin] [real] NULL,
	[PEI] [real] NULL,
	[MaintPEI] [real] NULL,
	[NonMaintPEI] [real] NULL
)

DECLARE @tblMI TABLE (
	[FactorSet] varchar(8) NOT NULL ,
	[Currency] varchar(4) NOT NULL ,
	[MaintIndex] [real] NULL ,
	[RoutIndex] [real] NULL ,
	[TAIndex] [real] NULL ,
	[MEI] [real] NULL,
	[MEI_Rout] real NULL,
	[MEI_TA] real NULL
)
DECLARE @CS TABLE (
	[Scenario] [varchar] (8)  NOT NULL ,
	[Currency] varchar(4) NOT NULL ,
	[GPV] [real] NULL ,
	[RMC] [real] NULL ,
	[GrossMargin] [real] NULL ,
	[TotCashOpExBbl] [real] NULL ,
	[CashMargin] [real] NULL ,
	[EnergyCost] [real] NULL ,
	[EnergyCost_Pur] [real] NULL ,
	[EnergyCost_Prod] [real] NULL
)
-- broke OpEx out from all scenarios because we are not actually using Scenario
DECLARE @OpEx TABLE (
	[FactorSet] [varchar] (8) NOT NULL ,
	[Currency] [varchar] (4) NOT NULL ,
	[TotCashOpExUEdc] [real] NULL ,
	[NonVolOpExUEdc] [real] NULL ,
	[NonVolOpExUEdc_SWB] [real] NULL ,
	[NonVolOpExUEdc_TA] [real] NULL ,
	[NonVolOpExUEdc_Rout] [real] NULL ,
	[NonVolOpExUEdc_OthContract] [real] NULL ,
	[NonVolOpExUEdc_Other] [real] NULL ,
	[VolOpExUEdc] [real] NULL ,
	[VolOpExUEdc_Energy] [real] NULL ,
	[VolOpExUEdc_ChemCat] [real] NULL ,
	[VolOpExUEdc_Other] [real] NULL ,
	[NEOpExUEdc] [real] NULL ,
	[NEOpExEdc] [real] NULL ,
	[NEI] [real] NULL 
)

DECLARE @AllKeys TABLE (
	[FactorSet] [varchar] (8) NOT NULL ,
	[Scenario] [varchar] (8) NOT NULL ,
	[Currency] [varchar] (4) NOT NULL ,
	[ROI] [real] NULL,
	[RV] [real] NULL, 
	[WorkingCptl] [real] NULL,
	[TotCptl] [real] NULL
)

INSERT INTO @General (SubmissionID, RefineryID, DataSet, PeriodStart, PeriodEnd, DaysInPeriod, CoLoc)
SELECT s.SubmissionID, s.RefineryID, s.DataSet, s.PeriodStart, s.PeriodEnd, s.NumDays, t.CoLoc
FROM SubmissionsAll s INNER JOIN TSort t ON t.RefineryID = s.RefineryID
WHERE s.SubmissionID = @SubmissionID
UPDATE @General
SET NetInputBPD = @NetInputBbl/@NumDays/1000,
CrudeInputBPD = (SELECT TotBbl/1000/@NumDays FROM CrudeTot WHERE SubmissionID = @SubmissionID),
CrudeAPI = (SELECT AvgGravity FROM CrudeTot WHERE SubmissionID = @SubmissionID),
CrudeSulfur = (SELECT AvgSulfur FROM CrudeTot WHERE SubmissionID = @SubmissionID),
GainPcnt = (SELECT GainBbl/NetInputBbl*100 FROM MaterialTot WHERE SubmissionID = @SubmissionID AND NetInputBbl > 0),
OccoVtPcnt = (SELECT OVTPcnt FROM PersST WHERE SubmissionID = @SubmissionID AND SectionID = 'TO'),
MPSOvtPcnt = (SELECT OVTPcnt FROM PersST WHERE SubmissionID = @SubmissionID AND SectionID = 'TM'),
ProcoCcmpsRatio = (SELECT ProcessOCCMPSRatio FROM PersTot WHERE SubmissionID = @SubmissionID),
MaintOCCMPSRatio = (SELECT MaintOCCMPSRatio FROM PersTot WHERE SubmissionID = @SubmissionID),
OCCAbsPcnt = (SELECT OCCPcnt FROM AbsenceTot WHERE SubmissionID = @SubmissionID),
OCCAbsPcnt_Sick = (SELECT SUM(OCCPcnt) FROM Absence WHERE SubmissionID = @SubmissionID AND CategoryID IN ('SICK', 'ONJOB')),
OCCAbsPcnt_Other = (SELECT SUM(OCCPcnt) FROM Absence WHERE SubmissionID = @SubmissionID AND CategoryID NOT IN ('SICK', 'ONJOB')),
MPSAbsPcnt = (SELECT MPSPcnt FROM AbsenceTot WHERE SubmissionID = @SubmissionID),
MPSAbsPcnt_Sick = (SELECT SUM(MPSPcnt) FROM Absence WHERE SubmissionID = @SubmissionID AND CategoryID IN ('SICK', 'ONJOB')),
MPSAbsPcnt_Other = (SELECT SUM(MPSPcnt) FROM Absence WHERE SubmissionID = @SubmissionID AND CategoryID NOT IN ('SICK','ONJOB'))
UPDATE @General SET OthInputBPD = ISNULL(NetInputBPD, 0) - ISNULL(CrudeInputBPD, 0)
IF @NetInputBbl > 0
	UPDATE g
	SET EnergyConsPerBbl = TotEnergyConsMBTU/(@NetInputBbl/1000),
	EnergyConsPerBbl_Pur = PurTotMBTU/(@NetInputBbl/1000),
	EnergyConsPerBbl_Prod = ProdTotMBTU/(@NetInputBbl/1000)
	FROM @General g INNER JOIN EnergyTot e ON e.SubmissionID = g.SubmissionID
	WHERE g.SubmissionID = @SubmissionID
INSERT INTO @FSOnly (FactorSet, Edc, UEdc, UEdcDays, UtilPcnt, UtilOSTA, ProcessUtilPcnt, VEI, EII)
SELECT t.FactorSet, t.Edc, t.UEdc, t.UEdc*@NumDays, t.UtilPcnt, t.UtilOSTA, p.UtilPcnt, t.VEI, t.EII
FROM FactorTotCalc t LEFT JOIN FactorProcessCalc p ON p.SubmissionID = t.SubmissionID AND p.FactorSet = t.FactorSet AND p.ProcessID = 'TotProc'
WHERE t.SubmissionID = @SubmissionID

UPDATE f
SET	MechAvail = m.MechAvail_Ann,
	OpAvail = m.OpAvail_Ann,
	OnStream = m.OnStream_Ann,
	OnStreamSlow = m.OnStreamSlow_Ann
FROM @FSOnly f INNER JOIN MaintAvailCalc m ON m.FactorSet = f.FactorSet
WHERE m.SubmissionID = @SubmissionID
UPDATE f
SET TotWHrEdc = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'TP'),
	OCCWHrEdc = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'TO'),
	OCCWHrEdc_Oper = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'OO'),
	OCCWHrEdc_Maint = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'OM'),
	OCCWHrEdc_Admin = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'OA'),
	MpsWhrEdc = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'TM'),
	MpsWhrEdc_Oper = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MO'),
	MpsWhrEdc_Maint = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MM'),
	MpsWhrEdc_Tech = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MT'),
	MpsWhrEdc_Admin = (SELECT TotWHrEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MA'),
	PEI = (SELECT TotWHrEffIndex FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'TP'),
	MaintPEI = (SELECT p.MaintPEI FROM PersTotCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.Scenario = 'CLIENT' AND p.Currency = 'USD'),
	NonMaintPEI = (SELECT p.NonMaintPEI FROM PersTotCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.Scenario = 'CLIENT' AND p.Currency = 'USD')
FROM @FSOnly f

UPDATE f
SET TotEqPEdc = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'TP'),
	OCCEqPEdc = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'TO'),
	OCCEqPEdc_Oper = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'OO'),
	OCCEqPEdc_Maint = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'OM'),
	OCCEqPEdc_Admin = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'OA'),
	MPSEqPEdc = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'TM'),
	MPSEqPEdc_Oper = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MO'),
	MPSEqPEdc_Maint = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MM'),
	MPSEqPEdc_Tech = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MT'),
	MPSEqPEdc_Admin = (SELECT TotEqPEdc FROM PersSTCalc p WHERE p.SubmissionID = @SubmissionID AND p.FactorSet = f.FactorSet AND p.SectionID = 'MA')
FROM @FSOnly f

INSERT INTO @tblMI (FactorSet, Currency, MaintIndex, RoutIndex, TAIndex, MEI, MEI_Rout, MEI_TA)
SELECT FactorSet, Currency, MaintIndex, RoutIndex, TAIndex, MaintEffIndex, RoutEffIndex, TAEffIndex
FROM MaintIndex WHERE SubmissionID = @SubmissionID

INSERT INTO @CS (Scenario, Currency, GPV, RMC, GrossMargin, TotCashOpExBbl, CashMargin)
SELECT Scenario, Currency, GPV, RMC, GrossMargin, CashOpEx, CashMargin
FROM MarginCalc WHERE SubmissionID = @SubmissionID AND DataType = 'Bbl'

UPDATE cs
SET EnergyCost = CASE WHEN m.TotEnergyConsMBTU > 0 THEN c.TotCostK*1000/m.TotEnergyConsMBTU END,
EnergyCost_Pur = CASE WHEN m.PurTotMBTU > 0 THEN c.PurTotCostK*1000/m.PurTotMBTU END,
EnergyCost_Prod = CASE WHEN m.ProdTotMBTU > 0 THEN c.ProdTotCostK*1000/m.ProdTotMBTU END
FROM @CS cs INNER JOIN EnergyTotCost c ON c.Currency = cs.Currency 
INNER JOIN EnergyTot m ON m.SubmissionID = c.SubmissionID
WHERE c.SubmissionID = @SubmissionID AND c.Scenario = 'CLIENT' /*cs.Scenario*/ -- Just using Client Prices

INSERT INTO @OpEx (FactorSet, Currency, TotCashOpExUEdc, 
NonVolOpExUEdc, NonVolOpExUEdc_SWB, NonVolOpExUEdc_TA, NonVolOpExUEdc_Rout, NonVolOpExUEdc_OthContract, NonVolOpExUEdc_Other,
VolOpExUEdc, VolOpExUEdc_Energy, VolOpExUEdc_ChemCat, VolOpExUEdc_Other, NEOpExUEdc)
SELECT FactorSet, Currency, TotCashOpEx, 
STNonVol, ISNULL(STSal, 0) + ISNULL(STBen, 0), ISNULL(TAAdj, 0), 
ISNULL(MaintMatl, 0) + ISNULL(ContMaintLabor, 0) + ISNULL(ContMaintMatl, 0) + ISNULL(Equip, 0), 
ISNULL(OthCont, 0), ISNULL(Tax, 0) + ISNULL(Insur, 0) + ISNULL(Envir, 0) + ISNULL(OthNonVol, 0) + ISNULL(GAPers, 0),
STVol, EnergyCost, ISNULL(Antiknock, 0) + ISNULL(Chemicals, 0) + ISNULL(Catalysts, 0), 
ISNULL(Royalties, 0) + ISNULL(PurOth, 0) + ISNULL(EmissionsPurch, 0) - ISNULL(EmissionsCredits, 0) + ISNULL(EmissionsTaxes, 0) + ISNULL(OthVol, 0) , NEOpEx
FROM OpExCalc WHERE SubmissionID = @SubmissionID AND DataType = 'UEdc' AND Scenario = 'CLIENT'

UPDATE a
SET NEOpExEdc = (SELECT NEOpEx FROM OpExCalc o WHERE o.DataType = 'Edc' AND o.FactorSet = a.FactorSet AND o.Scenario = 'CLIENT' AND o.Currency = a.Currency AND o.SubmissionID = @SubmissionID),
NEI = (SELECT NEOpEx FROM OpExCalc o WHERE o.DataType = 'NEI' AND o.FactorSet = a.FactorSet AND o.Scenario = 'CLIENT' AND o.Currency = a.Currency AND o.SubmissionID = @SubmissionID)
FROM @OpEx a

INSERT INTO @AllKeys (FactorSet, Scenario, Currency, ROI, RV, WorkingCptl, TotCptl)
SELECT FactorSet, Scenario, Currency, ROI, RV, WorkingCptl, TotCptl
FROM ROICalc WHERE SubmissionID = @SubmissionID

DELETE FROM GenSum WHERE SubmissionID = @SubmissionID
INSERT INTO GenSum (SubmissionID, FactorSet, Scenario, Currency, UOM, 
RefineryID, DataSet, PeriodStart, PeriodEnd, DaysInPeriod, CoLoc, 
NetInputBPD, CrudeInputBPD, OthInputBPD, CrudeAPI, CrudeSulfur, GainPcnt,
EnergyConsPerBbl, EnergyConsPerBbl_Pur, EnergyConsPerBbl_Prod, 
OccoVtPcnt, MPSOvtPcnt, ProcoCcmpsRatio, MaintOCCMPSRatio, 
OCCAbsPcnt, OCCAbsPcnt_Sick, OCCAbsPcnt_Other, 
MPSAbsPcnt, MPSAbsPcnt_Sick, MPSAbsPcnt_Other)
SELECT DISTINCT s.SubmissionID, f.FactorSet, m.Scenario, c.Currency, 'US',
s.RefineryID, s.DataSet, s.PeriodStart, s.PeriodEnd, s.DaysInPeriod, s.CoLoc, 
s.NetInputBPD, s.CrudeInputBPD, s.OthInputBPD, s.CrudeAPI, s.CrudeSulfur, s.GainPcnt,
s.EnergyConsPerBbl, s.EnergyConsPerBbl_Pur, s.EnergyConsPerBbl_Prod, 
s.OccoVtPcnt, s.MPSOvtPcnt, s.ProcoCcmpsRatio, s.MaintOCCMPSRatio, 
s.OCCAbsPcnt, s.OCCAbsPcnt_Sick, s.OCCAbsPcnt_Other, 
s.MPSAbsPcnt, s.MPSAbsPcnt_Sick, s.MPSAbsPcnt_Other
FROM @General s, CurrenciesToCalc c, FactorSets f, MarginCalc m
WHERE c.RefineryID = s.RefineryID AND f.RefineryType = dbo.GetRefineryType(s.RefineryID) AND m.SubmissionID = s.SubmissionID
AND (m.Scenario = 'CLIENT' OR m.Scenario = f.FactorSet) AND f.Calculate = 'Y'

UPDATE GenSum
SET Edc = x.Edc, UEdc = x.UEdc, UEdcDays = x.UEdcDays, 
UtilPcnt = x.UtilPcnt, UtilOSTA = x.UtilOSTA, ProcessUtilPcnt = x.ProcessUtilPcnt,
MechAvail = x.MechAvail, OpAvail = x.OpAvail, OnStream = x.OnStream, OnStreamSlow = x.OnStreamSlow,
VEI = x.VEI, EII = x.EII,
TotWHrEdc = x.TotWHrEdc, OCCWHrEdc = x.OCCWHrEdc,
OCCWHrEdc_Oper = x.OCCWHrEdc_Oper, OCCWHrEdc_Maint = x.OCCWHrEdc_Maint, OCCWHrEdc_Admin = x.OCCWHrEdc_Admin,
MpsWhrEdc = x.MpsWhrEdc, MpsWhrEdc_Oper = x.MpsWhrEdc_Oper, MpsWhrEdc_Maint = x.MpsWhrEdc_Maint,
MpsWhrEdc_Tech = x.MpsWhrEdc_Tech, MpsWhrEdc_Admin = x.MpsWhrEdc_Admin,
TotEqPEdc = x.TotEqPEdc, OCCEqPEdc = x.OCCEqPEdc,
OCCEqPEdc_Oper = x.OCCEqPEdc_Oper, OCCEqPEdc_Maint = x.OCCEqPEdc_Maint, OCCEqPEdc_Admin = x.OCCEqPEdc_Admin,
MPSEqPEdc = x.MPSEqPEdc, MPSEqPEdc_Oper = x.MPSEqPEdc_Oper, MPSEqPEdc_Maint = x.MPSEqPEdc_Maint,
MPSEqPEdc_Tech = x.MPSEqPEdc_Tech, MPSEqPEdc_Admin = x.MPSEqPEdc_Admin,
PEI = x.PEI, MaintPEI = x.MaintPEI, NonMaintPEI = x.NonMaintPEI
FROM GenSum INNER JOIN @FSOnly x ON x.FactorSet = GenSum.FactorSet
WHERE GenSum.SubmissionID = @SubmissionID

UPDATE GenSum
SET MaintIndex = x.MaintIndex, RoutIndex = x.RoutIndex, TAIndex = x.TAIndex, 
	MEI = x.MEI, MEI_Rout = x.MEI_Rout, MEI_TA = x.MEI_TA
FROM GenSum INNER JOIN @tblMI x ON GenSum.FactorSet = x.FactorSet AND GenSum.Currency = x.Currency
WHERE GenSum.SubmissionID = @SubmissionID
UPDATE GenSum
SET GPV = x.GPV, RMC = x.RMC, GrossMargin = x.GrossMargin,
TotCashOpExBbl = x.TotCashOpExBbl, CashMargin = x.CashMargin,
EnergyCost = x.EnergyCost, EnergyCost_Pur = x.EnergyCost_Pur, EnergyCost_Prod = x.EnergyCost_Prod
FROM GenSum INNER JOIN @CS x ON GenSum.Scenario = x.Scenario AND GenSum.Currency = x.Currency
WHERE GenSum.SubmissionID = @SubmissionID

UPDATE GenSum
SET TotCashOpExUEdc = x.TotCashOpExUEdc, NonVolOpExUEdc = x.NonVolOpExUEdc,
NonVolOpExUEdc_SWB = x.NonVolOpExUEdc_SWB, NonVolOpExUEdc_TA = x.NonVolOpExUEdc_TA,
NonVolOpExUEdc_Rout = x.NonVolOpExUEdc_Rout, NonVolOpExUEdc_OthContract = x.NonVolOpExUEdc_OthContract,
NonVolOpExUEdc_Other = x.NonVolOpExUEdc_Other, VolOpExUEdc = x.VolOpExUEdc,
VolOpExUEdc_Energy = x.VolOpExUEdc_Energy, VolOpExUEdc_ChemCat = x.VolOpExUEdc_ChemCat,
VolOpExUEdc_Other = x.VolOpExUEdc_Other, NEOpExUEdc = x.NEOpExUEdc, NEOpExEdc = x.NEOpExEdc, NEI = x.NEI
FROM GenSum INNER JOIN @OpEx x ON GenSum.FactorSet = x.FactorSet AND GenSum.Currency = x.Currency
WHERE GenSum.SubmissionID = @SubmissionID

UPDATE GenSum
SET ROI = x.ROI, RV = x.RV, WorkCptl = x.WorkingCptl, TotCptl = x.TotCptl
FROM GenSum INNER JOIN @AllKeys x ON GenSum.FactorSet = x.FactorSet AND GenSum.Scenario = x.Scenario AND GenSum.Currency = x.Currency
WHERE GenSum.SubmissionID = @SubmissionID

UPDATE GenSum
SET TotMaintForceWHrEdc = p.TotMaintForceWHrEdc, MaintForceCompWHrEdc = p.MaintForceCompWHrEdc, MaintForceContWHrEdc = p.MaintForceContWHrEdc
FROM GenSum INNER JOIN PersTotCalc p ON p.SubmissionID = GenSum.SubmissionID AND p.FactorSet = GenSum.FactorSet
WHERE GenSum.SubmissionID = @SubmissionID
--IF EXISTS (SELECT * FROM SubmissionsAll WHERE SubmissionID = @SubmissionID AND UOM = 'MET')
--BEGIN
	SELECT * INTO #met
	FROM GenSum WHERE SubmissionID = @SubmissionID
	UPDATE #met
	SET UOM = 'MET',
	EnergyConsPerBbl = EnergyConsPerBbl*1.055,
	EnergyConsPerBbl_Prod = EnergyConsPerBbl_Prod*1.055,
	EnergyConsPerBbl_Pur = EnergyConsPerBbl_Pur*1.055,
	EnergyCost = EnergyCost/1.055,
	EnergyCost_Prod = EnergyCost_Prod/1.055,
	EnergyCost_Pur = EnergyCost/1.055
	INSERT INTO GenSum
	SELECT * FROM #met
	DROP TABLE #met
--END

