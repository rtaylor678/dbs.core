﻿



CREATE   PROC [dbo].[spReportChevronRecon] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

DECLARE @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start12Mo = DATEADD(mm, -12, @PeriodEnd), @StartYTD = dbo.BuildDate(DATEPART(yy, @PeriodStart), 1, 1), @Start24Mo = DATEADD(mm, -24, @PeriodEnd)


DECLARE	
	@UEdc real, @PurUtilUEdc real, @RefUtilUEdc real, @Edc real, @UtilPcnt real,
	@TotProcessUEdc real, @TotProcessEdc real, @ProcessUtilPcnt real, 
	
	@MechUnavail_Act real, @MechUnavailSlow_Act real, @OpUnavail_Act real, @OpUnavailSlow_Act real, @OffStream_Act real, @OffStreamSlow_Act real,
	@ProcessUtilPcntCalc real,

	@TotCashOpEx real, @TotCashOpEx_Ytd real, @TotCashOpEx_Avg real, @TotCashOpExUEdc real, @TotCashOpExUEdc_Ytd real, 
	@TotCashOpExUedc_Avg real, @TotPurEnergy real,
	@TotPurEnergy_Ytd real, @TotPurEnergy_Avg real, @TotPurEnergyUEdc real, @TotPurEnergyUEdc_Ytd real,
	@TotPurEnergyUedc_Avg real, @TotProdEnergy real, @TotProdEnergy_Ytd real, @TotProdEnergy_Avg real,
    @TotProdEnergyUEdc real, @TotProdEnergyUEdc_Ytd real, @TotProdEnergyUedc_Avg real, @TotOthEnergy real,
	@TotOthEnergy_Ytd real, @TotOthEnergy_Avg real, @TotOthEnergyUEdc real, @TotOthEnergyUEdc_Ytd real,
	@TotOthEnergyUedc_Avg real, @NEOpEx real, @NEOpEx_Ytd real, @NEOpEx_Avg real, @NEOpExUEdc real,
	@NEOpExUEdc_Ytd real, @NEOpExUedc_Avg real, @UEdc_Ytd real, @Uedc_Avg real, 
	
	@OCCPO_STH real, @OCCPO_OVT real, @OCCPO_Contract real, @OCCPO_GA real, @OCCPO_AbsHrs real, @OCCPO_TotWHr real, @OCCPO_TotEqPEdc real,
	@OCCTAADJ_STH real, @OCCTAADJ_OVT real, @OCCTAADJ_Contract real, @OCCTAADJ_GA real, @OCCTAADJ_AbsHrs real, @OCCTAADJ_TotWHr real, @OCCTAADJ_TotEqPEdc real,
	@OCCMA_STH real, @OCCMA_OVT real, @OCCMA_Contract real, @OCCMA_GA real, @OCCMA_AbsHrs real, @OCCMA_TotWHr real, @OCCMA_TotEqPEdc real,
	@OCCAS_STH real, @OCCAS_OVT real, @OCCAS_Contract real, @OCCAS_GA real, @OCCAS_AbsHrs real, @OCCAS_TotWHr real, @OCCAS_TotEqPEdc real,
	@OCC_STH real, @OCC_OVT real, @OCC_Contract real, @OCC_GA real, @OCC_AbsHrs real, @OCC_TotWHr real, @OCC_TotEqPEdc real,

	@MPSPO_STH real, @MPSPO_OVT real, @MPSPO_Contract real, @MPSPO_GA real, @MPSPO_AbsHrs real, @MPSPO_TotWHr real, @MPSPO_TotEqPEdc real,
	@MPSTAADJ_STH real, @MPSTAADJ_OVT real, @MPSTAADJ_Contract real, @MPSTAADJ_GA real, @MPSTAADJ_AbsHrs real, @MPSTAADJ_TotWHr real, @MPSTAADJ_TotEqPEdc real,
	@MPSMA_STH real, @MPSMA_OVT real, @MPSMA_Contract real, @MPSMA_GA real, @MPSMA_AbsHrs real, @MPSMA_TotWHr real, @MPSMA_TotEqPEdc real,
	@MPSTS_STH real, @MPSTS_OVT real, @MPSTS_Contract real, @MPSTS_GA real, @MPSTS_AbsHrs real, @MPSTS_TotWHr real, @MPSTS_TotEqPEdc real,
	@MPSAS_STH real, @MPSAS_OVT real, @MPSAS_Contract real, @MPSAS_GA real, @MPSAS_AbsHrs real, @MPSAS_TotWHr real, @MPSAS_TotEqPEdc real,
	@MPS_STH real, @MPS_OVT real, @MPS_Contract real, @MPS_GA real, @MPS_AbsHrs real, @MPS_TotWHr real, @MPS_TotEqPEdc real,

	@AnnTACost real, @CurrRoutCost real, @TotMaintCost real, @TAIndex real, @RoutIndex real, @MaintIndex real,
	@AnnTACost_Ytd real, @CurrRoutCost_Ytd real, @TotMaintCost_Ytd real, @RoutIndex_Ytd real, @MaintIndex_Ytd real,
	@AnnTACost_Avg real, @CurrRoutCost_Avg real, @TotMaintCost_Avg real, @RoutIndex_Avg real, @MaintIndex_Avg real,
	
	@Edc_Ytd real,	@Edc_Avg real, 
	
	@RptSource_PUR real, @RptSource_PRO real, @RptSource_IN real, @RptSource_OUT real, @RptSource_SOL real,
	@SourceMWH_PUR real, @SourceMWH_PRO real, @SourceMWH_IN real, @SourceMWH_OUT real, @SourceMWH_SOL real,
	@TotEnergyConsMBTU real, @EnergyUseDay real, @ProcessStdEnergy real, @SensHeatStdEnergy real, 
	@OffsitesStdEnergy real, @AspStdEnergy real, @TotStdEnergy real, @EII real, 
	
	@RMI real, @RMIPriceUS real, @RMICost real, @RMI_Ytd real, @RMIPriceUS_Ytd real, @RMICost_Ytd real,
	@OTHRM real, @OTHRMPriceUS real, @OTHRMCost real, @OTHRM_Ytd real, @OTHRMPriceUS_Ytd real, @OTHRMCost_Ytd real,
	@RM real, @RMCost real, @RM_Ytd real, @RMCost_Ytd real,	@NetInputBPD real, @NetInputBPD_Ytd real, 
	
	@PROD real, @PRODPriceUS real, @PRODCost real, @PROD_Ytd real, @PRODPriceUS_Ytd real, @PRODCost_Ytd real,
	@RPF real, @RPFPriceUS real, @RPFCost real, @RPF_Ytd real, @RPFPriceUS_Ytd real, @RPFCost_Ytd real,
	@ASP real, @ASPPriceUS real, @ASPCost real, @ASP_Ytd real, @ASPPriceUS_Ytd real, @ASPCost_Ytd real,
	@SOLV real, @SOLVPriceUS real, @SOLVCost real, @SOLV_Ytd real, @SOLVPriceUS_Ytd real, @SOLVCost_Ytd real,
	@COKE real, @COKEPriceUS real, @COKECost real, @COKE_Ytd real, @COKEPriceUS_Ytd real, @COKECost_Ytd real,
	@MPROD real, @MPRODPriceUS real, @MPRODCost real, @MPROD_Ytd real, @MPRODPriceUS_Ytd real, @MPRODCost_Ytd real,
	@Yield real, @YIELDPriceUS real, @YIELDCost real, @YIELD_Ytd real, @YIELDPriceUS_Ytd real, @YIELDCost_Ytd real,

	@GrossMargin real, @GrossMargin_Ytd real, @TotGrossMargin real,	@TotGrossMargin_Ytd real, 
	@TotCashOpExBbl real, @TotCashOpExBbl_Ytd real, @TotCashMargin real, @CashMargin real,
	@TotCashMargin_Ytd real, @CashMargin_Ytd real

--Get Standard Variables Out of GenSum First (38)
SELECT @UEdc = UEdc/1000, @Edc = Edc/1000, @UtilPcnt = UtilPcnt, @ProcessUtilPcnt = ProcessUtilPcnt, @TotCashOpExUEdc = TotCashOpExUEdc, 
	@TotCashOpExUEdc_Ytd = TotCashOpExUEdc_Ytd, @TotCashOpExUedc_Avg = TotCashOpExUedc_Avg, @NEOpExUEdc = NEOpExUEdc, 
	@NEOpExUEdc_Ytd = NEOpExUEdc_Ytd, @NEOpExUedc_Avg = NEOpExUedc_Avg, @UEdc_Ytd = UEdc_Ytd/1000, @Uedc_Avg = Uedc_Avg/1000, 
	@TAIndex = TAIndex_Avg, @RoutIndex = RoutIndex, @RoutIndex_Ytd = RoutIndex_Ytd, @RoutIndex_Avg = RoutIndex_Avg, 
	@MaintIndex = RoutIndex+TAIndex_Avg, @MaintIndex_Ytd = MaintIndex_Ytd, @MaintIndex_Avg = MaintIndex_Avg, @Edc_Ytd = Edc_Ytd/1000, 
	@Edc_Avg = Edc_Avg/1000, @EII = EII, @NetInputBPD = NetInputBPD * @NumDays, @NetInputBPD_Ytd = NetInputBPD_Ytd * @NumDays, @GrossMargin = GrossMargin, 
	@GrossMargin_Ytd = GrossMargin_Ytd, @TotCashOpExBbl = TotCashOpExBbl, @TotCashOpExBbl_Ytd = TotCashOpExBbl_Ytd, @CashMargin = CashMargin,  @CashMargin_Ytd = CashMargin_Ytd 
FROM GenSum
WHERE SubmissionID = @SubmissionID AND DataSet='Actual' AND FactorSet = @FactorSet AND Currency = @Currency AND UOM = @UOM AND Scenario = @Scenario

SELECT @TotProcessUEdc = TotProcessUEdc/1000, @TotProcessEdc = TotProcessEdc/1000, @PurUtilUEdc = (ISNULL(PurElecUEdc,0)+ISNULL(PurStmUEdc,0))/1000
FROM FactorTotCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT @RefUtilUEdc = @UEdc - @PurUtilUEdc

-- Get the Availability Data
SELECT @MechUnavail_Act = 100 - MechAvail_Act, @MechUnavailSlow_Act = MechAvail_Act - MechAvailSlow_Act,
	@OpUnavail_Act = MechAvail_Act-OpAvail_Act, @OpUnavailSlow_Act = MechAvailSlow_Act - OpAvailSlow_Act,
	@OffStream_Act = OpAvail_Act-OnStream_Act, @OffStreamSlow_Act = OpAvailSlow_Act - OnStreamSlow_Act
FROM MaintAvailCalc WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet 

SELECT @ProcessUtilPcntCalc = 100 - @MechUnavail_Act - @MechUnavailSlow_Act - @OpUnavail_Act - @OpUnavailSlow_Act - @OffStream_Act - @OffStreamSlow_Act

--Get the OpEx Dollars Out of OpExAll
SELECT @TotCashOpEx = TotCashOpEx/1000, @TotPurEnergy = (ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0))/1000,
	@TotProdEnergy = (ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0))/1000, @TotOthEnergy = ISNULL(PurOth,0)/1000, @NEOpEx = NEOpEx/1000, @AnnTACost = TAAdj/1000 
FROM OpExAll
WHERE SubmissionID = @SubmissionID AND DataType='Adj' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotCashOpEx_Ytd = SUM(TotCashOpEx)/1000, @TotPurEnergy_Ytd = (SUM(ISNULL(PurElec,0))+SUM(ISNULL(PurSteam,0))+SUM(ISNULL(PurFG,0))+SUM(ISNULL(PurSolid,0))+SUM(ISNULL(PurLiquid,0)))/1000,
	@TotProdEnergy_Ytd = (SUM(ISNULL(RefProdFG,0))+SUM(ISNULL(RefProdOth,0)))/1000, @TotOthEnergy_Ytd = SUM(ISNULL(PurOth,0))/1000, @NEOpEx_Ytd = SUM(NEOpEx)/1000, @AnnTACost_Ytd = SUM(TAAdj)/1000  
FROM OpExAll o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and DataType='Adj' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotCashOpEx_Avg = SUM(TotCashOpEx)/1000, @TotPurEnergy_Avg = (SUM(ISNULL(PurElec,0))+SUM(ISNULL(PurSteam,0))+SUM(ISNULL(PurFG,0))+SUM(ISNULL(PurSolid,0))+SUM(ISNULL(PurLiquid,0)))/1000,
	@TotProdEnergy_Avg = (SUM(ISNULL(RefProdFG,0))+SUM(ISNULL(RefProdOth,0)))/1000, @TotOthEnergy_Avg = SUM(ISNULL(PurOth,0))/1000, @NEOpEx_Avg = SUM(NEOpEx)/1000, @AnnTACost_Avg = 2*SUM(TAAdj)/1000  
FROM OpExAll o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and DataType='Adj' AND Currency = @Currency AND Scenario = @Scenario

--Get the OpEx/UEdc Dollars Out of OpExAll
SELECT @TotPurEnergyUEdc = ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0),
	@TotProdEnergyUEdc = ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0), @TotOthEnergyUEdc = ISNULL(PurOth,0) 
FROM OpExCalc
WHERE SubmissionID = @SubmissionID AND DataType='UEdc' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotPurEnergyUEdc_Ytd = SUM((ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0))*Divisor)/SUM(Divisor),
	@TotProdEnergyUEdc_Ytd = SUM((ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0))*Divisor)/SUM(Divisor), @TotOthEnergyUEdc_Ytd = SUM(ISNULL(PurOth,0)*Divisor)/SUM(Divisor)
FROM OpExCalc o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and DataType='UEdc' AND Currency = @Currency AND Scenario = @Scenario

SELECT @TotPurEnergyUedc_Avg = SUM((ISNULL(PurElec,0)+ISNULL(PurSteam,0)+ISNULL(PurFG,0)+ISNULL(PurSolid,0)+ISNULL(PurLiquid,0))*Divisor)/SUM(Divisor),
	@TotProdEnergyUedc_Avg = SUM((ISNULL(RefProdFG,0)+ISNULL(RefProdOth,0))*Divisor)/SUM(Divisor), @TotOthEnergyUedc_Avg = SUM(ISNULL(PurOth,0)*Divisor)/SUM(Divisor)
FROM OpExCalc o
INNER JOIN Submissions s ON s.SubmissionID = o.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.PeriodStart >= @Start12Mo AND s.PeriodStart < @PeriodEnd AND s.SubmissionID = 1
and DataType='UEdc' AND Currency = @Currency AND Scenario = @Scenario

-- Get the Pers Hours by Subtotal Category
SELECT @OCCPO_STH = p.STH, @OCCPO_OVT = p.OVTHours, @OCCPO_Contract = p.Contract, @OCCPO_GA = p.GA, @OCCPO_AbsHrs = p.AbsHrs, @OCCPO_TotWHr = pc.TotWHr, @OCCPO_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'OCCPO' AND pc.FactorSet = 2008

SELECT @OCCTAADJ_STH = p.STH, @OCCTAADJ_OVT = p.OVTHours, @OCCTAADJ_Contract = p.Contract, @OCCTAADJ_GA = p.GA, @OCCTAADJ_AbsHrs = p.AbsHrs, @OCCTAADJ_TotWHr = pc.TotWHr, @OCCTAADJ_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'OCCTAADJ' AND pc.FactorSet = 2008

SELECT @OCCMA_STH = p.STH, @OCCMA_OVT = p.OVTHours, @OCCMA_Contract = p.Contract, @OCCMA_GA = p.GA, @OCCMA_AbsHrs = p.AbsHrs, @OCCMA_TotWHr = pc.TotWHr, @OCCMA_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'OCCMA' AND pc.FactorSet = 2008

SELECT @OCCAS_STH = p.STH, @OCCAS_OVT = p.OVTHours, @OCCAS_Contract = p.Contract, @OCCAS_GA = p.GA, @OCCAS_AbsHrs = p.AbsHrs, @OCCAS_TotWHr = pc.TotWHr, @OCCAS_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'OCCAS' AND pc.FactorSet = 2008

SELECT @OCC_STH = SUM(p.STH), @OCC_OVT = SUM(p.OVTHours), @OCC_Contract = SUM(p.Contract), @OCC_GA = SUM(p.GA), @OCC_AbsHrs = SUM(p.AbsHrs), @OCC_TotWHr = SUM(pc.TotWHr), @OCC_TotEqPEdc = SUM(pc.TotEqPEdc)
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId IN ('OCCPO','OCCMA','OCCTAADJ','OCCAS') AND pc.FactorSet = 2008

SELECT @MPSPO_STH = p.STH, @MPSPO_OVT = p.OVTHours, @MPSPO_Contract = p.Contract, @MPSPO_GA = p.GA, @MPSPO_AbsHrs = p.AbsHrs, @MPSPO_TotWHr = pc.TotWHr, @MPSPO_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'MPSPO' AND pc.FactorSet = 2008

SELECT @MPSTAADJ_STH = p.STH, @MPSTAADJ_OVT = p.OVTHours, @MPSTAADJ_Contract = p.Contract, @MPSTAADJ_GA = p.GA, @MPSTAADJ_AbsHrs = p.AbsHrs, @MPSTAADJ_TotWHr = pc.TotWHr, @MPSTAADJ_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'MPSTAADJ' AND pc.FactorSet = 2008

SELECT @MPSMA_STH = p.STH, @MPSMA_OVT = p.OVTHours, @MPSMA_Contract = p.Contract, @MPSMA_GA = p.GA, @MPSMA_AbsHrs = p.AbsHrs, @MPSMA_TotWHr = pc.TotWHr, @MPSMA_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'MPSMA' AND pc.FactorSet = 2008

SELECT @MPSTS_STH = p.STH, @MPSTS_OVT = p.OVTHours, @MPSTS_Contract = p.Contract, @MPSTS_GA = p.GA, @MPSTS_AbsHrs = p.AbsHrs, @MPSTS_TotWHr = pc.TotWHr, @MPSTS_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'MPSTS' AND pc.FactorSet = 2008

SELECT @MPSAS_STH = p.STH, @MPSAS_OVT = p.OVTHours, @MPSAS_Contract = p.Contract, @MPSAS_GA = p.GA, @MPSAS_AbsHrs = p.AbsHrs, @MPSAS_TotWHr = pc.TotWHr, @MPSAS_TotEqPEdc = pc.TotEqPEdc
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId = 'MPSAS' AND pc.FactorSet = 2008

SELECT @MPS_STH = SUM(p.STH), @MPS_OVT = SUM(p.OVTHours), @MPS_Contract = SUM(p.Contract), @MPS_GA = SUM(p.GA), @MPS_AbsHrs = SUM(p.AbsHrs), @MPS_TotWHr = SUM(pc.TotWHr), @MPS_TotEqPEdc = SUM(pc.TotEqPEdc)
FROM Pers p INNER JOIN PersCalc pc ON p.SubmissionID = pc.SubmissionID AND p.PersId = pc.PersId
WHERE p.SubmissionID = @SubmissionID AND p.PersId IN ('MPSPO','MPSMA','MPSTAADJ','MPSTS','MPSAS') AND pc.FactorSet = 2008

-- Get Maintenance Dollars
SELECT @CurrRoutCost = CurrRoutCost/1000, @TotMaintCost = ISNULL(CurrRoutCost,0)/1000+@AnnTACost
FROM MaintTotCost
WHERE SubmissionID = @SubmissionID AND Currency = @Currency

SELECT @CurrRoutCost_Ytd = SUM(CurrRoutCost)/1000
FROM MaintTotCost m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
and Currency = @Currency

SELECT @TotMaintCost_Ytd = @CurrRoutCost_Ytd+@AnnTACost_Ytd

SELECT @CurrRoutCost_Avg = SUM(CurrRoutCost)/1000
FROM MaintTotCost m INNER JOIN Submissions s ON s.SubmissionID = m.SubmissionID
WHERE s.RefineryID = @RefineryID and Currency = @Currency AND s.PeriodStart >= @Start24Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1

SELECT @TotMaintCost_Avg = @CurrRoutCost_Avg+@AnnTACost_Avg

-- Get Energy Data
SELECT @RptSource_PUR = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR'
SELECT @RptSource_PRO = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PRO'
SELECT @RptSource_IN = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'REF'
SELECT @RptSource_OUT = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'AFF'
SELECT @RptSource_SOL = SUM(RptSource) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL'

-- Get Electric Data
SELECT @SourceMWH_PUR = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'PUR'
SELECT @SourceMWH_PRO = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'PRO'
SELECT @SourceMWH_IN = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'REF'
SELECT @SourceMWH_OUT = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND TransferTo = 'AFF'
SELECT @SourceMWH_SOL = SUM(SourceMWH) FROM Electric WHERE SubmissionID = @SubmissionID AND TransType = 'SOL'

-- Total Consumption and Standard Energy
SELECT @TotEnergyConsMBTU = e.TotEnergyConsMBTU, @EnergyUseDay = EnergyUseDay, 
	@ProcessStdEnergy = TotStdEnergy - ISNULL(SensHeatStdEnergy,0) - ISNULL(OffsitesStdEnergy,0) - ISNULL(AspStdEnergy,0),
	@SensHeatStdEnergy = SensHeatStdEnergy, @OffsitesStdEnergy = OffsitesStdEnergy, @AspStdEnergy = AspStdEnergy,
	@TotStdEnergy = TotStdEnergy
FROM EnergyTot e INNER JOIN FactorTotCalc f ON e.SubmissionID = f.SubmissionID
WHERE e.SubmissionID = @SubmissionID AND f.FactorSet = @FactorSet

-- Get Raw Materials
SELECT @RMI = SUM(ISNULL(Bbl,0))/1000, @RMIPriceUS = SUM(Bbl*PriceUS)/SUM(Bbl), @RMICost = SUM(Bbl*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'RMI'

SELECT @RMI_Ytd = SUM(ISNULL(Bbl,0))/1000, @RMIPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @RMICost_Ytd = SUM(Bbl*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'RMI'

SELECT @OTHRM = SUM(ISNULL(Bbl,0))/1000, @OTHRMPriceUS = SUM(Bbl*PriceUS)/SUM(Bbl), @OTHRMCost = SUM(Bbl*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'OTHRM'

SELECT @OTHRM_Ytd = SUM(ISNULL(Bbl,0))/1000, @OTHRMPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @OTHRMCost_Ytd = SUM(Bbl*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'OTHRM'

SELECT @RM = @RMI+@OTHRM, @RMCost = @RMICost+@OTHRMCost, @RM_Ytd = @RMI_Ytd+@OTHRM_Ytd, @RMCost_Ytd = @RMICost_Ytd+@OTHRMCost_Ytd

-- Get Product Yields
SELECT @PROD = SUM(ISNULL(Bbl,0))/1000, @PRODPriceUS = SUM(Bbl*PriceUS)/SUM(Bbl), @PRODCost = SUM(Bbl*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'PROD'

SELECT @PROD_Ytd = SUM(ISNULL(Bbl,0))/1000, @PRODPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @PRODCost_Ytd = SUM(Bbl*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'PROD'

SELECT @RPF = SUM(ISNULL(Bbl,0))/1000, @RPFPriceUS = SUM(Bbl*PriceUS)/SUM(Bbl), @RPFCost = SUM(Bbl*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'RPF'

SELECT @RPF_Ytd = SUM(ISNULL(Bbl,0))/1000, @RPFPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @RPFCost_Ytd = SUM(Bbl*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'RPF'

SELECT @ASP = SUM(ISNULL(Bbl,0))/1000, @ASPPriceUS = SUM(Bbl*PriceUS)/SUM(Bbl), @ASPCost = SUM(Bbl*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'ASP'

SELECT @ASP_Ytd = SUM(ISNULL(Bbl,0))/1000, @ASPPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @ASPCost_Ytd = SUM(Bbl*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'ASP'

SELECT @COKE = SUM(ISNULL(Bbl,0))/1000, @COKEPriceUS = SUM(Bbl*PriceUS)/SUM(Bbl), @COKECost = SUM(Bbl*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'COKE'

SELECT @COKE_Ytd = SUM(ISNULL(Bbl,0))/1000, @COKEPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @COKECost_Ytd = SUM(Bbl*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'COKE'

SELECT @SOLV = SUM(ISNULL(Bbl,0))/1000, @SOLVPriceUS = SUM(Bbl*PriceUS)/SUM(Bbl), @SOLVCost = SUM(Bbl*PriceUS)/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'SOLV'

SELECT @SOLV_Ytd = SUM(ISNULL(Bbl,0))/1000, @SOLVPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @SOLVCost_Ytd = SUM(Bbl*PriceUS)/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'SOLV'

SELECT @MPROD = SUM(ISNULL(Bbl,0))/1000, @MPRODPriceUS = SUM(ISNULL(Bbl,0)*PriceUS)/SUM(Bbl), @MPRODCost = SUM(ISNULL(Bbl,0)*ISNULL(PriceUS,0))/1000000
FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'MPROD'

SELECT @MPROD_Ytd = SUM(ISNULL(Bbl,0))/1000, @MPRODPriceUS_Ytd = SUM(Bbl*PriceUS)/SUM(Bbl), @MPRODCost_Ytd = SUM(ISNULL(Bbl,0)*ISNULL(PriceUS,0))/1000000
FROM Yield y INNER JOIN Submissions s ON y.SubmissionID = s.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @StartYTD AND s.UseSubmission = 1 AND Category = 'MPROD'

SELECT @Yield = ISNULL(@PROD,0)+ISNULL(@RPF,0)+ISNULL(@ASP,0)+ISNULL(@COKE,0)+ISNULL(@SOLV,0)+ISNULL(@MPROD,0), 
	@YIELDCost = ISNULL(@PRODCost,0)+ISNULL(@RPFCost,0)+ISNULL(@ASPCost,0)+ISNULL(@COKECost,0)+ISNULL(@SOLVCost,0)+ISNULL(@MPRODCost,0), 
	@YIELD_Ytd = ISNULL(@PROD_Ytd,0)+ISNULL(@RPF_Ytd,0)+ISNULL(@ASP_Ytd,0)+ISNULL(@COKE_Ytd,0)+ISNULL(@SOLV_Ytd,0)+ISNULL(@MPROD_Ytd,0), 
	@YIELDCost_Ytd = ISNULL(@PRODCost_Ytd,0)+ISNULL(@RPFCost_Ytd,0)+ISNULL(@ASPCost_Ytd,0)+ISNULL(@COKECost_Ytd,0)+ISNULL(@SOLVCost_Ytd,0)+ISNULL(@MPRODCost_Ytd,0)
	
SELECT @TotGrossMargin = @YIELDCost - @RMCost, @TotGrossMargin_Ytd = @YIELDCost_Ytd - @RMCost_Ytd

SELECT @TotCashMargin = @TotGrossMargin-@TotCashOpEx, @TotCashMargin_Ytd = @TotGrossMargin_Ytd-@TotCashOpEx_Ytd

SELECT UEdc = @UEdc,
	PurUtilUEdc = @PurUtilUEdc,
	RefUtilUEdc = @RefUtilUEdc,
	Edc = @Edc,
	UtilPcnt = @UtilPcnt,
	TotProcessUEdc = @TotProcessUEdc,
	TotProcessEdc = @TotProcessEdc,
	ProcessUtilPcnt = @ProcessUtilPcnt,
	MechUnavail_Act = @MechUnavail_Act,
	MechUnavailSlow_Act = @MechUnavailSlow_Act,
	OpUnavail_Act = @OpUnavail_Act,
	OpUnavailSlow_Act = @OpUnavailSlow_Act,
	OffStream_Act = @OffStream_Act,
	OffStreamSlow_Act = @OffStreamSlow_Act,
	ProcessUtilPcntCalc = @ProcessUtilPcntCalc,
	TotCashOpEx = @TotCashOpEx,
	TotCashOpEx_Ytd = @TotCashOpEx_Ytd,
	TotCashOpEx_Avg = @TotCashOpEx_Avg,
	TotCashOpExUEdc = @TotCashOpExUEdc,
	TotCashOpExUEdc_Ytd = @TotCashOpExUEdc_Ytd,
	TotCashOpExUedc_Avg = @TotCashOpExUedc_Avg,
	TotPurEnergy = @TotPurEnergy,
	TotPurEnergy_Ytd = @TotPurEnergy_Ytd,
	TotPurEnergy_Avg = @TotPurEnergy_Avg,
	TotPurEnergyUEdc = @TotPurEnergyUEdc,
	TotPurEnergyUEdc_Ytd = @TotPurEnergyUEdc_Ytd,
	TotPurEnergyUedc_Avg = @TotPurEnergyUedc_Avg,
	TotProdEnergy = @TotProdEnergy,
	TotProdEnergy_Ytd = @TotProdEnergy_Ytd,
	TotProdEnergy_Avg = @TotProdEnergy_Avg,
	TotProdEnergyUEdc = @TotProdEnergyUEdc,
	TotProdEnergyUEdc_Ytd = @TotProdEnergyUEdc_Ytd,
	TotProdEnergyUedc_Avg = @TotProdEnergyUedc_Avg,
	TotOthEnergy = @TotOthEnergy,
	TotOthEnergy_Ytd = @TotOthEnergy_Ytd,
	TotOthEnergy_Avg = @TotOthEnergy_Avg,
	TotOthEnergyUEdc = @TotOthEnergyUEdc,
	TotOthEnergyUEdc_Ytd = @TotOthEnergyUEdc_Ytd,
	TotOthEnergyUedc_Avg = @TotOthEnergyUedc_Avg,
	NEOpEx = @NEOpEx,
	NEOpEx_Ytd = @NEOpEx_Ytd,
	NEOpEx_Avg = @NEOpEx_Avg,
	NEOpExUEdc = @NEOpExUEdc,
	NEOpExUEdc_Ytd = @NEOpExUEdc_Ytd,
	NEOpExUedc_Avg = @NEOpExUedc_Avg,
	UEdc_Ytd = @UEdc_Ytd,
	Uedc_Avg = @Uedc_Avg,
	OCCPO_STH = @OCCPO_STH,
	OCCPO_OVT = @OCCPO_OVT,
	OCCPO_Contract = @OCCPO_Contract,
	OCCPO_GA = @OCCPO_GA,
	OCCPO_AbsHrs = @OCCPO_AbsHrs,
	OCCPO_TotWHr = @OCCPO_TotWHr,
	OCCPO_TotEqPEdc = @OCCPO_TotEqPEdc,
	OCCTAADJ_STH = @OCCTAADJ_STH,
	OCCTAADJ_OVT = @OCCTAADJ_OVT,
	OCCTAADJ_Contract = @OCCTAADJ_Contract,
	OCCTAADJ_GA = @OCCTAADJ_GA,
	OCCTAADJ_AbsHrs = @OCCTAADJ_AbsHrs,
	OCCTAADJ_TotWHr = @OCCTAADJ_TotWHr,
	OCCTAADJ_TotEqPEdc = @OCCTAADJ_TotEqPEdc,
	OCCMA_STH = @OCCMA_STH,
	OCCMA_OVT = @OCCMA_OVT,
	OCCMA_Contract = @OCCMA_Contract,
	OCCMA_GA = @OCCMA_GA,
	OCCMA_AbsHrs = @OCCMA_AbsHrs,
	OCCMA_TotWHr = @OCCMA_TotWHr,
	OCCMA_TotEqPEdc = @OCCMA_TotEqPEdc,
	OCCAS_STH = @OCCAS_STH,
	OCCAS_OVT = @OCCAS_OVT,
	OCCAS_Contract = @OCCAS_Contract,
	OCCAS_GA = @OCCAS_GA,
	OCCAS_AbsHrs = @OCCAS_AbsHrs,
	OCCAS_TotWHr = @OCCAS_TotWHr,
	OCCAS_TotEqPEdc = @OCCAS_TotEqPEdc,
	OCC_STH = @OCC_STH,
	OCC_OVT = @OCC_OVT,
	OCC_Contract = @OCC_Contract,
	OCC_GA = @OCC_GA,
	OCC_AbsHrs = @OCC_AbsHrs,
	OCC_TotWHr = @OCC_TotWHr,
	OCC_TotEqPEdc = @OCC_TotEqPEdc,
	MPSPO_STH = @MPSPO_STH,
	MPSPO_OVT = @MPSPO_OVT,
	MPSPO_Contract = @MPSPO_Contract,
	MPSPO_GA = @MPSPO_GA,
	MPSPO_AbsHrs = @MPSPO_AbsHrs,
	MPSPO_TotWHr = @MPSPO_TotWHr,
	MPSPO_TotEqPEdc = @MPSPO_TotEqPEdc,
	MPSTAADJ_STH = @MPSTAADJ_STH,
	MPSTAADJ_OVT = @MPSTAADJ_OVT,
	MPSTAADJ_Contract = @MPSTAADJ_Contract,
	MPSTAADJ_GA = @MPSTAADJ_GA,
	MPSTAADJ_AbsHrs = @MPSTAADJ_AbsHrs,
	MPSTAADJ_TotWHr = @MPSTAADJ_TotWHr,
	MPSTAADJ_TotEqPEdc = @MPSTAADJ_TotEqPEdc,
	MPSMA_STH = @MPSMA_STH,
	MPSMA_OVT = @MPSMA_OVT,
	MPSMA_Contract = @MPSMA_Contract,
	MPSMA_GA = @MPSMA_GA,
	MPSMA_AbsHrs = @MPSMA_AbsHrs,
	MPSMA_TotWHr = @MPSMA_TotWHr,
	MPSMA_TotEqPEdc = @MPSMA_TotEqPEdc,
	MPSTS_STH = @MPSTS_STH,
	MPSTS_OVT = @MPSTS_OVT,
	MPSTS_Contract = @MPSTS_Contract,
	MPSTS_GA = @MPSTS_GA,
	MPSTS_AbsHrs = @MPSTS_AbsHrs,
	MPSTS_TotWHr = @MPSTS_TotWHr,
	MPSTS_TotEqPEdc = @MPSTS_TotEqPEdc,
	MPSAS_STH = @MPSAS_STH,
	MPSAS_OVT = @MPSAS_OVT,
	MPSAS_Contract = @MPSAS_Contract,
	MPSAS_GA = @MPSAS_GA,
	MPSAS_AbsHrs = @MPSAS_AbsHrs,
	MPSAS_TotWHr = @MPSAS_TotWHr,
	MPSAS_TotEqPEdc = @MPSAS_TotEqPEdc,
	MPS_STH = @MPS_STH,
	MPS_OVT = @MPS_OVT,
	MPS_Contract = @MPS_Contract,
	MPS_GA = @MPS_GA,
	MPS_AbsHrs = @MPS_AbsHrs,
	MPS_TotWHr = @MPS_TotWHr,
	MPS_TotEqPEdc = @MPS_TotEqPEdc,
	AnnTACost = @AnnTACost,
	CurrRoutCost = @CurrRoutCost,
	TotMaintCost = @TotMaintCost,
	TAIndex = @TAIndex,
	RoutIndex = @RoutIndex,
	MaintIndex = @MaintIndex,
	AnnTACost_Ytd = @AnnTACost_Ytd,
	CurrRoutCost_Ytd = @CurrRoutCost_Ytd,
	TotMaintCost_Ytd = @TotMaintCost_Ytd,
	RoutIndex_Ytd = @RoutIndex_Ytd,
	MaintIndex_Ytd = @MaintIndex_Ytd,
	AnnTACost_Avg = @AnnTACost_Avg,
	CurrRoutCost_Avg = @CurrRoutCost_Avg,
	TotMaintCost_Avg = @TotMaintCost_Avg,
	RoutIndex_Avg = @RoutIndex_Avg,
	MaintIndex_Avg = @MaintIndex_Avg,
	Edc_Ytd = @Edc_Ytd,
	Edc_Avg = @Edc_Avg,
	RptSource_PUR = @RptSource_PUR,
	RptSource_PRO = @RptSource_PRO,
	RptSource_IN = @RptSource_IN,
	RptSource_OUT = @RptSource_OUT,
	RptSource_SOL = @RptSource_SOL,
	SourceMWH_PUR = @SourceMWH_PUR,
	SourceMWH_PRO = @SourceMWH_PRO,
	SourceMWH_IN = @SourceMWH_IN,
	SourceMWH_OUT = @SourceMWH_OUT,
	SourceMWH_SOL = @SourceMWH_SOL,
	TotEnergyConsMBTU = @TotEnergyConsMBTU,
	EnergyUseDay = @EnergyUseDay,
	ProcessStdEnergy = @ProcessStdEnergy,
	SensHeatStdEnergy = @SensHeatStdEnergy,
	OffsitesStdEnergy = @OffsitesStdEnergy,
	AspStdEnergy = @AspStdEnergy,
	TotStdEnergy = @TotStdEnergy,
	EII = @EII,
	RMI = @RMI,
	RMIPriceUS = @RMIPriceUS,
	RMICost = @RMICost,
	RMI_Ytd = @RMI_Ytd,
	RMIPriceUS_Ytd = @RMIPriceUS_Ytd,
	RMICost_Ytd = @RMICost_Ytd,
	OTHRM = @OTHRM,
	OTHRMPriceUS = @OTHRMPriceUS,
	OTHRMCost = @OTHRMCost,
	OTHRM_Ytd = @OTHRM_Ytd,
	OTHRMPriceUS_Ytd = @OTHRMPriceUS_Ytd,
	OTHRMCost_Ytd = @OTHRMCost_Ytd,
	RM = @RM,
	RMCost = @RMCost,
	RM_Ytd = @RM_Ytd,
	RMCost_Ytd = @RMCost_Ytd,
	NetInputBPD = @NetInputBPD,
	NetInputBPD_Ytd = @NetInputBPD_Ytd,
	PROD = @PROD,
	PRODPriceUS = @PRODPriceUS,
	PRODCost = @PRODCost,
	PROD_Ytd = @PROD_Ytd,
	PRODPriceUS_Ytd = @PRODPriceUS_Ytd,
	PRODCost_Ytd = @PRODCost_Ytd,
	RPF = @RPF,
	RPFPriceUS = @RPFPriceUS,
	RPFCost = @RPFCost,
	RPF_Ytd = @RPF_Ytd,
	RPFPriceUS_Ytd = @RPFPriceUS_Ytd,
	RPFCost_Ytd = @RPFCost_Ytd,
	ASP = @ASP,
	ASPPriceUS = @ASPPriceUS,
	ASPCost = @ASPCost,
	ASP_Ytd = @ASP_Ytd,
	ASPPriceUS_Ytd = @ASPPriceUS_Ytd,
	ASPCost_Ytd = @ASPCost_Ytd,
	SOLV = @SOLV,
	SOLVPriceUS = @SOLVPriceUS,
	SOLVCost = @SOLVCost,
	SOLV_Ytd = @SOLV_Ytd,
	SOLVPriceUS_Ytd = @SOLVPriceUS_Ytd,
	SOLVCost_Ytd = @SOLVCost_Ytd,
	COKE = @COKE,
	COKEPriceUS = @COKEPriceUS,
	COKECost = @COKECost,
	COKE_Ytd = @COKE_Ytd,
	COKEPriceUS_Ytd = @COKEPriceUS_Ytd,
	COKECost_Ytd = @COKECost_Ytd,
	MPROD = @MPROD,
	MPRODPriceUS = @MPRODPriceUS,
	MPRODCost = @MPRODCost,
	MPROD_Ytd = @MPROD_Ytd,
	MPRODPriceUS_Ytd = @MPRODPriceUS_Ytd,
	MPRODCost_Ytd = @MPRODCost_Ytd,
	Yield = @Yield,
	YIELDPriceUS = @YIELDPriceUS,
	YIELDCost = @YIELDCost,
	YIELD_Ytd = @YIELD_Ytd,
	YIELDPriceUS_Ytd = @YIELDPriceUS_Ytd,
	YIELDCost_Ytd = @YIELDCost_Ytd,
	GrossMargin = @GrossMargin,
	GrossMargin_Ytd = @GrossMargin_Ytd,
	TotGrossMargin = @TotGrossMargin,
	TotGrossMargin_Ytd = @TotGrossMargin_Ytd,
	TotCashOpExBbl = @TotCashOpExBbl,
	TotCashOpExBbl_Ytd = @TotCashOpExBbl_Ytd,
	TotCashMargin = @TotCashMargin,
	CashMargin = @CashMargin,
	TotCashMargin_Ytd = @TotCashMargin_Ytd,
	CashMargin_Ytd = @CashMargin_Ytd

