﻿



CREATE    PROC [dbo].[spReportChevronKPI_Avg] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
IF @RefineryID = '179EUR'
	RETURN 1
	
DECLARE 
	@BU_UtilPcnt_Avg real, @BU_KEdc_Avg real, @BU_KUedc_Avg real, 
	@BU_MechAvail_Avg real, @BU_KMAEdc_Avg real, @BU_KProcEdc_Avg real,
	@BU_EII_Avg real, @BU_KEnergyUseDay_Avg real, @BU_KTotStdEnergy_Avg real,
	@BU_MaintIndex_Avg real, @BU_TAIndex_Avg real, @BU_RoutIndex_Avg real,   
	@BU_TotEqPEdc_Avg real, 
	@BU_NEOpExUedc_Avg real, @BU_TotCashOpExUedc_Avg real,
	@BU_CashMargin_Avg real, @BU_NetInputBPD_Avg real
DECLARE 
	@CT_UtilPcnt_Avg real, @CT_KEdc_Avg real, @CT_KUedc_Avg real, 
	@CT_MechAvail_Avg real, @CT_KMAEdc_Avg real, @CT_KProcEdc_Avg real,
	@CT_EII_Avg real, @CT_KEnergyUseDay_Avg real, @CT_KTotStdEnergy_Avg real,
	@CT_MaintIndex_Avg real, @CT_TAIndex_Avg real, @CT_RoutIndex_Avg real,   
	@CT_TotEqPEdc_Avg real, 
	@CT_NEOpExUedc_Avg real, @CT_TotCashOpExUedc_Avg real,
	@CT_CashMargin_Avg real, @CT_NetInputBPD_Avg real
DECLARE 
	@ES_UtilPcnt_Avg real, @ES_KEdc_Avg real, @ES_KUedc_Avg real, 
	@ES_MechAvail_Avg real, @ES_KMAEdc_Avg real, @ES_KProcEdc_Avg real,
	@ES_EII_Avg real, @ES_KEnergyUseDay_Avg real, @ES_KTotStdEnergy_Avg real,
	@ES_MaintIndex_Avg real, @ES_TAIndex_Avg real, @ES_RoutIndex_Avg real,   
	@ES_TotEqPEdc_Avg real, 
	@ES_NEOpExUedc_Avg real, @ES_TotCashOpExUedc_Avg real,
	@ES_CashMargin_Avg real, @ES_NetInputBPD_Avg real
DECLARE 
	@HI_UtilPcnt_Avg real, @HI_KEdc_Avg real, @HI_KUedc_Avg real, 
	@HI_MechAvail_Avg real, @HI_KMAEdc_Avg real, @HI_KProcEdc_Avg real,
	@HI_EII_Avg real, @HI_KEnergyUseDay_Avg real, @HI_KTotStdEnergy_Avg real,
	@HI_MaintIndex_Avg real, @HI_TAIndex_Avg real, @HI_RoutIndex_Avg real,   
	@HI_TotEqPEdc_Avg real, 
	@HI_NEOpExUedc_Avg real, @HI_TotCashOpExUedc_Avg real,
	@HI_CashMargin_Avg real, @HI_NetInputBPD_Avg real
DECLARE 
	@PA_UtilPcnt_Avg real, @PA_KEdc_Avg real, @PA_KUedc_Avg real, 
	@PA_MechAvail_Avg real, @PA_KMAEdc_Avg real, @PA_KProcEdc_Avg real,
	@PA_EII_Avg real, @PA_KEnergyUseDay_Avg real, @PA_KTotStdEnergy_Avg real,
	@PA_MaintIndex_Avg real, @PA_TAIndex_Avg real, @PA_RoutIndex_Avg real,   
	@PA_TotEqPEdc_Avg real, 
	@PA_NEOpExUedc_Avg real, @PA_TotCashOpExUedc_Avg real,
	@PA_CashMargin_Avg real, @PA_NetInputBPD_Avg real
DECLARE 
	@RI_UtilPcnt_Avg real, @RI_KEdc_Avg real, @RI_KUedc_Avg real, 
	@RI_MechAvail_Avg real, @RI_KMAEdc_Avg real, @RI_KProcEdc_Avg real,
	@RI_EII_Avg real, @RI_KEnergyUseDay_Avg real, @RI_KTotStdEnergy_Avg real,
	@RI_MaintIndex_Avg real, @RI_TAIndex_Avg real, @RI_RoutIndex_Avg real,   
	@RI_TotEqPEdc_Avg real, 
	@RI_NEOpExUedc_Avg real, @RI_TotCashOpExUedc_Avg real,
	@RI_CashMargin_Avg real, @RI_NetInputBPD_Avg real
DECLARE 
	@SL_UtilPcnt_Avg real, @SL_KEdc_Avg real, @SL_KUedc_Avg real, 
	@SL_MechAvail_Avg real, @SL_KMAEdc_Avg real, @SL_KProcEdc_Avg real,
	@SL_EII_Avg real, @SL_KEnergyUseDay_Avg real, @SL_KTotStdEnergy_Avg real,
	@SL_MaintIndex_Avg real, @SL_TAIndex_Avg real, @SL_RoutIndex_Avg real,   
	@SL_TotEqPEdc_Avg real, 
	@SL_NEOpExUedc_Avg real, @SL_TotCashOpExUedc_Avg real,
	@SL_CashMargin_Avg real, @SL_NetInputBPD_Avg real

DECLARE 
	@CHEVRON_UtilPcnt_Avg real, @CHEVRON_KEdc_Avg real, @CHEVRON_KUedc_Avg real, 
	@CHEVRON_MechAvail_Avg real, @CHEVRON_KMAEdc_Avg real, @CHEVRON_KProcEdc_Avg real,
	@CHEVRON_EII_Avg real, @CHEVRON_KEnergyUseDay_Avg real, @CHEVRON_KTotStdEnergy_Avg real,
	@CHEVRON_MaintIndex_Avg real, @CHEVRON_TAIndex_Avg real, @CHEVRON_RoutIndex_Avg real,   
	@CHEVRON_TotEqPEdc_Avg real, 
	@CHEVRON_NEOpExUedc_Avg real, @CHEVRON_TotCashOpExUedc_Avg real,
	@CHEVRON_CashMargin_Avg real, @CHEVRON_NetInputBPD_Avg real

EXEC [dbo].[spReportChevronKPI_Avg_REF] '162NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@BU_UtilPcnt_Avg OUTPUT, @KEdc_Avg=@BU_KEdc_Avg OUTPUT, @KUedc_Avg=@BU_KUedc_Avg OUTPUT, 
	@MechAvail_Avg=@BU_MechAvail_Avg OUTPUT, @KMAEdc_Avg=@BU_KMAEdc_Avg OUTPUT, @KProcEdc_Avg=@BU_KProcEdc_Avg OUTPUT,
	@EII_Avg=@BU_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@BU_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@BU_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@BU_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@BU_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@BU_RoutIndex_Avg OUTPUT,   
	@TotEqPEdc_Avg=@BU_TotEqPEdc_Avg OUTPUT, 
	@NEOpExUedc_Avg=@BU_NEOpExUedc_Avg OUTPUT, @TotCashOpExUedc_Avg=@BU_TotCashOpExUedc_Avg OUTPUT,
	@CashMargin_Avg=@BU_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@BU_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '53PAC', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@CT_UtilPcnt_Avg OUTPUT, @KEdc_Avg=@CT_KEdc_Avg OUTPUT, @KUedc_Avg=@CT_KUedc_Avg OUTPUT, 
	@MechAvail_Avg=@CT_MechAvail_Avg OUTPUT, @KMAEdc_Avg=@CT_KMAEdc_Avg OUTPUT, @KProcEdc_Avg=@CT_KProcEdc_Avg OUTPUT,
	@EII_Avg=@CT_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@CT_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@CT_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@CT_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@CT_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@CT_RoutIndex_Avg OUTPUT,   
	@TotEqPEdc_Avg=@CT_TotEqPEdc_Avg OUTPUT, 
	@NEOpExUedc_Avg=@CT_NEOpExUedc_Avg OUTPUT, @TotCashOpExUedc_Avg=@CT_TotCashOpExUedc_Avg OUTPUT,
	@CashMargin_Avg=@CT_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@CT_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '15NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@ES_UtilPcnt_Avg OUTPUT, @KEdc_Avg=@ES_KEdc_Avg OUTPUT, @KUedc_Avg=@ES_KUedc_Avg OUTPUT, 
	@MechAvail_Avg=@ES_MechAvail_Avg OUTPUT, @KMAEdc_Avg=@ES_KMAEdc_Avg OUTPUT, @KProcEdc_Avg=@ES_KProcEdc_Avg OUTPUT,
	@EII_Avg=@ES_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@ES_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@ES_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@ES_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@ES_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@ES_RoutIndex_Avg OUTPUT,   
	@TotEqPEdc_Avg=@ES_TotEqPEdc_Avg OUTPUT, 
	@NEOpExUedc_Avg=@ES_NEOpExUedc_Avg OUTPUT, @TotCashOpExUedc_Avg=@ES_TotCashOpExUedc_Avg OUTPUT,
	@CashMargin_Avg=@ES_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@ES_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '16NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@HI_UtilPcnt_Avg OUTPUT, @KEdc_Avg=@HI_KEdc_Avg OUTPUT, @KUedc_Avg=@HI_KUedc_Avg OUTPUT, 
	@MechAvail_Avg=@HI_MechAvail_Avg OUTPUT, @KMAEdc_Avg=@HI_KMAEdc_Avg OUTPUT, @KProcEdc_Avg=@HI_KProcEdc_Avg OUTPUT,
	@EII_Avg=@HI_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@HI_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@HI_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@HI_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@HI_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@HI_RoutIndex_Avg OUTPUT,   
	@TotEqPEdc_Avg=@HI_TotEqPEdc_Avg OUTPUT, 
	@NEOpExUedc_Avg=@HI_NEOpExUedc_Avg OUTPUT, @TotCashOpExUedc_Avg=@HI_TotCashOpExUedc_Avg OUTPUT,
	@CashMargin_Avg=@HI_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@HI_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '17NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@PA_UtilPcnt_Avg OUTPUT, @KEdc_Avg=@PA_KEdc_Avg OUTPUT, @KUedc_Avg=@PA_KUedc_Avg OUTPUT, 
	@MechAvail_Avg=@PA_MechAvail_Avg OUTPUT, @KMAEdc_Avg=@PA_KMAEdc_Avg OUTPUT, @KProcEdc_Avg=@PA_KProcEdc_Avg OUTPUT,
	@EII_Avg=@PA_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@PA_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@PA_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@PA_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@PA_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@PA_RoutIndex_Avg OUTPUT,   
	@TotEqPEdc_Avg=@PA_TotEqPEdc_Avg OUTPUT, 
	@NEOpExUedc_Avg=@PA_NEOpExUedc_Avg OUTPUT, @TotCashOpExUedc_Avg=@PA_TotCashOpExUedc_Avg OUTPUT,
	@CashMargin_Avg=@PA_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@PA_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '79FL', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@RI_UtilPcnt_Avg OUTPUT, @KEdc_Avg=@RI_KEdc_Avg OUTPUT, @KUedc_Avg=@RI_KUedc_Avg OUTPUT, 
	@MechAvail_Avg=@RI_MechAvail_Avg OUTPUT, @KMAEdc_Avg=@RI_KMAEdc_Avg OUTPUT, @KProcEdc_Avg=@RI_KProcEdc_Avg OUTPUT,
	@EII_Avg=@RI_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@RI_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@RI_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@RI_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@RI_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@RI_RoutIndex_Avg OUTPUT,   
	@TotEqPEdc_Avg=@RI_TotEqPEdc_Avg OUTPUT, 
	@NEOpExUedc_Avg=@RI_NEOpExUedc_Avg OUTPUT, @TotCashOpExUedc_Avg=@RI_TotCashOpExUedc_Avg OUTPUT,
	@CashMargin_Avg=@RI_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@RI_NetInputBPD_Avg OUTPUT

EXEC [dbo].[spReportChevronKPI_Avg_REF] '21NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@UtilPcnt_Avg=@SL_UtilPcnt_Avg OUTPUT, @KEdc_Avg=@SL_KEdc_Avg OUTPUT, @KUedc_Avg=@SL_KUedc_Avg OUTPUT, 
	@MechAvail_Avg=@SL_MechAvail_Avg OUTPUT, @KMAEdc_Avg=@SL_KMAEdc_Avg OUTPUT, @KProcEdc_Avg=@SL_KProcEdc_Avg OUTPUT,
	@EII_Avg=@SL_EII_Avg OUTPUT, @KEnergyUseDay_Avg=@SL_KEnergyUseDay_Avg OUTPUT, @KTotStdEnergy_Avg=@SL_KTotStdEnergy_Avg OUTPUT,
	@MaintIndex_Avg=@SL_MaintIndex_Avg OUTPUT, @TAIndex_Avg=@SL_TAIndex_Avg OUTPUT, @RoutIndex_Avg=@SL_RoutIndex_Avg OUTPUT,   
	@TotEqPEdc_Avg=@SL_TotEqPEdc_Avg OUTPUT, 
	@NEOpExUedc_Avg=@SL_NEOpExUedc_Avg OUTPUT, @TotCashOpExUedc_Avg=@SL_TotCashOpExUedc_Avg OUTPUT,
	@CashMargin_Avg=@SL_CashMargin_Avg OUTPUT, @NetInputBPD_Avg=@SL_NetInputBPD_Avg OUTPUT


SELECT	@CHEVRON_KEdc_Avg = ISNULL(@BU_KEdc_Avg,0) + ISNULL(@CT_KEdc_Avg,0) + ISNULL(@ES_KEdc_Avg,0) + ISNULL(@HI_KEdc_Avg,0) 
						  + ISNULL(@PA_KEdc_Avg,0) + ISNULL(@RI_KEdc_Avg,0) + ISNULL(@SL_KEdc_Avg,0),
		@CHEVRON_KUedc_Avg = ISNULL(@BU_KUedc_Avg,0) + ISNULL(@CT_KUedc_Avg,0) + ISNULL(@ES_KUedc_Avg,0) + ISNULL(@HI_KUedc_Avg,0) 
						   + ISNULL(@PA_KUedc_Avg,0) + ISNULL(@RI_KUedc_Avg,0) + ISNULL(@SL_KUedc_Avg,0),
		@CHEVRON_KMAEdc_Avg = ISNULL(@BU_KMAEdc_Avg,0) + ISNULL(@CT_KMAEdc_Avg,0) + ISNULL(@ES_KMAEdc_Avg,0) + ISNULL(@HI_KMAEdc_Avg,0) 
							+ ISNULL(@PA_KMAEdc_Avg,0) + ISNULL(@RI_KMAEdc_Avg,0) + ISNULL(@SL_KMAEdc_Avg,0),
		@CHEVRON_KProcEdc_Avg = ISNULL(@BU_KProcEdc_Avg,0) + ISNULL(@CT_KProcEdc_Avg,0) + ISNULL(@ES_KProcEdc_Avg,0) + ISNULL(@HI_KProcEdc_Avg,0) 
							  + ISNULL(@PA_KProcEdc_Avg,0) + ISNULL(@RI_KProcEdc_Avg,0) + ISNULL(@SL_KProcEdc_Avg,0),
		@CHEVRON_NetInputBPD_Avg = ISNULL(@BU_NetInputBPD_Avg,0) + ISNULL(@CT_NetInputBPD_Avg,0) + ISNULL(@ES_NetInputBPD_Avg,0) + ISNULL(@HI_NetInputBPD_Avg,0) 
								 + ISNULL(@PA_NetInputBPD_Avg,0) + ISNULL(@RI_NetInputBPD_Avg,0) + ISNULL(@SL_NetInputBPD_Avg,0),
		@CHEVRON_KEnergyUseDay_Avg = ISNULL(@BU_KEnergyUseDay_Avg,0) + ISNULL(@CT_KEnergyUseDay_Avg,0) + ISNULL(@ES_KEnergyUseDay_Avg,0) + ISNULL(@HI_KEnergyUseDay_Avg,0) 
								   + ISNULL(@PA_KEnergyUseDay_Avg,0) + ISNULL(@RI_KEnergyUseDay_Avg,0) + ISNULL(@SL_KEnergyUseDay_Avg,0),
		@CHEVRON_KTotStdEnergy_Avg = ISNULL(@BU_KTotStdEnergy_Avg,0) + ISNULL(@CT_KTotStdEnergy_Avg,0) + ISNULL(@ES_KTotStdEnergy_Avg,0) + ISNULL(@HI_KTotStdEnergy_Avg,0) 
								   + ISNULL(@PA_KTotStdEnergy_Avg,0) + ISNULL(@RI_KTotStdEnergy_Avg,0) + ISNULL(@SL_KTotStdEnergy_Avg,0)

IF @CHEVRON_KEdc_Avg > 0
	SELECT	@CHEVRON_UtilPcnt_Avg = 100.0 * @CHEVRON_KUedc_Avg / @CHEVRON_KEdc_Avg,
			@CHEVRON_MaintIndex_Avg = (ISNULL(@BU_KEdc_Avg,0)*ISNULL(@BU_MaintIndex_Avg,0) + ISNULL(@CT_KEdc_Avg,0)*ISNULL(@CT_MaintIndex_Avg,0) + ISNULL(@ES_KEdc_Avg,0)*ISNULL(@ES_MaintIndex_Avg,0) + ISNULL(@HI_KEdc_Avg,0)*ISNULL(@HI_MaintIndex_Avg,0) 
			   						 + ISNULL(@PA_KEdc_Avg,0)*ISNULL(@PA_MaintIndex_Avg,0) + ISNULL(@RI_KEdc_Avg,0)*ISNULL(@RI_MaintIndex_Avg,0) + ISNULL(@SL_KEdc_Avg,0)*ISNULL(@SL_MaintIndex_Avg,0))/@CHEVRON_KEdc_Avg,
			@CHEVRON_TAIndex_Avg = (ISNULL(@BU_KEdc_Avg,0)*ISNULL(@BU_TAIndex_Avg,0) + ISNULL(@CT_KEdc_Avg,0)*ISNULL(@CT_TAIndex_Avg,0) + ISNULL(@ES_KEdc_Avg,0)*ISNULL(@ES_TAIndex_Avg,0) + ISNULL(@HI_KEdc_Avg,0)*ISNULL(@HI_TAIndex_Avg,0) 
								      + ISNULL(@PA_KEdc_Avg,0)*ISNULL(@PA_TAIndex_Avg,0) + ISNULL(@RI_KEdc_Avg,0)*ISNULL(@RI_TAIndex_Avg,0) + ISNULL(@SL_KEdc_Avg,0)*ISNULL(@SL_TAIndex_Avg,0))/@CHEVRON_KEdc_Avg,
			@CHEVRON_RoutIndex_Avg = (ISNULL(@BU_KEdc_Avg,0)*ISNULL(@BU_RoutIndex_Avg,0) + ISNULL(@CT_KEdc_Avg,0)*ISNULL(@CT_RoutIndex_Avg,0) + ISNULL(@ES_KEdc_Avg,0)*ISNULL(@ES_RoutIndex_Avg,0) + ISNULL(@HI_KEdc_Avg,0)*ISNULL(@HI_RoutIndex_Avg,0) 
								    + ISNULL(@PA_KEdc_Avg,0)*ISNULL(@PA_RoutIndex_Avg,0) + ISNULL(@RI_KEdc_Avg,0)*ISNULL(@RI_RoutIndex_Avg,0) + ISNULL(@SL_KEdc_Avg,0)*ISNULL(@SL_RoutIndex_Avg,0))/@CHEVRON_KEdc_Avg,
			@CHEVRON_TotEqPEdc_Avg = (ISNULL(@BU_KEdc_Avg,0)*ISNULL(@BU_TotEqPEdc_Avg,0) + ISNULL(@CT_KEdc_Avg,0)*ISNULL(@CT_TotEqPEdc_Avg,0) + ISNULL(@ES_KEdc_Avg,0)*ISNULL(@ES_TotEqPEdc_Avg,0) + ISNULL(@HI_KEdc_Avg,0)*ISNULL(@HI_TotEqPEdc_Avg,0) 
									+ ISNULL(@PA_KEdc_Avg,0)*ISNULL(@PA_TotEqPEdc_Avg,0) + ISNULL(@RI_KEdc_Avg,0)*ISNULL(@RI_TotEqPEdc_Avg,0) + ISNULL(@SL_KEdc_Avg,0)*ISNULL(@SL_TotEqPEdc_Avg,0))/@CHEVRON_KEdc_Avg

IF @CHEVRON_KUedc_Avg > 0
	SELECT	@CHEVRON_NEOpExUedc_Avg = (ISNULL(@BU_KUedc_Avg,0)*ISNULL(@BU_NEOpExUedc_Avg,0) + ISNULL(@CT_KUedc_Avg,0)*ISNULL(@CT_NEOpExUedc_Avg,0) + ISNULL(@ES_KUedc_Avg,0)*ISNULL(@ES_NEOpExUedc_Avg,0) + ISNULL(@HI_KUedc_Avg,0)*ISNULL(@HI_NEOpExUedc_Avg,0) 
								     + ISNULL(@PA_KUedc_Avg,0)*ISNULL(@PA_NEOpExUedc_Avg,0) + ISNULL(@RI_KUedc_Avg,0)*ISNULL(@RI_NEOpExUedc_Avg,0) + ISNULL(@SL_KUedc_Avg,0)*ISNULL(@SL_NEOpExUedc_Avg,0))/@CHEVRON_KUedc_Avg,
			@CHEVRON_TotCashOpExUedc_Avg = (ISNULL(@BU_KUedc_Avg,0)*ISNULL(@BU_TotCashOpExUedc_Avg,0) + ISNULL(@CT_KUedc_Avg,0)*ISNULL(@CT_TotCashOpExUedc_Avg,0) + ISNULL(@ES_KUedc_Avg,0)*ISNULL(@ES_TotCashOpExUedc_Avg,0) + ISNULL(@HI_KUedc_Avg,0)*ISNULL(@HI_TotCashOpExUedc_Avg,0) 
									      + ISNULL(@PA_KUedc_Avg,0)*ISNULL(@PA_TotCashOpExUedc_Avg,0) + ISNULL(@RI_KUedc_Avg,0)*ISNULL(@RI_TotCashOpExUedc_Avg,0) + ISNULL(@SL_KUedc_Avg,0)*ISNULL(@SL_TotCashOpExUedc_Avg,0))/@CHEVRON_KUedc_Avg

IF @CHEVRON_KProcEdc_Avg > 0
	SELECT	@CHEVRON_MechAvail_Avg = 100.0 * @CHEVRON_KMAEdc_Avg / @CHEVRON_KProcEdc_Avg

IF @CHEVRON_KTotStdEnergy_Avg > 0
	SELECT	@CHEVRON_EII_Avg = 100.0 * @CHEVRON_KEnergyUseDay_Avg / @CHEVRON_KTotStdEnergy_Avg

IF @CHEVRON_NetInputBPD_Avg > 0
	SELECT	@CHEVRON_CashMargin_Avg = (ISNULL(@BU_NetInputBPD_Avg,0)*ISNULL(@BU_CashMargin_Avg,0) + ISNULL(@CT_NetInputBPD_Avg,0)*ISNULL(@CT_CashMargin_Avg,0) + ISNULL(@ES_NetInputBPD_Avg,0)*ISNULL(@ES_CashMargin_Avg,0) + ISNULL(@HI_NetInputBPD_Avg,0)*ISNULL(@HI_CashMargin_Avg,0) 
									 + ISNULL(@PA_NetInputBPD_Avg,0)*ISNULL(@PA_CashMargin_Avg,0) + ISNULL(@RI_NetInputBPD_Avg,0)*ISNULL(@RI_CashMargin_Avg,0) + ISNULL(@SL_NetInputBPD_Avg,0)*ISNULL(@SL_CashMargin_Avg,0))/@CHEVRON_NetInputBPD_Avg

SET NOCOUNT OFF

SELECT  ReportPeriod = CAST(CAST(@PeriodMonth as varchar(2)) + '/1/' + CAST(@PeriodYear as varchar(4)) as smalldatetime),
	BU_UtilPcnt_Avg=@BU_UtilPcnt_Avg, BU_KEdc_Avg=@BU_KEdc_Avg, BU_KUedc_Avg=@BU_KUedc_Avg, 
	BU_MechAvail_Avg=@BU_MechAvail_Avg, BU_KMAEdc_Avg=@BU_KMAEdc_Avg, BU_KProcEdc_Avg=@BU_KProcEdc_Avg,
	BU_EII_Avg=@BU_EII_Avg, BU_KEnergyUseDay_Avg=@BU_KEnergyUseDay_Avg, BU_KTotStdEnergy_Avg=@BU_KTotStdEnergy_Avg,
	BU_MaintIndex_Avg=@BU_MaintIndex_Avg, BU_TAIndex_Avg=@BU_TAIndex_Avg, BU_RoutIndex_Avg=@BU_RoutIndex_Avg,   
	BU_TotEqPEdc_Avg=@BU_TotEqPEdc_Avg, 
	BU_NEOpExUedc_Avg=@BU_NEOpExUedc_Avg, BU_TotCashOpExUedc_Avg=@BU_TotCashOpExUedc_Avg,
	BU_CashMargin_Avg=@BU_CashMargin_Avg, BU_NetInputBPD_Avg=@BU_NetInputBPD_Avg,

	CT_UtilPcnt_Avg=@CT_UtilPcnt_Avg, CT_KEdc_Avg=@CT_KEdc_Avg, CT_KUedc_Avg=@CT_KUedc_Avg, 
	CT_MechAvail_Avg=@CT_MechAvail_Avg, CT_KMAEdc_Avg=@CT_KMAEdc_Avg, CT_KProcEdc_Avg=@CT_KProcEdc_Avg,
	CT_EII_Avg=@CT_EII_Avg, CT_KEnergyUseDay_Avg=@CT_KEnergyUseDay_Avg, CT_KTotStdEnergy_Avg=@CT_KTotStdEnergy_Avg,
	CT_MaintIndex_Avg=@CT_MaintIndex_Avg, CT_TAIndex_Avg=@CT_TAIndex_Avg, CT_RoutIndex_Avg=@CT_RoutIndex_Avg,   
	CT_TotEqPEdc_Avg=@CT_TotEqPEdc_Avg, 
	CT_NEOpExUedc_Avg=@CT_NEOpExUedc_Avg, CT_TotCashOpExUedc_Avg=@CT_TotCashOpExUedc_Avg,
	CT_CashMargin_Avg=@CT_CashMargin_Avg, CT_NetInputBPD_Avg=@CT_NetInputBPD_Avg,

	ES_UtilPcnt_Avg=@ES_UtilPcnt_Avg, ES_KEdc_Avg=@ES_KEdc_Avg, ES_KUedc_Avg=@ES_KUedc_Avg, 
	ES_MechAvail_Avg=@ES_MechAvail_Avg, ES_KMAEdc_Avg=@ES_KMAEdc_Avg, ES_KProcEdc_Avg=@ES_KProcEdc_Avg,
	ES_EII_Avg=@ES_EII_Avg, ES_KEnergyUseDay_Avg=@ES_KEnergyUseDay_Avg, ES_KTotStdEnergy_Avg=@ES_KTotStdEnergy_Avg,
	ES_MaintIndex_Avg=@ES_MaintIndex_Avg, ES_TAIndex_Avg=@ES_TAIndex_Avg, ES_RoutIndex_Avg=@ES_RoutIndex_Avg,   
	ES_TotEqPEdc_Avg=@ES_TotEqPEdc_Avg, 
	ES_NEOpExUedc_Avg=@ES_NEOpExUedc_Avg, ES_TotCashOpExUedc_Avg=@ES_TotCashOpExUedc_Avg,
	ES_CashMargin_Avg=@ES_CashMargin_Avg, ES_NetInputBPD_Avg=@ES_NetInputBPD_Avg,

	HI_UtilPcnt_Avg=@HI_UtilPcnt_Avg, HI_KEdc_Avg=@HI_KEdc_Avg, HI_KUedc_Avg=@HI_KUedc_Avg, 
	HI_MechAvail_Avg=@HI_MechAvail_Avg, HI_KMAEdc_Avg=@HI_KMAEdc_Avg, HI_KProcEdc_Avg=@HI_KProcEdc_Avg,
	HI_EII_Avg=@HI_EII_Avg, HI_KEnergyUseDay_Avg=@HI_KEnergyUseDay_Avg, HI_KTotStdEnergy_Avg=@HI_KTotStdEnergy_Avg,
	HI_MaintIndex_Avg=@HI_MaintIndex_Avg, HI_TAIndex_Avg=@HI_TAIndex_Avg, HI_RoutIndex_Avg=@HI_RoutIndex_Avg,   
	HI_TotEqPEdc_Avg=@HI_TotEqPEdc_Avg, 
	HI_NEOpExUedc_Avg=@HI_NEOpExUedc_Avg, HI_TotCashOpExUedc_Avg=@HI_TotCashOpExUedc_Avg,
	HI_CashMargin_Avg=@HI_CashMargin_Avg, HI_NetInputBPD_Avg=@HI_NetInputBPD_Avg,

	PA_UtilPcnt_Avg=@PA_UtilPcnt_Avg, PA_KEdc_Avg=@PA_KEdc_Avg, PA_KUedc_Avg=@PA_KUedc_Avg, 
	PA_MechAvail_Avg=@PA_MechAvail_Avg, PA_KMAEdc_Avg=@PA_KMAEdc_Avg, PA_KProcEdc_Avg=@PA_KProcEdc_Avg,
	PA_EII_Avg=@PA_EII_Avg, PA_KEnergyUseDay_Avg=@PA_KEnergyUseDay_Avg, PA_KTotStdEnergy_Avg=@PA_KTotStdEnergy_Avg,
	PA_MaintIndex_Avg=@PA_MaintIndex_Avg, PA_TAIndex_Avg=@PA_TAIndex_Avg, PA_RoutIndex_Avg=@PA_RoutIndex_Avg,   
	PA_TotEqPEdc_Avg=@PA_TotEqPEdc_Avg, 
	PA_NEOpExUedc_Avg=@PA_NEOpExUedc_Avg, PA_TotCashOpExUedc_Avg=@PA_TotCashOpExUedc_Avg,
	PA_CashMargin_Avg=@PA_CashMargin_Avg, PA_NetInputBPD_Avg=@PA_NetInputBPD_Avg,

	RI_UtilPcnt_Avg=@RI_UtilPcnt_Avg, RI_KEdc_Avg=@RI_KEdc_Avg, RI_KUedc_Avg=@RI_KUedc_Avg, 
	RI_MechAvail_Avg=@RI_MechAvail_Avg, RI_KMAEdc_Avg=@RI_KMAEdc_Avg, RI_KProcEdc_Avg=@RI_KProcEdc_Avg,
	RI_EII_Avg=@RI_EII_Avg, RI_KEnergyUseDay_Avg=@RI_KEnergyUseDay_Avg, RI_KTotStdEnergy_Avg=@RI_KTotStdEnergy_Avg,
	RI_MaintIndex_Avg=@RI_MaintIndex_Avg, RI_TAIndex_Avg=@RI_TAIndex_Avg, RI_RoutIndex_Avg=@RI_RoutIndex_Avg,   
	RI_TotEqPEdc_Avg=@RI_TotEqPEdc_Avg, 
	RI_NEOpExUedc_Avg=@RI_NEOpExUedc_Avg, RI_TotCashOpExUedc_Avg=@RI_TotCashOpExUedc_Avg,
	RI_CashMargin_Avg=@RI_CashMargin_Avg, RI_NetInputBPD_Avg=@RI_NetInputBPD_Avg,

	SL_UtilPcnt_Avg=@SL_UtilPcnt_Avg, SL_KEdc_Avg=@SL_KEdc_Avg, SL_KUedc_Avg=@SL_KUedc_Avg, 
	SL_MechAvail_Avg=@SL_MechAvail_Avg, SL_KMAEdc_Avg=@SL_KMAEdc_Avg, SL_KProcEdc_Avg=@SL_KProcEdc_Avg,
	SL_EII_Avg=@SL_EII_Avg, SL_KEnergyUseDay_Avg=@SL_KEnergyUseDay_Avg, SL_KTotStdEnergy_Avg=@SL_KTotStdEnergy_Avg,
	SL_MaintIndex_Avg=@SL_MaintIndex_Avg, SL_TAIndex_Avg=@SL_TAIndex_Avg, SL_RoutIndex_Avg=@SL_RoutIndex_Avg,   
	SL_TotEqPEdc_Avg=@SL_TotEqPEdc_Avg, 
	SL_NEOpExUedc_Avg=@SL_NEOpExUedc_Avg, SL_TotCashOpExUedc_Avg=@SL_TotCashOpExUedc_Avg,
	SL_CashMargin_Avg=@SL_CashMargin_Avg, SL_NetInputBPD_Avg=@SL_NetInputBPD_Avg,

	CHEVRON_UtilPcnt_Avg=@CHEVRON_UtilPcnt_Avg, CHEVRON_KEdc_Avg=@CHEVRON_KEdc_Avg, CHEVRON_KUedc_Avg=@CHEVRON_KUedc_Avg, 
	CHEVRON_MechAvail_Avg=@CHEVRON_MechAvail_Avg, CHEVRON_KMAEdc_Avg=@CHEVRON_KMAEdc_Avg, CHEVRON_KProcEdc_Avg=@CHEVRON_KProcEdc_Avg,
	CHEVRON_EII_Avg=@CHEVRON_EII_Avg, CHEVRON_KEnergyUseDay_Avg=@CHEVRON_KEnergyUseDay_Avg, CHEVRON_KTotStdEnergy_Avg=@CHEVRON_KTotStdEnergy_Avg,
	CHEVRON_MaintIndex_Avg=@CHEVRON_MaintIndex_Avg, CHEVRON_TAIndex_Avg=@CHEVRON_TAIndex_Avg, CHEVRON_RoutIndex_Avg=@CHEVRON_RoutIndex_Avg,   
	CHEVRON_TotEqPEdc_Avg=@CHEVRON_TotEqPEdc_Avg, 
	CHEVRON_NEOpExUedc_Avg=@CHEVRON_NEOpExUedc_Avg, CHEVRON_TotCashOpExUedc_Avg=@CHEVRON_TotCashOpExUedc_Avg,
	CHEVRON_CashMargin_Avg=@CHEVRON_CashMargin_Avg, CHEVRON_NetInputBPD_Avg=@CHEVRON_NetInputBPD_Avg


