﻿

CREATE   PROC [dbo].[spProfileLiteKPIData] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListQTR dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)


DECLARE	
	@EII real, @EII_Ytd real, @EII_Avg real, 
	@EnergyUseDay real, @EnergyUseDay_Ytd real, @EnergyUseDay_Avg real, 
	@TotStdEnergy real, @TotStdEnergy_Ytd real, @TotStdEnergy_Avg real, 

	@RefUtilPcnt real, @RefUtilPcnt_Ytd real, @RefUtilPcnt_Avg real, 
	@Edc real, @Edc_Ytd real, @Edc_Avg real, 

	@ProcessEffIndex real, @ProcessEffIndex_Ytd real, @ProcessEffIndex_Avg real, 

	@VEI real, @VEI_Ytd real, @VEI_Avg real, 
	@ReportLossGain real, @ReportLossGain_Ytd real, @ReportLossGain_Avg real, 
	@EstGain real, @EstGain_Ytd real, @EstGain_Avg real, 
	@NetInputBPD real, @NetInputBPD_Ytd real, @NetInputBPD_Avg real,
	
	@OpAvail real, @OpAvail_Ytd real, @OpAvail_Avg real, 
	@MechUnavailTA real, @MechUnavailTA_Ytd real, @MechUnavailTA_Avg real, 
	@NonTAUnavail real, @NonTAUnavail_Ytd real, @NonTAUnavail_Avg real, 

	@PersIndex real, @PersIndex_Ytd real, @PersIndex_Avg real, 
	@AnnTAWhr real, @AnnTAWhr_Ytd real, @AnnTAWhr_Avg real,
	@NonTAWHr real, @NonTAWHr_Ytd real, @NonTAWHr_Avg real,
	@TAWHrEdc real,
	
	@MaintIndex real, @MaintIndex_Ytd real, @MaintIndex_Avg real, 
	@RoutCost real, @RoutCost_Ytd real, @RoutCost_Avg real, 
	@AnnTACost real, @AnnTACost_Ytd real, @AnnTACost_Avg real,

	@NEOpExEdc real, @NEOpExEdc_Ytd real, @NEOpExEdc_Avg real, 
	@NEOpEx real, @NEOpEx_Ytd real, @NEOpEx_Avg real, 

	@OpExUEdc real, @OpExUEdc_Ytd real, @OpExUedc_Avg real,
	@EnergyCost real, @EnergyCost_Ytd real, @EnergyCost_Avg real, 
	@TAAdj real, @TAAdj_Ytd real, @TAAdj_Avg real,
	@TotCashOpEx real, @TotCashOpEx_Ytd real, @TotCashOpEx_Avg real,
	@UEdc real, @UEdc_Ytd real, @Uedc_Avg real
	
SELECT @EII = NULL, @EII_Ytd = NULL, @EII_Avg = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_Ytd = NULL, @EnergyUseDay_Avg = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_Ytd = NULL, @TotStdEnergy_Avg = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_Ytd = NULL, @RefUtilPcnt_Avg = NULL, 
	@Edc = NULL, @Edc_Ytd = NULL, @Edc_Avg = NULL, 
	@UEdc = NULL, @UEdc_Ytd = NULL, @Uedc_Avg = NULL, 
	@VEI = NULL, @VEI_Ytd = NULL, @VEI_Avg = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_Ytd = NULL, @ReportLossGain_Avg = NULL, 
	@EstGain = NULL, @EstGain_Ytd = NULL, @EstGain_Avg = NULL, 
	@OpAvail = NULL, @OpAvail_Ytd = NULL, @OpAvail_Avg = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_Ytd = NULL, @MechUnavailTA_Avg = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_Ytd = NULL, @NonTAUnavail_Avg = NULL, 
	@MaintIndex = NULL, @MaintIndex_Ytd = NULL, @MaintIndex_Avg = NULL,
	@RoutCost = NULL, @RoutCost_Ytd = NULL, @RoutCost_Avg = NULL, 
	@PersIndex = NULL, @PersIndex_Ytd = NULL, @PersIndex_Avg = NULL, 
	@AnnTAWhr = NULL, @AnnTAWhr_Ytd = NULL, @AnnTAWhr_Avg = NULL,
	@NonTAWHr = NULL, @NonTAWHr_Ytd = NULL, @NonTAWHr_Avg = NULL,
	@NEOpExEdc = NULL, @NEOpExEdc_Ytd = NULL, @NEOpExEdc_Avg = NULL, 
	@NEOpEx = NULL, @NEOpEx_Ytd = NULL, @NEOpEx_Avg = NULL, 
	@OpExUEdc = NULL, @OpExUEdc_Ytd = NULL, @OpExUedc_Avg = NULL, 
	@TAAdj = NULL, @TAAdj_Ytd = NULL, @TAAdj_Avg = NULL,
	@EnergyCost = NULL, @EnergyCost_Ytd = NULL, @EnergyCost_Avg = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_Ytd = NULL, @TotCashOpEx_Avg = NULL

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

SELECT @EII = EII, @EnergyUseDay = EnergyUseDay, @TotStdEnergy = TotStdEnergy
	, @RefUtilPcnt = RefUtilPcnt, @Edc = Edc, @UEdc = UEdc
	, @VEI = VEI, @ReportLossGain = ReportLossGain, @EstGain = EstGain
	, @ProcessEffIndex = ProcessEffIndex, @NetInputBPD = NetInputBPD
	, @OpAvail = OpAvail, @MechUnavailTA = MechUnavailTA, @NonTAUnavail = MechUnavailRout + RegUnavail
	, @MaintIndex = MaintIndex, @RoutCost = RoutCost, @AnnTACost = AnnTACost
	, @PersIndex = PersIndex, @AnnTAWhr = NULL, @NonTAWHr = NULL
	, @NEOpExEdc = NEOpExEdc, @NEOpEx = NEOpEx
	, @OpExUEdc = OpExUEdc, @TAAdj = AnnTACost, @EnergyCost = EnergyCost, @TotCashOpEx = TotCashOpEx
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubListMonth)
SELECT @TAWHrEdc = (ISNULL(p.OCCTAWHrEdc,0) + ISNULL(p.MPSTAWHrEdc,0)) FROM dbo.SLPersTAAdj(@SubListMonth, @FactorSet) p
SELECT @AnnTAWHr = @TAWHrEdc*@Edc*100

SELECT @EII_Ytd = EII, @EnergyUseDay_Ytd = EnergyUseDay, @TotStdEnergy_Ytd = TotStdEnergy
	, @RefUtilPcnt_Ytd = RefUtilPcnt, @Edc_Ytd = Edc, @UEdc_Ytd = UEdc
	, @VEI_Ytd = VEI, @ReportLossGain_Ytd = ReportLossGain, @EstGain_Ytd = EstGain
	, @ProcessEffIndex_Ytd = ProcessEffIndex, @NetInputBPD_Ytd = NetInputBPD
	, @OpAvail_Ytd = OpAvail, @MechUnavailTA_Ytd = MechUnavailTA, @NonTAUnavail_Ytd = MechUnavailRout + RegUnavail
	, @MaintIndex_Ytd = MaintIndex, @RoutCost_Ytd = RoutCost, @AnnTACost_Ytd = AnnTACost
	, @PersIndex_Ytd = PersIndex, @AnnTAWhr_Ytd = NULL, @NonTAWHr_Ytd = NULL
	, @NEOpExEdc_Ytd = NEOpExEdc, @NEOpEx_Ytd = NEOpEx
	, @OpExUEdc_Ytd = OpExUEdc, @TAAdj_Ytd = AnnTACost, @EnergyCost_Ytd = EnergyCost, @TotCashOpEx_Ytd = TotCashOpEx
FROM dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr_Ytd = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubListYTD)
SELECT @AnnTAWHr_Ytd = @TAWHrEdc*@Edc_Ytd*100

SELECT @EII_Avg = EII, @EnergyUseDay_Avg = EnergyUseDay, @TotStdEnergy_Avg = TotStdEnergy
	, @RefUtilPcnt_Avg = RefUtilPcnt, @Edc_Avg = Edc, @Uedc_Avg = UEdc
	, @VEI_Avg = VEI, @ReportLossGain_Avg = ReportLossGain, @EstGain_Avg = EstGain
	, @ProcessEffIndex_Avg = ProcessEffIndex, @NetInputBPD_Avg = NetInputBPD
	, @PersIndex_Avg = PersIndex, @AnnTAWhr_Avg = NULL, @NonTAWHr_Avg = NULL
	, @NEOpExEdc_Avg = NEOpExEdc, @NEOpEx_Avg = NEOpEx
	, @OpExUedc_Avg = OpExUEdc, @TAAdj_Avg = AnnTACost, @EnergyCost_Avg = EnergyCost, @TotCashOpEx_Avg = TotCashOpEx
FROM dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM)

SELECT @NonTAWHr_Ytd = TotNonTAWHr/1000 FROM dbo.SLSumPersTot(@SubList12Mo)
SELECT @AnnTAWHr_Avg = @TAWHrEdc*@Edc_Avg*100

SELECT @OpAvail_Avg = OpAvail, @MechUnavailTA_Avg = MechUnavailTA, @NonTAUnavail_Avg = MechUnavailRout + RegUnavail
	, @MaintIndex_Avg = MaintIndex, @RoutCost_Avg = RoutCost, @AnnTACost_Avg = AnnTACost
FROM dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM)


SELECT 
	EII = @EII, EII_Ytd = @EII_Ytd, EII_Avg = @EII_Avg, 
	EnergyUseDay = @EnergyUseDay, EnergyUseDay_Ytd = @EnergyUseDay_Ytd, EnergyUseDay_Avg = @EnergyUseDay_Avg, 
	TotStdEnergy = @TotStdEnergy, TotStdEnergy_Ytd = @TotStdEnergy_Ytd, TotStdEnergy_Avg = @TotStdEnergy_Avg, 

	UtilPcnt = @RefUtilPcnt, UtilPcnt_Ytd = @RefUtilPcnt_Ytd, UtilPcnt_Avg = @RefUtilPcnt_Avg, 
	Edc = @Edc, Edc_Ytd = @Edc_Ytd, Edc_Avg = @Edc_Avg, 
	UtilUEdc = @Edc*@RefUtilPcnt/100, UtilUEdc_Ytd = @Edc_Ytd*@RefUtilPcnt_Ytd/100, UtilUedc_Avg = @Edc_Avg*@RefUtilPcnt_Avg/100, 
	
	ProcessEffIndex = @ProcessEffIndex, ProcessEffIndex_Ytd = @ProcessEffIndex_Ytd, ProcessEffIndex_Avg = @ProcessEffIndex_Avg, 
	VEI = @VEI, VEI_Ytd = @VEI_Ytd, VEI_Avg = @VEI_Avg, 
	NetInputBPD = @NetInputBPD, NetInputBPD_Ytd = @NetInputBPD_Ytd, NetInputBPD_Avg = @NetInputBPD_Avg,
	ReportLossGain = @ReportLossGain, ReportLossGain_Ytd = @ReportLossGain_Ytd, ReportLossGain_Avg = @ReportLossGain_Avg, 
	EstGain = @EstGain, EstGain_Ytd = @EstGain_Ytd, EstGain_Avg = @EstGain_Avg, 

	OpAvail = @OpAvail, OpAvail_Ytd = @OpAvail_Ytd, OpAvail_Avg = @OpAvail_Avg, 
	MechUnavailTA = @MechUnavailTA, MechUnavailTA_Ytd = @MechUnavailTA_Ytd, MechUnavailTA_Avg = @MechUnavailTA_Avg, 
	NonTAUnavail = @NonTAUnavail, NonTAUnavail_Ytd = @NonTAUnavail_Ytd, NonTAUnavail_Avg = @NonTAUnavail_Avg, 

	TotWHrEdc = @PersIndex, TotWhrEdc_Ytd = @PersIndex_Ytd, TotWhrEdc_Avg = @PersIndex_Avg, 
	AnnTAWhr = @AnnTAWhr, AnnTAWhr_Ytd = @AnnTAWhr_Ytd, AnnTAWhr_Avg = @AnnTAWhr_Avg,
	NonTAWHr = @NonTAWHr, NonTAWHr_Ytd = @NonTAWHr_Ytd, NonTAWHr_Avg = @NonTAWHr_Avg, 

	MaintIndex = @MaintIndex, MaintIndex_Ytd = @MaintIndex_Ytd, MaintIndex_Avg = @MaintIndex_Avg, 
	RoutCost = @RoutCost, RoutCost_Ytd = @RoutCost_Ytd, RoutCost_Avg = @RoutCost_Avg, 
	AnnTACost = @AnnTACost, AnnTACost_Ytd = @AnnTACost_Ytd, AnnTACost_Avg = @AnnTACost_Avg,

	NEOpExEdc = @NEOpExEdc, NEOpExEdc_Ytd = @NEOpExEdc_Ytd, NEOpExEdc_Avg = @NEOpExEdc_Avg, 
	NEOpEx = @NEOpEx, NEOpEx_Ytd = @NEOpEx_Ytd, NEOpEx_Avg = @NEOpEx_Avg, 

	TotCashOpExUEdc = @OpExUEdc, TotCashOpExUEdc_Ytd = @OpExUEdc_Ytd, TotCashOpExUedc_Avg = @OpExUedc_Avg, 
	TAAdj = @TAAdj, TAAdj_Ytd = @TAAdj_Ytd, TAAdj_Avg = @TAAdj_Avg,
	EnergyCost = @EnergyCost, EnergyCost_Ytd = @EnergyCost_Ytd, EnergyCost_Avg = @EnergyCost_Avg, 
	TotCashOpEx = @TotCashOpEx, TotCashOpEx_Ytd = @TotCashOpEx_Ytd, TotCashOpEx_Avg = @TotCashOpEx_Avg, 
	UEdc = @UEdc, UEdc_Ytd = @UEdc_Ytd, Uedc_Avg = @Uedc_Avg






