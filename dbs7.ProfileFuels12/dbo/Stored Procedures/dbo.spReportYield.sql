﻿CREATE   PROC [dbo].[spReportYield] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @Category varchar(5))
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays AS DaysInPeriod, Currency = @Currency,
y.MaterialID, MaterialName = CASE WHEN y.Category = 'PROD' THEN ISNULL(lu.SAIName, y.MaterialName) WHEN y.MaterialID = 'M106' THEN 'Purchased Energy Consumed for Sales' ELSE y.MaterialName END, 
Bbl = CAST(mcc.Bbl AS int),
PricePerBbl = CAST(mcc.PriceUS*dbo.ExchangeRate('USD',@Currency,s.PeriodStart) AS decimal(6, 2)),
Value = CAST(mcc.Bbl*mcc.PriceUS*dbo.ExchangeRate('USD',@Currency,s.PeriodStart)/1000 AS decimal(15, 1))
FROM TSort t INNER JOIN Submissions s ON s.RefineryID = t.RefineryID
INNER JOIN Yield y ON y.SubmissionID = s.SubmissionID
INNER JOIN MaterialCostCalc mcc ON mcc.SubmissionID = s.SubmissionID AND mcc.SortKey = y.SortKey
LEFT JOIN Material_LU lu ON lu.MaterialID = y.MaterialID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
AND mcc.Scenario = @Scenario AND y.Bbl <> 0 
AND 	((@Category = 'PROD' AND y.Category IN ('RPF', 'PROD') AND (ISNULL(t.FuelsLubesCombo, 0) <> 1 OR ISNULL(lu.LubesOnly, 0) <> 1)) /* Primary Products */
	OR (@Category = 'LUBE' AND y.Category = 'PROD' AND (ISNULL(t.FuelsLubesCombo, 0) = 1 AND ISNULL(lu.LubesOnly, 0) = 1)) /* Lube Products */
	OR (@Category = 'OTHRM' AND y.Category IN ('RMI', 'OTHRM') AND y.MaterialID <> 'CRD') /* Non-Crude raw materials */
	OR (@Category IN ('MPROD', 'SOLV', 'COKE', 'ASP') AND y.Category = @Category) /* Other products */
	)
ORDER BY s.PeriodStart, y.SortKey

