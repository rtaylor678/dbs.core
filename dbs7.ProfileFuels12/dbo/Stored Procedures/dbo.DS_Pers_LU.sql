﻿CREATE PROC [dbo].[DS_Pers_LU]
	
AS

SELECT RTRIM(PersId) as PersId,RTRIM(Description) as Description,PersCode,IncludeForInput,SortKey,RTRIM(ParentID) AS ParentID, RTRIM(Indent) AS Indent,RTRIM(DetailStudy) AS DetailStudy,RTRIM(DetailProfile) AS DetailProfile FROM Pers_LU  ORDER BY SortKey
