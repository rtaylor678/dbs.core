﻿
CREATE  PROC [dbo].[spReportBashneftScorecardItems] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT ChartTitle,ChartTitleRussian,SectionHeader,SectionHeaderRussian,SortKey,
CASE WHEN LEN(AxisLabelUS) > 0 THEN ', ' + AxisLabelUS ELSE AxisLabelUS END as AxisLabelUS , 
CASE WHEN LEN(AxisLabelMetric) > 0 THEN ', ' + AxisLabelMetric  ELSE AxisLabelMetric END as AxisLabelMetric,DataTable, 
TotField, TargetField, YTDField, AvgField, DecPlaces,
ValueField1 = TotField --CASE WHEN @IncludeAvg = 0 AND @IncludeYTD = 1 THEN YTDField WHEN @IncludeAvg = 1 AND @IncludeYTD = 0 THEN AvgField ELSE TotField END
FROM (
	SELECT ChartTitle, ChartTitleRussian, SectionHeader, SectionHeaderRussian, SortKey,
	ISNULL(CASE WHEN @Currency = 'RUB' THEN REPLACE(AxisLabelUSRussian,'CurrencyCode',N'руб.') ELSE AxisLabelUSRussian END, AxisLabelUS) as AxisLabelUS , 
	ISNULL(CASE WHEN @Currency = 'RUB' THEN REPLACE(AxisLabelMetricRussian,'CurrencyCode',N'руб.') ELSE AxisLabelMetricRussian END, AxisLabelMetric) as AxisLabelMetric,DataTable, 
	TotField, TargetField, YTDField, AvgField, DecPlaces,
	ValueField1 = TotField --CASE WHEN @IncludeAvg = 0 AND @IncludeYTD = 1 THEN YTDField WHEN @IncludeAvg = 1 AND @IncludeYTD = 0 THEN AvgField ELSE TotField END
	FROM Chart_LU
	WHERE CustomGroup = 0 AND ChartTitle IN ('Process Unit Utilization','Net Raw Material Input','Edc','UEdc','Energy Intensity Index'
		,'Maintenance Personnel Efficiency Index','Maintenance Index','Maintenance Efficiency Index'
		,'Cash Operating Expenses UEdc Basis','Average Purchased Energy Cost','Average Produced Fuel Cost','Average Energy Cost'
		,'Non-Turnaround Index','Turnaround Index','Non-Turnaround MEI','Turnaround MEI','Mechanical Availability','Operational Availability','Mechanical Unavailability due to T/A'
		,'Non-Energy Operating Expenses Edc Basis')) c
ORDER BY SortKey
--OR CustomGroup IN (SELECT CustomGroup FROM CoCustom WHERE CustomType = 'C' AND CompanyID IN (SELECT CompanyID FROM Submissions WHERE RefineryID = @RefineryID AND DataSet=@DataSet AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth))

