﻿




CREATE     PROC [dbo].[spReportTATotals] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

DECLARE @SubmissionID int, @FractionOfYear real
SELECT @SubmissionID = SubmissionID, @FractionOfYear = FractionOfYear
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

DECLARE @TotAnnTACost real, @MonthTACost real
SELECT @TotAnnTACost = mtc.AnnTACost, @MonthTACost = o.TAAdj
FROM MaintTotCost mtc INNER JOIN OpExAll o ON o.SubmissionID = mtc.SubmissionID AND o.Currency = mtc.Currency AND o.DataType = 'ADJ'
WHERE mtc.SubmissionID = @SubmissionID AND mtc.Currency = @Currency

DECLARE @TotTACompWHr real, @TotTAContWHr real, @TotTAEffort real, @TotAnnTAEffort real, @MonthTAEffort real
SELECT @TotTACompWHr = SUM(ISNULL(ta.TAOCCSTH,0) + ISNULL(ta.TAOCCOVT,0) + ISNULL(ta.TAMPSSTH*ISNULL(1+ta.TampSovtPcnt/100,1),0))
	, @TotTAContWHr = SUM(ISNULL(ta.TAContOCC,0)+ISNULL(ta.TAContMPS,0))
	, @TotTAEffort = SUM(ISNULL(ta.TAOCCSTH,0) + ISNULL(ta.TAOCCOVT,0) + ISNULL(ta.TAMPSSTH*ISNULL(1+ta.TampSovtPcnt/100,1),0) + ISNULL(ta.TAContOCC,0)+ISNULL(ta.TAContMPS,0))
	, @TotAnnTAEffort = SUM((ISNULL(ta.TAOCCSTH,0) + ISNULL(ta.TAOCCOVT,0) + ISNULL(ta.TAMPSSTH*ISNULL(1+ta.TampSovtPcnt/100,1),0) + ISNULL(ta.TAContOCC,0)+ISNULL(ta.TAContMPS,0))/ta.TAIntYrs)
	, @MonthTAEffort = SUM((ISNULL(ta.TAOCCSTH,0) + ISNULL(ta.TAOCCOVT,0) + ISNULL(ta.TAMPSSTH*ISNULL(1+ta.TampSovtPcnt/100,1),0) + ISNULL(ta.TAContOCC,0)+ISNULL(ta.TAContMPS,0))/ta.TAIntYrs)*@FractionOfYear
FROM dbo.TAForSubmission ta WHERE ta.SubmissionID = @SubmissionID

DECLARE @MechUnavailTA_Ann real
SELECT @MechUnavailTA_Ann = MechUnavailTA_Ann
FROM MaintAvailCalc
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

SELECT MechUnavailTA_Ann = @MechUnavailTA_Ann
	, TotTACompWHr = @TotTACompWHr, TotTAContWHr = @TotTAContWHr, TotTAEffort = @TotTAEffort
	, TotAnnTAEffort = @TotAnnTAEffort, MonthTAEffort = @MonthTAEffort
	, TotAnnTACost = @TotAnnTACost, MonthTACost = @MonthTACost
