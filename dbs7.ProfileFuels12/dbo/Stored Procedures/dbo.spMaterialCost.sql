﻿CREATE                  PROC [dbo].[spMaterialCost](@SubmissionID int)
AS
SET NOCOUNT ON

DELETE FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialNetCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID
DELETE FROM MaterialTotCost WHERE SubmissionID = @SubmissionID
DELETE FROM DataChecks WHERE SubmissionID = @SubmissionID AND DataCheckId = 'NoStudy$'

DECLARE @RefineryID varchar(6), @RptCurrency CurrencyCode, @RptCurrencyT15 CurrencyCode, @PeriodStart smalldatetime
SELECT @RefineryID = RefineryID, @RptCurrency = RptCurrency , @RptCurrencyT15 = RptCurrencyT15, @PeriodStart = PeriodStart
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID

DECLARE @RFCValue real, @RFCBbl real, @PurFGPrice real, @RFCGValue real
SELECT 	@RFCValue = ISNULL(ProdFGCostK, 0) + ISNULL(ProdOthCostK,0)
FROM EnergyTotCost 
WHERE SubmissionID = @SubmissionID AND Scenario = 'CLIENT' AND Currency = @RptCurrencyT15

SELECT @RFCBbl = Bbl FROM Yield WHERE SubmissionID = @SubmissionID AND MaterialID = 'RFC'

SELECT @PurFGPrice = SUM(PriceMBTULocal*SourceMBTU)/SUM(SourceMBTU)
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType IN ('NG','FG_','FG') AND PriceMBTULocal > 0 AND SourceMBTU > 0

IF @PurFGPrice IS NULL
	SELECT @PurFGPrice = AVG(PriceMBTULocal)
	FROM Energy
	WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType IN ('NG','FG_','FG') AND PriceMBTULocal > 0 
IF @PurFGPrice IS NULL
	SELECT @PurFGPrice = SUM(PriceMBTULocal*SourceMBTU)/SUM(SourceMBTU)
	FROM Energy
	WHERE (SubmissionID = @SubmissionID AND EnergyType NOT IN ('STM','LLH'))
	AND ((TransType = 'PUR' AND EnergyType NOT IN ('C2','C3','C4','NAP','DST'))
	OR TransType = 'DST')
	HAVING SUM(SourceMBTU)>0
IF @PurFGPrice IS NULL 
	SELECT @PurFGPrice = 0
ELSE
	SELECT @PurFGPrice = @PurFGPrice*6.05

SELECT @RFCGValue = SUM(PriceMBTULocal*UsageMBTU)/1000
FROM Energy
WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType <> 'GE'

SELECT @RFCGValue = ISNULL(@RFCGValue, 0) + ISNULL(SUM(PriceLocal*UsageMWH)/100, 0)
FROM Electric
WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND PriceLocal > 0 AND UsageMWH > 0

IF @RFCGValue IS NULL
	SELECT @RFCGValue = 0

IF @RptCurrency <> @RptCurrencyT15
	SELECT 	@PurFGPrice = @PurFGPrice * cc.ExchRate,
		@RFCGValue = @RFCGValue * cc.ExchRate
	FROM (VALUES (dbo.ExchangeRate(@RptCurrency, @RptCurrencyT15, @PeriodStart))) cc(ExchRate)
IF @RFCBbl > 0
	UPDATE Yield
	SET PriceLocal = CASE WHEN @RFCValue > 0 AND @RFCBbl > 0 THEN @RFCValue*1000/@RFCBbl ELSE @PurFGPrice END
	WHERE SubmissionID = @SubmissionID AND MaterialID IN ('RFC','RFS')

UPDATE Yield
SET PriceLocal = @PurFGPrice
WHERE SubmissionID = @SubmissionID AND MaterialID = 'M106'

UPDATE Yield
SET PriceLocal = 1000*@RFCGValue/Bbl
WHERE SubmissionID = @SubmissionID AND MaterialID = 'RFCG' AND Bbl > 0

UPDATE Yield
SET PriceUS = PriceLocal * dbo.ExchangeRate(@RptCurrencyT15, 'USD', @PeriodStart)
WHERE SubmissionID = @SubmissionID

-- Load client-supplied prices
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
SELECT SubmissionID, 'CLIENT', SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl
FROM Yield WHERE SubmissionID = @SubmissionID

-- Load study prices
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
SELECT y.SubmissionID, p.StudyYear, y.SortKey, y.MaterialID, y.Category, ISNULL(p.PricePerBbl*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM SubmissionsAll WHERE SubmissionID = @SubmissionID), 0), p.PricePerBbl, y.Bbl
FROM SubmissionsAll s INNER JOIN Yield y ON y.SubmissionID = s.SubmissionID
INNER JOIN MaterialPrices p ON p.RefineryID = s.RefineryID AND p.MaterialID = y.MaterialID
WHERE s.SubmissionID = @SubmissionID AND y.MaterialID NOT IN ('CRD','RFC','RFS','RFCG','M106','AKC','ADD')

-- Load Crude
INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
SELECT t.SubmissionID, p.Scenario, 101, 'CRD', 'RMI', ISNULL(p.AvgCost*(SELECT dbo.ExchangeRate('USD',RptCurrencyT15,PeriodStart) FROM SubmissionsAll WHERE SubmissionID = @SubmissionID), 0), ISNULL(p.AvgCost, 0), t.TotBbl
FROM CrudeTot t INNER JOIN CrudeTotPrice p ON p.SubmissionID = t.SubmissionID
WHERE t.SubmissionID = @SubmissionID AND p.Scenario <> 'CLIENT'

-- Load Refinery-produced fuels
DECLARE @Scenarios TABLE
	(Scenario varchar(8))
INSERT @Scenarios
SELECT DISTINCT Scenario FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID AND Scenario <> 'CLIENT'

IF EXISTS (SELECT * FROM @Scenarios)
BEGIN
	INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
	SELECT SubmissionID, s.Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl
	FROM MaterialCostCalc m, @Scenarios s
	WHERE m.SubmissionID = @SubmissionID AND m.Scenario = 'CLIENT' AND m.MaterialID IN ('RFC','RFS','RFCG','M106','AKC','ADD')
END

-- Load materials that do not have study pricing
IF EXISTS (SELECT * FROM @Scenarios)
BEGIN
	SELECT c.SubmissionID, s.Scenario, c.SortKey, c.MaterialID, c.Category, c.PriceLocal, c.PriceUS, c.Bbl
	INTO #Missing
	FROM MaterialCostCalc c, @Scenarios s
	WHERE c.SubmissionID = @SubmissionID AND c.Scenario = 'CLIENT' 
	AND NOT EXISTS (SELECT * FROM MaterialCostCalc x WHERE x.SubmissionID = c.SubmissionID AND x.Scenario = s.Scenario AND x.SortKey = c.SortKey)

	INSERT MaterialCostCalc (SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl)
	SELECT SubmissionID, Scenario, SortKey, MaterialID, Category, PriceLocal, PriceUS, Bbl
	FROM #Missing

	INSERT DataChecks (SubmissionID, DataCheckId, ItemSortKey, ItemDesc, Value1, Value2)
	SELECT y.SubmissionID, 'NoStudy$', y.SortKey, y.MaterialName, y.MaterialID, m.Scenario
	FROM #Missing m INNER JOIN Yield y ON y.SubmissionID = m.SubmissionID AND y.SortKey = m.SortKey
	WHERE y.MaterialID NOT IN ('ADD','AKC','GAIN') AND y.Bbl <> 0

	DROP TABLE #Missing
END

DECLARE @IsLubes bit; SELECT @IsLubes = 0;
IF dbo.GetRefineryType(@RefineryID) = 'LUBES'
	SET @IsLubes = 1
	
INSERT @Scenarios VALUES ('CLIENT')
DECLARE @RMC real, @GPV real
DECLARE @RawMat real, @Prod real, @ByProd real, @LWSRawMat real, @LWSProd real, @NetLWSRawMat real, @CogenCredit real
DECLARE @TermRM real, @TermProd real, @POXO2 real

SELECT @TermRM = ISNULL(ThirdPartyTerminalRM, 0)/1000 * cc.ExchRate, 
	@TermProd = ISNULL(ThirdPartyTerminalProd, 0)/1000 * cc.ExchRate,  
	@POXO2 = ISNULL(POXO2 , 0)/1000 * cc.ExchRate
FROM OpExAll CROSS APPLY (VALUES (dbo.ExchangeRate(@RptCurrency, 'USD', @PeriodStart))) cc(ExchRate)
WHERE SubmissionID = @SubmissionID AND DataType = 'RPT'

DECLARE @Scenario varchar(8)
DECLARE cScenarios CURSOR FAST_FORWARD
FOR	SELECT Scenario FROM @Scenarios
OPEN cScenarios
FETCH NEXT FROM cScenarios INTO @Scenario
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @IsLubes = 0
	BEGIN
		INSERT INTO MaterialNetCalc (SubmissionID, Scenario, Category, MaterialID, Bbl, MT, PricePerBbl)
		SELECT  m.SubmissionID, @Scenario, m.Category, m.MaterialID, m.Bbl, m.MT, p.PriceUS
		FROM MaterialNet m LEFT JOIN (SELECT MaterialID, Category, PriceUS = SUM(PriceUS*Bbl)/SUM(Bbl)
					FROM MaterialCostCalc WHERE SubmissionID = @SubmissionID AND Bbl > 0 AND Scenario = @Scenario
					GROUP BY MaterialID, Category) p ON p.Category = m.Category AND p.MaterialID = m.MaterialID
		WHERE m.SubmissionID = @SubmissionID 
		
		-- Calculate the Gross Value in M US$ by category and store in MaterialSTCalc
		INSERT INTO MaterialSTCalc (SubmissionID, Scenario, Category, GrossBbl, GrossMT, NetBbl, NetMT, GrossValueMUS)
		SELECT st.SubmissionID, @Scenario, st.Category, st.GrossBbl, st.GrossMT, st.NetBbl, st.NetMT, p.ValueMUS
		FROM MaterialST st LEFT JOIN (SELECT Category, ValueMUS = SUM(Bbl*PriceUS/1000000)
				FROM MaterialCostCalc c WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario
				GROUP BY Category) p ON p.Category = st.Category
		WHERE SubmissionID = @SubmissionID
		
		-- Calculate Net Values in M US$ for categories which are netted based on
		-- MaterialID (Other Raw Material Input, Miscellaneous Products,
		-- Asphalt, Coke) and record in MaterialSTCalc.
		UPDATE MaterialSTCalc
		SET NetValueMUS = (SELECT SUM(n.Bbl*n.PricePerBbl)/1000000
			FROM MaterialNetCalc n
			WHERE n.Category = MaterialSTCalc.Category
			AND n.SubmissionID = MaterialSTCalc.SubmissionID
			AND n.Scenario = MaterialSTCalc.Scenario)
		WHERE Category IN ('OTHRM', 'MPROD', 'ASP', 'COKE')
		AND SubmissionID = @SubmissionID AND Scenario = @Scenario
		
		-- For categories that are not netted, set the net value = gross value
		-- (Primary Raw Material Inputs, Primary Products, Solvents)
		UPDATE MaterialSTCalc SET NetValueMUS = GrossValueMUS
		WHERE Category IN ('RMI', 'SOLV', 'PROD', 'RPF')
		AND SubmissionID = @SubmissionID AND Scenario = @Scenario
		
		-- Set Gross value to 0 if it is NULL for calculations later
		UPDATE MaterialSTCalc SET GrossValueMUS = 0
		WHERE SubmissionID = @SubmissionID AND GrossValueMUS IS NULL AND Scenario = @Scenario
		
		-- Calculate the Net Values for Feedstocks to affiliates by subtracting
		-- the gross value of returns from the affiliate from the gross value
		-- of the feedstocks to the affiliate.
		DECLARE @i tinyint, @RCat varchar(5), @FCat varchar(5)
		SELECT @i = 0
		WHILE @i<2
		BEGIN
			IF @i = 0
				-- Feedstocks to Chemical Plants
				SELECT @RCat = 'RCHEM', @FCat = 'FCHEM'
			ELSE
				-- Feedstocks to Lube Plants
				SELECT @RCat = 'RLUBE', @FCat = 'FLUBE'
			IF NOT EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @FCat)
			BEGIN
				IF EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @RCat)
					INSERT INTO MaterialSTCalc (SubmissionID, Scenario, Category, GrossBbl, NetBbl, GrossValueMUS)
					VALUES (@SubmissionID, @Scenario, @FCat, 0, 0, 0)
	 		END
			IF NOT EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @RCat)
			BEGIN
				IF EXISTS (SELECT * FROM MaterialSTCalc WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario AND Category = @FCat)
					INSERT INTO MaterialSTCalc (SubmissionID, Scenario, Category, GrossBbl, NetBbl, GrossValueMUS)
					VALUES (@SubmissionID, @Scenario, @RCat, 0, 0, 0)
	 		END
		
			UPDATE MaterialSTCalc
			SET NetValueMUS = (SELECT F.GrossValueMUS-R.GrossValueMUS
				FROM MaterialSTCalc F, MaterialSTCalc R
				WHERE R.SubmissionID = F.SubmissionID AND R.Scenario = F.Scenario
				AND (F.Category = @FCat) AND (R.Category = @RCat)
				AND R.SubmissionID = MaterialSTCalc.SubmissionID
				AND R.Scenario = MaterialSTCalc.Scenario)
			WHERE Category = @FCat AND SubmissionID = @SubmissionID AND Scenario = @Scenario
			SELECT @i = @i+1
		END
		DECLARE @RawMatCost real, @ProdValue real
		SELECT 	@RawMatCost = SUM(CASE WHEN L.NetGroup = 'I' THEN T.NetValueMUS ELSE 0 END),
			@ProdValue = SUM(CASE WHEN L.NetGroup = 'Y' THEN T.NetValueMUS ELSE 0 END)
		FROM MaterialSTCalc T INNER JOIN MaterialCategory_LU L ON T.Category = L.Category
		WHERE SubmissionID = @SubmissionID AND Scenario = @Scenario
		
		SELECT	@RawMatCost = @RawMatCost + @TermRM + @POXO2,
			@ProdValue = @ProdValue - @TermProd
		
		INSERT MaterialTotCost (SubmissionID, Scenario, Currency, RawMatCost, ProdValue,
			ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2)
		SELECT @SubmissionID, @Scenario, 'USD', @RawMatCost, @ProdValue,
			@TermRM, @TermProd, @POXO2
	END
	ELSE BEGIN
			-- Cogen Credit
		DECLARE @M106 real, @RFCG real
		DECLARE @M106ValueMUS real, @RFCGValueMUS real
		DECLARE @FuelCost real, @FuelCostMT real
		
		SELECT @M106 = SUM(Bbl), @M106ValueMUS = SUM(PriceUS*Bbl)/1000000
		FROM MaterialCostCalc m
		WHERE SubmissionID = @SubmissionID AND MaterialID = 'M106' AND Scenario = @Scenario
		
		SELECT @RFCG = SUM(Bbl), @RFCGValueMUS = SUM(PriceUS*Bbl)/1000000
		FROM MaterialCostCalc m
		WHERE SubmissionID = @SubmissionID AND MaterialID = 'RFCG' AND Scenario = @Scenario

		IF @Scenario = 'CLIENT'
		BEGIN
			SELECT @FuelCost = [$(GlobalDB)].dbo.WtAvg(PriceMBTUUS, SourceMBTU)
			FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType NOT IN ('STM','PT')
		END
		ELSE BEGIN
			SELECT @FuelCost = p.PricePerBbl
			FROM SubmissionsAll s INNER JOIN MaterialPrices p ON p.RefineryID = s.RefineryID AND p.MaterialID = 'ERPF'
			WHERE s.SubmissionID = @SubmissionID AND CAST(p.StudyYear as varchar(8)) = @Scenario
			
			IF ISNULL(@FuelCost, 0) = 0
				SELECT @FuelCost = p.PricePerBbl
				FROM SubmissionsAll s INNER JOIN MaterialPrices p ON p.RefineryID = s.RefineryID AND p.MaterialID = 'FGAS'
				WHERE s.SubmissionID = @SubmissionID AND CAST(p.StudyYear as varchar(8)) = @Scenario
		END
		SELECT @FuelCost = @FuelCost * 6.05
		SELECT @CogenCredit = ISNULL(@RFCGValueMUS, 0) - (ISNULL(@M106ValueMUS, 0) + (ISNULL(@RFCG, 0) - ISNULL(@M106, 0)) * ISNULL(@FuelCost, 0)/1000000)
		-- End Cogen Credit
		
		-- Raw Material Cost and Product Value , MUS
		SELECT @RawMat = SUM(CASE WHEN l.GrossGroup = 'I' THEN m.PriceUS*m.Bbl ELSE 0 END)/1000000,
			@Prod = SUM(CASE WHEN l.GrossGroup = 'Y' THEN m.PriceUS*m.Bbl ELSE 0 END)/1000000,
			@ByProd = SUM(CASE WHEN l.NetGroup = 'BP' THEN m.PriceUS*m.Bbl ELSE 0 END)/1000000 -- By-product Value, MUS
		FROM MaterialCostCalc m INNER JOIN MaterialCategory_LU l on m.Category = l.Category
		WHERE m.SubmissionID = @SubmissionID AND m.Scenario = @Scenario
		
		SELECT 	@RawMat = @RawMat + ISNULL(@TermRM, 0) 
			,	@Prod = @Prod - ISNULL(@TermProd, 0)
		SELECT 	@LWSRawMat = @RawMat - ISNULL(@ByProd, 0), @LWSProd = @Prod - ISNULL(@ByProd, 0)
		SELECT @NetLWSRawMat = @LWSRawMat

		INSERT MaterialTotCost (SubmissionID, Scenario, Currency, RawMatCost, ProdValue, 
			ByProdValue, LWSRawMatCost, LWSProdValue, CogenCredit, NetLWSRawMatCost, 
			ThirdPartyTerminalRM, ThirdPartyTerminalProd)
		VALUES (@SubmissionID, @Scenario, 'USD', @RawMat, @Prod, 
			@ByProd, @LWSRawMat, @LWSProd, @CogenCredit, @NetLWSRawMat,	@TermRM, @TermProd)
	END
	FETCH NEXT FROM cScenarios INTO @Scenario
END
CLOSE cScenarios
DEALLOCATE cScenarios

INSERT MaterialTotCost (SubmissionID, Scenario, Currency, RawMatCost, ProdValue, 
	ByProdValue, LWSRawMatCost, LWSProdValue, CogenCredit, NetLWSRawMatCost, 
	ThirdPartyTerminalRM, ThirdPartyTerminalProd, POXO2)
SELECT m.SubmissionID, m.Scenario, c.Currency, 
m.RawMatCost*cc.ExchRate, 
m.ProdValue*cc.ExchRate, 
m.ByProdValue*cc.ExchRate,
m.LWSRawMatCost*cc.ExchRate, 
m.LWSProdValue*cc.ExchRate, 
m.CogenCredit*cc.ExchRate, 
m.NetLWSRawMatCost*cc.ExchRate, 
m.ThirdPartyTerminalRM*cc.ExchRate, 
m.ThirdPartyTerminalProd*cc.ExchRate,
m.POXO2*cc.ExchRate
FROM MaterialTotCost m INNER JOIN SubmissionsAll s ON s.SubmissionID = m.SubmissionID 
INNER JOIN CurrenciesToCalc c ON c.RefineryID = s.RefineryID
CROSS APPLY (VALUES (dbo.ExchangeRate(m.Currency, c.Currency, s.PeriodStart))) cc(ExchRate)
WHERE c.Currency <> 'USD' AND m.SubmissionID = @SubmissionID AND m.Currency = 'USD'

