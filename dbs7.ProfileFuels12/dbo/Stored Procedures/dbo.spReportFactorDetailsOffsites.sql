﻿



CREATE    PROC [dbo].[spReportFactorDetailsOffsites] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SELECT s.Location, s.PeriodStart, s.PeriodEnd, s.NumDays as DaysInPeriod, Currency = @Currency, UOM = @UOM,
f.Edc/1000 as TotEdc, f.UEdc/1000 as TotUEdc, f.UtilPcnt AS RefUtilPcnt, TotRSEdc/1000 as kEdc, TotRSUEdc/1000 as kUEdc, SensHeatStdEnergy,AspStdEnergy, 
OffsitesStdEnergy,TotStdEnergy,EstGain/s.NumDays as EstGain, 
TotRsMaintEffDiv/1000 as TotRsMaintEffDiv, f.MaintEffDiv/1000 as MaintEffDiv
,TotRsPersEffDiv/1000 as TotRsPersEffDiv, TotRsMaintPersEffDiv/1000 as TotRsMaintPersEffDiv,TotRsNonMaintPersEffDiv/1000 as TotRsNonMaintPersEffDiv
, f.MaintPersEffDiv/1000 as MaintPersEffDiv, f.NonMaintPersEffDiv/1000 as NonMaintPersEffDiv, f.PersEffDiv/1000 as PersEffDiv
,TotRSNEOpExEffDiv/1000 as TotRSNEOpExEffDiv,  f.NEOpExEffDiv/1000 as NEOpExEffDiv, 
TnkStdCap , TnkStdEdc/1000 AS TnkStdEdc,((PurElecUEdc+PurStmUEdc)/1000) AS PurchasedUtilityUEdc 
, ProcessEdc = pt.Edc/1000, ProcessUEdc = pt.UEdc/1000, ProcessUtilPcnt = pt.UtilPcnt
FROM FactorTotCalc f INNER JOIN Submissions s ON s.SubmissionID = f.SubmissionID
LEFT JOIN FactorProcessCalc pt ON pt.SubmissionID = f.SubmissionID AND pt.FactorSet = f.FactorSet AND pt.ProcessID = 'OperProc'
WHERE f.FactorSet=@FactorSet 
AND s.RefineryID=@RefineryID AND s.DataSet=@DataSet AND s.PeriodYear=ISNULL(@PeriodYear, s.PeriodYear) AND s.PeriodMonth=ISNULL(@PeriodMonth, s.PeriodMonth) AND s.UseSubmission = 1
ORDER BY s.PeriodStart DESC




