﻿
CREATE PROC [dbo].[spReportMonroeChartData2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS
SET NOCOUNT ON
SET @FactorSet = '2012' -- Set to 2012 rather than change the code in their workbooks

DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @PeriodStart12Mo smalldatetime
SELECT @PeriodEnd = PeriodEnd
FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)
IF @PeriodEnd IS NULL
	RETURN 1
IF EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND PeriodStart >= @PeriodStart12Mo AND PeriodStart < @PeriodEnd AND DataSet = @DataSet AND CalcsNeeded IS NOT NULL)
	RETURN 2
SELECT @PeriodStart12Mo = DATEADD(mm, -12, @PeriodEnd)

DECLARE @Data TABLE 
(	PeriodStart smalldatetime NOT NULL,
	PeriodEnd smalldatetime NULL,
	EII real NULL, 
	UtilOSTA real NULL, 
	VEI real NULL, 
	ProcessEffIndex real NULL, 
	MechAvail real NULL, 
	OpAvail real NULL, 
	MaintIndex real NULL,
	MEI real NULL,
	PersIndex real NULL, 
	MPEI real NULL,
	NEOpExEdc real NULL,
	OpExUEdc real NULL
)
DECLARE @msgString varchar(255)
SELECT @msgString = CAST(@PeriodEnd as varchar(20))

--- Everything Already Available in GenSum
INSERT INTO @Data (PeriodStart, PeriodEnd, UtilOSTA, MechAvail, OpAvail, EII, VEI, ProcessEffIndex, PersIndex, MPEI, MaintIndex, MEI, NEOpExEdc, OpExUEdc)
SELECT PeriodStart, PeriodEnd, UtilOSTA, MechAvail, OpAvail, EII, VEI, ProcessEffIndex, PersIndex, mPersEffIndex, MaintIndex, MaintEffIndex, NEOpExEdc, OpExUEdc
FROM dbo.GetCommonProfileLiteChartData(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM, 12)
/*
SELECT	s.PeriodStart, s.PeriodEnd, GenSum.UtilOSTA, OpAvail, GenSum.EII, GenSum.VEI, ProcessEffIndex = f.PEI, PersIndex = TotWHrEdc, MaintIndex = RoutIndex + TAIndex_Avg, NEOpExEdc, TotCashOpExUEdc
FROM GenSum LEFT JOIN FactorTotCalc f ON f.SubmissionID = GenSum.SubmissionID AND f.FactorSet = GenSum.FactorSet
INNER JOIN Submissions s ON s.SubmissionID = GenSum.SubmissionID
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND s.PeriodStart >= @PeriodStart12Mo AND s.PeriodStart < @PeriodEnd AND s.UseSubmission = 1
AND GenSum.FactorSet = @FactorSet AND GenSum.Currency = 'USD' AND GenSum.UOM = @UOM AND GenSum.Scenario = @Scenario
*/

IF (SELECT COUNT(*) FROM @Data) < 12
BEGIN
	DECLARE @Period smalldatetime
	SELECT @Period = DATEADD(mm, -1, @PeriodEnd)
	WHILE @Period >= @PeriodStart12Mo
	BEGIN
		IF NOT EXISTS (SELECT * FROM @Data WHERE PeriodStart = @Period)
			INSERT @Data (PeriodStart) VALUES (@Period)
		SELECT @Period = DATEADD(mm, -1, @Period)
	END
END

SELECT Period = CAST(DATEPART(mm, PeriodStart) as varchar(2)) + '/' + CAST(DATEPART(yy, PeriodStart) as varchar(4))
	, EII, UtilOSTA, VEI, MechAvail, OpAvail, PersIndex, MPEI, MaintIndex, MEI, NEOpExEdc, OpExUEdc, ProcessEffIndex
FROM @Data
ORDER BY PeriodStart ASC



