﻿
CREATE PROCEDURE [dbo].[spReportBashneftKPI2](@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'MET',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON 
SET @FactorSet = '2012'

DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
DECLARE @CompAvg bit = 0
IF (@RefineryID IN ('330EUR','331EUR','154FL') AND @DataSet = 'BASHONEREF') OR @RefineryID = '155FL'
	SET @CompAvg = 1

IF @CompAvg = 1
	SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
	FROM dbo.GetSubmission('330EUR', @PeriodYear, @PeriodMonth, 'Actual')
ELSE
	SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
	FROM dbo.GetSubmission(@RefineryID, @PeriodYear, @PeriodMonth, @DataSet)

IF @SubmissionID IS NULL
BEGIN
	--RAISERROR (N'Data has not been uploaded for this month.', -- Message text.
 --          10, -- Severity,
 --          1 --State
 --          );
	RETURN 1
END
ELSE IF @CalcsNeeded IS NOT NULL
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime, @StartQTR smalldatetime, @EndQuarter smalldatetime
SELECT @Start3Mo = p.Start3Mo, @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD, @StartQTR = p.StartQTR, @EndQuarter = p.EndQTR
FROM dbo.GetPeriods(@SubmissionID) p
DECLARE @StartQ2 smalldatetime, @StartQ3 smalldatetime, @StartQ4 smalldatetime, @EOY smalldatetime
SELECT @StartQ2 = DATEADD(mm, 3, @StartYTD), @StartQ3 = DATEADD(mm, 6, @StartYTD), @StartQ4 = DATEADD(mm, 9, @StartYTD), @EOY = DATEADD(YEAR, 1, @StartYTD)

DECLARE @SubListMonth dbo.SubmissionIDList, @SubListQTR dbo.SubmissionIDList, @SubListQ1 dbo.SubmissionIDList, @SubListQ2 dbo.SubmissionIDList, @SubListQ3 dbo.SubmissionIDList, @SubListQ4 dbo.SubmissionIDList
	, @SubListYTQ2 dbo.SubmissionIDList, @SubListYTQ3 dbo.SubmissionIDList, @SubListYear dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
IF @CompAvg = 1
BEGIN
	DECLARE @Refs TABLE (RefineryID varchar(6) NOT NULL, DataSet varchar(8) NOT NULL)
	INSERT @Refs VALUES ('154FL', 'Fuels'), ('330EUR', 'Actual'), ('331EUR', 'Actual')

	INSERT @SubListMonth SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @PeriodStart, @PeriodEnd)
	INSERT @SubListQTR SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartQTR, @EndQuarter)
	INSERT @SubListQ1 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartYTD, @StartQ2)
	INSERT @SubListQ2 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartQ2, @StartQ3)
	INSERT @SubListQ3 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartQ3, @StartQ4)
	INSERT @SubListQ4 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartQ4, @EOY)
	INSERT @SubListYTQ2 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartYTD, @StartQ3)
	INSERT @SubListYTQ3 SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartYTD, @StartQ4)
	INSERT @SubListYear SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @StartYTD, @EOY)
	INSERT @SubList12Mo SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @Start12Mo, @PeriodEnd)
	INSERT @SubList24Mo SELECT SubmissionID FROM @Refs r CROSS APPLY dbo.GetPeriodSubmissions(r.RefineryID, r.DataSet, @Start24Mo, @PeriodEnd)
	
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListMonth) WHERE ValidList = 'N')
		RETURN 1
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQTR) WHERE ValidList = 'N')
		DELETE FROM @SubListQTR
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ1) WHERE ValidList = 'N')
		DELETE FROM @SubListQ1
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ2) WHERE ValidList = 'N')
		DELETE FROM @SubListQ2
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ3) WHERE ValidList = 'N')
		DELETE FROM @SubListQ3
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListQ4) WHERE ValidList = 'N')
		DELETE FROM @SubListQ4
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListYTQ2) WHERE ValidList = 'N')
		DELETE FROM @SubListYTQ2
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListYTQ3) WHERE ValidList = 'N')
		DELETE FROM @SubListYTQ3
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubListYear) WHERE ValidList = 'N')
		DELETE FROM @SubListYear
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubList12Mo) WHERE ValidList = 'N')
		DELETE FROM @SubList12Mo
	IF EXISTS (SELECT * FROM dbo.SLCheckList(@SubList24Mo) WHERE ValidList = 'N')
		DELETE FROM @SubList24Mo

END
ELSE BEGIN
	INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)
	INSERT @SubListQTR SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartQTR, @EndQuarter)
	INSERT @SubListQ1 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @StartQ2)
	INSERT @SubListQ2 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartQ2, @StartQ3)
	INSERT @SubListQ3 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartQ3, @StartQ4)
	INSERT @SubListQ4 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartQ4, @EOY)
	INSERT @SubListYTQ2 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @StartQ3)
	INSERT @SubListYTQ3 SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @StartQ4)
	INSERT @SubListYear SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @EOY)
	INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
	INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)
END

IF EXISTS (SELECT * FROM Submissions WHERE CalcsNeeded IS NOT NULL AND SubmissionID IN (SELECT SubmissionID FROM @SubList24Mo))
BEGIN
	--RAISERROR (N'Calculations are not complete for this refinery.', -- Message text.
 --          10, -- Severity,
 --          2 --State
 --          );
	RETURN 2
END

SELECT ProcessUtilPcnt = m.ProcessUtilPcnt, ProcessEdc = m.ProcessEdc, ProcessUEdc = m.ProcessUEdc, OffsitesEdc = m.OffsitesEdc, Complexity = m.Complexity
	, EII = m.EII, EnergyUseDay = m.EnergyUseDay, TotStdEnergy = m.TotStdEnergy, FuelUseDay = m.FuelUseDay
	, OpAvail = m.OpAvail, MechUnavailTA = m.MechUnavailTA, NonTAOpUnavail = m.NonTAOpUnavail, TADD = m.TADD, NTAMDD = m.NTAMDD, RPDD = m.RPDD, NumDays = m.NumDays, MechAvail = m.MechAvail, NonTAMechUnavail = m.NonTAMechUnavail
	, MaintEffIndex = m.MaintEffIndex, RoutCost = m.RoutCost, AnnTACost = m.AnnTACost, MaintEffDiv = m.MaintEffDiv, TAIndex = m.TAIndex
	, mPersEffIndex = m.mPersEffIndex, MaintWHr = m.MaintWHr, OCCMaintWHr = m.OCCMaintWHr, MPSMaintWHr = m.MPSMaintWHr, mPersEffDiv = m.mPersEffDiv
	, TotCashOpExUEdc = m.TotCashOpExUEdc, TAAdj = m.TAAdj, EnergyCost = m.EnergyCost, TotCashOpEx = m.TotCashOpEx, UEdc = m.UEdc
	, NEOpExEdc = m.NEOpExEdc, NEOpEx = m.NEOpEx, Edc = m.Edc, ExchRate = m.ExchRate
	, ProcessUtilPcnt_QTR = q.ProcessUtilPcnt, ProcessEdc_QTR = q.ProcessEdc, ProcessUEdc_QTR = q.ProcessUEdc, OffsitesEdc_QTR = q.OffsitesEdc, Complexity_QTR = q.Complexity
	, EII_QTR = q.EII, EnergyUseDay_QTR = q.EnergyUseDay, TotStdEnergy_QTR = q.TotStdEnergy, FuelUseDay_QTR = q.FuelUseDay
	, OpAvail_QTR = q.OpAvail, MechUnavailTA_QTR = q.MechUnavailTA, NonTAOpUnavail_QTR = q.NonTAOpUnavail, TADD_QTR = q.TADD, NTAMDD_QTR = q.NTAMDD, RPDD_QTR = q.RPDD, NumDays_QTR = q.NumDays, MechAvail_QTR = q.MechAvail, NonTAMechUnavail_QTR = q.NonTAMechUnavail
	, MaintEffIndex_QTR = q.MaintEffIndex, RoutCost_QTR = q.RoutCost, AnnTACost_QTR = q.AnnTACost, MaintEffDiv_QTR = q.MaintEffDiv, TAIndex_QTR = q.TAIndex
	, mPersEffIndex_QTR = q.mPersEffIndex, MaintWHr_QTR = q.MaintWHr, OCCMaintWHr_QTR = q.OCCMaintWHr, MPSMaintWHr_QTR = q.MPSMaintWHr, mPersEffDiv_QTR = q.mPersEffDiv
	, TotCashOpExUEdc_QTR = q.TotCashOpExUEdc, TAAdj_QTR = q.TAAdj, EnergyCost_QTR = q.EnergyCost, TotCashOpEx_QTR = q.TotCashOpEx, UEdc_QTR = q.UEdc
	, NEOpExEdc_QTR = q.NEOpExEdc, NEOpEx_QTR = q.NEOpEx, Edc_QTR = q.Edc, ExchRate_QTR = q.ExchRate
	, ProcessUtilPcnt_Q1 = q1.ProcessUtilPcnt, ProcessEdc_Q1 = q1.ProcessEdc, ProcessUEdc_Q1 = q1.ProcessUEdc, OffsitesEdc_Q1 = q1.OffsitesEdc, Complexity_Q1 = q1.Complexity
	, EII_Q1 = q1.EII, EnergyUseDay_Q1 = q1.EnergyUseDay, TotStdEnergy_Q1 = q1.TotStdEnergy, FuelUseDay_Q1 = q1.FuelUseDay
	, OpAvail_Q1 = q1.OpAvail, MechUnavailTA_Q1 = q1.MechUnavailTA, NonTAOpUnavail_Q1 = q1.NonTAOpUnavail, TADD_Q1 = q1.TADD, NTAMDD_Q1 = q1.NTAMDD, RPDD_Q1 = q1.RPDD, NumDays_Q1 = q1.NumDays, MechAvail_Q1 = q1.MechAvail, NonTAMechUnavail_Q1 = q1.NonTAMechUnavail
	, MaintEffIndex_Q1 = q1.MaintEffIndex, RoutCost_Q1 = q1.RoutCost, AnnTACost_Q1 = q1.AnnTACost, MaintEffDiv_Q1 = q1.MaintEffDiv, TAIndex_Q1 = q1.TAIndex
	, mPersEffIndex_Q1 = q1.mPersEffIndex, MaintWHr_Q1 = q1.MaintWHr, OCCMaintWHr_Q1 = q1.OCCMaintWHr, MPSMaintWHr_Q1 = q1.MPSMaintWHr, mPersEffDiv_Q1 = q1.mPersEffDiv
	, TotCashOpExUEdc_Q1 = q1.TotCashOpExUEdc, TAAdj_Q1 = q1.TAAdj, EnergyCost_Q1 = q1.EnergyCost, TotCashOpEx_Q1 = q1.TotCashOpEx, UEdc_Q1 = q1.UEdc
	, NEOpExEdc_Q1 = q1.NEOpExEdc, NEOpEx_Q1 = q1.NEOpEx, Edc_Q1 = q1.Edc, ExchRate_Q1 = q1.ExchRate
	, ProcessUtilPcnt_Q2 = q2.ProcessUtilPcnt, ProcessEdc_Q2 = q2.ProcessEdc, ProcessUEdc_Q2 = q2.ProcessUEdc, OffsitesEdc_Q2 = q2.OffsitesEdc, Complexity_Q2 = q2.Complexity
	, EII_Q2 = q2.EII, EnergyUseDay_Q2 = q2.EnergyUseDay, TotStdEnergy_Q2 = q2.TotStdEnergy, FuelUseDay_Q2 = q2.FuelUseDay
	, OpAvail_Q2 = q2.OpAvail, MechUnavailTA_Q2 = q2.MechUnavailTA, NonTAOpUnavail_Q2 = q2.NonTAOpUnavail, TADD_Q2 = q2.TADD, NTAMDD_Q2 = q2.NTAMDD, RPDD_Q2 = q2.RPDD, NumDays_Q2 = q2.NumDays, MechAvail_Q2 = q2.MechAvail, NonTAMechUnavail_Q2 = q2.NonTAMechUnavail
	, MaintEffIndex_Q2 = q2.MaintEffIndex, RoutCost_Q2 = q2.RoutCost, AnnTACost_Q2 = q2.AnnTACost, MaintEffDiv_Q2 = q2.MaintEffDiv, TAIndex_Q2 = q2.TAIndex
	, mPersEffIndex_Q2 = q2.mPersEffIndex, MaintWHr_Q2 = q2.MaintWHr, OCCMaintWHr_Q2 = q2.OCCMaintWHr, MPSMaintWHr_Q2 = q2.MPSMaintWHr, mPersEffDiv_Q2 = q2.mPersEffDiv
	, TotCashOpExUEdc_Q2 = q2.TotCashOpExUEdc, TAAdj_Q2 = q2.TAAdj, EnergyCost_Q2 = q2.EnergyCost, TotCashOpEx_Q2 = q2.TotCashOpEx, UEdc_Q2 = q2.UEdc
	, NEOpExEdc_Q2 = q2.NEOpExEdc, NEOpEx_Q2 = q2.NEOpEx, Edc_Q2 = q2.Edc, ExchRate_Q2 = q2.ExchRate
	, ProcessUtilPcnt_Q3 = q3.ProcessUtilPcnt, ProcessEdc_Q3 = q3.ProcessEdc, ProcessUEdc_Q3 = q3.ProcessUEdc, OffsitesEdc_Q3 = q3.OffsitesEdc, Complexity_Q3 = q3.Complexity
	, EII_Q3 = q3.EII, EnergyUseDay_Q3 = q3.EnergyUseDay, TotStdEnergy_Q3 = q3.TotStdEnergy, FuelUseDay_Q3 = q3.FuelUseDay
	, OpAvail_Q3 = q3.OpAvail, MechUnavailTA_Q3 = q3.MechUnavailTA, NonTAOpUnavail_Q3 = q3.NonTAOpUnavail, TADD_Q3 = q3.TADD, NTAMDD_Q3 = q3.NTAMDD, RPDD_Q3 = q3.RPDD, NumDays_Q3 = q3.NumDays, MechAvail_Q3 = q3.MechAvail, NonTAMechUnavail_Q3 = q3.NonTAMechUnavail
	, MaintEffIndex_Q3 = q3.MaintEffIndex, RoutCost_Q3 = q3.RoutCost, AnnTACost_Q3 = q3.AnnTACost, MaintEffDiv_Q3 = q3.MaintEffDiv, TAIndex_Q3 = q3.TAIndex
	, mPersEffIndex_Q3 = q3.mPersEffIndex, MaintWHr_Q3 = q3.MaintWHr, OCCMaintWHr_Q3 = q3.OCCMaintWHr, MPSMaintWHr_Q3 = q3.MPSMaintWHr, mPersEffDiv_Q3 = q3.mPersEffDiv
	, TotCashOpExUEdc_Q3 = q3.TotCashOpExUEdc, TAAdj_Q3 = q3.TAAdj, EnergyCost_Q3 = q3.EnergyCost, TotCashOpEx_Q3 = q3.TotCashOpEx, UEdc_Q3 = q3.UEdc
	, NEOpExEdc_Q3 = q3.NEOpExEdc, NEOpEx_Q3 = q3.NEOpEx, Edc_Q3 = q3.Edc, ExchRate_Q3 = q3.ExchRate
	, ProcessUtilPcnt_Q4 = q4.ProcessUtilPcnt, ProcessEdc_Q4 = q4.ProcessEdc, ProcessUEdc_Q4 = q4.ProcessUEdc, OffsitesEdc_Q4 = q4.OffsitesEdc, Complexity_Q4 = q4.Complexity
	, EII_Q4 = q4.EII, EnergyUseDay_Q4 = q4.EnergyUseDay, TotStdEnergy_Q4 = q4.TotStdEnergy, FuelUseDay_Q4 = q4.FuelUseDay
	, OpAvail_Q4 = q4.OpAvail, MechUnavailTA_Q4 = q4.MechUnavailTA, NonTAOpUnavail_Q4 = q4.NonTAOpUnavail, TADD_Q4 = q4.TADD, NTAMDD_Q4 = q4.NTAMDD, RPDD_Q4 = q4.RPDD, NumDays_Q4 = q4.NumDays, MechAvail_Q4 = q4.MechAvail, NonTAMechUnavail_Q4 = q4.NonTAMechUnavail
	, MaintEffIndex_Q4 = q4.MaintEffIndex, RoutCost_Q4 = q4.RoutCost, AnnTACost_Q4 = q4.AnnTACost, MaintEffDiv_Q4 = q4.MaintEffDiv, TAIndex_Q4 = q4.TAIndex
	, mPersEffIndex_Q4 = q4.mPersEffIndex, MaintWHr_Q4 = q4.MaintWHr, OCCMaintWHr_Q4 = q4.OCCMaintWHr, MPSMaintWHr_Q4 = q4.MPSMaintWHr, mPersEffDiv_Q4 = q4.mPersEffDiv
	, TotCashOpExUEdc_Q4 = q4.TotCashOpExUEdc, TAAdj_Q4 = q4.TAAdj, EnergyCost_Q4 = q4.EnergyCost, TotCashOpEx_Q4 = q4.TotCashOpEx, UEdc_Q4 = q4.UEdc
	, NEOpExEdc_Q4 = q4.NEOpExEdc, NEOpEx_Q4 = q4.NEOpEx, Edc_Q4 = q4.Edc, ExchRate_Q4 = q4.ExchRate
	, ProcessUtilPcnt_YTQ2 = ytq2.ProcessUtilPcnt, ProcessEdc_YTQ2 = ytq2.ProcessEdc, ProcessUEdc_YTQ2 = ytq2.ProcessUEdc, OffsitesEdc_YTQ2 = ytq2.OffsitesEdc, Complexity_YTQ2 = ytq2.Complexity
	, EII_YTQ2 = ytq2.EII, EnergyUseDay_YTQ2 = ytq2.EnergyUseDay, TotStdEnergy_YTQ2 = ytq2.TotStdEnergy, FuelUseDay_YTQ2 = ytq2.FuelUseDay
	, OpAvail_YTQ2 = ytq2.OpAvail, MechUnavailTA_YTQ2 = ytq2.MechUnavailTA, NonTAOpUnavail_YTQ2 = ytq2.NonTAOpUnavail, TADD_YTQ2 = ytq2.TADD, NTAMDD_YTQ2 = ytq2.NTAMDD, RPDD_YTQ2 = ytq2.RPDD, NumDays_YTQ2 = ytq2.NumDays, MechAvail_YTQ2 = ytq2.MechAvail, NonTAMechUnavail_YTQ2 = ytq2.NonTAMechUnavail
	, MaintEffIndex_YTQ2 = ytq2.MaintEffIndex, RoutCost_YTQ2 = ytq2.RoutCost, AnnTACost_YTQ2 = ytq2.AnnTACost, MaintEffDiv_YTQ2 = ytq2.MaintEffDiv, TAIndex_YTQ2 = ytq2.TAIndex
	, mPersEffIndex_YTQ2 = ytq2.mPersEffIndex, MaintWHr_YTQ2 = ytq2.MaintWHr, OCCMaintWHr_YTQ2 = ytq2.OCCMaintWHr, MPSMaintWHr_YTQ2 = ytq2.MPSMaintWHr, mPersEffDiv_YTQ2 = ytq2.mPersEffDiv
	, TotCashOpExUEdc_YTQ2 = ytq2.TotCashOpExUEdc, TAAdj_YTQ2 = ytq2.TAAdj, EnergyCost_YTQ2 = ytq2.EnergyCost, TotCashOpEx_YTQ2 = ytq2.TotCashOpEx, UEdc_YTQ2 = ytq2.UEdc
	, NEOpExEdc_YTQ2 = ytq2.NEOpExEdc, NEOpEx_YTQ2 = ytq2.NEOpEx, Edc_YTQ2 = ytq2.Edc, ExchRate_YTQ2 = ytq2.ExchRate
	, ProcessUtilPcnt_YTQ3 = ytq3.ProcessUtilPcnt, ProcessEdc_YTQ3 = ytq3.ProcessEdc, ProcessUEdc_YTQ3 = ytq3.ProcessUEdc, OffsitesEdc_YTQ3 = ytq3.OffsitesEdc, Complexity_YTQ3 = ytq3.Complexity
	, EII_YTQ3 = ytq3.EII, EnergyUseDay_YTQ3 = ytq3.EnergyUseDay, TotStdEnergy_YTQ3 = ytq3.TotStdEnergy, FuelUseDay_YTQ3 = ytq3.FuelUseDay
	, OpAvail_YTQ3 = ytq3.OpAvail, MechUnavailTA_YTQ3 = ytq3.MechUnavailTA, NonTAOpUnavail_YTQ3 = ytq3.NonTAOpUnavail, TADD_YTQ3 = ytq3.TADD, NTAMDD_YTQ3 = ytq3.NTAMDD, RPDD_YTQ3 = ytq3.RPDD, NumDays_YTQ3 = ytq3.NumDays, MechAvail_YTQ3 = ytq3.MechAvail, NonTAMechUnavail_YTQ3 = ytq3.NonTAMechUnavail
	, MaintEffIndex_YTQ3 = ytq3.MaintEffIndex, RoutCost_YTQ3 = ytq3.RoutCost, AnnTACost_YTQ3 = ytq3.AnnTACost, MaintEffDiv_YTQ3 = ytq3.MaintEffDiv, TAIndex_YTQ3 = ytq3.TAIndex
	, mPersEffIndex_YTQ3 = ytq3.mPersEffIndex, MaintWHr_YTQ3 = ytq3.MaintWHr, OCCMaintWHr_YTQ3 = ytq3.OCCMaintWHr, MPSMaintWHr_YTQ3 = ytq3.MPSMaintWHr, mPersEffDiv_YTQ3 = ytq3.mPersEffDiv
	, TotCashOpExUEdc_YTQ3 = ytq3.TotCashOpExUEdc, TAAdj_YTQ3 = ytq3.TAAdj, EnergyCost_YTQ3 = ytq3.EnergyCost, TotCashOpEx_YTQ3 = ytq3.TotCashOpEx, UEdc_YTQ3 = ytq3.UEdc
	, NEOpExEdc_YTQ3 = ytq3.NEOpExEdc, NEOpEx_YTQ3 = ytq3.NEOpEx, Edc_YTQ3 = ytq3.Edc, ExchRate_YTQ3 = ytq3.ExchRate
	, ProcessUtilPcnt_Ytd = yr.ProcessUtilPcnt, ProcessEdc_Ytd = yr.ProcessEdc, ProcessUEdc_Ytd = yr.ProcessUEdc, OffsitesEdc_Ytd = yr.OffsitesEdc, Complexity_Ytd = yr.Complexity
	, EII_Ytd = yr.EII, EnergyUseDay_Ytd = yr.EnergyUseDay, TotStdEnergy_Ytd = yr.TotStdEnergy, FuelUseDay_Ytd = yr.FuelUseDay
	, OpAvail_Ytd = yr.OpAvail, MechUnavailTA_Ytd = yr.MechUnavailTA, NonTAOpUnavail_Ytd = yr.NonTAOpUnavail, TADD_Ytd = yr.TADD, NTAMDD_Ytd = yr.NTAMDD, RPDD_Ytd = yr.RPDD, NumDays_Ytd = yr.NumDays, MechAvail_Ytd = yr.MechAvail, NonTAMechUnavail_Ytd = yr.NonTAMechUnavail
	, MaintEffIndex_Ytd = yr.MaintEffIndex, RoutCost_Ytd = yr.RoutCost, AnnTACost_Ytd = yr.AnnTACost, MaintEffDiv_Ytd = yr.MaintEffDiv, TAIndex_Ytd = yr.TAIndex
	, mPersEffIndex_Ytd = yr.mPersEffIndex, MaintWHr_Ytd = yr.MaintWHr, OCCMaintWHr_Ytd = yr.OCCMaintWHr, MPSMaintWHr_Ytd = yr.MPSMaintWHr, mPersEffDiv_Ytd = yr.mPersEffDiv
	, TotCashOpExUEdc_Ytd = yr.TotCashOpExUEdc, TAAdj_Ytd = yr.TAAdj, EnergyCost_Ytd = yr.EnergyCost, TotCashOpEx_Ytd = yr.TotCashOpEx, UEdc_Ytd = yr.UEdc
	, NEOpExEdc_Ytd = yr.NEOpExEdc, NEOpEx_Ytd = yr.NEOpEx, Edc_Ytd = yr.Edc, ExchRate_Ytd = yr.ExchRate
	, ProcessUtilPcnt_Avg = avg12.ProcessUtilPcnt, ProcessEdc_Avg = avg12.ProcessEdc, ProcessUedc_Avg = avg12.ProcessUEdc, OffsitesEdc_Avg = avg12.OffsitesEdc, Complexity_Avg = avg12.Complexity
	, EII_Avg = avg12.EII, EnergyUseDay_Avg = avg12.EnergyUseDay, TotStdEnergy_Avg = avg12.TotStdEnergy, FuelUseDay_Avg = avg12.FuelUseDay
	, OpAvail_Avg = avg24.OpAvail, MechUnavailTA_Avg = avg24.MechUnavailTA, NonTAOpUnavail_Avg = avg24.NonTAOpUnavail, TADD_Avg = avg24.TADD, NTAMDD_Avg = avg24.NTAMDD, RPDD_Avg = avg24.RPDD, NumDays_Avg = avg24.NumDays, MechAvail_Avg = avg24.MechAvail, NonTAMechUnavail_Avg = avg24.NonTAMechUnavail
	, MaintEffIndex_Avg = mi24.MaintEffIndex, RoutCost_Avg = mi24.RoutEffIndex*avg24.MaintEffDiv/100, AnnTACost_Avg = mi24.TAEffIndex*avg24.MaintEffDiv/100, MaintEffDiv_Avg = avg24.MaintEffDiv, TAIndex_Avg = mi24.TAIndex
	, mPersEffIndex_Avg = avg12.mPersEffIndex, MaintWHr_Avg = avg12.MaintWHr, OCCMaintWHr_Avg = avg12.OCCMaintWHr, MPSMaintWHr_Avg = avg12.MPSMaintWHr, mPersEffDiv_Avg = avg12.mPersEffDiv
	, TotCashOpExUedc_Avg = avg12.TotCashOpExUEdc, TAAdj_Avg = avg12.TAAdj, EnergyCost_Avg = avg12.EnergyCost, TotCashOpEx_Avg = avg12.TotCashOpEx, Uedc_Avg = avg12.UEdc
	, NEOpExEdc_Avg = avg12.NEOpExEdc, NEOpEx_Avg = avg12.NEOpEx, Edc_Avg = avg12.Edc, ExchRate_Avg = avg12.ExchRate
FROM dbo.CalcBashneftKPIs2(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQTR, @FactorSet, @Scenario, @Currency, @UOM) q ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ1, @FactorSet, @Scenario, @Currency, @UOM) q1 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ2, @FactorSet, @Scenario, @Currency, @UOM) q2 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ3, @FactorSet, @Scenario, @Currency, @UOM) q3 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListQ4, @FactorSet, @Scenario, @Currency, @UOM) q4 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListYTQ2, @FactorSet, @Scenario, @Currency, @UOM) ytq2 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListYTQ3, @FactorSet, @Scenario, @Currency, @UOM) ytq3 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubListYear, @FactorSet, @Scenario, @Currency, @UOM) yr ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12 ON 1=1
   LEFT JOIN dbo.CalcBashneftKPIs2(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24 ON 1=1
   LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1

