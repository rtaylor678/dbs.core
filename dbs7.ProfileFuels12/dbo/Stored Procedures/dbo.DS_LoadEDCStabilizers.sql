﻿CREATE PROCEDURE [dbo].[DS_LoadEdcStabilizers]
		@RefineryID nvarchar(10)
AS
BEGIN
		SELECT EffDate,AnnInputBbl,AnnCokeBbl,AnnElecConsMWH, 
                  AnnRSCRUDE_RAIL,AnnRSCRUDE_TT,AnnRSCRUDE_TB,AnnRSCRUDE_OMB, 
                  AnnRSCRUDE_BB,AnnRSPROD_RAIL,AnnRSPROD_TT,AnnRSPROD_TB,AnnRSPROD_OMB,
                  AnnRSPROD_BB 
                  FROM LoadedEdcStabilizers 
                  WHERE RefineryID = @RefineryID
END
