﻿CREATE PROC [dbo].[ListOfUnits] (@RefineryID char(6), @PeriodYear smallint = NULL, @PeriodMonth smallint = NULL, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = NULL, @Scenario Scenario = NULL, @Currency CurrencyCode = NULL, @UOM varchar(5) = NULL)
AS
SELECT c2.UnitID, c2.ProcessID, c2.UnitName
FROM Submissions s2 INNER JOIN Config c2 ON c2.SubmissionID = s2.SubmissionID
INNER JOIN (SELECT c.UnitID, MAX(s.PeriodStart) AS LastEntry
	FROM Submissions s INNER JOIN Config c ON c.SubmissionID = s.SubmissionID
	WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet
	AND c.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD', 'COGEN', 'FCCPOWER', 'BLENDING') 
	GROUP BY c.UnitID) x ON x.UnitID = c2.UnitID AND x.LastEntry = s2.PeriodStart
WHERE s2.RefineryID = @RefineryID AND s2.DataSet = @DataSet
ORDER BY c2.SortKey

