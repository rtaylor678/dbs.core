﻿

CREATE PROC [dbo].[GetMaterialLU] (@RefineryID varchar(6))
AS
SET NOCOUNT ON

DECLARE @IncludeLubes bit
SELECT @IncludeLubes = FuelsLubesCombo
FROM TSort WHERE RefineryID = @RefineryID

DECLARE @MLU TABLE (
	MaterialID varchar(5) NOT NULL, 
	SAIName varchar(60) NULL, 
	SortKey smallint NULL, 
	BPSortKey smallint NULL,
	AllowInRMI bit NOT NULL, 
	AllowInOTHRM bit NOT NULL, 
	AllowInRChem bit NOT NULL, 
	AllowInRLube bit NOT NULL, 
	AllowInProd bit NOT NULL, 
	AllowInFLube bit NOT NULL,
	AllowInFChem bit NOT NULL, 
	AllowInASP bit NOT NULL, 
	AllowInCoke bit NOT NULL, 
	AllowInSolv bit NOT NULL, 
	AllowInMProd bit NOT NULL, 
	LubesOnly bit NOT NULL,
	AllowInRMB bit NOT NULL
)

INSERT INTO @MLU (MaterialID, SAIName, SortKey, BPSortKey,
	AllowInRMI, AllowInOTHRM, AllowInRChem, AllowInRLube, AllowInProd, AllowInFLube,
	AllowInFChem, AllowInASP, AllowInCoke, AllowInSolv, AllowInMProd, LubesOnly, AllowInRMB)
SELECT RTRIM(MaterialID) as MaterialID, RTRIM(SAIName) as SAIName, SortKey, BPSortKey,
	AllowInRMI, AllowInOTHRM, AllowInRChem, AllowInRLube, AllowInProd, AllowInFLube,
	AllowInFChem, AllowInASP, AllowInCoke, AllowInSolv, AllowInMProd, LubesOnly, AllowInRMB
FROM Material_LU
WHERE LubesOnly = 0 OR @IncludeLubes = 1

UPDATE @MLU
SET AllowInOTHRM = 1
WHERE AllowInOTHRM = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'OTHRM' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInRMB = 1
WHERE AllowInRMB = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'RMB' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInRChem = 1
WHERE AllowInRChem = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'RCHEM' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInRLube = 1
WHERE AllowInRLube = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'RLUBE' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInFChem = 1
WHERE AllowInFChem = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'FCHEM' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInFLube = 1
WHERE AllowInFLube = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'FLUBE' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInASP = 1
WHERE AllowInASP = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'ASP' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInCoke = 1
WHERE AllowInCoke = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'COKE' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInSolv = 1
WHERE AllowInSolv = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'SOLV' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInMProd = 1
WHERE AllowInMProd = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'MPROD' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))

UPDATE @MLU
SET AllowInProd = 1
WHERE AllowInProd = 0
AND MaterialID IN (SELECT MaterialID FROM Yield 
	WHERE Category = 'PROD' 
	AND SubmissionID IN (SELECT SubmissionID FROM Submissions WHERE RefineryID = @RefineryID))
SET NOCOUNT OFF
SELECT * FROM @MLU




