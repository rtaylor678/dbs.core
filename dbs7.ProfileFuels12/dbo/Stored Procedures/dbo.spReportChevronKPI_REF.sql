﻿
CREATE   PROC [dbo].[spReportChevronKPI_REF] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1,
	@UtilPcnt real = NULL OUTPUT, @KEdc real = NULL OUTPUT, @KUEdc real = NULL OUTPUT, 
	@MechAvail real = NULL OUTPUT, @KMAEdc real = NULL OUTPUT, @KProcEdc real = NULL OUTPUT,
	@EII real = NULL OUTPUT, @KEnergyUseDay real = NULL OUTPUT, @KTotStdEnergy real = NULL OUTPUT,
	@AdjMaintIndex real = NULL OUTPUT, @TAIndex_Avg real = NULL OUTPUT, @RoutIndex real = NULL OUTPUT,   
	@TotEqPEdc real = NULL OUTPUT, 
	@NEOpExUEdc real = NULL OUTPUT, @TotCashOpExUEdc real = NULL OUTPUT,
	@CashMargin real = NULL OUTPUT, @NetInputBPD real = NULL OUTPUT)

AS
SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = 'Actual' AND UseSubmission = 1

SELECT	@UtilPcnt = NULL, @KEdc = NULL, @KUEdc = NULL, 
	@MechAvail = NULL, @KMAEdc = NULL, @KProcEdc = NULL,
	@EII = NULL, @KEnergyUseDay = NULL, @KTotStdEnergy = NULL,
	@AdjMaintIndex = NULL, @TAIndex_Avg = NULL, @RoutIndex = NULL,   
	@TotEqPEdc = NULL, 
	@NEOpExUEdc = NULL, @TotCashOpExUEdc = NULL,
	@CashMargin = NULL, @NetInputBPD = NULL
IF @SubmissionID IS NULL
	RETURN 0

SELECT	@UtilPcnt = UtilPcnt, @KEdc = Edc/1000, @KUEdc = UEdc/1000, 
	@MechAvail = MechAvail, 
	@EII = EII,
	@AdjMaintIndex = RoutIndex+TAIndex_Avg, @TAIndex_Avg = TAIndex_Avg, @RoutIndex = RoutIndex,   
	@TotEqPEdc = TotEqPEdc, 
	@NEOpExUEdc = NEOpExUEdc, @TotCashOpExUEdc = TotCashOpExUEdc,
	@CashMargin = CashMargin, @NetInputBPD = NetInputBPD
FROM GenSum
WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND Currency = 'USD' AND UOM = @UOM AND Scenario = @Scenario

SELECT @KMAEdc = TotProcessEdc*@MechAvail/100/1000, @KProcEdc = TotProcessEdc/1000,
	@KEnergyUseDay = EnergyUseDay/1000, @KTotStdEnergy = TotStdEnergy/1000
FROM FactorTotCalc 
WHERE SubmissionID = @SubmissionID AND FactorSet = '2008'






