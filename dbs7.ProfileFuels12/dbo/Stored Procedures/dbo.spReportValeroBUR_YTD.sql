﻿CREATE   PROC [dbo].[spReportValeroBUR_Ytd] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15) = 'ACTUAL', 
	@FactorSet FactorSet = '2012', @Scenario Scenario = 'CLIENT', @Currency CurrencyCode = 'USD', @UOM varchar(5) = 'US',
	@IncludeTarget bit = 1, @IncludeYTD bit = 1, @IncludeAvg bit = 1)
AS

SET NOCOUNT ON
DECLARE @AD_KEdc_Ytd real, @AD_MechAvail_Ytd real, @AD_AdjMaintIndex_Ytd real, 
	@AD_UtilPcnt_Ytd real, @AD_NEOpExEdc_Ytd real, @AD_EII_Ytd real, @AD_TotWhrEdc_Ytd real,
	@AD_NetInputKBPD_Ytd real, @AD_CrudeKBPD_Ytd real, @AD_FCCRateKBPD_Ytd real, 
	@AD_NumContPers_Ytd real, @AD_RoutMechUnavail_Ytd real, @AD_TAMechUnavail_Ytd real
DECLARE @BN_KEdc_Ytd real, @BN_MechAvail_Ytd real, @BN_AdjMaintIndex_Ytd real, 
	@BN_UtilPcnt_Ytd real, @BN_NEOpExEdc_Ytd real, @BN_EII_Ytd real, @BN_TotWhrEdc_Ytd real,
	@BN_NetInputKBPD_Ytd real, @BN_CrudeKBPD_Ytd real, @BN_FCCRateKBPD_Ytd real, 
	@BN_NumContPers_Ytd real, @BN_RoutMechUnavail_Ytd real, @BN_TAMechUnavail_Ytd real
DECLARE @CC_KEdc_Ytd real, @CC_MechAvail_Ytd real, @CC_AdjMaintIndex_Ytd real, 
	@CC_UtilPcnt_Ytd real, @CC_NEOpExEdc_Ytd real, @CC_EII_Ytd real, @CC_TotWhrEdc_Ytd real,
	@CC_NetInputKBPD_Ytd real, @CC_CrudeKBPD_Ytd real, @CC_FCCRateKBPD_Ytd real, 
	@CC_NumContPers_Ytd real, @CC_RoutMechUnavail_Ytd real, @CC_TAMechUnavail_Ytd real
DECLARE @HO_KEdc_Ytd real, @HO_MechAvail_Ytd real, @HO_AdjMaintIndex_Ytd real, 
	@HO_UtilPcnt_Ytd real, @HO_NEOpExEdc_Ytd real, @HO_EII_Ytd real, @HO_TotWhrEdc_Ytd real,
	@HO_NetInputKBPD_Ytd real, @HO_CrudeKBPD_Ytd real, @HO_FCCRateKBPD_Ytd real, 
	@HO_NumContPers_Ytd real, @HO_RoutMechUnavail_Ytd real, @HO_TAMechUnavail_Ytd real
DECLARE @MK_KEdc_Ytd real, @MK_MechAvail_Ytd real, @MK_AdjMaintIndex_Ytd real, 
	@MK_UtilPcnt_Ytd real, @MK_NEOpExEdc_Ytd real, @MK_EII_Ytd real, @MK_TotWhrEdc_Ytd real,
	@MK_NetInputKBPD_Ytd real, @MK_CrudeKBPD_Ytd real, @MK_FCCRateKBPD_Ytd real, 
	@MK_NumContPers_Ytd real, @MK_RoutMechUnavail_Ytd real, @MK_TAMechUnavail_Ytd real
DECLARE @MP_KEdc_Ytd real, @MP_MechAvail_Ytd real, @MP_AdjMaintIndex_Ytd real, 
	@MP_UtilPcnt_Ytd real, @MP_NEOpExEdc_Ytd real, @MP_EII_Ytd real, @MP_TotWhrEdc_Ytd real,
	@MP_NetInputKBPD_Ytd real, @MP_CrudeKBPD_Ytd real, @MP_FCCRateKBPD_Ytd real, 
	@MP_NumContPers_Ytd real, @MP_RoutMechUnavail_Ytd real, @MP_TAMechUnavail_Ytd real
DECLARE @PA_KEdc_Ytd real, @PA_MechAvail_Ytd real, @PA_AdjMaintIndex_Ytd real, 
	@PA_UtilPcnt_Ytd real, @PA_NEOpExEdc_Ytd real, @PA_EII_Ytd real, @PA_TotWhrEdc_Ytd real,
	@PA_NetInputKBPD_Ytd real, @PA_CrudeKBPD_Ytd real, @PA_FCCRateKBPD_Ytd real, 
	@PA_NumContPers_Ytd real, @PA_RoutMechUnavail_Ytd real, @PA_TAMechUnavail_Ytd real
DECLARE @QB_KEdc_Ytd real, @QB_MechAvail_Ytd real, @QB_AdjMaintIndex_Ytd real, 
	@QB_UtilPcnt_Ytd real, @QB_NEOpExEdc_Ytd real, @QB_EII_Ytd real, @QB_TotWhrEdc_Ytd real,
	@QB_NetInputKBPD_Ytd real, @QB_CrudeKBPD_Ytd real, @QB_FCCRateKBPD_Ytd real, 
	@QB_NumContPers_Ytd real, @QB_RoutMechUnavail_Ytd real, @QB_TAMechUnavail_Ytd real
DECLARE @SC_KEdc_Ytd real, @SC_MechAvail_Ytd real, @SC_AdjMaintIndex_Ytd real, 
	@SC_UtilPcnt_Ytd real, @SC_NEOpExEdc_Ytd real, @SC_EII_Ytd real, @SC_TotWhrEdc_Ytd real,
	@SC_NetInputKBPD_Ytd real, @SC_CrudeKBPD_Ytd real, @SC_FCCRateKBPD_Ytd real, 
	@SC_NumContPers_Ytd real, @SC_RoutMechUnavail_Ytd real, @SC_TAMechUnavail_Ytd real
DECLARE @TC_KEdc_Ytd real, @TC_MechAvail_Ytd real, @TC_AdjMaintIndex_Ytd real, 
	@TC_UtilPcnt_Ytd real, @TC_NEOpExEdc_Ytd real, @TC_EII_Ytd real, @TC_TotWhrEdc_Ytd real,
	@TC_NetInputKBPD_Ytd real, @TC_CrudeKBPD_Ytd real, @TC_FCCRateKBPD_Ytd real, 
	@TC_NumContPers_Ytd real, @TC_RoutMechUnavail_Ytd real, @TC_TAMechUnavail_Ytd real
DECLARE @TR_KEdc_Ytd real, @TR_MechAvail_Ytd real, @TR_AdjMaintIndex_Ytd real, 
	@TR_UtilPcnt_Ytd real, @TR_NEOpExEdc_Ytd real, @TR_EII_Ytd real, @TR_TotWhrEdc_Ytd real,
	@TR_NetInputKBPD_Ytd real, @TR_CrudeKBPD_Ytd real, @TR_FCCRateKBPD_Ytd real, 
	@TR_NumContPers_Ytd real, @TR_RoutMechUnavail_Ytd real, @TR_TAMechUnavail_Ytd real
DECLARE @WM_KEdc_Ytd real, @WM_MechAvail_Ytd real, @WM_AdjMaintIndex_Ytd real, 
	@WM_UtilPcnt_Ytd real, @WM_NEOpExEdc_Ytd real, @WM_EII_Ytd real, @WM_TotWhrEdc_Ytd real,
	@WM_NetInputKBPD_Ytd real, @WM_CrudeKBPD_Ytd real, @WM_FCCRateKBPD_Ytd real, 
	@WM_NumContPers_Ytd real, @WM_RoutMechUnavail_Ytd real, @WM_TAMechUnavail_Ytd real
DECLARE @VALERO_KEdc_Ytd real, @VALERO_MechAvail_Ytd real, @VALERO_AdjMaintIndex_Ytd real, 
	@VALERO_UtilPcnt_Ytd real, @VALERO_NEOpExEdc_Ytd real, @VALERO_EII_Ytd real, @VALERO_TotWhrEdc_Ytd real,
	@VALERO_NetInputKBPD_Ytd real, @VALERO_CrudeKBPD_Ytd real, @VALERO_FCCRateKBPD_Ytd real, 
	@VALERO_NumContPers_Ytd real, @VALERO_RoutMechUnavail_Ytd real, @VALERO_TAMechUnavail_Ytd real

EXEC [dbo].[spReportValeroBURYTDByRef] '75NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @AD_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @AD_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @AD_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @AD_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @AD_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @AD_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @AD_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @AD_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @AD_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @AD_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @AD_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @AD_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @AD_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '33NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @BN_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @BN_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @BN_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @BN_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @BN_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @BN_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @BN_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @BN_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @BN_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @BN_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @BN_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @BN_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @BN_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '218NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @CC_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @CC_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @CC_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @CC_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @CC_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @CC_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @CC_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @CC_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @CC_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @CC_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @CC_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @CC_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @CC_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '98NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @HO_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @HO_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @HO_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @HO_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @HO_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @HO_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @HO_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @HO_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @HO_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @HO_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @HO_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @BN_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @HO_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '28NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @MK_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @MK_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @MK_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @MK_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @MK_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @MK_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @MK_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @MK_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @MK_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @MK_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @MK_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @MK_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @MK_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '38NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @MP_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @MP_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @MP_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @MP_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @MP_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @MP_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @MP_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @MP_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @MP_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @MP_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @MP_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @MP_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @MP_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '19NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @PA_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @PA_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @PA_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @PA_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @PA_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @PA_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @PA_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @PA_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @PA_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @PA_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @PA_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @PA_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @PA_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '110NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @QB_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @QB_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @QB_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @QB_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @QB_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @QB_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @QB_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @QB_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @QB_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @QB_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @QB_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @QB_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @QB_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '217NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @SC_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @SC_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @SC_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @SC_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @SC_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @SC_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @SC_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @SC_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @SC_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @SC_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @SC_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @SC_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @SC_TAMechUnavail_Ytd OUTPUT
	
EXEC [dbo].[spReportValeroBURYTDByRef] '72NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @TC_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @TC_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @TC_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @TC_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @TC_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @TC_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @TC_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @TC_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @TC_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @TC_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @TC_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @TC_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @TC_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '29NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @TR_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @TR_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @TR_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @TR_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @TR_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @TR_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @TR_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @TR_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @TR_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @TR_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @TR_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @TR_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @TR_TAMechUnavail_Ytd OUTPUT

EXEC [dbo].[spReportValeroBURYTDByRef] '91NSA', @PeriodYear, @PeriodMonth, @DataSet, @FactorSet, @Scenario, @Currency, @UOM,
	@KEdc_Ytd = @WM_KEdc_Ytd OUTPUT, @MechAvail_Ytd = @WM_MechAvail_Ytd OUTPUT, @AdjMaintIndex_Ytd = @WM_AdjMaintIndex_Ytd OUTPUT, 
	@UtilPcnt_Ytd = @WM_UtilPcnt_Ytd OUTPUT, @NEOpExEdc_Ytd = @WM_NEOpExEdc_Ytd OUTPUT, @EII_Ytd = @WM_EII_Ytd OUTPUT, 
	@TotWhrEdc_Ytd = @WM_TotWhrEdc_Ytd OUTPUT, @NetInputKBPD_Ytd = @WM_NetInputKBPD_Ytd OUTPUT, 
	@CrudeKBPD_Ytd = @WM_CrudeKBPD_Ytd OUTPUT, @FCCRateKBPD_Ytd = @WM_FCCRateKBPD_Ytd OUTPUT,  
	@NumContPers_Ytd = @WM_NumContPers_Ytd OUTPUT, @RoutMechUnavail_Ytd = @WM_RoutMechUnavail_Ytd OUTPUT, @TAMechUnavail_Ytd = @WM_TAMechUnavail_Ytd OUTPUT

SELECT 	@VALERO_KEdc_Ytd = ISNULL(@AD_KEdc_Ytd,0) + ISNULL(@BN_KEdc_Ytd,0) + ISNULL(@CC_KEdc_Ytd,0) + ISNULL(@HO_KEdc_Ytd,0) + ISNULL(@MK_KEdc_Ytd,0) 
			+ ISNULL(@MP_KEdc_Ytd,0) + ISNULL(@PA_KEdc_Ytd,0) + ISNULL(@QB_KEdc_Ytd,0) + ISNULL(@SC_KEdc_Ytd,0) 
			+ ISNULL(@TC_KEdc_Ytd,0) + ISNULL(@TR_KEdc_Ytd,0) + ISNULL(@WM_KEdc_Ytd,0), 
	@VALERO_NetInputKBPD_Ytd = ISNULL(@AD_NetInputKBPD_Ytd,0) + ISNULL(@BN_NetInputKBPD_Ytd,0) + ISNULL(@CC_NetInputKBPD_Ytd,0) + ISNULL(@HO_NetInputKBPD_Ytd,0) + ISNULL(@MK_NetInputKBPD_Ytd,0) 
			+ ISNULL(@MP_NetInputKBPD_Ytd,0) + ISNULL(@PA_NetInputKBPD_Ytd,0) + ISNULL(@QB_NetInputKBPD_Ytd,0) + ISNULL(@SC_NetInputKBPD_Ytd,0) 
			+ ISNULL(@TC_NetInputKBPD_Ytd,0) + ISNULL(@TR_NetInputKBPD_Ytd,0) + ISNULL(@WM_NetInputKBPD_Ytd,0),
	@VALERO_CrudeKBPD_Ytd = ISNULL(@AD_CrudeKBPD_Ytd,0) + ISNULL(@BN_CrudeKBPD_Ytd,0) + ISNULL(@CC_CrudeKBPD_Ytd,0) + ISNULL(@HO_CrudeKBPD_Ytd,0) + ISNULL(@MK_CrudeKBPD_Ytd,0) 
			+ ISNULL(@MP_CrudeKBPD_Ytd,0) + ISNULL(@PA_CrudeKBPD_Ytd,0) + ISNULL(@QB_CrudeKBPD_Ytd,0) + ISNULL(@SC_CrudeKBPD_Ytd,0) 
			+ ISNULL(@TC_CrudeKBPD_Ytd,0) + ISNULL(@TR_CrudeKBPD_Ytd,0) + ISNULL(@WM_CrudeKBPD_Ytd,0),
	@VALERO_FCCRateKBPD_Ytd = ISNULL(@AD_FCCRateKBPD_Ytd,0) + ISNULL(@BN_FCCRateKBPD_Ytd,0) + ISNULL(@CC_FCCRateKBPD_Ytd,0) + ISNULL(@HO_FCCRateKBPD_Ytd,0) + ISNULL(@MK_FCCRateKBPD_Ytd,0) 
			+ ISNULL(@MP_FCCRateKBPD_Ytd,0) + ISNULL(@PA_FCCRateKBPD_Ytd,0) + ISNULL(@QB_FCCRateKBPD_Ytd,0) + ISNULL(@SC_FCCRateKBPD_Ytd,0) 
			+ ISNULL(@TC_FCCRateKBPD_Ytd,0) + ISNULL(@TR_FCCRateKBPD_Ytd,0) + ISNULL(@WM_FCCRateKBPD_Ytd,0),
	@VALERO_NumContPers_Ytd = ISNULL(@AD_NumContPers_Ytd,0) + ISNULL(@BN_NumContPers_Ytd,0) + ISNULL(@CC_NumContPers_Ytd,0) + ISNULL(@HO_NumContPers_Ytd,0) + ISNULL(@MK_NumContPers_Ytd,0) 
			+ ISNULL(@MP_NumContPers_Ytd,0) + ISNULL(@PA_NumContPers_Ytd,0) + ISNULL(@QB_NumContPers_Ytd,0) + ISNULL(@SC_NumContPers_Ytd,0) 
			+ ISNULL(@TC_NumContPers_Ytd,0) + ISNULL(@TR_NumContPers_Ytd,0) + ISNULL(@WM_NumContPers_Ytd,0)
IF @VALERO_KEdc_Ytd > 0
	SELECT @VALERO_MechAvail_Ytd = (ISNULL(@AD_MechAvail_Ytd*@AD_KEdc_Ytd,0) + ISNULL(@BN_MechAvail_Ytd*@BN_KEdc_Ytd,0) + ISNULL(@CC_MechAvail_Ytd*@CC_KEdc_Ytd,0) 
			+ ISNULL(@HO_MechAvail_Ytd*@HO_KEdc_Ytd,0) + ISNULL(@MK_MechAvail_Ytd*@MK_KEdc_Ytd,0) + ISNULL(@MP_MechAvail_Ytd*@MP_KEdc_Ytd,0) 
			+ ISNULL(@PA_MechAvail_Ytd*@PA_KEdc_Ytd,0) + ISNULL(@QB_MechAvail_Ytd*@QB_KEdc_Ytd,0) 
			+ ISNULL(@SC_MechAvail_Ytd*@SC_KEdc_Ytd,0) + ISNULL(@TC_MechAvail_Ytd*@TC_KEdc_Ytd,0) + ISNULL(@TR_MechAvail_Ytd*@TR_KEdc_Ytd,0) 
			+ ISNULL(@WM_MechAvail_Ytd*@WM_KEdc_Ytd,0))/@VALERO_KEdc_Ytd, 
	@VALERO_AdjMaintIndex_Ytd = (ISNULL(@AD_AdjMaintIndex_Ytd*@AD_KEdc_Ytd,0) + ISNULL(@BN_AdjMaintIndex_Ytd*@BN_KEdc_Ytd,0) + ISNULL(@CC_AdjMaintIndex_Ytd*@CC_KEdc_Ytd,0) 
			+ ISNULL(@HO_AdjMaintIndex_Ytd*@HO_KEdc_Ytd,0) + ISNULL(@MK_AdjMaintIndex_Ytd*@MK_KEdc_Ytd,0) + ISNULL(@MP_AdjMaintIndex_Ytd*@MP_KEdc_Ytd,0) 
			+ ISNULL(@PA_AdjMaintIndex_Ytd*@PA_KEdc_Ytd,0) + ISNULL(@QB_AdjMaintIndex_Ytd*@QB_KEdc_Ytd,0) 
			+ ISNULL(@SC_AdjMaintIndex_Ytd*@SC_KEdc_Ytd,0) + ISNULL(@TC_AdjMaintIndex_Ytd*@TC_KEdc_Ytd,0) + ISNULL(@TR_AdjMaintIndex_Ytd*@TR_KEdc_Ytd,0) 
			+ ISNULL(@WM_AdjMaintIndex_Ytd*@WM_KEdc_Ytd,0))/@VALERO_KEdc_Ytd, 
	@VALERO_UtilPcnt_Ytd = (ISNULL(@AD_UtilPcnt_Ytd*@AD_KEdc_Ytd,0) + ISNULL(@BN_UtilPcnt_Ytd*@BN_KEdc_Ytd,0) + ISNULL(@CC_UtilPcnt_Ytd*@CC_KEdc_Ytd,0) 
			+ ISNULL(@HO_UtilPcnt_Ytd*@HO_KEdc_Ytd,0) + ISNULL(@MK_UtilPcnt_Ytd*@MK_KEdc_Ytd,0) + ISNULL(@MP_UtilPcnt_Ytd*@MP_KEdc_Ytd,0) 
			+ ISNULL(@PA_UtilPcnt_Ytd*@PA_KEdc_Ytd,0) + ISNULL(@QB_UtilPcnt_Ytd*@QB_KEdc_Ytd,0) 
			+ ISNULL(@SC_UtilPcnt_Ytd*@SC_KEdc_Ytd,0) + ISNULL(@TC_UtilPcnt_Ytd*@TC_KEdc_Ytd,0) + ISNULL(@TR_UtilPcnt_Ytd*@TR_KEdc_Ytd,0) 
			+ ISNULL(@WM_UtilPcnt_Ytd*@WM_KEdc_Ytd,0))/@VALERO_KEdc_Ytd, 
	@VALERO_NEOpExEdc_Ytd = (ISNULL(@AD_NEOpExEdc_Ytd*@AD_KEdc_Ytd,0) + ISNULL(@BN_NEOpExEdc_Ytd*@BN_KEdc_Ytd,0) + ISNULL(@CC_NEOpExEdc_Ytd*@CC_KEdc_Ytd,0) 
			+ ISNULL(@HO_NEOpExEdc_Ytd*@HO_KEdc_Ytd,0) + ISNULL(@MK_NEOpExEdc_Ytd*@MK_KEdc_Ytd,0) + ISNULL(@MP_NEOpExEdc_Ytd*@MP_KEdc_Ytd,0) 
			+ ISNULL(@PA_NEOpExEdc_Ytd*@PA_KEdc_Ytd,0) + ISNULL(@QB_NEOpExEdc_Ytd*@QB_KEdc_Ytd,0) 
			+ ISNULL(@SC_NEOpExEdc_Ytd*@SC_KEdc_Ytd,0) + ISNULL(@TC_NEOpExEdc_Ytd*@TC_KEdc_Ytd,0) + ISNULL(@TR_NEOpExEdc_Ytd*@TR_KEdc_Ytd,0) 
			+ ISNULL(@WM_NEOpExEdc_Ytd*@WM_KEdc_Ytd,0))/@VALERO_KEdc_Ytd, 
	@VALERO_TotWhrEdc_Ytd = (ISNULL(@AD_TotWhrEdc_Ytd*@AD_KEdc_Ytd,0) + ISNULL(@BN_TotWhrEdc_Ytd*@BN_KEdc_Ytd,0) + ISNULL(@CC_TotWhrEdc_Ytd*@CC_KEdc_Ytd,0) 
			+ ISNULL(@HO_TotWhrEdc_Ytd*@HO_KEdc_Ytd,0) + ISNULL(@MK_TotWhrEdc_Ytd*@MK_KEdc_Ytd,0) + ISNULL(@MP_TotWhrEdc_Ytd*@MP_KEdc_Ytd,0) 
			+ ISNULL(@PA_TotWhrEdc_Ytd*@PA_KEdc_Ytd,0) + ISNULL(@QB_TotWhrEdc_Ytd*@QB_KEdc_Ytd,0) 
			+ ISNULL(@SC_TotWhrEdc_Ytd*@SC_KEdc_Ytd,0) + ISNULL(@TC_TotWhrEdc_Ytd*@TC_KEdc_Ytd,0) + ISNULL(@TR_TotWhrEdc_Ytd*@TR_KEdc_Ytd,0) 
			+ ISNULL(@WM_TotWhrEdc_Ytd*@WM_KEdc_Ytd,0))/@VALERO_KEdc_Ytd, 
	@VALERO_RoutMechUnavail_Ytd = (ISNULL(@AD_RoutMechUnavail_Ytd*@AD_KEdc_Ytd,0) + ISNULL(@BN_RoutMechUnavail_Ytd*@BN_KEdc_Ytd,0) + ISNULL(@CC_RoutMechUnavail_Ytd*@CC_KEdc_Ytd,0) 
			+ ISNULL(@HO_RoutMechUnavail_Ytd*@HO_KEdc_Ytd,0) + ISNULL(@MK_RoutMechUnavail_Ytd*@MK_KEdc_Ytd,0) + ISNULL(@MP_RoutMechUnavail_Ytd*@MP_KEdc_Ytd,0) 
			+ ISNULL(@PA_RoutMechUnavail_Ytd*@PA_KEdc_Ytd,0) + ISNULL(@QB_RoutMechUnavail_Ytd*@QB_KEdc_Ytd,0) 
			+ ISNULL(@SC_RoutMechUnavail_Ytd*@SC_KEdc_Ytd,0) + ISNULL(@TC_RoutMechUnavail_Ytd*@TC_KEdc_Ytd,0) + ISNULL(@TR_RoutMechUnavail_Ytd*@TR_KEdc_Ytd,0) 
			+ ISNULL(@WM_RoutMechUnavail_Ytd*@WM_KEdc_Ytd,0))/@VALERO_KEdc_Ytd, 
	@VALERO_TAMechUnavail_Ytd = (ISNULL(@AD_TAMechUnavail_Ytd*@AD_KEdc_Ytd,0) + ISNULL(@BN_TAMechUnavail_Ytd*@BN_KEdc_Ytd,0) + ISNULL(@CC_TAMechUnavail_Ytd*@CC_KEdc_Ytd,0) 
			+ ISNULL(@HO_TAMechUnavail_Ytd*@HO_KEdc_Ytd,0) + ISNULL(@MK_TAMechUnavail_Ytd*@MK_KEdc_Ytd,0) + ISNULL(@MP_TAMechUnavail_Ytd*@MP_KEdc_Ytd,0) 
			+ ISNULL(@PA_TAMechUnavail_Ytd*@PA_KEdc_Ytd,0) + ISNULL(@QB_TAMechUnavail_Ytd*@QB_KEdc_Ytd,0) 
			+ ISNULL(@SC_TAMechUnavail_Ytd*@SC_KEdc_Ytd,0) + ISNULL(@TC_TAMechUnavail_Ytd*@TC_KEdc_Ytd,0) + ISNULL(@TR_TAMechUnavail_Ytd*@TR_KEdc_Ytd,0) 
			+ ISNULL(@WM_TAMechUnavail_Ytd*@WM_KEdc_Ytd,0))/@VALERO_KEdc_Ytd

SELECT @VALERO_EII_Ytd = 100*SUM(a.EnergyUseDay*s.NumDays)/SUM(a.TotStdEnergy*s.NumDays) 
FROM FactorTotCalc a INNER JOIN Submissions s ON s.SubmissionID = a.SubmissionID   
WHERE RefineryID IN ('75NSA','33NSA','218NSA','98NSA','28NSA','38NSA','63FL', '19NSA','110NSA','217NSA','72NSA', '29NSA','91NSA')
AND s.DataSet = 'Actual' AND s.PeriodYear = @PeriodYear AND s.PeriodMonth <= @PeriodMonth AND s.UseSubmission = 1
AND a.FactorSet = @FactorSet AND a.EnergyUseDay > 0 AND a.TotStdEnergy> 0 AND s.NumDays > 0

SET NOCOUNT OFF
SELECT ReportPeriod = CAST(CAST(@PeriodMonth as varchar(2)) + '/1/' + CAST(@PeriodYear as varchar(4)) as smalldatetime),
	AD_KEdc_Ytd = @AD_KEdc_Ytd, AD_MechAvail_Ytd = @AD_MechAvail_Ytd, AD_AdjMaintIndex_Ytd = @AD_AdjMaintIndex_Ytd, 
	AD_UtilPcnt_Ytd = @AD_UtilPcnt_Ytd, AD_NEOpExEdc_Ytd = @AD_NEOpExEdc_Ytd, AD_EII_Ytd = @AD_EII_Ytd, AD_TotWhrEdc_Ytd = @AD_TotWhrEdc_Ytd,
	AD_NetInputKBPD_Ytd = @AD_NetInputKBPD_Ytd, AD_CrudeKBPD_Ytd = @AD_CrudeKBPD_Ytd, AD_FCCRateKBPD_Ytd = @AD_FCCRateKBPD_Ytd, 
	AD_NumContPers_Ytd = @AD_NumContPers_Ytd, AD_RoutMechUnavail_Ytd = @AD_RoutMechUnavail_Ytd, AD_TAMechUnavail_Ytd = @AD_TAMechUnavail_Ytd,
	BN_KEdc_Ytd = @BN_KEdc_Ytd, BN_MechAvail_Ytd = @BN_MechAvail_Ytd, BN_AdjMaintIndex_Ytd = @BN_AdjMaintIndex_Ytd, 
	BN_UtilPcnt_Ytd = @BN_UtilPcnt_Ytd, BN_NEOpExEdc_Ytd = @BN_NEOpExEdc_Ytd, BN_EII_Ytd = @BN_EII_Ytd, BN_TotWhrEdc_Ytd = @BN_TotWhrEdc_Ytd,
	BN_NetInputKBPD_Ytd = @BN_NetInputKBPD_Ytd, BN_CrudeKBPD_Ytd = @BN_CrudeKBPD_Ytd, BN_FCCRateKBPD_Ytd = @BN_FCCRateKBPD_Ytd, 
	BN_NumContPers_Ytd = @BN_NumContPers_Ytd, BN_RoutMechUnavail_Ytd = @BN_RoutMechUnavail_Ytd, BN_TAMechUnavail_Ytd = @BN_TAMechUnavail_Ytd,
	CC_KEdc_Ytd = @CC_KEdc_Ytd, CC_MechAvail_Ytd = @CC_MechAvail_Ytd, CC_AdjMaintIndex_Ytd = @CC_AdjMaintIndex_Ytd, 
	CC_UtilPcnt_Ytd = @CC_UtilPcnt_Ytd, CC_NEOpExEdc_Ytd = @CC_NEOpExEdc_Ytd, CC_EII_Ytd = @CC_EII_Ytd, CC_TotWhrEdc_Ytd = @CC_TotWhrEdc_Ytd,
	CC_NetInputKBPD_Ytd = @CC_NetInputKBPD_Ytd, CC_CrudeKBPD_Ytd = @CC_CrudeKBPD_Ytd, CC_FCCRateKBPD_Ytd = @CC_FCCRateKBPD_Ytd, 
	CC_NumContPers_Ytd = @CC_NumContPers_Ytd, CC_RoutMechUnavail_Ytd = @CC_RoutMechUnavail_Ytd, CC_TAMechUnavail_Ytd = @CC_TAMechUnavail_Ytd,
	HO_KEdc_Ytd = @HO_KEdc_Ytd, HO_MechAvail_Ytd = @HO_MechAvail_Ytd, HO_AdjMaintIndex_Ytd = @HO_AdjMaintIndex_Ytd, 
	HO_UtilPcnt_Ytd = @HO_UtilPcnt_Ytd, HO_NEOpExEdc_Ytd = @HO_NEOpExEdc_Ytd, HO_EII_Ytd = @HO_EII_Ytd, HO_TotWhrEdc_Ytd = @HO_TotWhrEdc_Ytd,
	HO_NetInputKBPD_Ytd = @HO_NetInputKBPD_Ytd, HO_CrudeKBPD_Ytd = @HO_CrudeKBPD_Ytd, HO_FCCRateKBPD_Ytd = @HO_FCCRateKBPD_Ytd, 
	HO_NumContPers_Ytd = @HO_NumContPers_Ytd, HO_RoutMechUnavail_Ytd = @HO_RoutMechUnavail_Ytd, HO_TAMechUnavail_Ytd = @HO_TAMechUnavail_Ytd,
	MK_KEdc_Ytd = @MK_KEdc_Ytd, MK_MechAvail_Ytd = @MK_MechAvail_Ytd, MK_AdjMaintIndex_Ytd = @MK_AdjMaintIndex_Ytd, 
	MK_UtilPcnt_Ytd = @MK_UtilPcnt_Ytd, MK_NEOpExEdc_Ytd = @MK_NEOpExEdc_Ytd, MK_EII_Ytd = @MK_EII_Ytd, MK_TotWhrEdc_Ytd = @MK_TotWhrEdc_Ytd,
	MK_NetInputKBPD_Ytd = @MK_NetInputKBPD_Ytd, MK_CrudeKBPD_Ytd = @MK_CrudeKBPD_Ytd, MK_FCCRateKBPD_Ytd = @MK_FCCRateKBPD_Ytd, 
	MK_NumContPers_Ytd = @MK_NumContPers_Ytd, MK_RoutMechUnavail_Ytd = @MK_RoutMechUnavail_Ytd, MK_TAMechUnavail_Ytd = @MK_TAMechUnavail_Ytd,
	MP_KEdc_Ytd = @MP_KEdc_Ytd, MP_MechAvail_Ytd = @MP_MechAvail_Ytd, MP_AdjMaintIndex_Ytd = @MP_AdjMaintIndex_Ytd, 
	MP_UtilPcnt_Ytd = @MP_UtilPcnt_Ytd, MP_NEOpExEdc_Ytd = @MP_NEOpExEdc_Ytd, MP_EII_Ytd = @MP_EII_Ytd, MP_TotWhrEdc_Ytd = @MP_TotWhrEdc_Ytd,
	MP_NetInputKBPD_Ytd = @MP_NetInputKBPD_Ytd, MP_CrudeKBPD_Ytd = @MP_CrudeKBPD_Ytd, MP_FCCRateKBPD_Ytd = @MP_FCCRateKBPD_Ytd, 
	MP_NumContPers_Ytd = @MP_NumContPers_Ytd, MP_RoutMechUnavail_Ytd = @MP_RoutMechUnavail_Ytd, MP_TAMechUnavail_Ytd = @MP_TAMechUnavail_Ytd,
	PA_KEdc_Ytd = @PA_KEdc_Ytd, PA_MechAvail_Ytd = @PA_MechAvail_Ytd, PA_AdjMaintIndex_Ytd = @PA_AdjMaintIndex_Ytd, 
	PA_UtilPcnt_Ytd = @PA_UtilPcnt_Ytd, PA_NEOpExEdc_Ytd = @PA_NEOpExEdc_Ytd, PA_EII_Ytd = @PA_EII_Ytd, PA_TotWhrEdc_Ytd = @PA_TotWhrEdc_Ytd,
	PA_NetInputKBPD_Ytd = @PA_NetInputKBPD_Ytd, PA_CrudeKBPD_Ytd = @PA_CrudeKBPD_Ytd, PA_FCCRateKBPD_Ytd = @PA_FCCRateKBPD_Ytd, 
	PA_NumContPers_Ytd = @PA_NumContPers_Ytd, PA_RoutMechUnavail_Ytd = @PA_RoutMechUnavail_Ytd, PA_TAMechUnavail_Ytd = @PA_TAMechUnavail_Ytd,
	QB_KEdc_Ytd = @QB_KEdc_Ytd, QB_MechAvail_Ytd = @QB_MechAvail_Ytd, QB_AdjMaintIndex_Ytd = @QB_AdjMaintIndex_Ytd, 
	QB_UtilPcnt_Ytd = @QB_UtilPcnt_Ytd, QB_NEOpExEdc_Ytd = @QB_NEOpExEdc_Ytd, QB_EII_Ytd = @QB_EII_Ytd, QB_TotWhrEdc_Ytd = @QB_TotWhrEdc_Ytd,
	QB_NetInputKBPD_Ytd = @QB_NetInputKBPD_Ytd, QB_CrudeKBPD_Ytd = @QB_CrudeKBPD_Ytd, QB_FCCRateKBPD_Ytd = @QB_FCCRateKBPD_Ytd, 
	QB_NumContPers_Ytd = @QB_NumContPers_Ytd, QB_RoutMechUnavail_Ytd = @QB_RoutMechUnavail_Ytd, QB_TAMechUnavail_Ytd = @QB_TAMechUnavail_Ytd,
	SC_KEdc_Ytd = @SC_KEdc_Ytd, SC_MechAvail_Ytd = @SC_MechAvail_Ytd, SC_AdjMaintIndex_Ytd = @SC_AdjMaintIndex_Ytd, 
	SC_UtilPcnt_Ytd = @SC_UtilPcnt_Ytd, SC_NEOpExEdc_Ytd = @SC_NEOpExEdc_Ytd, SC_EII_Ytd = @SC_EII_Ytd, SC_TotWhrEdc_Ytd = @SC_TotWhrEdc_Ytd,
	SC_NetInputKBPD_Ytd = @SC_NetInputKBPD_Ytd, SC_CrudeKBPD_Ytd = @SC_CrudeKBPD_Ytd, SC_FCCRateKBPD_Ytd = @SC_FCCRateKBPD_Ytd, 
	SC_NumContPers_Ytd = @SC_NumContPers_Ytd, SC_RoutMechUnavail_Ytd = @SC_RoutMechUnavail_Ytd, SC_TAMechUnavail_Ytd = @SC_TAMechUnavail_Ytd,
	TC_KEdc_Ytd = @TC_KEdc_Ytd, TC_MechAvail_Ytd = @TC_MechAvail_Ytd, TC_AdjMaintIndex_Ytd = @TC_AdjMaintIndex_Ytd, 
	TC_UtilPcnt_Ytd = @TC_UtilPcnt_Ytd, TC_NEOpExEdc_Ytd = @TC_NEOpExEdc_Ytd, TC_EII_Ytd = @TC_EII_Ytd, TC_TotWhrEdc_Ytd = @TC_TotWhrEdc_Ytd,
	TC_NetInputKBPD_Ytd = @TC_NetInputKBPD_Ytd, TC_CrudeKBPD_Ytd = @TC_CrudeKBPD_Ytd, TC_FCCRateKBPD_Ytd = @TC_FCCRateKBPD_Ytd, 
	TC_NumContPers_Ytd = @TC_NumContPers_Ytd, TC_RoutMechUnavail_Ytd = @TC_RoutMechUnavail_Ytd, TC_TAMechUnavail_Ytd = @TC_TAMechUnavail_Ytd,
	TR_KEdc_Ytd = @TR_KEdc_Ytd, TR_MechAvail_Ytd = @TR_MechAvail_Ytd, TR_AdjMaintIndex_Ytd = @TR_AdjMaintIndex_Ytd, 
	TR_UtilPcnt_Ytd = @TR_UtilPcnt_Ytd, TR_NEOpExEdc_Ytd = @TR_NEOpExEdc_Ytd, TR_EII_Ytd = @TR_EII_Ytd, TR_TotWhrEdc_Ytd = @TR_TotWhrEdc_Ytd,
	TR_NetInputKBPD_Ytd = @TR_NetInputKBPD_Ytd, TR_CrudeKBPD_Ytd = @TR_CrudeKBPD_Ytd, TR_FCCRateKBPD_Ytd = @TR_FCCRateKBPD_Ytd, 
	TR_NumContPers_Ytd = @TR_NumContPers_Ytd, TR_RoutMechUnavail_Ytd = @TR_RoutMechUnavail_Ytd, TR_TAMechUnavail_Ytd = @TR_TAMechUnavail_Ytd,
	WM_KEdc_Ytd = @WM_KEdc_Ytd, WM_MechAvail_Ytd = @WM_MechAvail_Ytd, WM_AdjMaintIndex_Ytd = @WM_AdjMaintIndex_Ytd, 
	WM_UtilPcnt_Ytd = @WM_UtilPcnt_Ytd, WM_NEOpExEdc_Ytd = @WM_NEOpExEdc_Ytd, WM_EII_Ytd = @WM_EII_Ytd, WM_TotWhrEdc_Ytd = @WM_TotWhrEdc_Ytd,
	WM_NetInputKBPD_Ytd = @WM_NetInputKBPD_Ytd, WM_CrudeKBPD_Ytd = @WM_CrudeKBPD_Ytd, WM_FCCRateKBPD_Ytd = @WM_FCCRateKBPD_Ytd, 
	WM_NumContPers_Ytd = @WM_NumContPers_Ytd, WM_RoutMechUnavail_Ytd = @WM_RoutMechUnavail_Ytd, WM_TAMechUnavail_Ytd = @WM_TAMechUnavail_Ytd,
	VALERO_MEdc_Ytd = @VALERO_KEdc_Ytd/1000, VALERO_MechAvail_Ytd = @VALERO_MechAvail_Ytd, VALERO_AdjMaintIndex_Ytd = @VALERO_AdjMaintIndex_Ytd, 
	VALERO_UtilPcnt_Ytd = @VALERO_UtilPcnt_Ytd, VALERO_NEOpExEdc_Ytd = @VALERO_NEOpExEdc_Ytd, VALERO_EII_Ytd = @VALERO_EII_Ytd, VALERO_TotWhrEdc_Ytd = @VALERO_TotWhrEdc_Ytd,
	VALERO_NetInputKBPD_Ytd = @VALERO_NetInputKBPD_Ytd, VALERO_CrudeKBPD_Ytd = @VALERO_CrudeKBPD_Ytd, VALERO_FCCRateKBPD_Ytd = @VALERO_FCCRateKBPD_Ytd, 
	VALERO_NumContPers_Ytd = @VALERO_NumContPers_Ytd, VALERO_RoutMechUnavail_Ytd = @VALERO_RoutMechUnavail_Ytd, VALERO_TAMechUnavail_Ytd = @VALERO_TAMechUnavail_Ytd

