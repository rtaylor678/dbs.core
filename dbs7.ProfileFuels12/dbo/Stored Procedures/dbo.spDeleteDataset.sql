﻿CREATE PROCEDURE [dbo].[spDeleteDataSet](@RefineryID varchar(6), @DataSet varchar(15))
AS

DECLARE @SubmissionID int
WHILE EXISTS (SELECT * FROM Submissions WHERE RefineryID = @RefineryID AND DataSet = @DataSet)
BEGIN
	SELECT TOP 1 @SubmissionID = SubmissionID FROM SubmissionsAll WHERE RefineryID = @RefineryID AND DataSet = @DataSet ORDER BY PeriodStart DESC
	EXEC spDeleteSubmission @SubmissionID
END

/*DELETE FROM LoadTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet -- now keyed with SubmissionID */
DELETE FROM MaintTA WHERE RefineryID = @RefineryID AND DataSet = @DataSet
DELETE FROM MaintTACost WHERE RefineryID = @RefineryID AND DataSet = @DataSet
DELETE FROM ReadyForCalcs WHERE RefineryID = @RefineryID AND DataSet = @DataSet
DELETE FROM MaintRoutHist WHERE RefineryID = @RefineryID AND DataSet = @DataSet
--DELETE FROM LoadRoutHist WHERE RefineryID = @RefineryID AND DataSet = @DataSet
DELETE FROM LoadedEdcStabilizers WHERE RefineryID = @RefineryID AND DataSet = @DataSet
--DELETE FROM LoadEdcStabilizers WHERE RefineryID = @RefineryID AND DataSet = @DataSet
DELETE FROM StudyTankage WHERE RefineryID = @RefineryID AND DataSet = @DataSet


