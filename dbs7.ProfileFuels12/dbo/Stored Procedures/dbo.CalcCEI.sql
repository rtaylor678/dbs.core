﻿CREATE PROCEDURE [dbo].[CalcCEI](@SubmissionID int, @DEBUG bit = 0)
AS
DECLARE @FactorSet FactorSet
SELECT @FactorSet = '2006'
DECLARE @CO2_C_Ratio float, @AvgPurEnergyFraction real
SELECT @CO2_C_Ratio = 44.01/12.01, @AvgPurEnergyFraction = 0.075
SELECT @CO2_C_Ratio = 44.0/12.0

DELETE FROM CEI WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet

DECLARE @NumDays real
SELECT @NumDays = NumDays FROM dbo.SubmissionsAll WHERE SubmissionID = @SubmissionID
DECLARE @SE_EII float, @TotEnergyConsMBTU float, @EII real
SELECT @TotEnergyConsMBTU = TotEnergyConsMBTU FROM EnergyTot WHERE SubmissionID = @SubmissionID
SELECT @SE_EII = TotStdEnergy*@NumDays, @EII = EII
FROM FactorTotCalc WHERE FactorSet = @FactorSet AND SubmissionID = @SubmissionID
IF ISNULL(@SE_EII, 0) = 0 OR ISNULL(@EII, 0) = 0
BEGIN
	INSERT dbo.CEI (SubmissionID, FactorSet) VALUES (@SubmissionID, @FactorSet)
	RETURN
END

DECLARE @AE_StmPur float, @AE_StmTransIn float, @AE_StmDist float, @AE_StmSales float, @AE_GEDist float, @AE_GESales float, @AE_PurElec float, @AE_ProdElecAdj float
SELECT @AE_StmPur = SUM(SourceMBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'PUR' AND EnergyType IN ('STM','LLH')
SELECT @AE_StmTransIn = SUM(SourceMBTU), @AE_StmDist = SUM(UsageMBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND EnergyType IN ('STM','LLH') 
SELECT @AE_StmSales = SUM(UsageMBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType IN ('STM','LLH') 
SELECT @AE_GEDist = SUM(UsageMBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'DST' AND EnergyType = 'GE' 
SELECT @AE_GESales = SUM(UsageMBTU) FROM Energy WHERE SubmissionID = @SubmissionID AND TransType = 'SOL' AND EnergyType = 'GE' 
SELECT 	@AE_StmPur = ISNULL(@AE_StmPur, 0), @AE_StmTransIn = ISNULL(@AE_StmTransIn, 0),
	@AE_StmDist = ISNULL(@AE_StmDist, 0), @AE_StmSales = ISNULL(@AE_StmSales, 0), @AE_GEDist = ISNULL(@AE_GEDist, 0), @AE_GESales = ISNULL(@AE_GESales, 0)
SELECT 	@AE_PurElec = SUM(CASE WHEN TransType = 'CPU' THEN MBTU ELSE 0 END), 
	@AE_ProdElecAdj = SUM(CASE WHEN TransType = 'CPR' THEN MBTU ELSE 0 END)
FROM EnergyCons WHERE SubmissionID = @SubmissionID AND EnergyType = 'PT'
-- Standard Energy + actual Energy consumed to produce steam and electricity distributed/sold from refinery
DECLARE @AE_XFER_Sales_Stm_Elec float, @SE_CEI float
SELECT @AE_XFER_Sales_Stm_Elec = @AE_StmDist + @AE_StmSales + @AE_GEDist + @AE_GESales
SELECT @SE_CEI = @SE_EII + @AE_XFER_Sales_Stm_Elec

DECLARE @ElecMWH_Pur float, @ElecMWH_TransIn float, @ElecMWH_TransOut float, @ElecMWH_Sold float
SELECT 	@ElecMWH_Pur = SUM(CASE WHEN TransType = 'PUR' THEN SourceMWH ELSE 0 END)
	, @ElecMWH_TransIn = SUM(CASE WHEN TransType = 'DST' THEN SourceMWH ELSE 0 END)
	, @ElecMWH_TransOut = SUM(CASE WHEN TransType = 'DST' THEN -UsageMWH ELSE 0 END)
	, @ElecMWH_Sold = SUM(CASE WHEN TransType = 'SOL' THEN -UsageMWH ELSE 0 END)
FROM Electric WHERE SubmissionID = @SubmissionID 
SELECT 	@ElecMWH_Pur = ISNULL(@ElecMWH_Pur, 0), @ElecMWH_TransIn = ISNULL(@ElecMWH_TransIn, 0), 
	@ElecMWH_TransOut = ISNULL(@ElecMWH_TransOut, 0), @ElecMWH_Sold = ISNULL(@ElecMWH_Sold, 0)

DECLARE @AE_Fungible float, @ACE_Fungible float
SELECT 	@AE_Fungible = SUM(CASE WHEN TransType IN ('PUR', 'PRO', 'DST', 'SOL') THEN MBTU ELSE 0 END),
	--Cons_MBTU = -SUM(CASE WHEN TransType IN ('CPU','CPR') THEN MBTU ELSE 0 END),
	@ACE_Fungible = SUM(CASE WHEN TransType IN ('PUR', 'PRO', 'DST', 'SOL') THEN CE ELSE 0 END)
	--Cons_CE = -SUM(CASE WHEN TransType IN ('CPU','CPR') THEN CE ELSE 0 END)
FROM (SELECT E.EnergyType, E.TransType,MBTU=ISNULL(E.SourceMBTU,0)-ISNULL(E.UsageMBTU,0),	
	CE=(ISNULL(E.SourceMBTU,0)-ISNULL(E.UsageMBTU,0)) * CASE E.EnergyType
		WHEN 'C2' THEN 0.0171
		WHEN 'C3' THEN 0.0181
		WHEN 'C4' THEN 0.0187
		WHEN 'NAP' THEN 0.0191
		WHEN 'DST' THEN 0.0213
		WHEN 'R03' THEN 0.0233
		WHEN 'R10' THEN 0.0233
		WHEN 'R20' THEN 0.0233
		WHEN 'R30' THEN 0.0232
		WHEN 'RG3' THEN 0.0232
		WHEN 'RXV' THEN 0.0232
		WHEN 'COK' THEN 0.0293
		ELSE 0 END * @CO2_C_Ratio
	FROM Energy E
	WHERE (e.EnergyType IN ('C2', 'C3', 'C4', 'NAP', 'DST', 'R03', 'R10', 'R20', 'R30', 'RG3', 'RXV', 'COK')) 
	AND SubmissionID = @SubmissionID) x
SELECT @AE_Fungible = ISNULL(@AE_Fungible, 0), @ACE_Fungible = ISNULL(@ACE_Fungible, 0)

DECLARE @FG_PUR float, @FG_PRO float, @FG_DST float, @FG_SOL float, @FG_HeatContent real,
	@CE_PURFG float, @CE_PROFG float, @CE_DSTFG float, @CE_SOLFG float,
	@AE_FG float, @ACE_FG float, @CFE_PROFG real
SELECT 	@AE_FG = SUM(CASE WHEN TransType IN ('PUR', 'PRO', 'DST', 'SOL') THEN MBTU ELSE 0 END),
	--Cons_MBTU = -SUM(CASE WHEN TransType IN ('CPU','CPR') THEN MBTU ELSE 0 END),
	@FG_PUR = SUM(CASE WHEN TransType = 'PUR' THEN MBTU ELSE 0 END),
	@FG_PRO = SUM(CASE WHEN TransType = 'PRO' THEN MBTU ELSE 0 END),
	@FG_DST = SUM(CASE WHEN TransType = 'DST' THEN MBTU ELSE 0 END),
	@FG_SOL = SUM(CASE WHEN TransType = 'SOL' THEN MBTU ELSE 0 END),
	@CE_PURFG = SUM(CASE WHEN TransType = 'PUR' THEN CE ELSE 0 END),
	@CE_PROFG = SUM(CASE WHEN TransType = 'PRO' THEN CE ELSE 0 END),
	@ACE_FG = SUM(CASE WHEN TransType IN ('PUR', 'PRO', 'DST', 'SOL') THEN CE ELSE 0 END),
	@FG_HeatContent = AVG(HeatContent)
	--Cons_CE = -SUM(CASE WHEN TransType IN ('CPU','CPR') THEN CE ELSE 0 END)
FROM (SELECT E.EnergyType, E.TransType,MBTU=ISNULL(E.SourceMBTU,0)-ISNULL(E.UsageMBTU,0),	
	CE=(ISNULL(E.SourceMBTU,0)-ISNULL(E.UsageMBTU,0)) * CASE E.EnergyType	
		WHEN 'FG' THEN 0.0161
		ELSE 0 END, HeatContent = CAST(NULL as real)
	FROM Energy E
	WHERE (e.EnergyType = 'FG') 
	AND SubmissionID = @SubmissionID) x
/*
SELECT @CFE_PROFG = CASE WHEN ISNULL(Total, 0) BETWEEN 95 AND 105 THEN 
	(ISNULL(Hydrogen*0, 0) + ISNULL(Methane*14.387, 0) + ISNULL(Ethane*28.943, 0) + ISNULL(Ethylene*28.505, 0) + ISNULL(Propane*44.107, 0) + ISNULL(Propylene*43.800, 0) + ISNULL(Butane*58.865, 0) + ISNULL(Isobutane*58.632, 0) + ISNULL(C5Plus*80.143, 0) + ISNULL(Inerts*13.000, 0))/
	(ISNULL(Hydrogen*273, 0) + ISNULL(Methane*911, 0) + ISNULL(Ethane*1631, 0) + ISNULL(Ethylene*1489, 0) + ISNULL(Propane*2372, 0) + ISNULL(Propylene*2221, 0) + ISNULL(Butane*3081, 0) + ISNULL(Isobutane*3069, 0) + ISNULL(C5Plus*4131, 0) + ISNULL(Inerts*0, 0)) END
FROM FuelGas WHERE SubmissionID = @SubmissionID
*/
IF @CFE_PROFG IS NULL 
BEGIN
	IF @FG_HeatContent > 0
		SELECT @CFE_PROFG = (0.0202-3.112/@FG_HeatContent)
	ELSE
		SELECT @CFE_PROFG = 0.0172
END
SELECT @CE_PROFG = @FG_PRO*@CFE_PROFG
-- Make sure no variables are NULL and convert all to CO2 Equivalents
SELECT @FG_PUR = ISNULL(@FG_PUR, 0), @FG_PRO = ISNULL(@FG_PRO, 0), @FG_DST = ISNULL(@FG_DST, 0), @FG_SOL = ISNULL(@FG_SOL, 0), 
	@CE_PURFG = ISNULL(@CE_PURFG, 0), @CE_PROFG = ISNULL(@CE_PROFG, 0), @CE_DSTFG = ISNULL(@CE_DSTFG, 0), @CE_SOLFG = ISNULL(@CE_SOLFG, 0), 
	@AE_FG = ISNULL(@AE_FG, 0)
IF ABS(@FG_DST) < 5 
	SET @FG_DST = 0
IF ABS(@FG_SOL) < 5
	SET @FG_SOL = 0

-- Calculation CE for FG transferred to affiliate
IF @FG_DST=0
	SET @CE_DSTFG = 0
ELSE IF @FG_DST>0
	SELECT @CE_DSTFG = 0.0172*@FG_DST
ELSE IF -@FG_DST <= @FG_PUR
	SELECT @CE_DSTFG = @FG_DST/@FG_PUR*@CE_PURFG
ELSE
	SELECT @CE_DSTFG = -@CE_PURFG-(-@FG_DST-@FG_PUR)/@FG_PRO*@CE_PROFG
-- Calculation CE for FG sold to third party
IF @FG_SOL=0
	SET @CE_SOLFG = 0
ELSE IF -@FG_SOL-@FG_DST<=@FG_PUR
	SELECT @CE_SOLFG = CASE WHEN @FG_PUR = 0 THEN 0 ELSE @FG_SOL/@FG_PUR*@CE_PURFG END
ELSE IF -@FG_DST<@FG_PUR
	SELECT @CE_SOLFG = CASE WHEN @FG_PUR = 0 THEN 0 ELSE (-@FG_PUR-@FG_DST)/@FG_PUR*@CE_PURFG END 
					+ CASE WHEN @FG_PRO = 0 THEN 0 ELSE (@FG_SOL+@FG_PUR+@FG_DST)/@FG_PRO*@CE_PROFG END
ELSE IF @FG_PRO <> 0
	SELECT @CE_SOLFG = @FG_SOL/@FG_PRO*@CE_PROFG
ELSE
	SELECT @CE_SOLFG = 0
	
-- Calculation CE for FG consumed
SELECT @ACE_FG = @CE_PURFG + @CE_PROFG + @CE_DSTFG + @CE_SOLFG
IF @DEBUG = 1
	SELECT ACE_FG = @ACE_FG, CE_PURFG = @CE_PURFG, FG_PRO = @FG_PRO, CFE_PROFG = @CFE_PROFG, CE_PROFG = @CE_PROFG , CE_DSTFG = @CE_DSTFG , CE_SOLFG = @CE_SOLFG

SELECT 	@CE_PURFG = @CE_PURFG*@CO2_C_Ratio, 
	@CE_PROFG = @CE_PROFG*@CO2_C_Ratio, 
	@CE_DSTFG = @CE_DSTFG*@CO2_C_Ratio, 
	@CE_SOLFG = @CE_SOLFG*@CO2_C_Ratio,
	@ACE_FG = @ACE_FG*@CO2_C_Ratio
	
-- Steam Purchases Emissions
DECLARE @ACE_StmPur float, @ACE_StmTransIn float
IF (@AE_FG+@AE_Fungible) > 0
	SELECT @ACE_StmPur = @AE_StmPur * (@ACE_FG+@ACE_Fungible)/(@AE_FG+@AE_Fungible)
		,  @ACE_StmTransIn = @AE_StmTransIn * (@ACE_FG+@ACE_Fungible)/(@AE_FG+@AE_Fungible)

-- Calculation of Factors for fluid and flexi cokers (Coke consumed and Low BTU gas produced)
-- Equations 12-15, 18-21
DECLARE @UtilCapCOKFX float, @SE_FXCoke float, @SCE_FXCoke float
DECLARE @UtilCapCOKFC float, @SE_FCCoke float, @SCE_FCCoke float
DECLARE @AvgCOKFXDensity real, @SE_FXLBG float, @SCE_FXLBG float, @SE_FCLBG float, @SCE_FCLBG float
SELECT @UtilCapCOKFX = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'COK' AND ProcessType = 'FX'
SELECT @UtilCapCOKFC = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'COK' AND ProcessType = 'FC'
IF @UtilCapCOKFX > 0
	SELECT @AvgCOKFXDensity = SUM(dbo.UnitsConv(SAValue, 'API', 'KGM3')*UtilCap)/SUM(UtilCap)
	FROM Config c INNER JOIN ProcessData t2 ON t2.SubmissionID = c.SubmissionID AND t2.UnitID = c.UnitID AND t2.Property = 'FeedGravity'
	WHERE c.SubmissionID = @SubmissionID AND c.ProcessID = 'COK' AND c.ProcessType = 'FX'
SELECT 	@SE_FXCoke = @UtilCapCOKFX*@NumDays*0.280, @SE_FXLBG = @UtilCapCOKFX*@NumDays*0.95 /*(@AvgCOKFXDensity*0.0125-12)*/,
	@SE_FCCoke = @UtilCapCOKFC*@NumDays*0.350, @SE_FCLBG = @UtilCapCOKFC*@NumDays*0.12
SELECT 	@SCE_FXCoke = @SE_FXCoke*0.0293*@CO2_C_Ratio, @SCE_FXLBG = @SE_FXLBG*0.0344*@CO2_C_Ratio,
	@SCE_FCCoke = @SE_FCCoke*0.0293*@CO2_C_Ratio, @SCE_FCLBG = @SE_FCLBG*0.0344*@CO2_C_Ratio

-- Marketable Coke consumed in calcining (equations 16-17)
DECLARE @CALCINUtilCap float, @CALCIN_CalcMaxMBTU float, @CALCIN_CalcMBTU float, 
	@AE_MC float, @ACE_MC float, @SCE_CALCIN float, @SE_CALCIN float
SELECT @AE_MC = SUM(SourceMBTU) FROM Energy 
WHERE SubmissionID = @SubmissionID AND EnergyType = 'MC' AND TransType = 'PRO' 
SELECT @CALCINUtilCap = SUM(UtilCap) FROM Config
WHERE SubmissionID = @SubmissionID AND ProcessID = 'CALCIN'
SELECT @AE_MC = ISNULL(@AE_MC, 0), @CALCINUtilCap = ISNULL(@CALCINUtilCap, 0)

SELECT @CALCIN_CalcMaxMBTU = dbo.UnitsConv(@CALCINUtilCap, 'ST', 'LB')*@NumDays*(0.12/0.82)/8.45/42.0*6.43  --12% volatiles/82% Yield, additional assumed to be pet coke
SELECT @CALCIN_CalcMBTU = CASE WHEN @AE_MC < @CALCIN_CalcMaxMBTU THEN @AE_MC ELSE @CALCIN_CalcMaxMBTU END
SELECT @ACE_MC = (@CALCIN_CalcMBTU*0.0233+(@AE_MC-@CALCIN_CalcMBTU)*0.0293)*@CO2_C_Ratio
SELECT @SE_CALCIN = 0.22*dbo.UnitsConv(@CALCINUtilCap, 'ST', 'LB')*@NumDays*(14500/1E6)
SELECT @SCE_CALCIN = @SE_CALCIN*0.0253*@CO2_C_Ratio
IF @DEBUG = 1
	SELECT AE_MC = @AE_MC, ACE_MC = @ACE_MC/@CO2_C_Ratio, CALCINUtilCap = @CALCINUtilCap, CALCIN_CalcMaxMBTU = @CALCIN_CalcMaxMBTU, CALCIN_CalcMBTU = @CALCIN_CalcMBTU, SCE_CALCIN = @SCE_CALCIN/@CO2_C_Ratio, SE_CALCIN = @SE_CALCIN
-- Non-Marketable Coke Emissions
DECLARE @AE_NMC float, @ACE_NMC float
SELECT @AE_NMC = SUM(SourceMBTU) FROM Energy 
WHERE SubmissionID = @SubmissionID AND EnergyType = 'NMC' AND TransType = 'PRO' 
SELECT @AE_NMC = ISNULL(@AE_NMC, 0)
SELECT @ACE_NMC = @AE_NMC*0.0293*@CO2_C_Ratio

-- Low BTU Gas Consumption
DECLARE @LBG_PUR float, @LBG_PRO float, @LBG_DST float, @LBG_SOL float, @LBG_HeatContent real,
	@CE_PURLBG float, @CE_PROLBG float, @CE_DSTLBG float, @CE_SOLLBG float,
	@AE_LBG float, @ACE_LBG float, @CEF_LBG real
SELECT 	@AE_LBG = SUM(CASE WHEN TransType IN ('PUR', 'PRO', 'DST', 'SOL') THEN MBTU ELSE 0 END),
	--Cons_MBTU = -SUM(CASE WHEN TransType IN ('CPU','CPR') THEN MBTU ELSE 0 END),
	@LBG_PUR = SUM(CASE WHEN TransType = 'PUR' THEN MBTU ELSE 0 END),
	@LBG_PRO = SUM(CASE WHEN TransType = 'PRO' THEN MBTU ELSE 0 END),
	@LBG_DST = SUM(CASE WHEN TransType = 'DST' THEN MBTU ELSE 0 END),
	@LBG_SOL = SUM(CASE WHEN TransType = 'SOL' THEN MBTU ELSE 0 END),
	@CE_PURLBG = SUM(CASE WHEN TransType = 'PUR' THEN CE ELSE 0 END)*@CO2_C_Ratio,
	@CE_PROLBG = SUM(CASE WHEN TransType = 'PRO' THEN CE ELSE 0 END)*@CO2_C_Ratio,
	@ACE_LBG = SUM(CASE WHEN TransType IN ('PUR', 'PRO', 'DST', 'SOL') THEN CE ELSE 0 END)*@CO2_C_Ratio,
	@LBG_HeatContent = AVG(HeatContent)
	--Cons_CE = -SUM(CASE WHEN TransType IN ('CPU','CPR') THEN CE ELSE 0 END)
FROM (SELECT E.EnergyType, E.TransType,MBTU=ISNULL(E.SourceMBTU,0)-ISNULL(E.UsageMBTU,0),	
	CE=(ISNULL(E.SourceMBTU,0)-ISNULL(E.UsageMBTU,0)) * CASE E.EnergyType	
		WHEN 'LBG' THEN 0.03
		ELSE 0 END, HeatContent = CAST(NULL as real)
	FROM Energy E
	WHERE (e.EnergyType = 'LBG') AND SubmissionID = @SubmissionID) x

DECLARE @PSA_UtilCap float, @PSA_FeedH2 real, @PSA_H2Loss real, @PSA_FeedCH4 real, @PSA_FeedCO2 real, @PSA_Tailgas_BtuPerSCF real
DECLARE @PSA_TailGasMBTU float, @PSA_TailGasMBTU_H2CH4 float, @SE_PSATailGasLBG float, @CE_PSATailGasLBG float
DECLARE @U70_UtilCap float, @U71_UtilCap float, @U72_UtilCap float, @U73_UtilCap float, @HYGPOX_UtilCap float
DECLARE	@SE_U73LBG float, @CE_U73LBG float, @SE_U71LBG float, @CE_U71LBG float 
DECLARE	@SE_U72POX float, @SE_U73POX float, @SE_HYGPOX float, 
	@SCE_U72POX float, @SCE_U73POX float, @SCE_HYGPOX float,
	@SCE_H2_U72 float
DECLARE @OriginalLBG_MBTU float, @OriginalLBG_CE float, @OriginalLBG_CEFactor float

SELECT @PSA_UtilCap = SUM(c.UtilCap)
FROM Config c WHERE c.SubmissionID = @SubmissionID AND c.UtilCap > 0 -- AND c.ProcessType = 'PSA'

SELECT @PSA_FeedH2 = SUM(ISNULL(t2.SAValue, 0)*c.UtilCap)/SUM(c.UtilCap)/100
FROM Config c LEFT JOIN ProcessData t2 ON t2.SubmissionID = c.SubmissionID AND t2.UnitID = c.UnitID AND t2.Property = 'FeedH2'
WHERE c.SubmissionID = @SubmissionID AND c.UtilCap > 0 AND c.ProcessID = 'H2PURE' -- AND c.ProcessType = 'PSA'

SELECT @PSA_H2Loss = SUM(ISNULL(t2.SAValue, 0)*c.UtilCap)/SUM(c.UtilCap)/100
FROM Config c LEFT JOIN ProcessData t2 ON t2.SubmissionID = c.SubmissionID AND t2.UnitID = c.UnitID AND t2.Property = 'H2Loss'
WHERE c.SubmissionID = @SubmissionID AND c.UtilCap > 0 AND c.ProcessID = 'H2PURE' -- AND c.ProcessType = 'PSA'

SELECT @PSA_FeedCH4 = ISNULL(0.14*(1-@PSA_FeedH2), 0)
SELECT @PSA_FeedCO2 = 1-ISNULL(@PSA_FeedH2,0)-@PSA_FeedCH4

SELECT 	@U70_UtilCap = SUM(CASE WHEN ProcessID = 'U70' THEN UtilCap ELSE 0 END) 
	,@U71_UtilCap = SUM(CASE WHEN ProcessID = 'U71' THEN UtilCap ELSE 0 END) 
	,@U72_UtilCap = SUM(CASE WHEN ProcessID = 'U72' THEN UtilCap ELSE 0 END) 
	,@U73_UtilCap = SUM(CASE WHEN ProcessID = 'U73' THEN UtilCap ELSE 0 END) 
	,@HYGPOX_UtilCap = SUM(CASE WHEN ProcessID = 'HYG' AND ProcessType = 'POX' THEN UtilCap ELSE 0 END) 
FROM Config WHERE SubmissionID = @SubmissionID
SELECT 	@U70_UtilCap = ISNULL(@U70_UtilCap, 0), @U71_UtilCap = ISNULL(@U71_UtilCap, 0), 
	@U72_UtilCap = ISNULL(@U72_UtilCap, 0), @U73_UtilCap = ISNULL(@U73_UtilCap, 0), @HYGPOX_UtilCap = ISNULL(@HYGPOX_UtilCap, 0)

SELECT 	@SE_U73POX = @U73_UtilCap*@NumDays*0.369, 
	@SE_U72POX = @U72_UtilCap*@NumDays*60/1000,
	@SE_HYGPOX = @HYGPOX_UtilCap*@NumDays*60/1000,
	@SE_U73LBG = @U73_UtilCap*@NumDays*0.29,
	@SE_U71LBG = @U71_UtilCap*@NumDays*0.027
SELECT 	@SCE_U73POX = @SE_U73POX*0.0233*@CO2_C_Ratio, 
	@SCE_U72POX = @SE_U72POX*0.0293*@CO2_C_Ratio, 
	@SCE_HYGPOX = @SE_HYGPOX*0.0293*@CO2_C_Ratio,
	@CE_U73LBG = @SE_U73LBG * 0.029*@CO2_C_Ratio,
	@CE_U71LBG = @SE_U71LBG * 0.0289*@CO2_C_Ratio,
	@SCE_H2_U72 = (@U72_UtilCap*@NumDays*0.369*0.0232 - @SE_U72POX*0.0293)*@CO2_C_Ratio

SELECT @AE_LBG = ISNULL(@AE_LBG,0), @SE_FCLBG = ISNULL(@SE_FCLBG, 0), @SE_FXLBG = ISNULL(@SE_FXLBG, 0), @SE_U71LBG = ISNULL(@SE_U71LBG, 0), @SE_U73LBG = ISNULL(@SE_U73LBG, 0)
SELECT @PSA_Tailgas_BtuPerSCF = @PSA_H2Loss*@PSA_FeedH2/(@PSA_H2Loss*@PSA_FeedH2+@PSA_FeedCH4+@PSA_FeedCO2)*275+@PSA_FeedCH4/(@PSA_H2Loss*@PSA_FeedH2+@PSA_FeedCH4+@PSA_FeedCO2)*894
SELECT @PSA_TailGasMBTU = @AE_LBG-@SE_FCLBG-@SE_FXLBG-@SE_U71LBG-@SE_U73LBG
IF @PSA_TailGasMBTU < 0  
	SET @PSA_TailGasMBTU = 0
SELECT @PSA_TailGasMBTU_H2CH4 = ISNULL(@PSA_UtilCap*(@PSA_FeedH2*@PSA_H2Loss+@PSA_FeedCH4+@PSA_FeedCO2)*@PSA_Tailgas_BtuPerSCF*@NumDays/1000, 0)

IF @DEBUG = 1
BEGIN
	SELECT LBG_HeatContent = @LBG_HeatContent, PSA_UtilCap = @PSA_UtilCap, PSA_FeedH2 = @PSA_FeedH2, PSA_H2Loss = @PSA_H2Loss, PSA_FeedCH4 = @PSA_FeedCH4, PSA_FeedCO2 = @PSA_FeedCO2, PSA_Tailgas_BtuPerSCF = @PSA_Tailgas_BtuPerSCF, PSA_TailGasMBTU = @PSA_TailGasMBTU, PSA_TailGasMBTU_H2CH4 = @PSA_TailGasMBTU_H2CH4
	SELECT U73UtilCap = @U73_UtilCap, SE_U73LBG = @SE_U73LBG, CE_U73LBG = @CE_U73LBG/@CO2_C_Ratio
	SELECT U71UtilCap = @U71_UtilCap, SE_U71LBG = @SE_U71LBG, CE_U71LBG = @CE_U71LBG/@CO2_C_Ratio
END

SELECT @SE_PSATailGasLBG = CASE WHEN @PSA_TailGasMBTU_H2CH4 < @PSA_TailGasMBTU THEN @PSA_TailGasMBTU_H2CH4 ELSE @PSA_TailGasMBTU END
IF @PSA_TailGasMBTU_H2CH4 > 0
	SELECT @CE_PSATailGasLBG = @PSA_UtilCap*(CASE WHEN @PSA_FeedCH4+@PSA_FeedCO2 > 0.1 THEN 0.1 ELSE @PSA_FeedCH4+@PSA_FeedCO2 END)/0.3794*12/2204.6*@NumDays*@SE_PSATailGasLBG/@PSA_TailGasMBTU_H2CH4*@CO2_C_Ratio
ELSE
	SELECT @CE_PSATailGasLBG = 0
SELECT 	@OriginalLBG_MBTU = ISNULL(@SE_U73LBG,0)+ISNULL(@SE_PSATailGasLBG,0)+ISNULL(@SE_FCLBG,0)+ISNULL(@SE_FXLBG,0),
	@OriginalLBG_CE = ISNULL(@CE_U73LBG,0)+ISNULL(@CE_PSATailGasLBG,0)+ISNULL(@SCE_FCLBG,0)+ISNULL(@SCE_FXLBG,0)
SELECT 	@OriginalLBG_CEFactor = CASE WHEN @OriginalLBG_MBTU > 0 THEN @OriginalLBG_CE/@OriginalLBG_MBTU ELSE 0 END

IF @DEBUG = 1
	SELECT SE_PSATailGasLBG = @SE_PSATailGasLBG, CE_PSATailGasLBG = @CE_PSATailGasLBG/@CO2_C_Ratio, OriginalLBG_MBTU = @OriginalLBG_MBTU, OriginalLBG_CE = @OriginalLBG_CE/@CO2_C_Ratio, OriginalLBG_CEFactor = @OriginalLBG_CEFactor/@CO2_C_Ratio

DECLARE @HYG_UtilCap float, @HYG_FeedH2 float, @HYG_H2Loss float, @HYG_H2Loss_SCFD float, @HYG_H2Loss_MBTU float, @HYG_CE float
DECLARE @UtilCap float, @FeedH2 real, @H2Loss real, @CO2RejectType tinyint, @UnitID int
DECLARE cHYG CURSOR LOCAL FAST_FORWARD
FOR	SELECT c.UnitID, c.UtilCap FROM Config c WHERE c.SubmissionID = @SubmissionID AND c.UtilCap > 0 AND c.ProcessID = 'HYG'
OPEN cHYG
FETCH NEXT FROM cHYG INTO @UnitID, @UtilCap
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @FeedH2 = SAValue
	FROM ProcessData WHERE SubmissionID = @SubmissionID AND UnitID = @UnitID AND Property = 'FeedH2'
	SELECT @H2Loss = SAValue
	FROM ProcessData WHERE SubmissionID = @SubmissionID AND UnitID = @UnitID AND Property = 'H2Loss'
	SELECT @CO2RejectType = SAValue
	FROM ProcessData WHERE SubmissionID = @SubmissionID AND UnitID = @UnitID AND Property = 'CO2RejectType'

	IF @H2Loss IS NULL
		SELECT @H2Loss = CASE WHEN ISNULL(@CO2RejectType, 0) = 4 THEN 2 ELSE 12.5 END
	ELSE IF @H2Loss = 0 AND ISNULL(@CO2RejectType, 0) <> 4
		SET @H2Loss = 12.5
	SELECT 	@HYG_UtilCap = ISNULL(@HYG_UtilCap, 0) + ISNULL(@UtilCap, 0)
		,@HYG_FeedH2 = ISNULL(@HYG_FeedH2, 0) + ISNULL(@FeedH2/100*@UtilCap, 0)
		,@HYG_H2Loss = ISNULL(@HYG_H2Loss, 0) + ISNULL(@UtilCap*@H2Loss/100,0)
		,@HYG_H2Loss_SCFD = ISNULL(@HYG_H2Loss_SCFD, 0) + ISNULL(@UtilCap*@H2Loss,0)/(100-@H2Loss)
	FETCH NEXT FROM cHYG INTO @UnitID, @UtilCap
END
CLOSE cHYG
DEALLOCATE cHYG
IF @HYG_UtilCap > 0
	SELECT @HYG_FeedH2 = @HYG_FeedH2/@UtilCap, @HYG_H2Loss = @HYG_H2Loss/@UtilCap

--SELECT @HYG_H2Loss_SCFD = @HYG_UtilCap/(1-@HYG_H2Loss)/10
SELECT @HYG_H2Loss_MBTU = @HYG_H2Loss_SCFD*@NumDays*0.275
SELECT @HYG_CE = @HYG_H2Loss_MBTU*0.049*@CO2_C_Ratio
IF @DEBUG = 1
	SELECT HYG_UtilCap = @HYG_UtilCap, HYG_FeedH2 = @HYG_FeedH2, HYG_H2Loss = @HYG_H2Loss, HYG_H2Loss_SCFD = @HYG_H2Loss_SCFD, HYG_H2Loss_MBTU = @HYG_H2Loss_MBTU, HYG_CE = @HYG_CE/@CO2_C_Ratio

DECLARE @SecondaryLBG_MBTU float, @SecondaryLBG_CE float, @SecondaryLBG_CEFactor float
SELECT @SecondaryLBG_MBTU = ISNULL(@SE_U71LBG, 0) + ISNULL(@HYG_H2Loss_MBTU, 0),
	@SecondaryLBG_CE = ISNULL(@CE_U71LBG, 0) + ISNULL(@HYG_CE, 0)
SELECT @SecondaryLBG_CEFactor = ISNULL(@SecondaryLBG_CE, 0)/CASE WHEN @SecondaryLBG_MBTU > 0 THEN @SecondaryLBG_MBTU END
IF @DEBUG = 1
	SELECT SecondaryLBG_MBTU = @SecondaryLBG_MBTU, SecondaryLBG_CE = @SecondaryLBG_CE/@CO2_C_Ratio, SecondaryLBG_CEFactor = @SecondaryLBG_CEFactor/@CO2_C_Ratio

IF @OriginalLBG_MBTU > @AE_LBG
	SET @OriginalLBG_MBTU = @AE_LBG
SELECT @OriginalLBG_CE = @OriginalLBG_CEFactor*@OriginalLBG_MBTU
IF @SecondaryLBG_MBTU > (@AE_LBG-@OriginalLBG_MBTU)
	SELECT @SecondaryLBG_MBTU = (@AE_LBG-@OriginalLBG_MBTU)
SELECT @SecondaryLBG_CE = @SecondaryLBG_MBTU*@SecondaryLBG_CEFactor
DECLARE @OtherLBG_MBTU float, @OtherLBG_CE float, @LBG_CE float, @LBG_CE_Factor real
SELECT @OtherLBG_MBTU = @AE_LBG - @OriginalLBG_MBTU - @SecondaryLBG_MBTU
SELECT @OtherLBG_CE = 0.0172*@OtherLBG_MBTU*@CO2_C_Ratio
SELECT @LBG_CE = ISNULL(@OriginalLBG_CE, 0) + ISNULL(@SecondaryLBG_CE, 0) + ISNULL(@OtherLBG_CE, 0)
SELECT @LBG_CE_Factor = CASE WHEN @AE_LBG > 0 THEN @LBG_CE/@AE_LBG END

IF @DEBUG = 1
	SELECT OriginalLBG_CE = @OriginalLBG_CE/@CO2_C_Ratio, 
		SecondaryLBG_CE = @SecondaryLBG_CE/@CO2_C_Ratio, 
		OtherLBG_CE = @OtherLBG_CE/@CO2_C_Ratio, 
		LBG_CE = @LBG_CE/@CO2_C_Ratio, 
		LBG_CEF = @LBG_CE_Factor/@CO2_C_Ratio

DECLARE @NetLBG_MBTU float, @NetLBG_CE float, @NetLBG_CEFactor real
SELECT @NetLBG_MBTU = ISNULL(@AE_LBG, 0) - ISNULL(@SecondaryLBG_MBTU, 0)
SELECT @NetLBG_CE = ISNULL(@LBG_CE, 0) - ISNULL(@SecondaryLBG_CE, 0)
IF @NetLBG_MBTU > 0
	SELECT @NetLBG_CEFactor = @NetLBG_CE/@NetLBG_MBTU
IF @DEBUG = 1
BEGIN
	SELECT LBG_PRO = @LBG_PRO, AE_LBG = @AE_LBG, SecondaryLBG_MBTU = @SecondaryLBG_MBTU, LBG_CE = @LBG_CE/@CO2_C_Ratio, SecondaryLBG_CE = @SecondaryLBG_CE/@CO2_C_Ratio, NetLBG_CEFactor = @NetLBG_CEFactor/@CO2_C_Ratio, NetLBG_CE = @NetLBG_CE/@CO2_C_Ratio, NetLBG_MBTU = @NetLBG_MBTU
	SELECT CEF = @NetLBG_CEFactor/@CO2_C_Ratio , CE_NetLBG = @NetLBG_CE/@CO2_C_Ratio, NetLBG = @NetLBG_MBTU
END

IF @LBG_PRO > 0 AND ISNULL(@NetLBG_CEFactor, 0) > 0
	SELECT @CEF_LBG = @NetLBG_CEFactor
ELSE IF ISNULL(@SecondaryLBG_MBTU, 0) > 0
	SELECT @CEF_LBG = 0
ELSE
	SELECT @CEF_LBG = 0.03*@CO2_C_Ratio

SELECT @ACE_LBG = @AE_LBG*@CEF_LBG
IF @DEBUG = 1
	SELECT ACE_LBG = @ACE_LBG/@CO2_C_Ratio , AE_LBG = @AE_LBG, CEF_LBG = @CEF_LBG/@CO2_C_Ratio

DECLARE @T2_HYG TABLE (
	UnitID int NOT NULL,
	ProcessID varchar(5) NOT NULL,
	ProcessType varchar(5) NOT NULL,
	Cap real NOT NULL,
	UtilCap real NOT NULL,
	CO2RejectType varchar(4) NULL,
	H2Loss real NULL,
	FeedH2 real NULL,
	FeedC1 real NULL,
	FeedC2 real NULL,
	FeedC3 real NULL,
	FeedC4 real NULL,
	FeedLPG real NULL,
	FeedNap real NULL,
	FeedKero real NULL,
	FeedDiesel real NULL,
	FeedHGO real NULL,
	FeedARC real NULL,
	FeedVacResid real NULL,
	FeedLubeExtracts real NULL,
	FeedAsp real NULL,
	FeedCoke real NULL
)
INSERT @T2_HYG (UnitID, ProcessID, ProcessType, Cap, UtilCap, CO2RejectType, 
	H2Loss, FeedH2, FeedC1,FeedC2, FeedC3, FeedC4, 
	FeedLPG, FeedNap, FeedKero, FeedDiesel, FeedHGO, FeedARC, FeedVacResid, FeedLubeExtracts, FeedAsp, FeedCoke)
SELECT c.UnitID, c.ProcessID, c.ProcessType, c.Cap, c.UtilCap
, (SELECT CASE RptValue WHEN 1 THEN 'CRYO' WHEN 2 THEN 'PRSM' WHEN 3 THEN 'PSA' WHEN 4 THEN 'SOLV' ELSE '' END FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'CO2RejectType')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'H2Loss')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedH2')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedC1')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedC2')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedC3')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedC4')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedLPG')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedNap')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedKero')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedDiesel')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedHGO')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedARC')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedVacResid')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedLubeExtracts')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedAsp')
, (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedCoke')
FROM Config c WHERE c.ProcessID = 'HYG' AND c.SubmissionID = @SubmissionID AND c.UtilCap > 0

SELECT UnitID, ProcessID, ProcessType, UtilCap, H2Loss = CASE WHEN t2.H2Loss IS NULL OR (t2.H2Loss = 0 /*AND t2.@CO2RejectType <> 'SOLV'*/) THEN 12.5 ELSE t2.H2Loss END/100
	, SE_POX = CASE WHEN ProcessType = 'POX' THEN UtilCap*@NumDays*60/1000 ELSE 0 END
	, Carbon = CASE WHEN ProcessType IN ('HSM','HSN') THEN (ISNULL(0*FeedH2,0)+ISNULL(1*FeedC1,0)+ISNULL(2*FeedC2,0)+ISNULL(3*FeedC3,0)+ISNULL(4*FeedC4,0)
					+ ISNULL(4*FeedLPG,0)+ISNULL(8*FeedNap,0)+ISNULL(12*FeedKero,0)+ISNULL(17*FeedDiesel,0)+ISNULL(28*FeedHGO,0)+ISNULL(28*FeedARC,0)+ISNULL(28*FeedVacResid,0)+ISNULL(28*FeedAsp,0)+ISNULL(28*FeedCoke,0))
			WHEN ProcessType = 'POX' THEN (ISNULL(0*FeedH2,0)+ISNULL(1*FeedC1,0)+ISNULL(2*FeedC2,0)+ISNULL(3*FeedC3,0)+ISNULL(4*FeedC4,0)
					+ISNULL(4*FeedLPG,0)+ISNULL(8*FeedNap,0)+ISNULL(12*FeedKero,0)+ISNULL(17*FeedDiesel,0)+ISNULL(28*FeedHGO,0)+ISNULL(28*FeedARC,0)+ISNULL(28*FeedVacResid,0)+ISNULL(28*FeedAsp,0)+ISNULL(28*FeedCoke,0))
			ELSE 1 END
	, H2Yield = CASE WHEN ProcessType IN ('HSM','HSN') THEN (ISNULL(1*FeedH2,0)+ISNULL(4*FeedC1,0)+ISNULL(7*FeedC2,0)+ISNULL(10*FeedC3,0)+ISNULL(13*FeedC4,0)
					+ ISNULL(13*FeedLPG,0)+ISNULL(25.5*FeedNap,0)+ISNULL(36*FeedKero,0)+ISNULL(49.5*FeedDiesel,0)+ISNULL(75*FeedHGO,0)+ISNULL(79.5*FeedARC,0)+ISNULL(75*FeedVacResid,0)+ISNULL(68.5*FeedAsp,0)+ISNULL(66.5*FeedCoke,0))
			WHEN ProcessType = 'POX' THEN (ISNULL(1*FeedH2,0)+ISNULL(3*FeedC1,0)+ISNULL(4.9*FeedC2,0)+ISNULL(6.9*FeedC3,0)+ISNULL(8.8*FeedC4,0)
					+ISNULL(8.8*FeedLPG,0)+ISNULL(17.1*FeedNap,0)+ISNULL(23.4*FeedKero,0)+ISNULL(31.7*FeedDiesel,0)+ISNULL(45.6*FeedHGO,0)+ISNULL(50.1*FeedARC,0)+ISNULL(45.6*FeedVacResid,0)+ISNULL(46.8*FeedAsp,0)+ISNULL(37.1*FeedCoke,0))
			ELSE 1 END
	, FeedFractionGas = (ISNULL(FeedH2,0)+ISNULL(FeedC1,0) + ISNULL(FeedC2,0) + ISNULL(FeedC3,0) + ISNULL(FeedC4,0))/100
	, FeedFractionOther = (ISNULL(FeedLPG,0)+ISNULL(FeedNap,0) + ISNULL(FeedKero,0) + ISNULL(FeedDiesel,0) + ISNULL(FeedHGO,0) + ISNULL(FeedARC,0) + ISNULL(FeedVacResid,0) + ISNULL(FeedLubeExtracts,0) + ISNULL(FeedAsp,0) + ISNULL(FeedCoke,0))/100
	, FeedFractionLiquid = CAST(NULL AS real)
	, GasFeedH2 = (ISNULL(FeedH2*1,0)+ISNULL(FeedC1*4,0) + ISNULL(FeedC2*7,0) + ISNULL(FeedC3*10,0) + ISNULL(FeedC4*13,0))
	, GasFeedCO2 = (ISNULL(FeedH2*0,0)+ISNULL(FeedC1*1,0) + ISNULL(FeedC2*2,0) + ISNULL(FeedC3*3,0) + ISNULL(FeedC4*4,0))
	, LiquidFeedCE = CASE WHEN ProcessType = 'HSN' THEN (ISNULL(FeedLPG*4.41,0)+ISNULL(FeedNap*4.5,0) + ISNULL(FeedKero*4.78,0) + ISNULL(FeedDiesel*4.93,0) + ISNULL(FeedHGO*5.4,0) + ISNULL(FeedARC*5.1,0) + ISNULL(FeedVacResid*5.4,0) + ISNULL(FeedLubeExtracts*5.5 /*Guess by GLC*/,0) + ISNULL(FeedAsp*5.86,0) + ISNULL(FeedCoke*6,0))
			ELSE (ISNULL(FeedLPG*6.5,0)+ISNULL(FeedNap*6.7,0) + ISNULL(FeedKero*7.4,0) + ISNULL(FeedDiesel*7.7,0) + ISNULL(FeedHGO*8.8,0) + ISNULL(FeedARC*8,0) + ISNULL(FeedVacResid*8.8,0) + ISNULL(FeedLubeExtracts*8.8 /*Guess by GLC*/,0) + ISNULL(FeedAsp*10.3,0) + ISNULL(FeedCoke*10.8,0)) END/1000/100
	, CE_KSCF_GasFeed = CAST(NULL AS Float), CE_KSCF_GasFeedPlusLosses = CAST(NULL AS Float)
	, CE_KSCF_LiquidFeed = CAST(NULL AS Float), CE_KSCF_LiquidPlusLosses = CAST(NULL AS Float)
	, GrossCE = CAST(NULL AS Float), CE_POX = CAST(NULL AS Float), CE_H2Prod = CAST(NULL AS Float)
	, ACEF = CAST(NULL AS float), ACE = CAST(NULL AS float), SCEF = CAST(NULL AS float), SCE = CAST(NULL AS float)
	, ACELessSCE_NonPOX = CAST(NULL AS float), ACELessSCE_POX = CAST(NULL AS float)
INTO #CalcHYG
FROM @T2_HYG t2 WHERE ProcessType IN ('HSM','HSN','POX')


UPDATE #CalcHYG SET FeedFractionLiquid = CASE WHEN ProcessType = 'HSM' THEN 0 ELSE (1-FeedFractionGas) END
UPDATE #CalcHYG SET CE_KSCF_GasFeed = CASE WHEN FeedFractionLiquid = 1 THEN 0 WHEN FeedFractionGas = 0 THEN 0.0036 ELSE GasFeedCO2/GasFeedH2*(12.01/379.4)*1000/2204.6 END
UPDATE #CalcHYG SET CE_KSCF_GasFeedPlusLosses = CE_KSCF_GasFeed/(1-H2Loss)
UPDATE #CalcHYG SET CE_KSCF_LiquidFeed = CASE WHEN FeedFractionOther = 0 THEN CASE ProcessType WHEN 'HSN' THEN 0.0045 WHEN 'POX' THEN 0.0088 ELSE 0 END ELSE LiquidFeedCE/FeedFractionOther END
UPDATE #CalcHYG SET CE_KSCF_LiquidPlusLosses = CE_KSCF_LiquidFeed/(1-H2Loss)
UPDATE #CalcHYG SET ACEF = CE_KSCF_LiquidPlusLosses*FeedFractionLiquid+(1-FeedFractionLiquid)*CE_KSCF_GasFeedPlusLosses
UPDATE #CalcHYG SET SCEF = (CE_KSCF_LiquidFeed*FeedFractionLiquid+(1-FeedFractionLiquid)*CE_KSCF_GasFeed)/(1-0.125)
UPDATE #CalcHYG SET ACE = ACEF*UtilCap*@NumDays*@CO2_C_Ratio, SCE = SCEF*UtilCap*@NumDays*@CO2_C_Ratio
UPDATE #CalcHYG SET ACELessSCE_NonPOX = CASE WHEN ProcessType IN ('HSM','HSN') THEN ACE-SCE ELSE 0 END, ACELessSCE_POX = CASE WHEN ProcessType IN ('HSM','HSN') THEN 0 ELSE ACE-SCE END
UPDATE #CalcHYG SET Carbon = 0, H2Yield = 20 WHERE H2Yield = 0 AND ProcessType = 'HSM'
UPDATE #CalcHYG SET Carbon = Carbon + (100-H2Yield), H2Yield = H2Yield + 4*(100-H2Yield) WHERE H2Yield < 100 AND ProcessType = 'HSM'
UPDATE #CalcHYG SET Carbon = 4, H2Yield = 13 WHERE H2Yield = 0 AND ProcessType = 'HSN'
UPDATE #CalcHYG SET Carbon = 28, H2Yield = 45.6 WHERE H2Yield = 0 AND ProcessType = 'POX'
UPDATE #CalcHYG
SET 	CE_POX = SE_POX*0.0293*@CO2_C_Ratio,
	GrossCE = UtilCap*@NumDays*Carbon/H2Yield*CASE WHEN ProcessID = 'U72' THEN 0.369*0.0233 ELSE 12/0.3794/2204.6/.875 END*@CO2_C_Ratio
UPDATE #CalcHYG SET CE_H2Prod = GrossCE - ISNULL(CE_POX, 0)

-- Hydrogen Production Emissions
DECLARE @ACE_H2_SMR float, @SCE_H2_SMR float
DECLARE @CE_H2Gross float, @CE_H2GrossPOX float, @CE_H2GrossU72 float,
	@CE_H2_SMR float, @ACE_H2_POX float, @SCE_H2_POX float, @SCE_MEOH float
SELECT @CE_H2Gross = SUM(GrossCE), @CE_H2GrossPOX = SUM(CASE WHEN ProcessType = 'POX' THEN GrossCE ELSE 0 END)
FROM #CalcHYG WHERE ProcessID = 'HYG'
SELECT @SCE_H2_SMR = SUM(SCE) FROM #CalcHYG WHERE ProcessType <> 'POX'
SELECT @SCE_H2_POX = SUM(SCE) FROM #CalcHYG WHERE ProcessType = 'POX'
SELECT @ACE_H2_SMR = SUM(ACELessSCE_NonPox), @ACE_H2_POX = SUM(ACELessSCE_POX)
FROM #CalcHYG
SELECT 	@SCE_H2_SMR = ISNULL(@SCE_H2_SMR, 0), 
	@SCE_H2_POX = ISNULL(@SCE_H2_POX, 0) - ISNULL(@SCE_HYGPOX, 0)
SELECT 	@ACE_H2_SMR = ISNULL(@ACE_H2_SMR, 0) + @SCE_H2_SMR,
	@ACE_H2_POX = ISNULL(@ACE_H2_POX, 0) + ISNULL(@SCE_H2_U72, 0) + @SCE_H2_POX


SELECT @CE_H2GrossU72 = @U72_UtilCap*@NumDays*0.369*0.0232*@CO2_C_Ratio
--SELECT @ACE_H2_POX = ISNULL(@CE_H2GrossPOX, 0) - ISNULL(@SCE_HYGPOX, 0)
--SELECT @SCE_H2_SMR=SUM(SCE)-ISNULL(@SCE_HYGPOX,0)-ISNULL(@ACE_H2_POX,0) FROM #CalcHYG
--SELECT @ACE_H2_SMR=ISNULL(@SCE_H2_SMR,0)+SUM(ACELessSCE_NonPox) FROM #CalcHYG
--SELECT @SCE_H2_SMR = ISNULL(@SCE_H2_SMR, 0), @ACE_H2_SMR = ISNULL(@ACE_H2_SMR, 0)
IF @DEBUG = 1
BEGIN
	SELECT CE_H2GrossPOX = ISNULL(@CE_H2GrossPOX, 0)/@CO2_C_Ratio, SCE_HYGPOX = ISNULL(@SCE_HYGPOX, 0)/@CO2_C_Ratio, ACE_H2_POX = @ACE_H2_POX/@CO2_C_Ratio
	SELECT * FROM #CalcHYG
END

DROP TABLE #CalcHYG
SELECT @SCE_MEOH = -@U70_UtilCap*@NumDays*0.0472*@CO2_C_Ratio
IF @DEBUG = 1
	SELECT U70_UtilCap = @U70_UtilCap, SCE_MEOH = @SCE_MEOH/@CO2_C_Ratio
-- Indirect Emissions Related to Hydrogen
DECLARE @ACE_H2Pur float, @ACE_H2TransIn float, @ACE_H2TransOut float, @ACE_H2Sales float, @ACE_H2Deficit float
SELECT TransType = CASE Category WHEN 'RMI' THEN CASE WHEN MaterialID LIKE 'L%' THEN 'XIn' ELSE 'P' END WHEN 'RLUBE' THEN 'Xin' WHEN 'FFUEL' THEN 'Xout' WHEN 'OTHRM' THEN 'P' WHEN 'RCHEM' THEN 'Xin' WHEN 'FLUBE' THEN 'Xout' WHEN 'MPROD' THEN 'S' WHEN 'FCHEM' THEN 'Xout' END,
CE = 	CASE WHEN Bbl > 0 THEN Bbl*CASE WHEN MaterialID LIKE '[M,L]26' THEN 0.0488*1.15
				WHEN MaterialID LIKE '[M,L]78' THEN 0.0208
				WHEN MaterialID LIKE '[M,L]118' THEN 0.0279
				WHEN MaterialID LIKE '[M,L]119' THEN 0.0358 END*@CO2_C_Ratio
	ELSE MT*CASE 	WHEN MaterialID LIKE '[M,L]26' THEN .81*1.15
			WHEN MaterialID LIKE '[M,L]78' THEN .168
			WHEN MaterialID LIKE '[M,L]118' THEN .248
			WHEN MaterialID LIKE '[M,L]119' THEN .388 END
	END*1.5
INTO #H2Indirect
FROM Yield 
WHERE SubmissionID = @SubmissionID AND MaterialID IN ('M26','M78','M118','M119','L26','L78','L118','L119')

SELECT 	@ACE_H2Pur = SUM(CASE WHEN TransType = 'P' THEN CE ELSE 0 END),
	@ACE_H2TransIn = SUM(CASE WHEN TransType = 'XIn' THEN CE ELSE 0 END),
	@ACE_H2TransOut = -SUM(CASE WHEN TransType = 'XOut' THEN CE ELSE 0 END),
	@ACE_H2Sales = -SUM(CASE WHEN TransType = 'S' THEN CE ELSE 0 END)
FROM #H2Indirect
SELECT 	@ACE_H2Pur = ISNULL(@ACE_H2Pur, 0),
	@ACE_H2TransIn = ISNULL(@ACE_H2TransIn, 0),
	@ACE_H2TransOut = ISNULL(@ACE_H2TransOut, 0),
	@ACE_H2Sales = ISNULL(@ACE_H2Sales, 0)
SELECT @ACE_H2Deficit = @ACE_H2Pur + @ACE_H2TransIn + @ACE_H2TransOut + @ACE_H2Sales
IF @ACE_H2Deficit < 0 
	SELECT @ACE_H2Deficit = 0

DROP TABLE #H2Indirect

-- Purchased Electricity
DECLARE @RegionalEEF real
SELECT @RegionalEEF = c.EEF
FROM dbo.CountryFactors c INNER JOIN TSort t ON t.Country = c.Country
INNER JOIN dbo.SubmissionsAll s ON s.RefineryID = t.RefineryID
WHERE s.SubmissionID = @SubmissionID

DECLARE @ACE_PurElec float, @SE_PurElec float, @SCE_PurElec float, @ACE_ElecTransIn float
SELECT 	@ACE_PurElec = @ElecMWH_Pur*@RegionalEEF*@CO2_C_Ratio, @ACE_ElecTransIn = 0
	, @SE_PurElec = @AvgPurEnergyFraction*@SE_EII --Avg of 7.5% of Energy is purchased electricity
SELECT 	@SCE_PurElec = @SE_PurElec/9.09*@RegionalEEF*@CO2_C_Ratio
IF @ElecMWH_TransIn > -@ElecMWH_TransOut
	SELECT @ACE_ElecTransIn = (@ElecMWH_TransIn + @ElecMWH_TransOut)*@RegionalEEF*@CO2_C_Ratio

-- Asphalt blowing
DECLARE @ASP_UtilCap float, @ACE_ASP_CO2 float, @SCE_ASP_CO2 float, @ACE_ASP_CH4 float, @SCE_ASP_CH4 float
SELECT @ASP_UtilCap = SUM(UtilCap) FROM Config WHERE SubmissionID = @SubmissionID AND ProcessID = 'ASP'
SELECT @ACE_ASP_CO2 = ISNULL(@ASP_UtilCap, 0)*@NumDays/6.06*0.03*0.223,
	@ACE_ASP_CH4 = ISNULL(@ASP_UtilCap, 0)*@NumDays/6.06*0.03*0.117*23
SELECT @SCE_ASP_CO2 = @ACE_ASP_CO2, @SCE_ASP_CH4 = @ACE_ASP_CH4

-- CO2 Sales
DECLARE @CO2SalesMT float, @SCE_CO2Sales float, @ACE_CO2Sales float
SELECT @CO2SalesMT = SUM(ISNULL(MT, Bbl/7.7))
FROM Yield WHERE SubmissionID = @SubmissionID AND MaterialID IN ('M126','M237') AND Category IN ('MPROD','NLUBE')
SELECT @CO2SalesMT = ISNULL(@CO2SalesMT,0)
SELECT @SCE_CO2Sales = -@CO2SalesMT, @ACE_CO2Sales = -@CO2SalesMT
IF @DEBUG = 1
	SELECT CO2SalesMT = @CO2SalesMT, @ACE_CO2Sales

-- Flare Losses
DECLARE @InputMT float, @SCEF_Flare real, @SCE_FlareLoss float, @FlareLoss real, @ACE_FlareLoss float
SELECT @FlareLoss = FlareLoss
FROM RefineryFactors f INNER JOIN dbo.SubmissionsAll s ON s.RefineryID = f.RefineryID AND f.EffDate <= s.PeriodStart AND f.EffUntil >= s.PeriodEnd
WHERE SubmissionID = @SubmissionID AND FlareLoss > 0

/*IF dbo.IsLubeRefinery(@SubmissionID) = 1
BEGIN
	SELECT @InputMT = TotInputMT
	FROM MaterialTot WHERE SubmissionID = @SubmissionID
	SELECT @SCEF_Flare = 0.0006
END
ELSE BEGIN
*/
	SELECT @InputMT = NetInputBbl*0.159*(SELECT AvgDensity/1000 FROM CrudeTot WHERE CrudeTot.SubmissionID = MaterialTot.SubmissionID)
	FROM MaterialTot WHERE SubmissionID = @SubmissionID
	SELECT @SCEF_Flare = 0.0017
--END
SELECT 	@SCE_FlareLoss = 0.77*@InputMT*@SCEF_Flare*@CO2_C_Ratio, 
	@ACE_FlareLoss = 0.77*@InputMT*ISNULL(@FlareLoss/100, @SCEF_Flare)*@CO2_C_Ratio

DECLARE @ACE_OthProcessCO2 float, @SCE_OthProcessCO2 float
SELECT 	@ACE_OthProcessCO2 = ISNULL(@ACE_ASP_CO2, 0) + ISNULL(@ACE_FlareLoss, 0) + ISNULL(@ACE_CO2Sales, 0), 
	@SCE_OthProcessCO2 = ISNULL(@SCE_ASP_CO2, 0) + ISNULL(@SCE_FlareLoss, 0) + ISNULL(@SCE_CO2Sales, 0)

-- Methane Annex F Basis
DECLARE @SCE_CH4 float, @InputBbl float, @CombustionCH4 float, @FlareCH4Emissions float
SELECT @InputBbl = ISNULL(NetInputbbl, TotInputBbl) FROM MaterialTot m WHERE m.SubmissionID = @SubmissionID
SELECT @CombustionCH4 = SUM(UtilCap*CH4_MTperMBbl) 
FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID 
WHERE c.SubmissionID = @SubmissionID AND lu.CH4_MTperMBbl IS NOT NULL --AND c.ProcessID <> 'ASP'
SELECT @FlareCH4Emissions = @InputBbl/1000*0.189/379.3*16/2205 --*@NumDays
SELECT @CombustionCH4 = @NumDays*ISNULL(@CombustionCH4/1E6, 0) 
SELECT @SCE_CH4 = 23*(8.4 + (0.3941+2.6209+0.036+0.0451)*@InputBbl/1E6 + ISNULL(@CombustionCH4,0) + @FlareCH4Emissions) --+ ISNULL(@SCE_ASP_CH4, 0)
IF @DEBUG = 1
BEGIN
	SELECT c.ProcessID, UtilCap = SUM(UtilCap) , CombustionCH4 = SUM(UtilCap*CH4_MTperMBbl)
	FROM Config c INNER JOIN ProcessID_LU lu ON lu.ProcessID = c.ProcessID 
	WHERE c.SubmissionID = @SubmissionID AND lu.CH4_MTperMBbl IS NOT NULL
	GROUP BY c.ProcessID
	SELECT SCE_CH4 = @SCE_CH4, AllInputfactors = (8.4 + (0.3941+2.6209+0.036+0.0451)*@InputBbl/1E6), Combustion = ISNULL(@CombustionCH4,0), ASP = ISNULL(@SCE_ASP_CH4, 0), Flare = @FlareCH4Emissions
END
-- Coke on Catalyst
DECLARE @AE_COC float, @TotFCCStdEnergy float
SELECT @AE_COC = SourceMBTU FROM Energy WHERE SubmissionID = @SubmissionID  AND EnergyType = 'COC' AND TransType = 'PRO'
SELECT @TotFCCStdEnergy = SUM(StdEnergy) FROM FactorCalc WHERE SubmissionID = @SubmissionID AND FactorSet = @FactorSet AND ProcessID = 'FCC'
SELECT @AE_COC = ISNULL(@AE_COC, 0)
IF @TotFCCStdEnergy = 0
	SET @TotFCCStdEnergy = NULL

DECLARE @tblFCC_COC TABLE
(
	UnitID int NOT NULL,
	ProcessType varchar(4) NULL,
	UtilCap real NOT NULL,
	FeedTemp real NULL,
	PreheatTemp real NULL,
	Conv real NULL,
	CatOilRatio real NULL,
	Coke real NULL,
	FeedDensity real NULL,
	FeedSulfur real NULL,
	FeedConcarbon real NULL,
	FeedASTM90 real NULL,
	UOPK real NULL,
	FeedUVGO real NULL,
	FeedUVR real NULL,
	FeedUOther real NULL,
	FeedHVGO real NULL,
	FeedHVR real NULL,
	FeedHOther real NULL,
	FeedUVGODensity real NULL,
	FeedUVGOSulfur real NULL,
	FeedUVRDensity real NULL,
	FeedUVRSulfur real NULL,
	FeedHVGODensity real NULL,
	FeedHVGOSulfur real NULL,
	FeedHVRDensity real NULL,
	FeedHVRSulfur real NULL,
	StdEnergy real NULL,
	AllocCOC_MBTU float NULL,
	AllocCOC_KBTUPerBbl real NULL,
	UtilCap_MLbPerDay real NULL,
	CokeMake_MLbPerDay real NULL,
	AllocCOC_MBTUPerDay float NULL,
	HeatValue_BtuPerLb real NULL,
	FeedUVGO_BPD real NULL,
	FeedUVR_BPD real NULL,
	FeedUOther_BPD real NULL,
	FeedHVGO_BPD real NULL,
	FeedHVR_BPD real NULL,
	FeedHOther_BPD real NULL,
	FeedUVGO_MLbPD real NULL,
	FeedUVR_MLbPD real NULL,
	FeedUOther_MLbPD real NULL,
	FeedHVGO_MLbPD real NULL,
	FeedHVR_MLbPD real NULL,
	FeedHOther_MLbPD real NULL,
	Feed_MLbPD real NULL,
	SulfurUVGO_LbPD real NULL,
	SulfurUVR_LbPD real NULL,
	SulfurUOther_LbPD real NULL,
	SulfurHVGO_LbPD real NULL,
	SulfurHVR_LbPD real NULL,
	SulfurHOther_LbPD real NULL,
	Sulfur_LbPD real NULL,
	CokeSulfur_LbPD real NULL,
	CokeCarbonFraction_Calc real NULL,
	CokeCarbonFraction_Min real NULL,
	CokeCarbonFraction_Max real NULL,
	CokeCarbonFraction real NULL,
	ActualCE_MLbPD real NULL,
	ActualCE_kgPerbbl real NULL,
	ActualCE_gPerKBTUCoke real NULL,
	StdCE_kgPerbbl real NULL,
	CE_COC float NULL,
	SE_COC float NULL,
	SE_COC_Adj float NULL
)

INSERT @tblFCC_COC (UnitID, ProcessType, UtilCap, FeedTemp, PreheatTemp, Conv, CatOilRatio, Coke, FeedDensity, FeedSulfur, FeedConcarbon, FeedASTM90, UOPK, 
	FeedUVGO, FeedUVR, FeedUOther, FeedHVGO, FeedHVR, FeedHOther, 
	FeedUVGODensity, FeedUVGOSulfur, FeedUVRDensity, FeedUVRSulfur, FeedHVGODensity, FeedHVGOSulfur, FeedHVRDensity, FeedHVRSulfur,	StdEnergy)
SELECT c.UnitID, c.ProcessType, c.UtilCap, 
	FeedTemp = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedTemp'), 
	PHeatTemp = (SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'PHeatTemp'), 
	Conv = (SELECT CASE WHEN RptValue BETWEEN 0.1 AND 100 THEN RptValue END FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'Conv'), 
	CatOilRatio = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'CatOilRatio'),
	Coke = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'Coke'), 
	FeedDensity = ISNULL((SELECT dbo.UnitsConv(SAValue, 'API', 'KGM3') FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedGravity'), 910), 
	FeedSulfur = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedSulfur'), 
	FeedConcarbon = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedConCarbon'), 
	FeedASTM90 = ISNULL((SELECT SAValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedASTM90'), 1000), -- ??? Defaulted ASTM90 for testing
	UOPK = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'UOPK'), 
	FeedUVGO = ISNULL((SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedUVGO'), 0), 
	FeedUVR = ISNULL((SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedUVR'), 0), 
	FeedUOther =  ISNULL((SELECT SUM(RptValue) FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property IN ('FeedUVirginGO','FeedUCrackedGO','FeedUAGO','FeedUDAO','FeedUCGO','FeedUARC','FeedULube')), 0),
	FeedHVGO = ISNULL((SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedHVGO'), 0), 
	FeedHVR = ISNULL((SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedHVR'), 0), 
	FeedHOther = ISNULL((SELECT SUM(RptValue) FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property IN ('FeedHVirginGO','FeedHCrackedGO','FeedHAGO','FeedHDAO','FeedHCGO','FeedHARC','FeedHLube')), 0),
	FeedUVGODensity = (SELECT dbo.UnitsConv(SAValue, 'API', 'KGM3') FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedUVGODensity'), 
	FeedUVGOSulfur = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedUVGOSulfur'), 
	FeedUVRDensity = (SELECT dbo.UnitsConv(SAValue, 'API', 'KGM3') FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedUVRDensity'), 
	FeedUVRSulfur = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedUVRSulfur'), 
	FeedHVGODensity = (SELECT dbo.UnitsConv(SAValue, 'API', 'KGM3') FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedHVGODensity'), 
	FeedHVGOSulfur = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedHVGOSulfur'), 
	FeedHVRDensity = (SELECT dbo.UnitsConv(SAValue, 'API', 'KGM3') FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedHVRDensity'), 
	FeedHVRSulfur = (SELECT RptValue FROM ProcessData WHERE ProcessData.SubmissionID = @SubmissionID AND ProcessData.UnitID = c.UnitID AND ProcessData.Property = 'FeedHVRSulfur'), 
	fc.StdEnergy
FROM Config c INNER JOIN FactorCalc fc ON fc.SubmissionID = c.SubmissionID AND fc.UnitID = c.UnitID AND fc.FactorSet = @FactorSet
WHERE c.SubmissionID = @SubmissionID AND c.UtilCap > 0 AND c.ProcessID = 'FCC'

UPDATE @tblFCC_COC
SET	AllocCOC_MBTU = @AE_COC*StdEnergy/@TotFCCStdEnergy,
	UtilCap_MLbPerDay = UtilCap*42*8.33*(CASE WHEN FeedDensity > 500 THEN FeedDensity END/1000)/1000000

UPDATE @tblFCC_COC
SET	AllocCOC_KBTUPerBbl = AllocCOC_MBTU/(UtilCap*@NumDays)*1000,
	CokeMake_MLbPerDay = UtilCap_MLbPerDay*Coke/100,
	AllocCOC_MBTUPerDay = AllocCOC_MBTU/@NumDays

UPDATE @tblFCC_COC
SET	HeatValue_BtuPerLb = CASE WHEN CokeMake_MLbPerDay = 0 THEN 0 ELSE AllocCOC_MBTUPerDay/CokeMake_MLbPerDay END,
	FeedUVGO_BPD = ISNULL(UtilCap*42*8.33*(FeedUVGO/100),0),
	FeedUVR_BPD = ISNULL(UtilCap*42*8.33*(FeedUVR/100),0),
	FeedUOther_BPD = ISNULL(UtilCap*42*8.33*(FeedUOther/100),0),
	FeedHVGO_BPD = ISNULL(UtilCap*42*8.33*(FeedHVGO/100),0),
	FeedHVR_BPD = ISNULL(UtilCap*42*8.33*(FeedHVR/100),0),
	FeedHOther_BPD = ISNULL(UtilCap*42*8.33*(FeedHOther/100),0)

UPDATE @tblFCC_COC
SET	FeedUVGO_MLbPD = FeedUVGO_BPD*(CASE WHEN FeedUVGODensity>0 THEN FeedUVGODensity ELSE FeedDensity END/1000)/1000000,
	FeedUVR_MLbPD = FeedUVR_BPD*(CASE WHEN FeedUVRDensity>0 THEN FeedUVRDensity ELSE FeedDensity END/1000)/1000000,
	FeedHVGO_MLbPD = FeedHVGO_BPD*(CASE WHEN FeedHVGODensity>0 THEN FeedHVGODensity ELSE FeedDensity END/1000)/1000000,
	FeedHVR_MLbPD = FeedHVR_BPD*(CASE WHEN FeedHVRDensity>0 THEN FeedHVRDensity ELSE FeedDensity END/1000)/1000000

UPDATE @tblFCC_COC
SET	FeedUOther_MLbPD = CASE WHEN FeedUOther > 0 THEN (UtilCap_MLbPerDay-(FeedUVGO_MLbPD+FeedUVR_MLbPD+FeedHVGO_MLbPD+FeedHVR_MLbPD))*(FeedUOther_BPD/(FeedUOther_BPD+FeedHOther_BPD)) ELSE 0 END,
	FeedHOther_MLbPD = CASE WHEN FeedHOther > 0 THEN (UtilCap_MLbPerDay-(FeedUVGO_MLbPD+FeedUVR_MLbPD+FeedHVGO_MLbPD+FeedHVR_MLbPD))*(FeedHOther_BPD/(FeedUOther_BPD+FeedHOther_BPD)) ELSE 0 END

UPDATE @tblFCC_COC
SET	Feed_MLbPD = FeedUVGO_MLbPD+FeedUVR_MLbPD+FeedHVGO_MLbPD+FeedHVR_MLbPD+FeedUOther_MLbPD+FeedHOther_MLbPD,
	Sulfur_LbPD = UtilCap_MLbPerDay*1000000*ISNULL(FeedSulfur,0)/100,
	SulfurUVGO_LbPD = ISNULL(FeedUVGO_MLbPD*1000000*ISNULL(FeedUVGOSulfur, FeedSulfur)/100,0),
	SulfurUVR_LbPD = ISNULL(FeedUVR_MLbPD*1000000*ISNULL(FeedUVRSulfur,FeedSulfur)/100,0),
	SulfurHVGO_LbPD = ISNULL(FeedHVGO_MLbPD*1000000*ISNULL(FeedHVGOSulfur, FeedSulfur)/100,0),
	SulfurHVR_LbPD = ISNULL(FeedHVR_MLbPD*1000000*ISNULL(FeedHVRSulfur,FeedSulfur)/1000,0)

UPDATE @tblFCC_COC
SET	SulfurUOther_LbPD = ISNULL(CASE WHEN FeedUOther_MLbPD > 0 THEN FeedUOther_MLbPD/(FeedUOther_MLbPD+FeedHOther_MLbPD)*(Sulfur_LbPD-SulfurUVGO_LbPD-SulfurUVR_LbPD-SulfurHVGO_LbPD-SulfurHVR_LbPD) ELSE 0 END,0),
	SulfurHOther_LbPD = ISNULL(CASE WHEN FeedHOther_MLbPD > 0 THEN FeedHOther_MLbPD/(FeedUOther_MLbPD+FeedHOther_MLbPD)*(Sulfur_LbPD-SulfurUVGO_LbPD-SulfurUVR_LbPD-SulfurHVGO_LbPD-SulfurHVR_LbPD) ELSE 0 END,0)

UPDATE @tblFCC_COC
SET	CokeSulfur_LbPD = ((SulfurUVGO_LbPD+SulfurUVR_LbPD+SulfurUOther_LbPD)*0.1+(SulfurHVGO_LbPD+SulfurHVR_LbPD+SulfurHOther_LbPD)*0.2)*ISNULL(Conv,0)/100

UPDATE @tblFCC_COC
SET	CokeCarbonFraction_Calc=1.4424-AllocCOC_MBTUPerDay*1000000/(31892*CokeMake_MLbPerDay*1000000)-1.3176*CokeSulfur_LbPD/(CokeMake_MLbPerDay*1000000),
	CokeCarbonFraction_Min = 0.85-CokeSulfur_LbPD/(CokeMake_MLbPerDay*1000000),
	CokeCarbonFraction_Max = 0.98-CokeSulfur_LbPD/(CokeMake_MLbPerDay*1000000)
WHERE CokeMake_MLbPerDay <> 0

UPDATE @tblFCC_COC
SET	CokeCarbonFraction = CASE WHEN CokeCarbonFraction_Calc < CokeCarbonFraction_Min THEN CokeCarbonFraction_Min WHEN CokeCarbonFraction_Calc > CokeCarbonFraction_Max THEN CokeCarbonFraction_Max ELSE CokeCarbonFraction_Calc END
UPDATE @tblFCC_COC
SET	ActualCE_MLbPD = CokeCarbonFraction*CokeMake_MLbPerDay
UPDATE @tblFCC_COC
SET	ActualCE_kgPerbbl = ActualCE_MLbPD*1000000/2.205/UtilCap
UPDATE @tblFCC_COC
SET	ActualCE_gPerKBTUCoke = ActualCE_kgPerbbl/CASE WHEN AllocCOC_KBTUPerBbl > 0 THEN AllocCOC_KBTUPerBbl END *1000

IF @DEBUG = 1
	SELECT UnitID, FeedConcarbon, UOPK, FeedDensity, CatOilRatio, Conv, FeedASTM90, FeedTemp, PreheatTemp
	FROM @tblFCC_COC
	
UPDATE @tblFCC_COC
SET	StdCE_kgPerbbl = CASE WHEN ProcessType IN ('RCC','MRCC') THEN 0.6024 ELSE 0 END 
	+ 0.5704*POWER(ISNULL(FeedConcarbon, 1.17), 1.1248) 
	- 4.9044*POWER(ISNULL(UOPK, 11.8), 0.4254)
	+ 3.1232E-6*POWER(ISNULL(FeedDensity, 910.6), 2.2206)
	+ 0.4481*POWER(ISNULL(CatOilRatio, 6.64), 0.7374)
	+ 0.1918*POWER(ISNULL(Conv, 74.2), 0.6878)
	+ 0.1518*(ISNULL(Conv, 74.2)/(100-ISNULL(Conv, 74.2)))
	+ 0.06229*POWER(ISNULL(FeedASTM90,1013), 0.5593)
	- 2.0853E-4*POWER(CASE WHEN ISNULL(FeedTemp, -1)>ISNULL(PreHeatTemp, 0) THEN ISNULL(FeedTemp, 259) ELSE ISNULL(PreHeatTemp, 259) END, 1.2713)

UPDATE @tblFCC_COC
SET	CE_COC = @NumDays*StdCE_kgPerbbl*UtilCap/1000
UPDATE @tblFCC_COC
SET	SE_COC =CE_COC/CokeCarbonFraction/UtilCap/@NumDays/0.159/FeedDensity/Coke*100000*AllocCOC_MBTU
UPDATE @tblFCC_COC
SET	SE_COC_Adj = CASE WHEN SE_COC > CE_COC/0.022 THEN CE_COC/0.022 WHEN SE_COC < CE_COC/0.028 THEN CE_COC/0.028 ELSE SE_COC END

DECLARE @SE_COC float, @SCE_COC float, @ACE_COC float
SELECT @SE_COC = SUM(SE_COC_Adj), @SCE_COC = SUM(CE_COC)*@CO2_C_Ratio, @ACE_COC= SUM(ActualCE_gPerKBTUCoke*AllocCOC_MBTU)*@CO2_C_Ratio/1000
FROM @tblFCC_COC
SELECT @SE_COC = ISNULL(@SE_COC, 0), @SCE_COC = ISNULL(@SCE_COC, 0), @ACE_COC = ISNULL(@ACE_COC, 0)

IF @AE_COC > 0
BEGIN
	IF @ACE_COC <= 0
		SELECT @ACE_COC = @AE_COC*0.0293
	ELSE IF @ACE_COC > 0.028*@AE_COC*@CO2_C_Ratio
		SELECT @ACE_COC = 0.028*@AE_COC*@CO2_C_Ratio
	ELSE IF @ACE_COC < 0.022*@AE_COC*@CO2_C_Ratio
		SELECT @ACE_COC = 0.022*@AE_COC*@CO2_C_Ratio
END

IF @DEBUG = 1
	SELECT *, ACE_COC = (ActualCE_gPerKBTUCoke*AllocCOC_MBTU)/1000 FROM @tblFCC_COC

-- Nitrous Oxide
DECLARE @ACE_N2O float, @SCE_N2O float
SELECT @ACE_N2O = (@AE_Fungible + @AE_FG + @AE_LBG)*0.00013*@CO2_C_Ratio
SELECT @SCE_N2O = (@SE_EII - ISNULL(@SE_COC, 0) - ISNULL(@SE_FCCOKE, 0) - ISNULL(@SE_FXCOKE, 0) - ISNULL(@SE_CALCIN, 0) - ISNULL(@SE_U73POX, 0) - ISNULL(@SE_U72POX, 0) - ISNULL(@SE_HYGPOX, 0) - ISNULL(@SE_PurElec, 0))*0.00013*@CO2_C_Ratio


DECLARE @ACE_H2Prod float, @SCE_H2Prod float, @SE_FG float, @SCE_FG float
SELECT 	@ACE_H2Prod = ISNULL(@ACE_H2_SMR, 0) + ISNULL(@ACE_H2_POX, 0) + ISNULL(@SCE_MEOH, 0),
	@SCE_H2Prod = ISNULL(@SCE_H2_SMR, 0) + ISNULL(@SCE_H2_POX, 0) + ISNULL(@SCE_H2_U72, 0) + ISNULL(@SCE_MEOH, 0)
SELECT @SE_FG = @SE_CEI - ISNULL(@SE_COC, 0) - ISNULL(@SE_FXCoke, 0) - ISNULL(@SE_FCCoke, 0) - ISNULL(@SE_FXLBG, 0) - ISNULL(@SE_FCLBG, 0) 
		- ISNULL(@SE_CALCIN, 0) - ISNULL(@SE_U73POX, 0) - ISNULL(@SE_U72POX, 0) - ISNULL(@SE_HYGPOX, 0) - (@AvgPurEnergyFraction * @SE_EII)
SELECT @SCE_FG = @SE_FG*0.0161*@CO2_C_Ratio

DECLARE @ACE_DirectsCO2_Energy float, @SCE_DirectsCO2_Energy float, @ACE_TotalCO2_Energy float, @SCE_TotalCO2_Energy float
DECLARE @ACE_DirectsCO2 float, @SCE_DirectsCO2 float, @ACE_TotalCO2 float, @SCE_TotalCO2 float, @SE_FG_Indirects float, @SCE_FG_Indirects float
DECLARE	@IndirectMBTU float, @SE_EII_Directs float, @SE_NonReferenceFuel float, @SCE_NonReferenceFuel float, @SCE_H2Prod_Directs float, @SE_Directs_ReferenceFuel float, 
	@SE_FG_Directs float, @SCE_FG_Directs float, @ACE_CEI_CO2 float, @SCE_CEI_CO2 float
DECLARE @AE_Directs float, @ACE_Directs float, @SE_Directs float, @SCE_Directs float, @CEI_Directs real,
	@AE_EII_Total float, @AE_EII_Directs float, @AE_Total float, @ACE_Total float, @SE_Total float, @SCE_Total float, @CEI_Total real
SELECT	@ACE_TotalCO2_Energy = @ACE_Fungible + @ACE_FG + @ACE_LBG + @ACE_MC + @ACE_NMC + @ACE_COC + @ACE_StmPur + @ACE_StmTransIn + @ACE_PurElec + @ACE_ElecTransIn,
	@SCE_TotalCO2_Energy = @SCE_FG + ISNULL(@SCE_FCLBG, 0) + ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_HYGPOX, 0) + ISNULL(@SCE_U72POX, 0) + ISNULL(@SCE_U73POX, 0) + ISNULL(@SCE_CALCIN, 0) + ISNULL(@SCE_FCCoke, 0) + ISNULL(@SCE_FXCoke,0) + ISNULL(@SCE_COC,0) + ISNULL(@SCE_PurElec,0)
SELECT	@ACE_TotalCO2 = @ACE_TotalCO2_Energy + ISNULL(@ACE_H2Prod, 0) + ISNULL(@ACE_H2Deficit, 0) + ISNULL(@ACE_OthProcessCO2, 0),
	@SCE_TotalCO2 = @SCE_TotalCO2_Energy + ISNULL(@SCE_H2Prod, 0) + ISNULL(@ACE_H2Deficit, 0) + ISNULL(@SCE_OthProcessCO2, 0)
SELECT 	@AE_Total = @AE_Fungible + @AE_FG + @AE_LBG + @AE_MC + @AE_NMC + @AE_COC + @AE_StmPur + @AE_StmTransIn + @AE_PurElec + @AE_ProdElecAdj, 
	@SE_Total = @SE_FG + ISNULL(@SE_FCLBG, 0) + ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_HYGPOX, 0) + ISNULL(@SE_U72POX, 0) + ISNULL(@SE_U73POX, 0) + ISNULL(@SE_CALCIN, 0) + ISNULL(@SE_FCCOKE, 0) + ISNULL(@SE_FXCOKE,0) + ISNULL(@SE_COC,0) + ISNULL(@SE_PurElec,0), 
	@ACE_Total = @ACE_TotalCO2 + ISNULL(@SCE_CH4, 0) + ISNULL(@ACE_N2O, 0),
	@SCE_Total = @SCE_TotalCO2 + ISNULL(@SCE_CH4, 0) + ISNULL(@SCE_N2O, 0)
SELECT @CEI_Total = @ACE_Total/@SCE_Total*100

SELECT @IndirectMBTU = @AE_StmPur + @AE_StmTransIn + ISNULL(@AE_PurElec, 0)
SELECT @SE_EII_Directs = (@TotEnergyConsMBTU-@IndirectMBTU)/(@EII/100)
SELECT 	@SE_NonReferenceFuel = ISNULL(@SE_COC,0) + ISNULL(@SE_FCCOKE, 0) + ISNULL(@SE_FXCOKE,0) + ISNULL(@SE_FCLBG, 0) + ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_CALCIN, 0) + ISNULL(@SE_U73POX, 0) + ISNULL(@SE_U72POX, 0),
	@SCE_NonReferenceFuel = ISNULL(@SCE_COC,0) + ISNULL(@SCE_FCCOKE, 0) + ISNULL(@SCE_FXCOKE,0) + ISNULL(@SCE_FCLBG, 0) + ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_CALCIN, 0) + ISNULL(@SCE_U73POX, 0) + ISNULL(@SCE_U72POX, 0),
	@SCE_H2Prod_Directs = @SCE_H2Prod
SELECT @SE_Directs_ReferenceFuel = @SE_EII_Directs - @SE_NonReferenceFuel + @AE_XFER_Sales_Stm_Elec
SELECT 	@AE_Directs = @AE_Fungible + @AE_FG + @AE_LBG + @AE_MC + @AE_NMC + @AE_COC
SELECT 	@AE_EII_Total = @AE_Total - (@AE_StmDist + @AE_StmSales + @AE_GEDist + @AE_GESales),
	@AE_EII_Directs = @AE_Directs - (@AE_StmDist + @AE_StmSales + @AE_GEDist + @AE_GESales)
SELECT 	@SE_FG_Directs = @AE_EII_Directs/(@EII/100)-(ISNULL(@SE_FCLBG, 0) + ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_HYGPOX, 0) + ISNULL(@SE_U72POX, 0) + ISNULL(@SE_U73POX, 0) + ISNULL(@SE_CALCIN, 0) + ISNULL(@SE_FCCOKE, 0) + ISNULL(@SE_FXCOKE,0) + ISNULL(@SE_COC,0)) + (@AE_StmDist + @AE_StmSales + @AE_GEDist + @AE_GESales)
SELECT 	@SCE_FG_Directs = CASE WHEN @SE_FG_Directs > 0 THEN @SE_FG_Directs*(@SCE_FG/@SE_FG) ELSE 0 END
SELECT 	@SE_FG_Indirects = @SE_FG - @SE_FG_Directs, @SCE_FG_Indirects = @SCE_FG - @SCE_FG_Directs

SELECT 	@ACE_CEI_CO2 = @ACE_Fungible + @ACE_FG + @ACE_MC + @ACE_NMC + @ACE_COC, 
	@SCE_CEI_CO2 = @SCE_FG_Directs + ISNULL(@SCE_FCLBG, 0) + ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_HYGPOX, 0) + ISNULL(@SCE_U72POX, 0) + ISNULL(@SCE_U73POX, 0) + ISNULL(@SCE_CALCIN, 0) + ISNULL(@SCE_FCCoke, 0) + ISNULL(@SCE_FXCoke,0) + ISNULL(@SCE_COC,0)
SELECT	@SE_Directs = @SE_FG_Directs + ISNULL(@SE_FCLBG, 0) + ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_HYGPOX, 0) + ISNULL(@SE_U72POX, 0) + ISNULL(@SE_U73POX, 0) + ISNULL(@SE_CALCIN, 0) + ISNULL(@SE_FCCOKE, 0) + ISNULL(@SE_FXCOKE,0) + ISNULL(@SE_COC,0),
	@SCE_Directs = @SCE_CEI_CO2 + @SCE_H2Prod_Directs + ISNULL(@SCE_OthProcessCO2, 0) + ISNULL(@SCE_CH4, 0) + ISNULL(@SCE_N2O, 0),
	@ACE_Directs = @ACE_Total - (ISNULL(@ACE_H2Deficit, 0) + @ACE_PurElec + @ACE_ElecTransIn + @ACE_StmPur + @ACE_StmTransIn)
SELECT	@ACE_DirectsCO2_Energy = @ACE_Fungible + @ACE_FG + @ACE_LBG + @ACE_MC + @ACE_NMC + @ACE_COC,
	@SCE_DirectsCO2_Energy = @SCE_FG_Directs + ISNULL(@SCE_FCLBG, 0) + ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_HYGPOX, 0) + ISNULL(@SCE_U72POX, 0) + ISNULL(@SCE_U73POX, 0) + ISNULL(@SCE_CALCIN, 0) + ISNULL(@SCE_FCCoke, 0) + ISNULL(@SCE_FXCoke,0) + ISNULL(@SCE_COC,0)
SELECT	@ACE_DirectsCO2 = @ACE_DirectsCO2_Energy + ISNULL(@ACE_H2Prod, 0) + ISNULL(@ACE_OthProcessCO2, 0),
	@SCE_DirectsCO2 = @SCE_DirectsCO2_Energy + ISNULL(@SCE_H2Prod, 0) + ISNULL(@SCE_OthProcessCO2, 0)
SELECT @CEI_Directs = @ACE_Directs/@SCE_Directs*100

IF @DEBUG = 1
	SELECT 	IndirectMBTU = @IndirectMBTU , SE_EII_Directs = @SE_EII_Directs , SE_NonReferenceFuel = @SE_NonReferenceFuel , @SCE_NonReferenceFuel AS SCE_NonReferenceFuel, @SCE_H2Prod_Directs AS SCE_H2Prod_Directs, @SE_Directs_ReferenceFuel AS SE_Directs_ReferenceFuel, 
		@ACE_CEI_CO2 AS ACE_CEI_CO2, @SCE_CEI_CO2 AS SCE_CEI_CO2,
		@SE_FG_Directs AS SE_FG_Directs, @SCE_FG_Directs AS SCE_FG_Directs,
		@AE_Directs AS AE_Directs, @ACE_Directs AS ACE_Directs, @SE_Directs AS SE_Directs, @SCE_Directs AS SCE_Directs, @CEI_Directs AS CEI_Directs,
		@AE_EII_Directs AS AE_EII_Directs, @AE_EII_Total AS AE_EII_Total, @AE_Total AS AE_Total, @ACE_Total AS ACE_Total, @SE_Total AS SE_Total, @SCE_Total AS SCE_Total, @CEI_Total AS CEI_Total

INSERT INTO CEI (SubmissionID, FactorSet, CEI_Directs, AE_Directs, ACE_Directs, SE_Directs, SCE_Directs, AE_EII_Directs, SE_EII_Directs, 
	CEI_Total, AE_Total, ACE_Total, SE_Total, SCE_Total, AE_EII_Total, SE_EII_Total, 
	AE_Fungible, ACE_Fungible, AE_FG, ACE_FG, SE_FG, SCE_FG, SE_FG_Directs, SCE_FG_Directs, SE_FG_Indirects, SCE_FG_Indirects, AE_LBG, ACE_LBG, 
	SE_COKLBG, SCE_COKLBG, SE_U73POX, SCE_U73POX, SE_POX, SCE_POX, 
	AE_MC, ACE_MC, SE_CALCIN, SCE_CALCIN, AE_NMC, ACE_NMC, SE_FCFXCoke, SCE_FCFXCoke, AE_COC, ACE_COC, SE_COC, SCE_COC, 
	AE_StmPur, ACE_StmPur, AE_StmTransIn, ACE_StmTransIn, AE_PurElec, ACE_PurElec, SE_PurElec, SCE_PurElec, AE_ProdElecAdj, ACE_ElecTransIn, 
	AE_StmDist, AE_StmSales, AE_GEDist, AE_GESales, 
	ACE_H2_SMR, SCE_H2_SMR, ACE_H2_POX, SCE_H2_POX, SCE_MEOH, ACE_H2Prod, SCE_H2Prod, SCE_H2Prod_Directs, ACE_H2Pur, ACE_H2TransIn, ACE_H2TransOut, ACE_H2Sales, ACE_H2Deficit, 
	ACE_ASP_CO2, SCE_ASP_CO2, ACE_FlareLoss, SCE_FlareLoss, ACE_CO2Sales, SCE_CO2Sales, ACE_OthProcessCO2, SCE_OthProcessCO2, SCE_CH4, ACE_N2O, SCE_N2O,
	ACE_DirectsCO2_Energy, SCE_DirectsCO2_Energy, ACE_TotalCO2_Energy, SCE_TotalCO2_Energy, 
	ACE_TotalCO2, SCE_TotalCO2, ACE_DirectsCO2, SCE_DirectsCO2)
SELECT @SubmissionID, @FactorSet
	, @CEI_Directs, @AE_Directs, @ACE_Directs, @SE_Directs, @SCE_Directs, @AE_EII_Directs, @SE_EII_Directs
	, @CEI_Total, @AE_Total, @ACE_Total, @SE_Total, @SCE_Total, @AE_EII_Total, @SE_EII
	, @AE_Fungible, @ACE_Fungible, @AE_FG, @ACE_FG, @SE_FG, @SCE_FG, @SE_FG_Directs, @SCE_FG_Directs, @SE_FG_Indirects, @SCE_FG_Indirects, @AE_LBG, @ACE_LBG
	, ISNULL(@SE_FXLBG, 0) + ISNULL(@SE_FCLBG, 0), ISNULL(@SCE_FXLBG, 0) + ISNULL(@SCE_FCLBG, 0)
	, @SE_U73POX, @SCE_U73POX, ISNULL(@SE_U72POX, 0) + ISNULL(@SE_HYGPOX, 0), ISNULL(@SCE_U72POX, 0) + ISNULL(@SCE_HYGPOX, 0)
	, @AE_MC, @ACE_MC, @SE_CALCIN, @SCE_CALCIN, @AE_NMC, @ACE_NMC
	, ISNULL(@SE_FXCoke, 0) + ISNULL(@SE_FCCoke, 0), ISNULL(@SCE_FXCoke, 0) + ISNULL(@SCE_FCCoke, 0)
	, @AE_COC, @ACE_COC, @SE_COC, @SCE_COC
	, @AE_StmPur, @ACE_StmPur, @AE_StmTransIn, @ACE_StmTransIn, @AE_PurElec, @ACE_PurElec, @SE_PurElec, @SCE_PurElec, @AE_ProdElecAdj
	, @ACE_ElecTransIn, -@AE_StmDist, -@AE_StmSales, -@AE_GEDist, -@AE_GESales
	, @ACE_H2_SMR, @SCE_H2_SMR, ISNULL(@ACE_H2_POX, 0), ISNULL(@SCE_H2_POX, 0) + ISNULL(@SCE_H2_U72, 0), @SCE_MEOH, @ACE_H2Prod, @SCE_H2Prod, @SCE_H2Prod
	, @ACE_H2Pur, @ACE_H2TransIn, @ACE_H2TransOut, @ACE_H2Sales, @ACE_H2Deficit
	, @ACE_ASP_CO2, @SCE_ASP_CO2, @ACE_FlareLoss, @SCE_FlareLoss, @ACE_CO2Sales, @SCE_CO2Sales, @ACE_OthProcessCO2, @SCE_OthProcessCO2
	, @SCE_CH4, @ACE_N2O, @SCE_N2O
	, @ACE_DirectsCO2_Energy, @SCE_DirectsCO2_Energy, @ACE_TotalCO2_Energy, @SCE_TotalCO2_Energy
	, @ACE_TotalCO2, @SCE_TotalCO2, @ACE_DirectsCO2, @SCE_DirectsCO2

IF @DEBUG = 1
	SELECT * FROM CEI WHERE SubmissionID = @SubmissionID
