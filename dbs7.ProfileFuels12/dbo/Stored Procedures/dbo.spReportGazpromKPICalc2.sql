﻿


CREATE   PROC [dbo].[spReportGazpromKPICalc2] (@RefineryID char(6), @PeriodYear smallint, @PeriodMonth smallint, @DataSet varchar(15), 
	@FactorSet FactorSet, @Scenario Scenario, @Currency CurrencyCode, @UOM varchar(5),
	@EII real = NULL OUTPUT, @EII_QTR real = NULL OUTPUT, @EII_Avg real = NULL OUTPUT, @EII_Ytd real = NULL OUTPUT, 
	@EnergyUseDay real = NULL OUTPUT, @EnergyUseDay_QTR real = NULL OUTPUT, @EnergyUseDay_Avg real = NULL OUTPUT, @EnergyUseDay_Ytd real = NULL OUTPUT, 
	@TotStdEnergy real = NULL OUTPUT, @TotStdEnergy_QTR real = NULL OUTPUT, @TotStdEnergy_Avg real = NULL OUTPUT, @TotStdEnergy_Ytd real = NULL OUTPUT, 
	@RefUtilPcnt real = NULL OUTPUT, @RefUtilPcnt_QTR real = NULL OUTPUT, @RefUtilPcnt_Avg real = NULL OUTPUT, @RefUtilPcnt_Ytd real = NULL OUTPUT, 
	@Edc real = NULL OUTPUT, @Edc_QTR real = NULL OUTPUT, @Edc_Avg real = NULL OUTPUT, @Edc_Ytd real = NULL OUTPUT, 
	@UEdc real = NULL OUTPUT, @UEdc_QTR real = NULL OUTPUT, @Uedc_Avg real = NULL OUTPUT, @UEdc_Ytd real = NULL OUTPUT, 
	@VEI real = NULL OUTPUT, @VEI_QTR real = NULL OUTPUT, @VEI_Avg real = NULL OUTPUT, @VEI_Ytd real = NULL OUTPUT, 
	@ReportLossGain real = NULL OUTPUT, @ReportLossGain_QTR real = NULL OUTPUT, @ReportLossGain_Avg real = NULL OUTPUT, @ReportLossGain_Ytd real = NULL OUTPUT, 
	@EstGain real = NULL OUTPUT, @EstGain_QTR real = NULL OUTPUT, @EstGain_Avg real = NULL OUTPUT, @EstGain_Ytd real = NULL OUTPUT, 
	@OpAvail real = NULL OUTPUT, @OpAvail_QTR real = NULL OUTPUT, @OpAvail_Avg real = NULL OUTPUT, @OpAvail_Ytd real = NULL OUTPUT, 
	@MechUnavailTA real = NULL OUTPUT, @MechUnavailTA_QTR real = NULL OUTPUT, @MechUnavailTA_Avg real = NULL OUTPUT, @MechUnavailTA_Ytd real = NULL OUTPUT, 
	@NonTAUnavail real = NULL OUTPUT, @NonTAUnavail_QTR real = NULL OUTPUT, @NonTAUnavail_Avg real = NULL OUTPUT, @NonTAUnavail_Ytd real = NULL OUTPUT, 
	@RoutIndex real = NULL OUTPUT, @RoutIndex_QTR real = NULL OUTPUT, @RoutIndex_Avg real = NULL OUTPUT, @RoutIndex_Ytd real = NULL OUTPUT,
	@RoutCost real = NULL OUTPUT, @RoutCost_QTR real = NULL OUTPUT, @RoutCost_Avg real = NULL OUTPUT, @RoutCost_Ytd real = NULL OUTPUT, 
	@PersIndex real = NULL OUTPUT, @PersIndex_QTR real = NULL OUTPUT, @PersIndex_Avg real = NULL OUTPUT, @PersIndex_Ytd real = NULL OUTPUT, 
	@AnnTAWhr real = NULL OUTPUT, @AnnTAWhr_QTR real = NULL OUTPUT, @AnnTAWhr_Avg real = NULL OUTPUT, @AnnTAWhr_Ytd real = NULL OUTPUT,
	@NonTAWHr real = NULL OUTPUT, @NonTAWHr_QTR real = NULL OUTPUT, @NonTAWHr_Avg real = NULL OUTPUT, @NonTAWHr_Ytd real = NULL OUTPUT,
	@NEOpExEdc real = NULL OUTPUT, @NEOpExEdc_QTR real = NULL OUTPUT, @NEOpExEdc_Avg real = NULL OUTPUT, @NEOpExEdc_Ytd real = NULL OUTPUT, 
	@NEOpEx real = NULL OUTPUT, @NEOpEx_QTR real = NULL OUTPUT, @NEOpEx_Avg real = NULL OUTPUT, @NEOpEx_Ytd real = NULL OUTPUT, 
	@OpExUEdc real = NULL OUTPUT, @OpExUEdc_QTR real = NULL OUTPUT, @OpExUedc_Avg real = NULL OUTPUT, @OpExUEdc_Ytd real = NULL OUTPUT, 
	@TAAdj real = NULL OUTPUT, @TAAdj_QTR real = NULL OUTPUT, @TAAdj_Avg real = NULL OUTPUT, @TAAdj_Ytd real = NULL OUTPUT,
	@EnergyCost real = NULL OUTPUT, @EnergyCost_QTR real = NULL OUTPUT, @EnergyCost_Avg real = NULL OUTPUT, @EnergyCost_Ytd real = NULL OUTPUT, 
	@TotCashOpEx real = NULL OUTPUT, @TotCashOpEx_QTR real = NULL OUTPUT, @TotCashOpEx_Avg real = NULL OUTPUT, @TotCashOpEx_Ytd real = NULL OUTPUT
	)
AS

SELECT @EII = NULL, @EII_QTR = NULL, @EII_Avg = NULL, @EII_Ytd = NULL, 
	@EnergyUseDay = NULL, @EnergyUseDay_QTR = NULL, @EnergyUseDay_Avg = NULL, @EnergyUseDay_Ytd = NULL, 
	@TotStdEnergy = NULL, @TotStdEnergy_QTR = NULL, @TotStdEnergy_Avg = NULL, @TotStdEnergy_Ytd = NULL, 
	@RefUtilPcnt = NULL, @RefUtilPcnt_QTR = NULL, @RefUtilPcnt_Avg = NULL, @RefUtilPcnt_Ytd = NULL, 
	@Edc = NULL, @Edc_QTR = NULL, @Edc_Avg = NULL, @Edc_Ytd = NULL, 
	@UEdc = NULL, @UEdc_QTR = NULL, @Uedc_Avg = NULL, @UEdc_Ytd = NULL, 
	@VEI = NULL, @VEI_QTR = NULL, @VEI_Avg = NULL, @VEI_Ytd = NULL, 
	@ReportLossGain = NULL, @ReportLossGain_QTR = NULL, @ReportLossGain_Avg = NULL, @ReportLossGain_Ytd = NULL,
	@EstGain = NULL, @EstGain_QTR = NULL, @EstGain_Avg = NULL, @EstGain_Ytd = NULL, 
	@OpAvail = NULL, @OpAvail_QTR = NULL, @OpAvail_Avg = NULL, @OpAvail_Ytd = NULL, 
	@MechUnavailTA = NULL, @MechUnavailTA_QTR = NULL, @MechUnavailTA_Avg = NULL, @MechUnavailTA_Ytd = NULL, 
	@NonTAUnavail = NULL, @NonTAUnavail_QTR = NULL, @NonTAUnavail_Avg = NULL, @NonTAUnavail_Ytd = NULL, 
	@RoutIndex = NULL, @RoutIndex_QTR = NULL, @RoutIndex_Avg = NULL, @RoutIndex_Ytd = NULL,
	@RoutCost = NULL, @RoutCost_QTR = NULL, @RoutCost_Avg = NULL, @RoutCost_Ytd = NULL, 
	@PersIndex = NULL, @PersIndex_QTR = NULL, @PersIndex_Avg = NULL, @PersIndex_Ytd = NULL, 
	@AnnTAWhr = NULL, @AnnTAWhr_QTR = NULL, @AnnTAWhr_Avg = NULL, @AnnTAWhr_Ytd = NULL,
	@NonTAWHr = NULL, @NonTAWHr_QTR = NULL, @NonTAWHr_Avg = NULL, @NonTAWHr_Ytd = NULL,
	@NEOpExEdc = NULL, @NEOpExEdc_QTR = NULL, @NEOpExEdc_Avg = NULL, @NEOpExEdc_Ytd = NULL, 
	@NEOpEx = NULL, @NEOpEx_QTR = NULL, @NEOpEx_Avg = NULL, @NEOpEx_Ytd = NULL, 
	@OpExUEdc = NULL, @OpExUEdc_QTR = NULL, @OpExUedc_Avg = NULL, @OpExUEdc_Ytd = NULL, 
	@TAAdj = NULL, @TAAdj_QTR = NULL, @TAAdj_Avg = NULL, @TAAdj_Ytd = NULL,
	@EnergyCost = NULL, @EnergyCost_QTR = NULL, @EnergyCost_Avg = NULL, @EnergyCost_Ytd = NULL, 
	@TotCashOpEx = NULL, @TotCashOpEx_QTR = NULL, @TotCashOpEx_Avg = NULL, @TotCashOpEx_Ytd = NULL

SET NOCOUNT ON
DECLARE @SubmissionID int, @NumDays real, @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @CalcsNeeded char(1)
SELECT @SubmissionID = SubmissionID , @NumDays = NumDays, @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @CalcsNeeded = CalcsNeeded
FROM Submissions WHERE RefineryID = @RefineryID AND PeriodYear = @PeriodYear AND PeriodMonth = @PeriodMonth AND DataSet = @DataSet

IF @SubmissionID IS NULL
	RETURN 1
ELSE IF @CalcsNeeded IS NOT NULL
	RETURN 2

DECLARE @Start3Mo smalldatetime, @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @Start3Mo = Start3Mo, @Start12Mo = Start12Mo, @StartYTD = StartYTD, @Start24Mo = Start24Mo
FROM dbo.GetPeriods(@SubmissionID)

IF DATEPART(yy, @Start3Mo) < 2010
	SET @Start3Mo = '12/31/2009'
IF DATEPART(yy, @Start12Mo) < 2010
	SET @Start12Mo = '12/31/2009'
	
DECLARE @SubListMonth dbo.SubmissionIDList, @SubList3Mo dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList12Mo dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListMonth SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @PeriodStart, @PeriodEnd)
INSERT @SubList3Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start3Mo, @PeriodEnd)
INSERT @SubListYTD SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @PeriodEnd)
INSERT @SubList12Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
INSERT @SubList24Mo SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)

SELECT @EII = m.EII, @EII_QTR = avg3mo.EII, @EII_Avg = avg12mo.EII, @EII_Ytd = avgYTD.EII, 
	@EnergyUseDay = m.EnergyUseDay/1000, @EnergyUseDay_QTR = avg3mo.EnergyUseDay/1000, @EnergyUseDay_Avg = avg12mo.EnergyUseDay/1000, @EnergyUseDay_Ytd = avgYTD.EnergyUseDay/1000, 
	@TotStdEnergy = m.TotStdEnergy/1000, @TotStdEnergy_QTR = avg3mo.TotStdEnergy/1000, @TotStdEnergy_Avg = avg12mo.TotStdEnergy/1000, @TotStdEnergy_Ytd = avgYTD.TotStdEnergy/1000, 
	@RefUtilPcnt = m.RefUtilPcnt, @RefUtilPcnt_QTR = avg3mo.RefUtilPcnt, @RefUtilPcnt_Avg = avg12mo.RefUtilPcnt, @RefUtilPcnt_Ytd = avgYTD.RefUtilPcnt, 
	@Edc = m.Edc/1000, @Edc_QTR = avg3mo.Edc/1000, @Edc_Avg = avg12mo.Edc/1000, @Edc_Ytd = avgYTD.Edc/1000, 
	@UEdc = m.UEdc/1000, @UEdc_QTR = avg3mo.UEdc/1000, @Uedc_Avg = avg12mo.UEdc/1000, @UEdc_Ytd = avgYTD.UEdc/1000, 
	@VEI = m.VEI, @VEI_QTR = avg3mo.VEI, @VEI_Avg = avg12mo.VEI, @VEI_Ytd = avgYTD.VEI, 
	@ReportLossGain = m.ReportLossGain, @ReportLossGain_QTR = avg3mo.ReportLossGain, @ReportLossGain_Avg = avg12mo.ReportLossGain, @ReportLossGain_Ytd = avgYTD.ReportLossGain, 
	@EstGain = m.EstGain, @EstGain_QTR = avg3mo.EstGain, @EstGain_Avg = avg12mo.EstGain, @EstGain_Ytd = avgYTD.EstGain, 
	@OpAvail = m.OpAvail, @OpAvail_QTR = avg3mo.OpAvail, @OpAvail_Avg = avg24mo.OpAvail, @OpAvail_Ytd = avgYTD.OpAvail, 
	@MechUnavailTA = m.MechUnavailTA, @MechUnavailTA_QTR = avg3mo.MechUnavailTA, @MechUnavailTA_Avg = avg24mo.MechUnavailTA, @MechUnavailTA_Ytd = avgYTD.MechUnavailTA, 
	@NonTAUnavail = m.MechUnavailRout + m.RegUnavail, @NonTAUnavail_QTR = avg3mo.MechUnavailRout + avg3mo.RegUnavail, @NonTAUnavail_Avg = avg24mo.MechUnavailRout + avg24mo.RegUnavail, @NonTAUnavail_Ytd = avgYTD.MechUnavailRout + avgYTD.RegUnavail, 
	@RoutIndex = m.RoutIndex, @RoutIndex_QTR = avg3mo.RoutIndex, @RoutIndex_Avg = mi24.RoutIndex, @RoutIndex_Ytd = avgYTD.RoutIndex,
	@RoutCost = m.RoutCost/1000, @RoutCost_QTR = avg3mo.RoutCost/1000, @RoutCost_Avg = mi24.RoutEffIndex*avg24mo.MaintEffDiv/100/1e6, @RoutCost_Ytd = avgYTD.RoutCost/1000, 
	@PersIndex = m.PersIndex, @PersIndex_QTR = avg3mo.PersIndex, @PersIndex_Avg = avg12mo.PersIndex, @PersIndex_Ytd = avgYTD.PersIndex, 
	@AnnTAWhr = m.MaintTAWHr_k, @AnnTAWhr_QTR = avg3mo.MaintTAWHr_k, @AnnTAWhr_Avg = avg12mo.MaintTAWHr_k, @AnnTAWhr_Ytd = avgYTD.MaintTAWHr_k,
	@NonTAWHr = m.TotNonTAWHr_k, @NonTAWHr_QTR = avg3mo.TotNonTAWHr_k, @NonTAWHr_Avg = avg12mo.TotNonTAWHr_k, @NonTAWHr_Ytd = avgYTD.TotNonTAWHr_k,
	@NEOpExEdc = m.NEOpExEdc, @NEOpExEdc_QTR = avg3mo.NEOpExEdc, @NEOpExEdc_Avg = avg12mo.NEOpExEdc, @NEOpExEdc_Ytd = avgYTD.NEOpExEdc, 
	@NEOpEx = m.NEOpEx/1000, @NEOpEx_QTR = avg3mo.NEOpEx/1000, @NEOpEx_Avg = avg12mo.NEOpEx/1000, @NEOpEx_Ytd = avgYTD.NEOpEx/1000, 
	@OpExUEdc = m.OpExUEdc, @OpExUEdc_QTR = avg3mo.OpExUEdc, @OpExUedc_Avg = avg12mo.OpExUEdc, @OpExUEdc_Ytd = avgYTD.OpExUEdc, 
	@TAAdj = m.AnnTACost/1000, @TAAdj_QTR = avg3mo.AnnTACost/1000, @TAAdj_Avg = avg12mo.AnnTACost/1000, @TAAdj_Ytd = avgYTD.AnnTACost/1000,
	@EnergyCost = m.EnergyCost/1000, @EnergyCost_QTR = avg3mo.EnergyCost/1000, @EnergyCost_Avg = avg12mo.EnergyCost/1000, @EnergyCost_Ytd = avgYTD.EnergyCost/1000, 
	@TotCashOpEx = m.TotCashOpEx/1000, @TotCashOpEx_QTR = avg3mo.TotCashOpEx/1000, @TotCashOpEx_Avg = avg12mo.TotCashOpEx/1000, @TotCashOpEx_Ytd = avgYTD.TotCashOpEx/1000
FROM dbo.SLProfileLiteKPIs(@SubListMonth, @FactorSet, @Scenario, @Currency, @UOM) m
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList3Mo, @FactorSet, @Scenario, @Currency, @UOM) avg3mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubListYTD, @FactorSet, @Scenario, @Currency, @UOM) avgYTD ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList12Mo, @FactorSet, @Scenario, @Currency, @UOM) avg12mo ON 1=1
LEFT JOIN dbo.SLProfileLiteKPIs(@SubList24Mo, @FactorSet, @Scenario, @Currency, @UOM) avg24mo ON 1=1
LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, @FactorSet, @Currency) mi24 ON 1=1

