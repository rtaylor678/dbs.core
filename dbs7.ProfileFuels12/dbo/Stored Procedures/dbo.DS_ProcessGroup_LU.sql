﻿CREATE PROC [dbo].[DS_ProcessGroup_LU]
	
AS

SELECT RTRIM(ProcessGroup) AS ProcessGroup, RTRIM(Description) AS GroupDesc, 
                        SortKey
                        FROM ProcessGroup_LU WHERE SortKey NOT IN (2,3)
                        ORDER BY SortKey
