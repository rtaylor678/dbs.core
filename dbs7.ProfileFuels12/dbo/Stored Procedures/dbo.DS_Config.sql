﻿CREATE PROC [dbo].[DS_Config]
	@RefineryID nvarchar(20)
AS

SELECT UnitID, C.SortKey, RTRIM(C.ProcessID) as ProcessID, 
                    RTRIM(ProcessType) as ProcessType, InServicePcnt, DesignFeedSulfur,
                    RTRIM(UnitName) as UnitName, RTRIM(ProcessGroup) as ProcessGroup, 
                    RptCap, RptStmCap, (CASE WHEN (UtilPcnt<> NULL) THEN 0.0 END) AS UtilPcnt , (CASE WHEN (StmUtilPcnt<> NULL) THEN 0.0 END) AS  StmUtilPcnt,C.EnergyPcnt 
                    FROM Config C, ProcessID_LU P
                     WHERE C.ProcessID = P.ProcessID AND C.ProcessID NOT IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND ProfileProcFacility = 'Y' AND 
                     SubmissionID = (SELECT TOP 1 SubmissionID From dbo.Submissions WHERE RefineryID = @RefineryID ORDER BY PeriodStart DESC) ORDER BY C.SortKey


