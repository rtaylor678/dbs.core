﻿
CREATE                       PROC [dbo].[spUnitCalcs](@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @NumDays int, @RefineryID char(6), @PeriodStart smalldatetime, @DataSet varchar(15)
SELECT @NumDays = NumDays, @RefineryID = RefineryID, @PeriodStart = PeriodStart, @DataSet = DataSet
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
--IF EXISTS (SELECT * FROM Config WHERE SubmissionID = @SubmissionID AND BlockOp IS NOT NULL)
--BEGIN
	--- Update ModePcnt
--END
DECLARE @AnnInputBbl float, @AnnCokeBbl float, @AnnElecConsMWH float,
	@AnnRSCRUDE_RAIL float, @AnnRSCRUDE_TT float, @AnnRSCRUDE_TB float, @AnnRSCRUDE_OMB float, @AnnRSCRUDE_BB float, 
	@AnnRSPROD_RAIL float, @AnnRSPROD_TT float, @AnnRSPROD_TB float, @AnnRSPROD_OMB float, @AnnRSPROD_BB float
SELECT @AnnInputBbl = AnnInputBbl, @AnnCokeBbl = AnnCokeBbl, @AnnElecConsMWH = AnnElecConsMWH,
	@AnnRSCRUDE_RAIL = AnnRSCRUDE_RAIL, @AnnRSCRUDE_TT = AnnRSCRUDE_TT, @AnnRSCRUDE_TB = AnnRSCRUDE_TB, @AnnRSCRUDE_OMB = AnnRSCRUDE_OMB, @AnnRSCRUDE_BB = AnnRSCRUDE_BB, 
	@AnnRSPROD_RAIL = AnnRSPROD_RAIL, @AnnRSPROD_TT = AnnRSPROD_TT, @AnnRSPROD_TB = AnnRSPROD_TB, @AnnRSPROD_OMB = AnnRSPROD_OMB, @AnnRSPROD_BB = AnnRSPROD_BB
FROM EdcStabilizers
WHERE SubmissionID = @SubmissionID 

DECLARE @RSOverrides TABLE (ProcessID varchar(8) NOT NULL, ProcessType varchar(4) NOT NULL, AnnBbl real NULL, BPD real NULL)
IF @AnnRSCRUDE_RAIL IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSCRUDE', 'RAIL', @AnnRSCRUDE_RAIL)
IF @AnnRSCRUDE_TT IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSCRUDE', 'TT', @AnnRSCRUDE_TT)
IF @AnnRSCRUDE_TB IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSCRUDE', 'TB', @AnnRSCRUDE_TB)
IF @AnnRSCRUDE_OMB IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSCRUDE', 'OMB', @AnnRSCRUDE_OMB)
IF @AnnRSCRUDE_BB IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSCRUDE', 'BB', @AnnRSCRUDE_BB)
IF @AnnRSPROD_RAIL IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSPROD', 'RAIL', @AnnRSPROD_RAIL)
IF @AnnRSPROD_TT IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSPROD', 'TT', @AnnRSPROD_TT)
IF @AnnRSPROD_TB IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSPROD', 'TB', @AnnRSPROD_TB)
IF @AnnRSPROD_OMB IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSPROD', 'OMB', @AnnRSPROD_OMB)
IF @AnnRSPROD_BB IS NOT NULL
	INSERT @RSOverrides (ProcessID, ProcessType, AnnBbl) VALUES ('RSPROD', 'BB', @AnnRSPROD_BB)
UPDATE @RSOverrides SET BPD = AnnBbl/365.0

DECLARE @NewUnitID int

-- Handle MFCapacity
UPDATE Config
SET MFCap = Cap * d.Ratio, StmCap = NULL, StmUtilPcnt = 0
FROM Config INNER JOIN SubmissionsAll s ON s.SubmissionID = Config.SubmissionID 
INNER JOIN UnitDefaults d ON d.RefineryID = s.RefineryID AND d.UnitID = config.UnitID AND s.PeriodStart >= d.EffDate AND s.PeriodStart < d.EffUntil
WHERE Config.SubmissionID = @SubmissionID AND ISNULL(Config.MFCap, 0) = 0 AND Config.ProcessID IN ('SDWAX','SOLVEX','LHYC','CDWAX','WDOIL','LHYFT','WHYFT','LACID','WACID')
UPDATE Config
SET MFUtilPcnt = Cap*UtilPcnt/MFCap
FROM Config INNER JOIN SubmissionsAll s ON s.SubmissionID = Config.SubmissionID 
WHERE Config.SubmissionID = @SubmissionID AND Config.MFCap > 0 AND Config.ProcessID IN ('SDWAX','SOLVEX','LHYC','CDWAX','WDOIL','LHYFT','WHYFT','LACID','WACID')

-- Load special calculated data for Factors
DELETE FROM Config
WHERE ProcessID IN ('TNK+BLND','OFFCOKE','ELECDIST','TNKSTD') AND SubmissionID = @SubmissionID

DECLARE @NextUnitID UnitID, @MaxConfigUnitID UnitID, @MaxRoutUnitID UnitID
SELECT @MaxConfigUnitID = MAX(UnitID) FROM Config WHERE SubmissionID = @SubmissionID
SELECT @MaxRoutUnitID = MAX(UnitID) FROM MaintRout WHERE SubmissionID = @SubmissionID
IF @MaxConfigUnitID IS NULL
	RETURN 1
IF @MaxConfigUnitID >= ISNULL(@MaxRoutUnitID,0)
	SELECT @NextUnitID = @MaxConfigUnitID + 1
ELSE
	SELECT @NextUnitID = @MaxRoutUnitID + 1
IF @NextUnitID < 91000
	SET @NewUnitID = 91001

-- Tankange & Blending Capacity (Cap excludes leased capacity, StmCap includes leased)
DECLARE @TankCap real, @TankCapRefOwn real, @TankCnt real, @TankCntRefOwn real, @TotInputBbl float, @TankInputBPD real, @TankRefOwnFraction real
IF EXISTS (SELECT * FROM Inventory WHERE SubmissionID = @SubmissionID)
	SELECT @TankCap = SUM(FuelsStorage), @TankCapRefOwn = SUM(FuelsStorage*(100-isnull(LeasedPcnt,0))/100)
		, @TankCnt = SUM(NumTank), @TankCntRefOwn = SUM(NumTank*(100-isnull(LeasedPcnt,0))/100)
	FROM Inventory WHERE SubmissionID = @SubmissionID
ELSE
	SELECT @TankCap = SUM(FuelsStorage), @TankCapRefOwn = SUM(FuelsStorage)
		, @TankCnt = SUM(NumTank), @TankCntRefOwn = SUM(NumTank)
	FROM dbo.StudyTankage WHERE RefineryID = @RefineryID AND DataSet = @DataSet AND EffDate <= @PeriodStart AND EffUntil > @PeriodStart

SELECT @TotInputBbl = TotInputBbl FROM MaterialTot WHERE SubmissionID = @SubmissionID
IF @TankCap = 0
	SELECT @TankRefOwnFraction = 1, @TankInputBPD = @TotInputBbl/@NumDays
ELSE BEGIN
	SELECT @TankRefOwnFraction = @TankCapRefOwn/@TankCap
	IF @TotInputBbl/@TankCap > 100 
		SELECT @TankInputBPD = 30*@TankCap/365.0
	ELSE
		SELECT @TankInputBPD = @TotInputBbl/@NumDays
END

-- Tankange & Blending Capacity (Cap excludes leased tanks, StmCap includes leased)
INSERT Config (SubmissionID, UnitID, ProcessID, ProcessType, SortKey, UnitName, Cap, StmCap, UtilPcnt, StmUtilPcnt, InServicePcnt, YearsOper, MHPerWeek)
SELECT @SubmissionID, @NextUnitID, ProcessID, 'TCAP', SortKey, 'Tankage Capacity', 
Cap = @TankCapRefOwn / 365.0, StmCap = @TankCap / 365.0, 
UtilPcnt = 100, 100, InServicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
FROM ProcessID_LU WHERE ProcessID = 'TNK+BLND'
SELECT @NextUnitID = @NextUnitID + 1
-- Tankange & Blending Number of Tanks (Cap excludes leased tanks, StmCap includes leased)
INSERT Config (SubmissionID, UnitID, ProcessID, ProcessType, SortKey, UnitName, Cap, StmCap, UtilPcnt, StmUtilPcnt, InServicePcnt, YearsOper, MHPerWeek)
SELECT @SubmissionID, @NextUnitID, ProcessID, 'TCNT', SortKey, 'Number of Tanks', 
Cap = @TankCntRefOwn, StmCap = @TankCnt, 
UtilPcnt = 100, 100, InServicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
FROM ProcessID_LU WHERE ProcessID = 'TNK+BLND'
SELECT @NextUnitID = @NextUnitID + 1
-- Tankage and Blending Throughput (Net Input Barrels/day) (Cap accounts for leased capacity, StmCap includes leased)
INSERT Config (SubmissionID, UnitID, ProcessID, ProcessType, SortKey, UnitName, Cap, StmCap, UtilPcnt, StmUtilPcnt, InServicePcnt, YearsOper, MHPerWeek)
SELECT @SubmissionID, @NextUnitID, ProcessID, 'TBPD', SortKey, 'Input Barrels/Day', 
Cap = @TankInputBPD*@TankRefOwnFraction, StmCap = @TankInputBPD, 
UtilPcnt = 100, 100, InServicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
FROM ProcessID_LU WHERE ProcessID = 'TNK+BLND'
SELECT @NextUnitID = @NextUnitID + 1

-- Coke handling
DECLARE @CokeBPD real
IF @AnnCokeBbl IS NOT NULL
	SET @CokeBPD = @AnnCokeBbl/365.0
ELSE 
	SELECT @CokeBPD = SUM(Bbl)/@NumDays FROM Yield WHERE SubmissionID = @SubmissionID AND Category = 'COKE'
IF @CokeBPD > 0
BEGIN
	SET @NewUnitID = NULL
	SELECT @NewUnitID = MIN(UnitID) FROM MaintRout WHERE SubmissionID = @SubmissionID AND ProcessID = 'OFFCOKE'
	IF @NewUnitID IS NULL
	BEGIN
		SELECT @NewUnitID = @NextUnitID
		SELECT @NextUnitID = @NextUnitID + 1
	END
	INSERT Config (SubmissionID, UnitID, ProcessID, SortKey, UnitName, Cap, UtilPcnt, InServicePcnt, YearsOper, MHPerWeek)
	SELECT @SubmissionID, @NewUnitID, ProcessID, SortKey, 'Coke Handling', Cap = @CokeBPD, UtilPcnt = 100, InservicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
	FROM ProcessID_LU
	WHERE ProcessID = 'OFFCOKE' 
END
-- Electrical distribution capacity
DECLARE @ElecDistCap real
IF @AnnElecConsMWH IS NOT NULL
	SET @ElecDistCap = @AnnElecConsMWH*1000/(365.0 * 24)
ELSE 
	SELECT @ElecDistCap = CASE WHEN TotPowerConsMWH > 0 THEN TotPowerConsMWH ELSE 0 END*1000/(@NumDays * 24) 
	FROM EnergyTot WHERE SubmissionID = @SubmissionID
IF @ElecDistCap > 0
BEGIN
	INSERT Config (SubmissionID, UnitID, ProcessID, SortKey, UnitName, Cap, UtilPcnt, InServicePcnt, YearsOper, MHPerWeek)
	SELECT @SubmissionID, @NextUnitID, ProcessID, SortKey, Description, Cap = @ElecDistCap, UtilPcnt = 100, InservicePcnt = 100, YearsOper = NULL, MHPerWk = NULL
	FROM ProcessID_LU WHERE ProcessID = 'ELECDIST'
	SELECT @NextUnitID = @NextUnitID + 1
END

UPDATE Config
SET 	AllocPcntOfCap = ISNULL(AllocPcntOfCap, 100),
	AllocPcntOfTime = ISNULL(AllocPcntOfTime, 100),
	InserviceCap = Cap*ISNULL(InservicePcnt, 0)/100,
	UtilCap = Cap*ISNULL(InservicePcnt,0)*ISNULL(UtilPcnt,0)/10000,
	ActualCap = Cap/(CASE WHEN ISNULL(AllocPcntOfCap, 100) > 0 THEN ISNULL(AllocPcntOfCap, 100)/100 ELSE NULL END),
	InserviceStmCap = StmCap*ISNULL(InservicePcnt, 0)/100,
	StmUtilCap = StmCap*ISNULL(InservicePcnt,0)*ISNULL(StmUtilPcnt,0)/10000,
	ActualStmCap = StmCap/(CASE WHEN ISNULL(AllocPcntOfCap, 100) > 0 THEN ISNULL(AllocPcntOfCap,100)/100 ELSE NULL END),
	InserviceMFCap = MFCap*ISNULL(InservicePcnt, 0)/100,
	MFUtilCap = MFCap*ISNULL(InservicePcnt,0)*ISNULL(MFUtilPcnt,0)/10000,
	ActualMFCap = MFCap/(CASE WHEN ISNULL(AllocPcntOfCap, 100) > 0 THEN ISNULL(AllocPcntOfCap,100)/100 ELSE NULL END),
	AllocUtilPcnt = UtilPcnt/(CASE WHEN ISNULL(AllocPcntOfTime, 100) > 0 THEN ISNULL(AllocPcntOfTime, 100)/100 ELSE NULL END),
	ModePcnt = ISNULL(ModePcnt, 100)
WHERE SubmissionID = @SubmissionID
UPDATE ConfigRS
SET 	Vessels = Throughput/CASE WHEN AvgSize > 0 THEN AvgSize ELSE NULL END,
	BPD = Throughput/@NumDays
WHERE SubmissionID = @SubmissionID
DELETE FROM ProcessTotCalc WHERE SubmissionID = @SubmissionID
INSERT INTO ProcessTotCalc(SubmissionID, ProcessID, NumUnits, Cap, UtilCap, StmCap, StmUtilCap, InServiceCap, InserviceStmCap, ActualCap, ActualStmCap, AllocPcntOfTime, MFCap, MFUtilCap, InserviceMFCap, ActualMFCap)
SELECT SubmissionID, ProcessID, COUNT(*), SUM(Cap), SUM(UtilCap), SUM(StmCap), SUM(StmUtilCap), SUM(InServiceCap), SUM(InserviceStmCap), SUM(ActualCap), SUM(ActualStmCap), SUM(AllocPcntOfTime*Cap)/SUM(Cap),
	SUM(MFCap), SUM(MFUtilCap), SUM(InserviceMFCap), SUM(ActualMFCap)
FROM Config
WHERE (BlockOp IS NULL OR BlockOp = 'BP') AND Cap > 0
AND SubmissionID = @SubmissionID
GROUP BY SubmissionID, ProcessID
DECLARE @RVLocFactor real, @InflFactor real
SELECT @RVLocFactor = RVLocFactor, @InflFactor = InflFactor
FROM SubmissionsAll s INNER JOIN RefineryFactors r ON r.RefineryID = s.RefineryID AND s.PeriodStart BETWEEN r.EffDate AND r.EffUntil
WHERE s.SubmissionID = @SubmissionID

DELETE FROM FactorCalc WHERE SubmissionID = @SubmissionID
DELETE FROM RefProcessGroupings WHERE SubmissionID = @SubmissionID

/* Calculate percent of hydrogen in feed for hydrogen generation units for standard Energy calculation */
/* Has to be here because spLoadFactorData that does the other updates to UnitFactorData happens before UtilCap is calculated */
UPDATE UnitFactorData
SET EIIFeedH2PcntProdH2 = CASE
			WHEN FeedRateGas > 0 THEN ISNULL(FeedH2,0)*FeedRateGas/CASE WHEN c.UtilCap > 0 THEN c.UtilCap END 
			ELSE ISNULL(FeedH2,0)/((100-ISNULL(FeedH2,0))*7.6 + ISNULL(FeedH2,0))*100
			END
FROM UnitFactorData INNER JOIN Config c ON c.SubmissionID = UnitFactorData.SubmissionID AND c.UnitID = UnitFactorData.UnitID AND c.ProcessID = 'HYG'
WHERE UnitFactorData.SubmissionID = @SubmissionID

DECLARE cFactorSets CURSOR LOCAL SCROLL
FOR
SELECT FactorSet, NEOpExEffMult, MaintEffMult, PersEffMult, MaintPersEffMult, NonMaintPersEffMult, CalcFCCCOBoiler, ProcessIncludesSets
FROM FactorSets WHERE Calculate = 'Y' AND RefineryType = dbo.GetRefineryType(@RefineryID)

DECLARE @FactorSet FactorSet, @NEOpExEffMult real, @MaintEffMult real, @PersEffMult real, @MaintPersEffMult real, @NonMaintPersEffMult real, @CalcFCCCOBoiler char(1), @ProcessIncludesSets varchar(15)
DECLARE @UnitID int, @ProcessID ProcessID, @ProcessType ProcessType, @Cap real, @UtilPcnt real, @Cap2 real, @UtilPcnt2 real, @InservicePcnt real, @UtilCap2 real, @AllocPcntOfTime real, @AllocPcntOfCap real, @ActualCap real, @ActualCap2 real, @ModePcnt real, @DesignFeedSulfur real
DECLARE @ProcessGroup [char](1), @ConvGroup [char](1), @IsProcessUnit [bit], @Operated [bit], @InProcessUtil [bit], @AncillaryUnit [bit], @IsProcessUnitSet char(1)
DECLARE @StdMult real, @UtilMult real, @PlantMult real, @NPMult real, @NPPlantMult real
DECLARE @FullEdc real, @FullUEdc real, @FullNEOpExEffDiv real, @FullMaintEffDiv real, @FullPersEffDiv real, @FullMaintPersEffDiv real, @FullNonMaintPersEffDiv real, @FullRV real
DECLARE @FullEdc2 real, @FullNEOpExEffDiv2 real, @FullMaintEffDiv2 real, @FullPersEffDiv2 real, @FullMaintPersEffDiv2 real, @FullNonMaintPersEffDiv2 real, @FullRV2 real
DECLARE @EdcFactor real, @EdcFactor2 real, @EdcNoMult real, @UEdcNoMult real, 
	@NonProratedPlantEdc real, @NonProratedEdc real, @PlantEdc real, 
	@RVSulfurFactor real, @RV real, @NonProratedRV real, @PlantRV real, @NonProratedPlantRV real,
	@NEOpExEffFactor real, @NEOpExEffFactor2 real, @NEOpExEffDiv real, @NonProratedNEOpExEffDiv real, @PlantNEOpExEffDiv real, @NonProratedPlantNEOpExEffDiv real,
	@MaintEffFactor real, @MaintEffFactor2 real, @MaintEffDiv real, @NonProratedMaintEffDiv real, @PlantMaintEffDiv real, @NonProratedPlantMaintEffDiv real,
	@PersEffFactor real, @PersEffFactor2 real, @PersEffDiv real, @NonProratedPersEffDiv real, @PlantPersEffDiv real, @NonProratedPlantPersEffDiv real,
	@MaintPersEffFactor real, @MaintPersEffFactor2 real, @MaintPersEffDiv real, @NonProratedMaintPersEffDiv real, @PlantMaintPersEffDiv real, @NonProratedPlantMaintPersEffDiv real,
	@NonMaintPersEffFactor real, @NonMaintPersEffFactor2 real, @NonMaintPersEffDiv real, @NonProratedNonMaintPersEffDiv real, @PlantNonMaintPersEffDiv real, @NonProratedPlantNonMaintPersEffDiv real
DECLARE @EdcConstant real, @EdcExponent real, @EdcMinCap real, @EdcMaxCap real,
	@NEOpExEffConstant real, @NEOpExEffExponent real, @NEOpExEffMinCap real, @NEOpExEffMaxCap real,
	@MaintEffConstant real, @MaintEffExponent real, @MaintEffMinCap real, @MaintEffMaxCap real,
	@PersEffConstant real, @PersEffExponent real, @PersEffMinCap real, @PersEffMaxCap real,
	@MaintPersEffConstant real, @MaintPersEffExponent real, @MaintPersEffMinCap real, @MaintPersEffMaxCap real,
	@NonMaintPersEffConstant real, @NonMaintPersEffExponent real, @NonMaintPersEffMinCap real, @NonMaintPersEffMaxCap real,
	@RVConstant real, @RVExponent real, @RVNomCap real, @SulfurAdj char(1), @BaseSulfur real,
	@MultGroup ProcessID, @UnitConvPcntFormula varchar(255)
DECLARE cUnits CURSOR LOCAL FAST_FORWARD 
FOR
SELECT UnitID, ProcessID, ProcessType, Cap, UtilPcnt, CASE WHEN MFCap > 0 THEN MFCap ELSE StmCap END, CASE WHEN MFCap > 0 THEN MFUtilPcnt ELSE StmUtilPcnt END, InServicePcnt, AllocPcntOfTime, AllocPcntOfCap, ActualCap, CASE WHEN MFCap > 0 THEN ActualMFCap ELSE ActualStmCap END, ModePcnt, DesignFeedSulfur
FROM Config WHERE SubmissionID = @SubmissionID
UNION
SELECT UnitID, ProcessID, ProcessType, Cap = ISNULL(BPD, 0), UtilPcnt = 100, StmCap = NULL, StmUtilPcnt = NULL, InservicePcnt = 100, AllocPcntOfTime = 100, AllocPcntOfCap = 100, ActualCap = ISNULL(BPD, 0), ActualStmCap = NULL, ModePcnt = 100, DesignFeedSulfur = NULL
FROM ConfigRS WHERE SubmissionID = @SubmissionID
UNION
SELECT -1*UnitID, ProcessID = 'FCCCOBLR', ProcessType = 'LGFB', Cap = FiredDutyKLBPerHr, UtilPcnt = 100, NULL, NULL, InservicePcnt = 100, AllocPcntOfTime = 100, AllocPcntOfCap = 100, ActualCap = FiredDutyKLBPerHr, NULL, ModePcnt = 100, DesignFeedSulfur = NULL
FROM (	SELECT c.UnitID, FiredDutyKLBPerHr = dbo.GetFCCCOBlrFiredCap(c.SubmissionID, c.UnitID)
		FROM Config c WHERE c.ProcessID = 'FCC' AND c.SubmissionID = @SubmissionID
	) COBlrs /*INNER JOIN FactorSets fs ON fs.CalcFCCCOBoiler = 'Y'*/

OPEN cFactorSets
OPEN cUnits
FETCH NEXT FROM cUnits INTO @UnitID, @ProcessID, @ProcessType, @Cap, @UtilPcnt, @Cap2, @UtilPcnt2, @InservicePcnt, @AllocPcntOfTime, @AllocPcntOfCap, @ActualCap, @ActualCap2, @ModePcnt, @DesignFeedSulfur
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @ProcessGroup = NULL, @ConvGroup = NULL, @IsProcessUnit = 0, @Operated = 1, @InProcessUtil = 0, @AncillaryUnit = 0
	SELECT @ProcessGroup = ProcessGroup, @ConvGroup = ConvGroup, @IsProcessUnitSet = IsProcessUnitSet
	FROM ProcessID_LU WHERE ProcessID = @ProcessID
	IF EXISTS (SELECT * FROM IdleUnits WHERE RefineryID = @RefineryID AND UnitID = @UnitID AND IdledDate <= @PeriodStart  AND (RestartDate IS NULL OR RestartDate < @PeriodStart))
		SET @Operated = 0

	-- Override Receipt/shipment volumes if provided in EdcStabilizers
	IF @ProcessID = 'RSCRUDE' OR @ProcessID = 'RSPROD'
	BEGIN
		IF EXISTS (SELECT * FROM @RSOverrides WHERE ProcessID = @ProcessID AND ProcessType = @ProcessType AND BPD IS NOT NULL)
		BEGIN
			SELECT @Cap = BPD FROM @RSOverrides WHERE ProcessID = @ProcessID AND ProcessType = @ProcessType
		END
		SELECT @ActualCap = @Cap
	END

	SELECT 	@NPPlantMult = (@AllocPcntOfCap/100)
	SELECT	@NPMult = @NPPlantMult * (@AllocPcntOfTime/100) 
	SELECT	@PlantMult = @NPPlantMult * (@InservicePcnt/100)
	SELECT	@StdMult = @NPMult * (@InservicePcnt/100) 
	SELECT	@UtilMult = @PlantMult * (@ModePcnt/100)
		
	FETCH FIRST FROM cFactorSets INTO @FactorSet, @NEOpExEffMult, @MaintEffMult, @PersEffMult, @MaintPersEffMult, @NonMaintPersEffMult, @CalcFCCCOBoiler, @ProcessIncludesSets
	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @IsProcessUnit = CASE WHEN CHARINDEX(@IsProcessUnitSet, @ProcessIncludesSets) > 0 THEN 1 ELSE 0 END
		SELECT @AncillaryUnit = CASE WHEN @ProcessGroup IN ('P','F','A','O','L','W') AND @IsProcessUnit = 0 THEN 1 ELSE 0 END
		SELECT @InProcessUtil = CASE WHEN @IsProcessUnit = 1 AND @Operated = 1 THEN 1 ELSE 0 END

		SELECT	@EdcConstant = NULL, @EdcExponent = NULL, @EdcMinCap = NULL, @EdcMaxCap = NULL, 
			@NEOpExEffConstant = NULL, @NEOpExEffExponent = NULL, @NEOpExEffMinCap = NULL, @NEOpExEffMaxCap = NULL,
			@MaintEffConstant = NULL, @MaintEffExponent = NULL, @MaintEffMinCap = NULL, @MaintEffMaxCap = NULL,
			@PersEffConstant = NULL, @PersEffExponent = NULL, @PersEffMinCap = NULL, @PersEffMaxCap = NULL,
			@MaintPersEffConstant = NULL, @MaintPersEffExponent = NULL, @MaintPersEffMinCap = NULL, @MaintPersEffMaxCap = NULL,
			@NonMaintPersEffConstant = NULL, @NonMaintPersEffExponent = NULL, @NonMaintPersEffMinCap = NULL, @NonMaintPersEffMaxCap = NULL,
			@RVConstant = NULL, @RVExponent = NULL, @RVNomCap = NULL, @SulfurAdj = NULL, @BaseSulfur = NULL,
			@MultGroup = NULL, @UnitConvPcntFormula = NULL
		SELECT	@EdcConstant = EdcConstant, @EdcExponent = EdcExponent, @EdcMinCap = EdcMinCap, @EdcMaxCap = EdcMaxCap, 
			@NEOpExEffConstant = NEOpExEffConstant, @NEOpExEffExponent = NEOpExEffExponent, @NEOpExEffMinCap = NEOpExEffMinCap, @NEOpExEffMaxCap = NEOpExEffMaxCap,
			@MaintEffConstant = MaintEffConstant, @MaintEffExponent = MaintEffExponent, @MaintEffMinCap = MaintEffMinCap, @MaintEffMaxCap = MaintEffMaxCap,
			@PersEffConstant = PersEffConstant, @PersEffExponent = PersEffExponent, @PersEffMinCap = PersEffMinCap, @PersEffMaxCap = PersEffMaxCap,
			@MaintPersEffConstant = MaintPersEffConstant, @MaintPersEffExponent = MaintPersEffExponent, @MaintPersEffMinCap = MaintPersEffMinCap, @MaintPersEffMaxCap = MaintPersEffMaxCap,
			@NonMaintPersEffConstant = NonMaintPersEffConstant, @NonMaintPersEffExponent = NonMaintPersEffExponent, @NonMaintPersEffMinCap = NonMaintPersEffMinCap, @NonMaintPersEffMaxCap = NonMaintPersEffMaxCap,
			@RVConstant = RVConstant, @RVExponent = RVExponent, @RVNomCap = RVNomCap, @SulfurAdj = SulfurAdj, @BaseSulfur = BaseSulfur,
			@MultGroup = MultGroup, @UnitConvPcntFormula = UnitConvPcntFormula
		FROM Factors 
		WHERE FactorSet = @FactorSet  AND CapType IN ('','E','F') 
			AND ((ProcessID = @ProcessID AND ProcessType = @ProcessType) OR (@ProcessID = 'FCCCOBLR' AND @CalcFCCCOBoiler = 'Y' AND ProcessID = 'STEAMGEN' AND ProcessType = @ProcessType))
		
		EXEC spCalcFactor @ActualCap, @EdcConstant, @EdcExponent, @EdcMinCap, @EdcMaxCap, @EdcFactor OUTPUT, @FullEdc OUTPUT
		SELECT @FullUEdc = @FullEdc*@UtilPcnt/100
		EXEC spCalcFactor @ActualCap, @NEOpExEffConstant, @NEOpExEffExponent, @NEOpExEffMinCap, @NEOpExEffMaxCap, @NEOpExEffFactor OUTPUT, @FullNEOpExEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @MaintEffConstant, @MaintEffExponent, @MaintEffMinCap, @MaintEffMaxCap, @MaintEffFactor OUTPUT, @FullMaintEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @PersEffConstant, @PersEffExponent, @PersEffMinCap, @PersEffMaxCap, @PersEffFactor OUTPUT, @FullPersEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @MaintPersEffConstant, @MaintPersEffExponent, @MaintPersEffMinCap, @MaintPersEffMaxCap, @MaintPersEffFactor OUTPUT, @FullMaintPersEffDiv OUTPUT
		EXEC spCalcFactor @ActualCap, @NonMaintPersEffConstant, @NonMaintPersEffExponent, @NonMaintPersEffMinCap, @NonMaintPersEffMaxCap, @NonMaintPersEffFactor OUTPUT, @FullNonMaintPersEffDiv OUTPUT
		SELECT @RVSulfurFactor = ISNULL(dbo.RVSulfurAdj(@DesignFeedSulfur, @BaseSulfur, @SulfurAdj), 1)
		SELECT @FullRV = @RVConstant * power(@ActualCap/@RVNomCap, @RVExponent)
		IF @ActualCap2 > 0
		BEGIN
			SELECT	@EdcConstant = NULL, @EdcExponent = NULL, @EdcMinCap = NULL, @EdcMaxCap = NULL, 
				@NEOpExEffConstant = NULL, @NEOpExEffExponent = NULL, @NEOpExEffMinCap = NULL, @NEOpExEffMaxCap = NULL,
				@MaintEffConstant = NULL, @MaintEffExponent = NULL, @MaintEffMinCap = NULL, @MaintEffMaxCap = NULL,
				@PersEffConstant = NULL, @PersEffExponent = NULL, @PersEffMinCap = NULL, @PersEffMaxCap = NULL,
				@MaintPersEffConstant = NULL, @MaintPersEffExponent = NULL, @MaintPersEffMinCap = NULL, @MaintPersEffMaxCap = NULL,
				@NonMaintPersEffConstant = NULL, @NonMaintPersEffExponent = NULL, @NonMaintPersEffMinCap = NULL, @NonMaintPersEffMaxCap = NULL,
				@RVConstant = NULL, @RVExponent = NULL, @RVNomCap = NULL, @SulfurAdj = NULL, @BaseSulfur = NULL,
				@MultGroup = NULL, @UnitConvPcntFormula = NULL
			SELECT	@EdcConstant = EdcConstant, @EdcExponent = EdcExponent, @EdcMinCap = EdcMinCap, @EdcMaxCap = EdcMaxCap, 
				@NEOpExEffConstant = NEOpExEffConstant, @NEOpExEffExponent = NEOpExEffExponent, @NEOpExEffMinCap = NEOpExEffMinCap, @NEOpExEffMaxCap = NEOpExEffMaxCap,
				@MaintEffConstant = MaintEffConstant, @MaintEffExponent = MaintEffExponent, @MaintEffMinCap = MaintEffMinCap, @MaintEffMaxCap = MaintEffMaxCap,
				@PersEffConstant = PersEffConstant, @PersEffExponent = PersEffExponent, @PersEffMinCap = PersEffMinCap, @PersEffMaxCap = PersEffMaxCap,
				@MaintPersEffConstant = MaintPersEffConstant, @MaintPersEffExponent = MaintPersEffExponent, @MaintPersEffMinCap = MaintPersEffMinCap, @MaintPersEffMaxCap = MaintPersEffMaxCap,
				@NonMaintPersEffConstant = NonMaintPersEffConstant, @NonMaintPersEffExponent = NonMaintPersEffExponent, @NonMaintPersEffMinCap = NonMaintPersEffMinCap, @NonMaintPersEffMaxCap = NonMaintPersEffMaxCap,
				@RVConstant = RVConstant, @RVExponent = RVExponent, @RVNomCap = RVNomCap, @SulfurAdj = SulfurAdj, @BaseSulfur = BaseSulfur,
				@MultGroup = MultGroup, @UnitConvPcntFormula = UnitConvPcntFormula
			FROM Factors WHERE FactorSet = @FactorSet AND ProcessID = @ProcessID AND ProcessType = @ProcessType AND CapType IN ('Stm','MF')
			
			EXEC spCalcFactor @ActualCap2, @EdcConstant, @EdcExponent, @EdcMinCap, @EdcMaxCap, @EdcFactor2 OUTPUT, @FullEdc2 OUTPUT
			EXEC spCalcFactor @ActualCap2, @NEOpExEffConstant, @NEOpExEffExponent, @NEOpExEffMinCap, @NEOpExEffMaxCap, @NEOpExEffFactor2 OUTPUT, @FullNEOpExEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualCap2, @MaintEffConstant, @MaintEffExponent, @MaintEffMinCap, @MaintEffMaxCap, @MaintEffFactor2 OUTPUT, @FullMaintEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualCap2, @PersEffConstant, @PersEffExponent, @PersEffMinCap, @PersEffMaxCap, @PersEffFactor2 OUTPUT, @FullPersEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualCap2, @MaintPersEffConstant, @MaintPersEffExponent, @MaintPersEffMinCap, @MaintPersEffMaxCap, @MaintPersEffFactor2 OUTPUT, @FullMaintPersEffDiv2 OUTPUT
			EXEC spCalcFactor @ActualCap2, @NonMaintPersEffConstant, @NonMaintPersEffExponent, @NonMaintPersEffMinCap, @NonMaintPersEffMaxCap, @NonMaintPersEffFactor2 OUTPUT, @FullNonMaintPersEffDiv2 OUTPUT
			SELECT @FullRV2 = ISNULL(@RVConstant * power(@ActualCap2/@RVNomCap, @RVExponent), 0)
			SELECT 	@FullEdc = @FullEdc + ISNULL(@FullEdc2, 0),
				@FullUEdc = @FullUEdc + ISNULL(@FullEdc2*@UtilPcnt2/100, 0),
				@FullNEOpExEffDiv = @FullNEOpExEffDiv + ISNULL(@FullNEOpExEffDiv2, 0),
				@FullMaintEffDiv = @FullMaintEffDiv + ISNULL(@FullMaintEffDiv2, 0),
				@FullPersEffDiv = @FullPersEffDiv + ISNULL(@FullPersEffDiv2, 0),
				@FullMaintPersEffDiv = @FullMaintPersEffDiv + ISNULL(@FullMaintPersEffDiv2, 0),
				@FullNonMaintPersEffDiv = @FullNonMaintPersEffDiv + ISNULL(@FullNonMaintPersEffDiv2, 0),
				@FullRV = @FullRV + ISNULL(@FullRV2, 0)
		END
		ELSE BEGIN
			SELECT 	@EdcFactor2 = NULL, @NEOpExEffFactor2 = NULL, @MaintEffFactor2 = NULL, @PersEffFactor2 = NULL, @MaintPersEffFactor2 = NULL, @NonMaintPersEffFactor2 = NULL, 
				@FullEdc2 = NULL, @FullNEOpExEffDiv2 = NULL, @FullMaintEffDiv2 = NULL, @FullPersEffDiv2 = NULL, @FullMaintPersEffDiv2 = NULL, @FullNonMaintPersEffDiv2 = NULL, @FullRV2 = NULL
		END
		SELECT	@NEOpExEffFactor = @NEOpExEffFactor * @NEOpExEffMult, @NEOpExEffFactor2 = @NEOpExEffFactor2 * @NEOpExEffMult, @FullNEOpExEffDiv = @FullNEOpExEffDiv * @NEOpExEffMult
		SELECT	@MaintEffFactor = @MaintEffFactor * @MaintEffMult, @MaintEffFactor2 = @MaintEffFactor2 * @MaintEffMult, @FullMaintEffDiv = @FullMaintEffDiv * @MaintEffMult
		SELECT	@PersEffFactor = @PersEffFactor * @PersEffMult, @PersEffFactor2 = @PersEffFactor2 * @PersEffMult, @FullPersEffDiv = @FullPersEffDiv * @PersEffMult
		SELECT	@MaintPersEffFactor = @MaintPersEffFactor * @MaintPersEffMult, @MaintPersEffFactor2 = @MaintPersEffFactor2 * @MaintPersEffMult, @FullMaintPersEffDiv = @FullMaintPersEffDiv * @MaintPersEffMult
		SELECT	@NonMaintPersEffFactor = @NonMaintPersEffFactor * @NonMaintPersEffMult, @NonMaintPersEffFactor2 = @NonMaintPersEffFactor2 * @NonMaintPersEffMult, @FullNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @NonMaintPersEffMult
		SELECT 	@EdcNoMult = @FullEdc*@StdMult, 
			@UEdcNoMult = @FullUEdc*@UtilMult, 
			@NonProratedPlantEdc = @FullEdc*@NPPlantMult, 
			@NonProratedEdc = @FullEdc*@NPMult, 
			@PlantEdc = @FullEdc*@PlantMult
		SELECT 	@NEOpExEffDiv = @FullNEOpExEffDiv*@StdMult,
			@NonProratedPlantNEOpExEffDiv = @FullNEOpExEffDiv*@NPPlantMult,
			@NonProratedNEOpExEffDiv = @FullNEOpExEffDiv*@NPMult,
			@PlantNEOpExEffDiv = @FullNEOpExEffDiv*@PlantMult
		SELECT 	@MaintEffDiv = @FullMaintEffDiv*@StdMult,
			@NonProratedPlantMaintEffDiv = @FullMaintEffDiv*@NPPlantMult,
			@NonProratedMaintEffDiv = @FullMaintEffDiv*@NPMult,
			@PlantMaintEffDiv = @FullMaintEffDiv*@PlantMult
		SELECT 	@PersEffDiv = @FullPersEffDiv*@StdMult,
			@NonProratedPlantPersEffDiv = @FullPersEffDiv * @NPPlantMult,
			@NonProratedPersEffDiv = @FullPersEffDiv * @NPMult,
			@PlantPersEffDiv = @FullPersEffDiv * @PlantMult
		SELECT 	@MaintPersEffDiv = @FullMaintPersEffDiv*@StdMult,
			@NonProratedPlantMaintPersEffDiv = @FullMaintPersEffDiv * @NPPlantMult,
			@NonProratedMaintPersEffDiv = @FullMaintPersEffDiv * @NPMult,
			@PlantMaintPersEffDiv = @FullMaintPersEffDiv * @PlantMult
		SELECT 	@NonMaintPersEffDiv = @FullNonMaintPersEffDiv*@StdMult,
			@NonProratedPlantNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @NPPlantMult,
			@NonProratedNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @NPMult,
			@PlantNonMaintPersEffDiv = @FullNonMaintPersEffDiv * @PlantMult
		SELECT 	@FullRV = @FullRV * @RVSulfurFactor * @RVLocFactor * @InflFactor
		SELECT 	@RV = @FullRV * @StdMult,
			@NonProratedRV = @FullRV * @NPMult,
			@PlantRV = @FullRV * @PlantMult,
			@NonProratedPlantRV = @FullRV * @NPPlantMult
		
		IF @FullPersEffDiv IS NULL AND @FullMaintPersEffDiv IS NOT NULL
			SELECT @FullPersEffDiv = @FullMaintPersEffDiv + @FullNonMaintPersEffDiv
				,	@PlantPersEffDiv = @PlantMaintPersEffDiv + @PlantNonMaintPersEffDiv
				,	@NonProratedPersEffDiv = @NonProratedMaintPersEffDiv + @NonProratedNonMaintPersEffDiv
				,	@NonProratedPlantPersEffDiv = @NonProratedPlantMaintPersEffDiv + @NonProratedPlantNonMaintPersEffDiv
				,	@PersEffDiv = @MaintPersEffDiv + @NonMaintPersEffDiv

		INSERT INTO FactorCalc (SubmissionID, FactorSet, UnitID, ProcessID, 
			EdcFactor, EdcFactor2, MultGroup, EdcNoMult, UEdcNoMult, UtilPcnt,
			NonProratedPlantEdc, NonProratedEdc, PlantEdc, 
			RVSulfurFactor, RV, NonProratedRV, PlantRV, NonProratedPlantRV,
			NEOpExEffFactor, NEOpExEffFactor2, NEOpExEffDiv, NonProratedNEOpExEffDiv, PlantNEOpExEffDiv, NonProratedPlantNEOpExEffDiv,
			MaintEffFactor, MaintEffFactor2, MaintEffDiv, NonProratedMaintEffDiv, PlantMaintEffDiv, NonProratedPlantMaintEffDiv,
			PersEffFactor, PersEffFactor2, PersEffDiv, NonProratedPersEffDiv, PlantPersEffDiv, NonProratedPlantPersEffDiv,
			MaintPersEffFactor, MaintPersEffFactor2, MaintPersEffDiv, NonProratedMaintPersEffDiv, PlantMaintPersEffDiv, NonProratedPlantMaintPersEffDiv,
			NonMaintPersEffFactor, NonMaintPersEffFactor2, NonMaintPersEffDiv, NonProratedNonMaintPersEffDiv, PlantNonMaintPersEffDiv, NonProratedPlantNonMaintPersEffDiv)
		SELECT	@SubmissionID, @FactorSet, @UnitID, @ProcessID, 
			@EdcFactor, @EdcFactor2, @MultGroup, @EdcNoMult, @UEdcNoMult, CASE WHEN @EdcNoMult = 0 THEN @UtilPcnt ELSE @UEdcNoMult/@EdcNoMult*100 END,
			@NonProratedPlantEdc, @NonProratedEdc, @PlantEdc,
			@RVSulfurFactor, @RV, @NonProratedRV, @PlantRV, @NonProratedPlantRV,
			@NEOpExEffFactor, @NEOpExEffFactor2, @NEOpExEffDiv, @NonProratedNEOpExEffDiv, @PlantNEOpExEffDiv, @NonProratedPlantNEOpExEffDiv,
			@MaintEffFactor, @MaintEffFactor2, @MaintEffDiv, @NonProratedMaintEffDiv, @PlantMaintEffDiv, @NonProratedPlantMaintEffDiv,
			@PersEffFactor, @PersEffFactor2, @PersEffDiv, @NonProratedPersEffDiv, @PlantPersEffDiv, @NonProratedPlantPersEffDiv,
			@MaintPersEffFactor, @MaintPersEffFactor2, @MaintPersEffDiv, @NonProratedMaintPersEffDiv, @PlantMaintPersEffDiv, @NonProratedPlantMaintPersEffDiv,
			@NonMaintPersEffFactor, @NonMaintPersEffFactor2, @NonMaintPersEffDiv, @NonProratedNonMaintPersEffDiv, @PlantNonMaintPersEffDiv, @NonProratedPlantNonMaintPersEffDiv
			
		INSERT RefProcessGroupings (SubmissionID, FactorSet, ProcessGrouping, UnitID)
		SELECT DISTINCT @SubmissionID, @FactorSet, ProcessGrouping = ISNULL(pg.ProcessGrouping, @ProcessID), ISNULL(@UnitID, -1)
		FROM ProcessGrouping_LU pg 
		WHERE (pg.IsProcessUnit IS NULL OR pg.IsProcessUnit = @IsProcessUnit) 
			AND (pg.Operated IS NULL OR pg.Operated = @Operated)
			AND (pg.ProcessGroup IS NULL OR pg.ProcessGroup = @ProcessGroup) 
			AND (pg.ConvGroup IS NULL OR pg.ConvGroup = @ConvGroup)
			AND (pg.AncillaryUnit IS NULL OR pg.AncillaryUnit = @AncillaryUnit)
	
		FETCH NEXT FROM cFactorSets INTO @FactorSet, @NEOpExEffMult, @MaintEffMult, @PersEffMult, @MaintPersEffMult, @NonMaintPersEffMult, @CalcFCCCOBoiler, @ProcessIncludesSets
	END
	FETCH NEXT FROM cUnits INTO @UnitID, @ProcessID, @ProcessType, @Cap, @UtilPcnt, @Cap2, @UtilPcnt2, @InservicePcnt, @AllocPcntOfTime, @AllocPcntOfCap, @ActualCap, @ActualCap2, @ModePcnt, @DesignFeedSulfur
END
CLOSE cUnits
DEALLOCATE cUnits
CLOSE cFactorSets
DEALLOCATE cFactorSets
DECLARE @UtilCap real, @EIIFormula varchar(255), @VEIFormula varchar(255), @EIIFormula2 varchar(255), @VEIFormula2 varchar(255)
DECLARE @strUnitID varchar(20), @strSubmissionID varchar(20)
DECLARE cFactorCalc CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
	SELECT 	c.UnitID, FactorCalc.FactorSet, c.UtilCap, CASE WHEN fs.CapType = 'MF' THEN c.MFUtilCap ELSE c.StmUtilCap END,
	EIIFormula = CASE WHEN c.ProcessID = 'ASP' THEN 'NULL' ELSE f.EIIFormula END, f.VEIFormula,
	EIIFormula = CASE WHEN c.ProcessID = 'ASP' THEN 'NULL' ELSE fs.EIIFormula END, fs.VEIFormula
	FROM FactorCalc INNER JOIN Config c ON c.SubmissionID = FactorCalc.SubmissionID AND c.UnitID = FactorCalc.UnitID
	INNER JOIN Factors f ON f.FactorSet = FactorCalc.FactorSet AND f.ProcessID = c.ProcessID AND f.ProcessType = c.ProcessType AND f.CapType IN ('','E','F')
	LEFT JOIN Factors fs ON fs.FactorSet = FactorCalc.FactorSet AND fs.ProcessID = c.ProcessID AND fs.ProcessType = c.ProcessType AND fs.CapType IN ('Stm','MF')
	WHERE FactorCalc.SubmissionID = @SubmissionID
OPEN cFactorCalc
FETCH NEXT FROM cFactorCalc INTO @UnitID, @FactorSet, @UtilCap, @UtilCap2, @EIIFormula, @VEIFormula, @EIIFormula2, @VEIFormula2
WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT 	@strUnitID = CAST(@UnitID AS varchar(20)), 
		@strSubmissionID = CAST(@SubmissionID AS varchar(20))
	SELECT @EIIFormula = CASE WHEN ISNULL(@EIIFormula, '') = '' THEN 'NULL' WHEN ISNULL(@UtilCap, 0) <= 0 THEN 'NULL' ELSE CAST(@UtilCap AS varchar(20)) + '*(' + @EIIFormula + ')/1000' END
		, @EIIFormula2 = CASE WHEN ISNULL(@EIIFormula2, '') = '' THEN 'NULL' WHEN ISNULL(@UtilCap2, 0) <= 0 THEN 'NULL' ELSE CAST(@UtilCap2 AS varchar(20)) + '*(' + @EIIFormula2 + ')/1000' END
		, @VEIFormula = CASE WHEN ISNULL(@VEIFormula, '') = '' THEN 'NULL' WHEN ISNULL(@UtilCap, 0) <= 0 THEN 'NULL' ELSE CAST(@UtilCap AS varchar(20)) + '*(' + @VEIFormula + ')' END
		, @VEIFormula2 = CASE WHEN ISNULL(@VEIFormula2, '') = '' THEN 'NULL' WHEN ISNULL(@UtilCap2, 0) <= 0 THEN 'NULL' ELSE CAST(@UtilCap2 AS varchar(20)) + '*(' + @VEIFormula2 + ')' END
	EXEC ('UPDATE FactorCalc SET StdEnergy = ISNULL(' + @EIIFormula + ', 0) + ISNULL(' + @EIIFormula2 + ',0), '
		+ ' StdGain = ISNULL(' + @VEIFormula + ',0) + ISNULL(' + @VEIFormula2 + ', 0)'
		+ ' FROM FactorCalc LEFT JOIN UnitFactorData u ON u.SubmissionID = FactorCalc.SubmissionID AND u.UnitID = FactorCalc.UnitID '
		+ ' WHERE FactorCalc.SubmissionID = ' + @strSubmissionID + ' AND FactorCalc.FactorSet = ''' + @FactorSet + ''''
		+ ' AND FactorCalc.UnitID = ' + @strUnitID)
	FETCH NEXT FROM cFactorCalc INTO @UnitID, @FactorSet, @UtilCap, @UtilCap2, @EIIFormula, @VEIFormula, @EIIFormula2, @VEIFormula2
END
CLOSE cFactorCalc
DEALLOCATE cFactorCalc

UPDATE fcc
SET EdcNoMult = fcc.EdcNoMult + ISNULL(blr.EdcNoMult,0), UEdcNoMult = fcc.UEdcNoMult + ISNULL(blr.UEdcNoMult,0), RV = fcc.RV + ISNULL(blr.RV,0), 
	NEOpExEffDiv = fcc.NEOpExEffDiv + ISNULL(blr.NEOpExEffDiv,0), MaintEffDiv = fcc.MaintEffDiv + ISNULL(blr.MaintEffDiv,0), PersEffDiv = fcc.PersEffDiv + ISNULL(blr.PersEffDiv,0), 
	NonMaintPersEffDiv = fcc.NonMaintPersEffDiv + ISNULL(blr.NonMaintPersEffDiv,0), MaintPersEffDiv = fcc.MaintPersEffDiv + ISNULL(blr.MaintPersEffDiv,0), 
	
	NonProratedEdc = fcc.NonProratedEdc + ISNULL(blr.NonProratedEdc,0), NonProratedRV = fcc.NonProratedRV + ISNULL(blr.NonProratedRV,0), NonProratedNEOpExEffDiv = fcc.NonProratedNEOpExEffDiv + ISNULL(blr.NonProratedNEOpExEffDiv,0), 
	NonProratedMaintEffDiv = fcc.NonProratedMaintEffDiv + ISNULL(blr.NonProratedMaintEffDiv,0), NonProratedPersEffDiv = fcc.NonProratedPersEffDiv + ISNULL(blr.NonProratedPersEffDiv,0), 
	NonProratedNonMaintPersEffDiv = fcc.NonProratedNonMaintPersEffDiv + ISNULL(blr.NonProratedNonMaintPersEffDiv,0), NonProratedMaintPersEffDiv = fcc.NonProratedMaintPersEffDiv + ISNULL(blr.NonProratedMaintPersEffDiv,0), 
	
	PlantEdc = fcc.PlantEdc + ISNULL(blr.PlantEdc,0), PlantRV = fcc.PlantRV + ISNULL(blr.PlantRV,0), PlantNEOpExEffDiv = fcc.PlantNEOpExEffDiv + ISNULL(blr.PlantNEOpExEffDiv,0), 
	PlantMaintEffDiv = fcc.PlantMaintEffDiv + ISNULL(blr.PlantMaintEffDiv,0), PlantPersEffDiv = fcc.PlantPersEffDiv + ISNULL(blr.PlantPersEffDiv,0), 
	PlantNonMaintPersEffDiv = fcc.PlantNonMaintPersEffDiv + ISNULL(blr.PlantNonMaintPersEffDiv,0), PlantMaintPersEffDiv = fcc.PlantMaintPersEffDiv + ISNULL(blr.PlantMaintPersEffDiv,0),
	
	NonProratedPlantEdc = fcc.NonProratedPlantEdc + ISNULL(blr.NonProratedPlantEdc,0), NonProratedPlantRV = fcc.NonProratedPlantRV + ISNULL(blr.NonProratedPlantRV,0), NonProratedPlantNEOpExEffDiv = fcc.NonProratedPlantNEOpExEffDiv + ISNULL(blr.NonProratedPlantNEOpExEffDiv,0), 
	NonProratedPlantMaintEffDiv = fcc.NonProratedPlantMaintEffDiv + ISNULL(blr.NonProratedPlantMaintEffDiv,0), NonProratedPlantPersEffDiv = fcc.NonProratedPlantPersEffDiv + ISNULL(blr.NonProratedPlantPersEffDiv,0), 
	NonProratedPlantNonMaintPersEffDiv = fcc.NonProratedPlantNonMaintPersEffDiv + ISNULL(blr.NonProratedPlantNonMaintPersEffDiv,0), NonProratedPlantMaintPersEffDiv = fcc.NonProratedPlantMaintPersEffDiv + ISNULL(blr.NonProratedPlantMaintPersEffDiv,0), 
	
	ConvEdc = fcc.ConvEdc + ISNULL(blr.ConvEdc,0), ConvUEdc = fcc.ConvUEdc + ISNULL(blr.ConvUEdc,0)
FROM FactorCalc fcc INNER JOIN FactorCalc blr ON blr.SubmissionID = fcc.SubmissionID AND blr.UnitID = -1*fcc.UnitID AND blr.ProcessID = 'FCCCOBLR' AND blr.FactorSet = fcc.FactorSet
WHERE fcc.SubmissionID = @SubmissionID AND fcc.ProcessID = 'FCC' AND blr.EdcNoMult IS NOT NULL

DELETE FROM RefProcessGroupings 
WHERE SubmissionID = @SubmissionID 
	AND UnitID IN (SELECT UnitID FROM FactorCalc WHERE SubmissionID = @SubmissionID AND ProcessID = 'FCCCOBLR')

DELETE FROM FactorCalc WHERE SubmissionID = @SubmissionID AND ProcessID = 'FCCCOBLR'

EXEC spFactorTotCalc @SubmissionID



