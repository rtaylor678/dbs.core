﻿CREATE               PROC [dbo].[spAverages] (@SubmissionID int)
AS
SET NOCOUNT ON
DECLARE @PeriodStart smalldatetime, @PeriodEnd smalldatetime, @RefineryID varchar(6), @DataSet varchar(15)
DECLARE @Start12Mo smalldatetime, @StartYTD smalldatetime, @Start24Mo smalldatetime
SELECT @PeriodStart = PeriodStart, @PeriodEnd = PeriodEnd, @RefineryID = RefineryID, @DataSet = DataSet
FROM SubmissionsAll WHERE SubmissionID = @SubmissionID
SELECT @Start12Mo = p.Start12Mo, @Start24Mo = p.Start24Mo, @StartYTD = p.StartYTD FROM dbo.GetPeriods(@SubmissionID) p

IF @RefineryID IN ('106FL','150FL','322EUR') AND DATEPART(yy, @Start12Mo) < 2011 AND DATEPART(yy, @PeriodStart) >= 2011
BEGIN
	SELECT @Start12Mo = '12/31/2010', @Start24Mo = '12/31/2010'
	IF @StartYTD < @Start12Mo
		SET @StartYTD = @Start12Mo
END

DECLARE @SubListAVG dbo.SubmissionIDList, @SubListYTD dbo.SubmissionIDList, @SubList24Mo dbo.SubmissionIDList
INSERT @SubListAVG(SubmissionID)
SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start12Mo, @PeriodEnd)
INSERT @SubListYTD(SubmissionID)
SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @StartYTD, @PeriodEnd)
INSERT @SubList24Mo(SubmissionID)
SELECT SubmissionID FROM dbo.GetPeriodSubmissions(@RefineryID, @DataSet, @Start24Mo, @PeriodEnd)

EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Started'

--EXEC spAverageMaint @SubmissionID
-- 24-month maintenance indexes
UPDATE MaintIndex
SET RoutIndex_Avg = c.RoutIndex, RoutMatlIndex_Avg = c.RoutMatlIndex
	, MaintIndex_Avg = c.MaintIndex, MaintMatlIndex_Avg = c.MaintMatlIndex
	, RoutEffIndex_Avg = c.RoutEffIndex, MaintEffIndex_Avg = c.MaintEffIndex
FROM MaintIndex LEFT JOIN dbo.SLMaintIndex(@SubList24Mo, 24, NULL, NULL) c ON c.FactorSet = MaintIndex.FactorSet AND c.Currency = MaintIndex.Currency
WHERE MaintIndex.SubmissionID = @SubmissionID

-- Update GenSum with 24-month maintenance indexes
UPDATE GenSum
SET RoutIndex_Avg = m.RoutIndex_Avg, TAIndex_Avg = m.TAIndex_Avg, MaintIndex_Avg = m.MaintIndex_Avg
	, MEI_Rout_Avg = m.RoutEffIndex_Avg, MEI_TA_Avg = m.TAEffIndex_Avg, MEI_Avg = m.MaintEffIndex_Avg
FROM GenSum LEFT JOIN MaintIndex m ON m.SubmissionID = GenSum.SubmissionID AND m.FactorSet = GenSum.FactorSet AND m.Currency = GenSum.Currency

-- Year-to-date maintenance indexes
UPDATE GenSum
SET RoutIndex_Ytd = c.RoutIndex, TAIndex_Ytd = c.TAIndex, MaintIndex_Ytd = c.MaintIndex
	, MEI_Rout_Ytd = c.RoutEffIndex, MEI_TA_Ytd = c.TAEffIndex, MEI_Ytd = c.MaintEffIndex
FROM GenSum LEFT JOIN dbo.SLMaintIndex(@SubListYTD, 0, NULL, NULL) c ON c.FactorSet = GenSum.FactorSet AND c.Currency = GenSum.Currency
WHERE GenSum.SubmissionID = @SubmissionID

-- Annualized maintenance costs by unit
DECLARE @UnitAnnRout TABLE(UnitID int NOT NULL, Currency varchar(4) NOT NULL, RoutCost real NULL, RoutMatl real NULL)
INSERT @UnitAnnRout(UnitID, Currency, RoutCost, RoutMatl)
SELECT m.UnitID, m.Currency, RoutCost = SUM(m.CurrRoutCost)/SUM(s.FractionOfYear), RoutMatl = SUM(m.CurrRoutMatl)/SUM(s.FractionOfYear)
FROM MaintCost m INNER JOIN @SubList24Mo sl ON sl.SubmissionID = m.SubmissionID
INNER JOIN Submissions s ON s.SubmissionID = sl.SubmissionID
GROUP BY s.RefineryID, s.DataSet, m.UnitID, m.Currency

UPDATE @UnitAnnRout SET RoutCost = 0 WHERE RoutCost < 0
UPDATE @UnitAnnRout SET RoutMatl = 0 WHERE RoutMatl < 0

UPDATE MaintCost
SET AnnRoutCost = ISNULL(r.RoutCost, 0), AnnRoutMatl = r.RoutMatl,
	AnnMaintCost = ISNULL(MaintCost.AnnTACost, 0) + ISNULL(r.RoutCost, 0),
	AnnMaintMatl = ISNULL(MaintCost.AnnTAMatl, 0) + ISNULL(r.RoutMatl, 0)
FROM MaintCost INNER JOIN @UnitAnnRout r ON r.UnitID = MaintCost.UnitID AND r.Currency = MaintCost.Currency
WHERE MaintCost.SubmissionID = @SubmissionID

IF EXISTS (SELECT * FROM Submissions WHERE SubmissionID = @SubmissionID AND CalcsNeeded = 'M')
BEGIN
	UPDATE Submissions SET CalcsNeeded = NULL
	WHERE SubmissionID = @SubmissionID AND CalcsNeeded = 'M'

	EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Maintenance Indexes updated'
END
ELSE BEGIN
--EXEC spAverageAvail @SubmissionID
UPDATE MaintCalc
SET PeriodHrs_Avg = a.PeriodHrs, PeriodHrsOSTA_Avg = a.PeriodHrsOSTA,
	MechUnavailTA_Act_Avg = a.MechUnavailTA_Act, MechAvailOSTA_Avg = a.MechAvailOSTA, 
	MechAvail_Act_Avg = a.MechAvail_Act, MechAvailSlow_Act_Avg = a.MechAvailSlow_Act, 
	OpAvail_Act_Avg = a.OpAvail_Act, OpAvailSlow_Act_Avg = a.OpAvailSlow_Act, 
	OnStream_Act_Avg = a.OnStream_Act, OnStreamSlow_Act_Avg = a.OnStreamSlow_Act,
	MechAvail_Ann_Avg = a.MechAvail_Ann, MechAvailSlow_Ann_Avg = a.MechAvailSlow_Ann, 
	OpAvail_Ann_Avg = a.OpAvail_Ann, OpAvailSlow_Ann_Avg = a.OpAvailSlow_Ann, 
	OnStream_Ann_Avg = a.OnStream_Ann, OnStreamSlow_Ann_Avg = a.OnStreamSlow_Ann
FROM MaintCalc LEFT JOIN dbo.SLUnitAvailability(@SubList24Mo) a ON a.UnitID = MaintCalc.UnitID AND a.FactorSet = dbo.GetCurrentFactorSet()
WHERE MaintCalc.SubmissionID = @SubmissionID

UPDATE MaintCalc
SET PeriodHrs_Ytd = a.PeriodHrs, PeriodHrsOSTA_Ytd = a.PeriodHrsOSTA,
	MechUnavailTA_Act_Ytd = a.MechUnavailTA_Act, MechAvailOSTA_Ytd = a.MechAvailOSTA, 
	MechAvail_Act_Ytd = a.MechAvail_Act, MechAvailSlow_Act_Ytd = a.MechAvailSlow_Act, 
	OpAvail_Act_Ytd = a.OpAvail_Act, OpAvailSlow_Act_Ytd = a.OpAvailSlow_Act, 
	OnStream_Act_Ytd = a.OnStream_Act, OnStreamSlow_Act_Ytd = a.OnStreamSlow_Act,
	MechAvail_Ann_Ytd = a.MechAvail_Ann, MechAvailSlow_Ann_Ytd = a.MechAvailSlow_Ann, 
	OpAvail_Ann_Ytd = a.OpAvail_Ann, OpAvailSlow_Ann_Ytd = a.OpAvailSlow_Ann, 
	OnStream_Ann_Ytd = a.OnStream_Ann, OnStreamSlow_Ann_Ytd = a.OnStreamSlow_Ann
FROM MaintCalc LEFT JOIN dbo.SLUnitAvailability(@SubListYTD) a ON a.UnitID = MaintCalc.UnitID AND a.FactorSet = dbo.GetCurrentFactorSet()
WHERE MaintCalc.SubmissionID = @SubmissionID

UPDATE MaintProcess
SET MechAvail_Ann_Avg=a.MechAvail_Ann, 
MechAvail_Act_Avg=a.MechAvail_Act, 
MechAvailSlow_Ann_Avg=a.MechAvailSlow_Ann, 
MechAvailSlow_Act_Avg=a.MechAvailSlow_Act, 
MechAvailOSTA_Avg=a.MechAvailOSTA, 
OpAvail_Ann_Avg=a.OpAvail_Ann, 
OpAvail_Act_Avg=a.OpAvail_Act, 
OpAvailSlow_Ann_Avg=a.OpAvailSlow_Ann, 
OpAvailSlow_Act_Avg=a.OpAvailSlow_Act, 
OnStream_Ann_Avg=a.OnStream_Ann, 
OnStream_Act_Avg=a.OnStream_Act, 
OnStreamSlow_Ann_Avg=a.OnStreamSlow_Ann, 
OnStreamSlow_Act_Avg=a.OnStreamSlow_Act, 
MechAvail_Ann_Ytd=y.MechAvail_Ann, 
MechAvail_Act_Ytd=y.MechAvail_Act, 
MechAvailSlow_Ann_Ytd=y.MechAvailSlow_Ann, 
MechAvailSlow_Act_Ytd=y.MechAvailSlow_Act, 
MechAvailOSTA_Ytd=y.MechAvailOSTA, 
OpAvail_Ann_Ytd=y.OpAvail_Ann, 
OpAvail_Act_Ytd=y.OpAvail_Act, 
OpAvailSlow_Ann_Ytd=y.OpAvailSlow_Ann, 
OpAvailSlow_Act_Ytd=y.OpAvailSlow_Act, 
OnStream_Ann_Ytd=y.OnStream_Ann, 
OnStream_Act_Ytd=y.OnStream_Act, 
OnStreamSlow_Ann_Ytd=y.OnStreamSlow_Ann, 
OnStreamSlow_Act_Ytd=y.OnStreamSlow_Act, 
MechAvail_Ann_Target=t.MechAvail_Ann_Target, 
MechAvail_Act_Target=t.MechAvail_Act_Target, 
MechAvailSlow_Ann_Target=t.MechAvailSlow_Ann_Target, 
MechAvailSlow_Act_Target=t.MechAvailSlow_Act_Target, 
OpAvail_Ann_Target=t.OpAvail_Ann_Target, 
OpAvail_Act_Target=t.OpAvail_Act_Target, 
OpAvailSlow_Ann_Target=t.OpAvailSlow_Ann_Target, 
OpAvailSlow_Act_Target=t.OpAvailSlow_Act_Target, 
OnStream_Ann_Target=t.OnStream_Ann_Target, 
OnStream_Act_Target=t.OnStream_Act_Target, 
OnStreamSlow_Ann_Target=t.OnStreamSlow_Ann_Target, 
OnStreamSlow_Act_Target=t.OnStreamSlow_Act_Target
FROM MaintProcess LEFT JOIN dbo.SLProcessAvailability(@SubList24Mo) a ON a.ProcessID = MaintProcess.ProcessID AND a.FactorSet = MaintProcess.FactorSet
LEFT JOIN dbo.SLProcessAvailability(@SubListYTD) y ON y.ProcessID = MaintProcess.ProcessID AND y.FactorSet = MaintProcess.FactorSet
LEFT JOIN dbo.CalcProcessAvailTargets(@SubmissionID) t ON t.ProcessID = MaintProcess.ProcessID
WHERE MaintProcess.SubmissionID = @SubmissionID

UPDATE MaintAvailCalc
SET MechAvail_Ann_Avg=p.MechAvail_Ann_Avg,MechAvailSlow_Ann_Avg=p.MechAvailSlow_Ann_Avg, 
OpAvail_Ann_Avg=p.OpAvail_Ann_Avg, OpAvailSlow_Ann_Avg=p.OpAvailSlow_Ann_Avg, 
OnStream_Ann_Avg=p.OnStream_Ann_Avg, OnStreamSlow_Ann_Avg=p.OnStreamSlow_Ann_Avg, 
MechAvail_Act_Avg=p.MechAvail_Act_Avg, MechAvailSlow_Act_Avg=p.MechAvailSlow_Act_Avg, 
OpAvail_Act_Avg=p.OpAvail_Act_Avg, OpAvailSlow_Act_Avg=p.OpAvailSlow_Act_Avg, 
OnStream_Act_Avg=p.OnStream_Act_Avg, OnStreamSlow_Act_Avg=p.OnStreamSlow_Act_Avg, 
MechAvailOSTA_Avg=p.MechAvailOSTA_Avg,
MechAvail_Ann_Ytd=p.MechAvail_Ann_Ytd, MechAvailSlow_Ann_Ytd=p.MechAvailSlow_Ann_Ytd, 
OpAvail_Ann_Ytd=p.OpAvail_Ann_Ytd, OpAvailSlow_Ann_Ytd=p.OpAvailSlow_Ann_Ytd, 
OnStream_Ann_Ytd=p.OnStream_Ann_Ytd, OnStreamSlow_Ann_Ytd=p.OnStreamSlow_Ann_Ytd, 
MechAvail_Act_Ytd=p.MechAvail_Act_Ytd, MechAvailSlow_Act_Ytd=p.MechAvailSlow_Act_Ytd, 
OpAvail_Act_Ytd=p.OpAvail_Act_Ytd, OpAvailSlow_Act_Ytd=p.OpAvailSlow_Act_Ytd, 
OnStream_Act_Ytd=p.OnStream_Act_Ytd, OnStreamSlow_Act_Ytd=p.OnStreamSlow_Act_Ytd, 
MechAvailOSTA_Ytd=p.MechAvailOSTA_Ytd
FROM MaintAvailCalc LEFT JOIN MaintProcess p ON p.SubmissionID = MaintAvailCalc.SubmissionID AND p.FactorSet = MaintAvailCalc.FactorSet
INNER JOIN FactorSets fs ON fs.FactorSet = MaintAvailCalc.FactorSet
WHERE MaintAvailCalc.SubmissionID = @SubmissionID AND p.ProcessID = CASE WHEN fs.IdleUnitsInProcessResults = 'Y' THEN 'TotProc' ELSE 'OperProc' END

UPDATE GenSum
SET MechAvail = m.MechAvail_Ann, MechAvail_Avg = m.MechAvail_Ann_Avg, MechAvail_Ytd = m.MechAvail_Ann_Ytd, 
/*MechAvailSlow = m.MechAvailSlow_Ann, MechAvailSlow_Avg = m.MechAvailSlow_Ann_Avg, MechAvailSlow_Ytd = m.MechAvailSlow_Ann_Ytd, */
OpAvail = m.OpAvail_Ann, OpAvail_Avg = m.OpAvail_Ann_Avg, OpAvail_Ytd = m.OpAvail_Ann_Ytd, 
/*OpAvailSlow = m.OpAvailSlow_Ann, OpAvailSlow_Avg = m.OpAvailSlow_Ann_Avg, OpAvailSlow_Ytd = m.OpAvailSlow_Ann_Ytd, */
OnStream = m.OnStream_Ann, OnStream_Avg = m.OnStream_Ann_Avg, OnStream_Ytd = m.OnStream_Ann_Ytd, 
OnStreamSlow = m.OnStreamSlow_Ann, OnStreamSlow_Avg = m.OnStreamSlow_Ann_Avg, OnStreamSlow_Ytd = m.OnStreamSlow_Ann_Ytd
FROM GenSum LEFT JOIN MaintAvailCalc m ON m.SubmissionID = GenSum.SubmissionID AND m.FactorSet = GenSum.FactorSet
WHERE GenSum.SubmissionID = @SubmissionID

-- Average Crude Gravity and Sulfur
DECLARE @CrudeAPI_Avg real, @CrudeAPI_Ytd real, @CrudeSulfur_Avg real, @CrudeSulfur_Ytd real
SELECT @CrudeAPI_Avg = Gravity, @CrudeSulfur_Avg = Sulfur FROM dbo.SLAverageCrude(@SubListAVG)
SELECT @CrudeAPI_Ytd = Gravity, @CrudeSulfur_Ytd = Sulfur FROM dbo.SLAverageCrude(@SubListYTD)

-- Average Net Input, kBPD and Gain, %
DECLARE @GainPcnt_Avg real, @GainPcnt_Ytd real, @NetInputBPD_Avg real, @NetInputBPD_Ytd real
SELECT @NetInputBPD_Avg = NetInputkBPD, @GainPcnt_Avg = GainPcnt FROM dbo.SLAverageYield(@SubListAVG)
SELECT @NetInputBPD_Ytd = NetInputkBPD, @GainPcnt_Ytd = GainPcnt FROM dbo.SLAverageYield(@SubListYTD)

-- Average Personnel percentages and ratios
DECLARE @MPSAbsPcnt_Avg real, @MPSAbsPcnt_Ytd real, @MPSOvtPcnt_Avg real, @MPSOvtPcnt_Ytd real
DECLARE @OCCAbsPcnt_Avg real, @OCCAbsPcnt_Ytd real, @OccoVtPcnt_Avg real, @OccoVtPcnt_Ytd real
DECLARE @MaintOCCMPSRatio_Avg real, @MaintOCCMPSRatio_Ytd real, @ProcoCcmpsRatio_Avg real, @ProcoCcmpsRatio_Ytd real
SELECT @OCCAbsPcnt_Avg = dbo.SLAvgOCCAbsPcnt(@SubListAVG), @MPSAbsPcnt_Avg = dbo.SLAvgMPSAbsPcnt(@SubListAVG)
	, @OccoVtPcnt_Avg = dbo.SLAvgOvertimePcnt(@SubListAVG, 'TO'), @MPSOvtPcnt_Avg = dbo.SLAvgOvertimePcnt(@SubListAVG, 'TM')
	, @ProcoCcmpsRatio_Avg = dbo.SLAvgOCCMPSRatio(@SubListAVG,'OO','MO'), @MaintOCCMPSRatio_Avg = dbo.SLAvgOCCMPSRatio(@SubListAVG,'OM','MM')
SELECT @OCCAbsPcnt_Ytd = dbo.SLAvgOCCAbsPcnt(@SubListYTD), @MPSAbsPcnt_Ytd = dbo.SLAvgMPSAbsPcnt(@SubListYTD)
	, @OccoVtPcnt_Ytd = dbo.SLAvgOvertimePcnt(@SubListYTD, 'TO'), @MPSOvtPcnt_Ytd = dbo.SLAvgOvertimePcnt(@SubListYTD, 'TM')
	, @ProcoCcmpsRatio_Ytd = dbo.SLAvgOCCMPSRatio(@SubListYTD,'OO','MO'), @MaintOCCMPSRatio_Ytd = dbo.SLAvgOCCMPSRatio(@SubListYTD,'OM','MM')

-- Energy Consumption per Bbl Input
DECLARE @KBTUPerBbl_Avg real, @MJPerBbl_Avg real, @KBTUPerBbl_Ytd real, @MJPerBbl_Ytd real
SELECT @KBTUPerBbl_Avg = dbo.SLAvgEnergyConsKBTUPerBbl(@SubListAVG)
	, @KBTUPerBbl_Ytd = dbo.SLAvgEnergyConsKBTUPerBbl(@SubListYTD)
SELECT @MJPerBbl_Avg = [$(GlobalDB)].dbo.UnitsConv(@KBTUPerBbl_Avg, 'KBTU/B','MJ/B')
	, @MJPerBbl_Ytd = [$(GlobalDB)].dbo.UnitsConv(@KBTUPerBbl_Ytd, 'KBTU/B','MJ/B')

-- Update GenSum with these simple averages that are independent of UOM, Currency and FactorSet
UPDATE GenSum
SET CrudeAPI_Avg = @CrudeAPI_Avg, CrudeAPI_Ytd = @CrudeAPI_Ytd, CrudeSulfur_Avg = @CrudeSulfur_Avg, CrudeSulfur_Ytd = @CrudeSulfur_Ytd,
GainPcnt_Avg = @GainPcnt_Avg, GainPcnt_Ytd = @GainPcnt_Ytd, NetInputBPD_Avg = @NetInputBPD_Avg, NetInputBPD_Ytd = @NetInputBPD_Ytd,
MPSAbsPcnt_Avg = @MPSAbsPcnt_Avg, MPSAbsPcnt_Ytd = @MPSAbsPcnt_Ytd, MPSOvtPcnt_Avg = @MPSOvtPcnt_Avg, MPSOvtPcnt_Ytd = @MPSOvtPcnt_Ytd,
OCCAbsPcnt_Avg = @OCCAbsPcnt_Avg, OCCAbsPcnt_Ytd = @OCCAbsPcnt_Ytd, OccoVtPcnt_Avg = @OccoVtPcnt_Avg, OccoVtPcnt_Ytd = @OccoVtPcnt_Ytd,
MaintOCCMPSRatio_Avg = @MaintOCCMPSRatio_Avg, ProcoCcmpsRatio_Avg = @ProcoCcmpsRatio_Avg, 
MaintOCCMPSRatio_Ytd = @MaintOCCMPSRatio_Ytd, ProcoCcmpsRatio_Ytd = @ProcoCcmpsRatio_Ytd,
EnergyConsPerBbl_Avg = CASE UOM WHEN 'US' THEN @KBTUPerBbl_Avg WHEN 'MET' THEN @MJPerBbl_Avg END, 
EnergyConsPerBbl_Ytd = CASE UOM WHEN 'US' THEN @KBTUPerBbl_Ytd WHEN 'MET' THEN @MJPerBbl_Ytd END
WHERE SubmissionID = @SubmissionID
-- Averages for variables that are a function of FactorSet only
DECLARE @FactorSet FactorSet
	UPDATE GenSum 
	SET  Edc_Avg = favg.AvgEdc, Uedc_Avg = favg.AvgUEdc, EII_Avg = favg.EII, VEI_Avg = favg.VEI, UtilPcnt_Avg = favg.UtilPcnt, UtilOSTA_Avg = favg.UtilOSTA, ProcessUtilPcnt_Avg = favg.ProcessUtilPcnt,
	     Edc_Ytd = fytd.AvgEdc, UEdc_Ytd = fytd.AvgUEdc, EII_Ytd = favg.EII, VEI_Ytd = favg.VEI, UtilPcnt_Ytd = favg.UtilPcnt, UtilOSTA_Ytd = favg.UtilOSTA, ProcessUtilPcnt_Ytd = favg.ProcessUtilPcnt,
	     OCCWHrEdc_Avg = pavg.OCCWHrEdc, MpsWhrEdc_Avg = pavg.MpsWhrEdc, TotWhrEdc_Avg = pavg.TotWHrEdc,
	     OCCWHrEdc_Ytd = pytd.OCCWHrEdc, MpsWhrEdc_Ytd = pytd.MpsWhrEdc, TotWhrEdc_Ytd = pytd.TotWHrEdc,
	     OCCEqPEdc_Avg = NULL, MPSEqPEdc_Avg = NULL, TotEqPEdc_Avg = NULL,
	     OCCEqPEdc_Ytd = NULL, MPSEqPEdc_Ytd = NULL, TotEqPEdc_Ytd = NULL,
	     TotMaintForceWHrEdc_Avg = pavg.TotMaintForceWHrEdc, TotMaintForceWHrEdc_Ytd = pytd.TotMaintForceWHrEdc,
	     PEI_Avg = pavg.PEI, MaintPEI_Avg = pavg.MaintPEI, NonMaintPEI_Avg = pavg.NonMaintPEI, 
	     PEI_Ytd = pytd.PEI, MaintPEI_Ytd = pytd.MaintPEI, NonMaintPEI_Ytd = pytd.NonMaintPEI
	FROM GenSum LEFT JOIN dbo.SLAverageFactors(@SubListAVG, NULL) favg ON favg.FactorSet = GenSum.FactorSet
		LEFT JOIN dbo.SLAverageFactors(@SubListYTD, NULL) fytd ON fytd.FactorSet = GenSum.FactorSet
		LEFT JOIN dbo.SLAveragePersKPIs(@SubListAVG, NULL) pavg ON pavg.FactorSet = GenSum.FactorSet
		LEFT JOIN dbo.SLAveragePersKPIs(@SubListYTD, NULL) pytd ON pytd.FactorSet = GenSum.FactorSet
	WHERE GenSum.SubmissionID = @SubmissionID 
	
-- Average CEI
UPDATE CEI2008
SET CEI_Avg = dbo.SLAverageCEI(@SubListAVG, FactorSet)
, CEI_Ytd = dbo.SLAverageCEI(@SubListYTD, FactorSet)
WHERE SubmissionID = @SubmissionID

-- Average Energy Costs
UPDATE GenSum
SET EnergyCost_Avg = CASE UOM WHEN 'US' THEN a.TotCostMBTU WHEN 'MET' THEN a.TotCostGJ END
	, EnergyCost_Prod_Avg = CASE UOM WHEN 'US' THEN a.ProdCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
	, EnergyCost_Pur_Avg = CASE UOM WHEN 'US' THEN a.PurCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
FROM GenSum INNER JOIN dbo.SLAverageEnergyCost(@SubListAVG) a ON a.Currency = GenSum.Currency
WHERE GenSum.SubmissionID = @SubmissionID

UPDATE GenSum
SET EnergyCost_Ytd = CASE UOM WHEN 'US' THEN a.TotCostMBTU WHEN 'MET' THEN a.TotCostGJ END
	, EnergyCost_Prod_Ytd = CASE UOM WHEN 'US' THEN a.ProdCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
	, EnergyCost_Pur_Ytd = CASE UOM WHEN 'US' THEN a.PurCostMBTU WHEN 'MET' THEN a.ProdCostGJ END
FROM GenSum INNER JOIN dbo.SLAverageEnergyCost(@SubListYTD) a ON a.Currency = GenSum.Currency
WHERE GenSum.SubmissionID = @SubmissionID 

-- Average OpEx KPIs
UPDATE GenSum
SET TotCashOpExUedc_Avg = o.TotCashOpExUEdc, VolOpExUedc_Avg = o.VolOpExUEdc, NonVolOpExUedc_Avg = o.NonVolOpExUEdc
    , NEOpExUedc_Avg = o.NEOpExUEdc, NEOpExEdc_Avg = o.NEOpExEdc, NEI_Avg = o.NEI
    , TotCashOpExBbl_Avg = o.TotCashOpExBbl
	, TotCashOpExUEdc_Ytd = y.TotCashOpExUEdc, VolOpExUEdc_Ytd = y.VolOpExUEdc, NonVolOpExUEdc_Ytd = y.NonVolOpExUEdc 
    , NEOpExUEdc_Ytd = y.NEOpExUEdc, NEOpExEdc_Ytd = y.NEOpExEdc, NEI_Ytd = y.NEI
    , TotCashOpExBbl_Ytd = y.TotCashOpExBbl
FROM GenSum LEFT JOIN [dbo].[SLAverageOpEx](@SubListAVG, NULL, 'CLIENT', NULL) o ON o.FactorSet = GenSum.FactorSet AND o.Currency = GenSum.Currency
	LEFT JOIN [dbo].[SLAverageOpEx](@SubListYTD, NULL, 'CLIENT', NULL) y ON y.FactorSet = GenSum.FactorSet AND y.Currency = GenSum.Currency
WHERE GenSum.SubmissionID = @SubmissionID

-- Average Margins
UPDATE GenSum
SET GPV_Avg = a.GPV, RMC_Avg = a.RMC, GrossMargin_Avg = a.GrossMargin, CashMargin_Avg = a.GrossMargin + a.OthRev - GenSum.TotCashOpExBbl_Avg
   ,GPV_Ytd = y.GPV, RMC_Ytd = y.RMC, GrossMargin_Ytd = y.GrossMargin, CashMargin_Ytd = y.GrossMargin + y.OthRev - GenSum.TotCashOpExBbl_Ytd
FROM GenSum LEFT JOIN dbo.SLAverageGrossMargin(@SubListAVG) a ON GenSum.Scenario = a.Scenario AND GenSum.Currency = a.Currency
	LEFT JOIN dbo.SLAverageGrossMargin(@SubListYTD) y ON GenSum.Scenario = y.Scenario AND GenSum.Currency = y.Currency
WHERE GenSum.SubmissionID = @SubmissionID

UPDATE GenSum
SET ROI_Avg = r.ROI, RV_Avg = r.RV, TotCptl_Avg = r.TotCptl
FROM GenSum LEFT JOIN dbo.SLAverageMarginROI(@SubListAVG, NULL, NULL, NULL) r ON r.FactorSet = GenSum.FactorSet AND r.Currency = GenSum.Currency AND r.Scenario = GenSum.Scenario
WHERE GenSum.SubmissionID = @SubmissionID 

UPDATE GenSum
SET ROI_Ytd = r.ROI, RV_Ytd = r.RV, TotCptl_Ytd = r.TotCptl
FROM GenSum LEFT JOIN dbo.SLAverageMarginROI(@SubListYTD, NULL, NULL, NULL) r ON r.FactorSet = GenSum.FactorSet AND r.Currency = GenSum.Currency AND r.Scenario = GenSum.Scenario
WHERE GenSum.SubmissionID = @SubmissionID 

UPDATE Submissions SET CalcsNeeded = NULL
WHERE SubmissionID = @SubmissionID 
AND CalcsNeeded IN ('A','M')

EXEC spLogMessage @SubmissionID = @SubmissionID, @Source = 'spAverages', @MessageText = 'Completed'

END


