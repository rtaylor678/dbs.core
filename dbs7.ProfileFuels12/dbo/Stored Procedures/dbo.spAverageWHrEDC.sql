﻿CREATE     PROC [dbo].[spAverageWHrEdc](@RefineryID varchar(6), @DataSet varchar(15), @PeriodStart smalldatetime, @PeriodEnd smalldatetime, 
	@FactorSet FactorSet, @OCCWHrEdc real OUTPUT, @MpsWhrEdc real OUTPUT, @TotWHrEdc real OUTPUT, @TotMaintForceWHrEdc real OUTPUT, 
	@PEI real OUTPUT, @MaintPEI real OUTPUT, @NonMaintPEI real OUTPUT)
AS
SELECT p.SectionID, TotWHrEdc = SUM(p.TotWHrEdc*p.WHrEdcDivisor)/SUM(p.WHrEdcDivisor), 
	PEI = SUM(p.TotWHrEffIndex*p.EffDivisor)/SUM(CASE WHEN p.EffDivisor = 0 THEN NULL ELSE p.EffDivisor END)
INTO #calc
FROM PersSTCalc p 
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd
AND p.SectionID IN ('TO','TM','TP')
GROUP BY p.SectionID
HAVING SUM(p.WHrEdcDivisor) > 0

SELECT 	@OCCWHrEdc = AVG(CASE WHEN SectionID = 'TO' THEN TotWHrEdc END),
	@MpsWhrEdc = AVG(CASE WHEN SectionID = 'TM' THEN TotWHrEdc END),
	@TotWHrEdc = AVG(CASE WHEN SectionID = 'TP' THEN TotWHrEdc END),
	@PEI = AVG(CASE WHEN SectionID = 'TP' THEN PEI END)
FROM #calc

SELECT @TotMaintForceWHrEdc = SUM(p.TotMaintForceWHrEdc*p.WHrEdcDivisor)/SUM(p.WHrEdcDivisor),
@MaintPEI = SUM(CASE WHEN p.MaintPEIDivisor > 0 THEN p.MaintPEI*p.MaintPEIDivisor END)/SUM(CASE WHEN p.MaintPEIDivisor > 0 THEN p.MaintPEIDivisor END),
@NonMaintPEI = SUM(CASE WHEN p.NonMaintPEIDivisor > 0 THEN p.NonMaintPEI*p.NonMaintPEIDivisor END)/SUM(CASE WHEN p.NonMaintPEIDivisor > 0 THEN p.NonMaintPEIDivisor END)
FROM PersTotCalc p
INNER JOIN Submissions s ON s.SubmissionID = p.SubmissionID 
WHERE s.RefineryID = @RefineryID AND s.DataSet = @DataSet AND p.FactorSet = @FactorSet
AND s.PeriodStart >= @PeriodStart AND s.PeriodStart < @PeriodEnd AND p.Currency = 'USD'

