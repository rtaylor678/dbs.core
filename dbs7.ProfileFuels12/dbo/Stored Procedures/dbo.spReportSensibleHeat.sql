﻿CREATE   PROC [dbo].[spReportSensibleHeat](@SubmissionID int = NULL, @RefineryID char(6) = NULL, @PeriodYear smallint = NULL, @PeriodMonth tinyint = NULL, @FactorSet FactorSet, @TotBbl float OUTPUT, @BPD real OUTPUT)
AS
IF @SubmissionID IS NULL AND @RefineryID IS NULL
BEGIN
	RAISERROR ('You must supply either a submission id or a refinery id.', 16, 1)
	RETURN
END

SET NOCOUNT ON

SELECT s.SubmissionID, shc.CriteriaNum, SUM(C.Cap) TotCap
INTO #shcrit
FROM Submissions s, Config C, SensHeatCriteria shc
WHERE s.SubmissionID = c.SubmissionID AND C.ProcessID = shc.ProcessID 
AND (C.ProcessType = shc.ProcessType OR shc.ProcessType = '')
AND s.SubmissionID = ISNULL(@SubmissionID, s.SubmissionID) AND s.RefineryID = ISNULL(@RefineryID, s.RefineryID) AND PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth)
AND (@SubmissionID IS NULL OR s.UseSubmission = 1)
GROUP BY s.SubmissionID, shc.CriteriaNum

-- Sensible Heat
SELECT s.SubmissionID, s.Location, s.PeriodStart, Y.SortKey, Y.MaterialID, Y.MaterialName, Y.Bbl, BPD = Y.Bbl/s.NumDays
INTO #SensHeat
FROM Submissions s, Yield Y, Material_LU L
WHERE Y.SubmissionID = s.SubmissionID AND Category IN ('RMI', 'OTHRM')
AND Y.MaterialID = L.MaterialID AND (( L.SensHeatCriteria IS NOT NULL
AND (L.SensHeatCriteria = 0 OR EXISTS (SELECT * FROM #shcrit c WHERE c.SubmissionID = s.SubmissionID AND c.CriteriaNum = L.SensHeatCriteria AND c.TotCap > 0))
) or (y.MaterialID = 'CRD' AND @FactorSet In ('2002','2004')))
AND s.SubmissionID = ISNULL(@SubmissionID, s.SubmissionID) AND s.RefineryID = ISNULL(@RefineryID, s.RefineryID) AND PeriodYear = ISNULL(@PeriodYear, s.PeriodYear) AND PeriodMonth = ISNULL(@PeriodMonth, s.PeriodMonth)
AND (@SubmissionID IS NULL OR s.UseSubmission = 1)

IF (SELECT COUNT(DISTINCT SubmissionID) FROM #SensHeat) = 1
	SELECT @TotBbl = SUM(Bbl), @BPD = SUM(BPD)
	FROM #SensHeat
ELSE
	SELECT @TotBbl = NULL, @BPD = NULL

SET NOCOUNT OFF

SELECT Location, PeriodStart, SortKey, MaterialID, MaterialName, Bbl, BPD
FROM #SensHeat
WHERE Bbl <> 0
ORDER BY PeriodStart DESC, SortKey

DROP TABLE #shcrit
DROP TABLE #SensHeat

