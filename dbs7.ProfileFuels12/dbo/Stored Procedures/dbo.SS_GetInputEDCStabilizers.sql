﻿CREATE PROC [dbo].[SS_GetInputEdcStabilizers]
	@RefineryID nvarchar(10)
AS

SELECT TOP 1 s.RefineryID, i.EffDate, i.AnnInputBbl,
            i.AnnCokeBbl,i.AnnElecConsMWH,i.AnnRSCRUDE_RAIL,i.AnnRSCRUDE_TT,
            i.AnnRSCRUDE_TB,i.AnnRSCRUDE_OMB,i.AnnRSCRUDE_BB,i.AnnRSPROD_RAIL,
            i.AnnRSCRUDE_TT,i.AnnRSCRUDE_TB,i.AnnRSCRUDE_OMB,i.AnnRSPROD_BB
            FROM dbo.LoadEdcStabilizers i INNER JOIN dbo.Submissions s ON s.SubmissionID = i.SubmissionID
            WHERE s.RefineryID=@RefineryID AND s.UseSubmission=1
            ORDER BY s.PeriodStart DESC

