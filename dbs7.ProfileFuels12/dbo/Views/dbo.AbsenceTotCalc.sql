﻿


CREATE VIEW [dbo].[AbsenceTotCalc] AS
SELECT a.SubmissionID, e.FactorSet, a.OCCAbs, a.MPSAbs, a.TotAbs,
a.OCCPcnt, a.MPSPcnt, a.TotPcnt,
OCCAbsEdc = a.OCCAbs/(e.Edc/100*s.FractionOfYear), 
MPSAbsEdc = a.MPSAbs/(e.Edc/100*s.FractionOfYear), 
TotAbsEdc = a.TotAbs/(e.Edc/100*s.FractionOfYear)
FROM AbsenceTot a INNER JOIN FactorTotCalc e ON e.SubmissionID = a.SubmissionID
INNER JOIN dbo.SubmissionsAll s ON s.SubmissionID = a.SubmissionID


